<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'use_path_style_endpoint' => env('AWS_USE_PATH_STYLE_ENDPOINT', false),
        ],

        'samba' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR'),
        ],

        'samba-tmp' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/tmp',
        ],

        'samba_sync' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/sincronizacion',
        ],

        'samba_media' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/media',
        ],

        'samba_media_uploads' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/media/img/uploads',
        ],

        'samba_guias' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/media/docs/guias',
        ],

        'samba_tickets' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/media/docs/tickets',
        ],

        'media' =>[
            'driver' => 'local',
            'root'   => realpath(dirname(__DIR__).'/../media.lancetahg.com.mx/'),
        ],

        'samba_clientes' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/datos/cliente',
            'url'    => env('APP_URL').'/storage/samba/datos/cliente',
            'visibility' => 'private',
            'permissions' => [
                'file' => [
                    'public' => 0664,
                    'private' => 0600,
                ],
                'dir' => [
                    'public' => 0775,
                    'private' => 0700,
                ],
            ],
        ],
        'samba-solicitudes-cd' =>[
            'driver' => 'local',
            'root'   => env('SAMBA_DIR').'/datos/solicitudes-cd',
            'url'    => env('APP_URL').'/storage/samba/datos/solicitudes-cd',
            'visibility' => 'private',
            'permissions' => [
                'file' => [
                    'public' => 0664,
                    'private' => 0600,
                ],
                'dir' => [
                    'public' => 0775,
                    'private' => 0700,
                ],
            ],
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
