<div class="heading-container">
    <h1 style="font-size: 20px;">{{ $subject }}</h1>
    <div class="alert alert-info">
        <h2>{!! $titulo !!}</h2>
    </div>
    <div class="contenido">
        {!! $mensaje->estadisticas_ini  ?? '' !!} {!! $mensaje->tabla_inicial ?? '' !!}
        {!! $mensaje->estadisticas_fin  ?? '' !!} {!! $mensaje->tabla_final ?? '' !!}
    </div>
</div>