<div class="heading-container">
    <h1 style="font-size: 16px;">{{ $data->titulo ?? '' }}</h1>
    @isset($data->alert)
    <div class="alert alert-info">
        <h2>{!! $data->alert !!}</h2>
    </div>        
    @endisset
    <div class="contenido">
        {!! $data->mensaje  ?? '' !!}
    </div>
</div>