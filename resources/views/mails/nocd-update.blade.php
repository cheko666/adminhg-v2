<style>
    table#headerMessage .logo-container img {
        max-width: 280px;
        margin-bottom: 30px;
    }    
    .nocd-container {
        padding: 10px;
        text-align: left;
        font-weight: bold;
        background-color: #d0ddfa;
    }
</style>
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="headerMessage" class="datos">
    <tr>
        <td align="center">
            <div class="logo-container">
                <img src="https://www.lancetahg.com.mx/img/store-logo-default-xs-1496332314.png" alt="">
            </div>
        </td>
    </tr>
</table>
<div class="heading-container">
    <h1 style="font-size: 20px;">{{ $data->subtitulo ?? '' }}</h1>
    <div class="contenido">
        <div class="alert alert-success">
            <p>Buen día {{ $data->scd->titulo_nombre_formatted ?? '' }}</p>
            <p>Su solicitud de Tarjeta de Cliente Distinguido ha sido confirmada.</p>
            <div class="nocd-container">No. de Cliente Distinguido: <strong>{{ $data->scd->no_cd }}</strong></div>
        </div>
        <p>Para hacer válido su descuento le pedimos:</p>
        <ul>
            <li>Presentarse al mostrador de cualquiera de nuestras sucursales
            <li>Indique que recibió notificación con su número de Cliente Distinguido.</li>
            <li>El jefe de tienda validará en sistema su registro.</li>
            <li>Se le entregará el plástico físicamente.</li>
            <li>y ¡listo!, Ya puede hacer uso de su descuento en cualquiera de nuestras tiendas o Callcenter.</li>
        </ul>        
        <div class="alert alert-warning"><strong>Muy importante:</strong> Cada vez que acuda a alguna de las tiendas de Lanceta HG a realizar una compra, será necesario que la presente en caja antes de hacer el pago para que se le pueda otorgar el descuento.</div>
    </div>
</div>