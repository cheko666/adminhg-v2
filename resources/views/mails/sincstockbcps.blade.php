<div class="heading-container">
    <h1 style="font-size: 20px;">{{ $subject }}</h1>
    <div class="alert alert-info">
        <h2>{!! $titulo !!}</h2>
    </div>
    <div class="contenido">
        {!! $mensaje->cuerpo_mensaje  ?? '' !!}
    </div>
</div>