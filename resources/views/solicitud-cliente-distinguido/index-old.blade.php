@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('content_header')
<h1 class="alert alert-warning">{{ $title }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="">
            <table id="dt-index" class="table table-striped">
                <thead>
                <tr>
                    <th width="60" data-filter="text">ID</th>
                    <th width="120" data-filter="text">R.F.C.</th>
                    <th width="300" data-filter="text">Nombre</th>
                    <th width="250" data-filter="text">Email</th>
                    <th width="80" data-filter="text">Teléfono</th>
                    <th width="40">C.P.</th>
                    <th width="120" data-filter="text">Fecha Registro</th>
                    <th width="150">Acción</th>
                </tr>
                </thead>
            </table>
        </div>

    </div><!--row-->

    <div class="modal fade" id="Modal"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content form-content">

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('css')
<style>
    #dt-index th input {
        width: 90%;
    }
</style>
@stop

@section('plugins.Datatables', true)

@section('js')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url()->full() }}';

        $(document).ready( function () {
            $(function (){

                var dtable = $('#dt-index').DataTable({
                    "language":{ "url": "{{ asset('/vendor/datatables/plugins/es-MX.json') }}" },
                    dom:
                    "<'row'"+
                    "<'col-md-2'l>"+
                    "<'col-md-7'B>"+
                    "<'col-md-3 text-right'f>"+
                    ">"+
                    "<'row dt-table'"+
                    "<'col-md-12'tr>"+
                    ">"+
                    "<'row'"+
                    "<'col-md-5'i>"+
                    "<'col-md-7 text-right'p>"+
                    ">",
                    columnDefs: [
                        {"className": "dt-center", "targets": [0,1,2,3]}
                    ],
                    processing: true,
                    serverSide: false,
                    // colReorder: true,
                    responsive: true,
                    lengthMenu: [10,30,60],
//                    pageLength: 20,
//                    scrollCollapse: true,
//                    scrollY:"420px",
                    initComplete: function(settings) {
                        var columns = $('#dt-index thead th[data-filter="select"]');
                        // Apply the search
                        this.api()
                                .columns(columns)
                                .every(function () {
                                    var column = this;
                                    var select = $('<select><option value="">Selecc.</option></select>')
                                            .appendTo( $('#dt-index thead tr:eq(1) th[data-filter="select"]').empty() )
                                            .on( 'change', function () {
                                                var val = $.fn.dataTable.util.escapeRegex(
                                                        $(this).val()
                                                );

                                                column.search( val ? '^'+val+'$' : '', true, false ).draw();
                                            } );
                                    column.data().unique().sort().each( function ( d, j ) {
                                        select.append( '<option value="'+d+'">'+d+'</option>' );
                                    } );
                                });

                        $('#'+settings.nTable.id+'_filter input')
                                .wrap('<div class="d-inline-flex position-relative"></div>')
                                .after('<button type="button" class="close position-absolute" aria-label="Close" style="right:5px"><span aria-hidden="true">&times;</span></button>')
                                .attr('required','required').attr('title','Search');

                        // Click Event on Clear button
                        $(document).on('click', '#'+settings.nTable.id+'_filter button', function(){
                            $('#'+settings.nTable.id).DataTable({
                                "retrieve": true,
                            }).search('').draw(); // reDraw table
                        });
                    },
                    ajax: "{!! route('solicitud-cliente-distinguido.index-old') !!}",
                    columns: [
                        {data:'id', name:'id'},
                        {data:'rfc',name:'rfc'},
                        {data:'nombre',name:'nombre'},
                        {data:'mail',name:'mail'},
                        {data:'telefono',name:'telefono'},
                        {data:'cod_postal',name:'cod_postal'},
                        {data:'fecha_registro',name:'fecha_registro'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]

                });

                $('#dt-index thead tr').clone(true).appendTo($('#dt-index thead'));
                $('#dt-index thead tr:eq(1) th[data-filter="text"]').each( function () {
                    var title = $(this).text();
                    $(this).html( '<input style="width:90% !important" type="text" placeholder="'+title+'" />' );
                });

                $('#dt-index thead tr:eq(1) th[data-filter="false"]').html('');

                $('#dt-index thead tr:eq(1) th').each( function (i) {
                    $( 'input', this ).on( 'keyup', function () {
                        if ( dtable.column(i).search() !== this.value ) {
                            dtable
                            .column(i)
                            .search( this.value )
                            .draw();
                        }
                    });
                });
                var state = dtable.state.loaded();
                if (state) {
                    var columns = $('#dt-index thead th[data-filter="text"]');
                    dtable.columns(columns).eq(0).each(function (colIdx) {
                        var colSearch = state.columns[colIdx].search;

                        if (colSearch.search) {
                            $('#dt-index thead tr:eq(1) th:eq('+colIdx+') input').val(colSearch.search);

                        }
                    });

                    dtable.draw();
                }

            });

        });

        $('#Modal').on('shown.bs.modal', function (event) {
//            $('#content').html('<div class="loading"><img src="images/loader.gif" alt="loading" /><br/>Un momento, por favor...</div>');

            var button = $(event.relatedTarget) // Button that triggered the modal
            var this_id = button.attr('data-id');
            var model = button.attr('data-model');
            var url = uri + '/' + this_id;

            $.ajax({
                url: url,
                cache: false,
                type: 'GET',
                data: {_token: token},
                success: function (data) {
                    setTimeout(function()
                    {
                        $("fa-spinner").fadeOut("slow");
                    },2000);
                    $('.modal-content').html(data.view);

                }
            });                

        });

        $('#Modal').on('hidden.bs.modal', function (event) {
            $('.modal-content').html('');
        });
    </script>
@endsection