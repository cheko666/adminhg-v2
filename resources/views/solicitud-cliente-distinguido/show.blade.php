@extends('layouts.iframe')

@section('title')  @stop

@section('content_header')
{{-- <h1 class="text-center"></h1> --}}
@stop

@section('adminlte_css')
<style>
    div.content {
        width: 100%;
    }
    .content .inner {
        width: 600px;
        margin: 0 auto;
        padding:20px;
        color:#686969;
        font-size:13px;
        line-height:18px;
        font-family:Open Sans, Arial, sans-serif;
    }
    table#headerMessage .logo-container img {
        max-width: 280px;
        margin-bottom: 30px;
    }
    @media (max-width: 990px) {
        .content .inner {
            width: 100%;
        }
    }
    .header {
        /** Extra personal styles **/
        /*background-color: red;*/
        text-align: center;
        width: 100%;
        padding-bottom: 0;
    }
    .header img.logo {
        width: 260px;
        padding-top: 20px;
    }
    .content {
        font-family:Open Sans, Arial, sans-serif;
        width: 100%;
        /*background-color: blue;*/
        top:0;
    }
    .footer {
        font-family:Open Sans, Arial, sans-serif;
        height: 35px;
        /*background-color: green;*/
        text-align: center;
        font-size: 9px;
        color: #666666;
        padding-top: 9px;
    }
    table.datos h1 {
        font-size:18px;
        font-weight: bold;
        margin:0 auto 7px;
    }
    table.datos h2,
    table.datos h3 {
        font-size: 15px;
        margin: 0;
        text-align:left;
        color:#999999;
    }
    table.documentacion td {
        text-align: center;
        padding: 10px;
    }
    table.documentacion td .documento {
        padding: 0 0 15px;
        display: block;
    }
    table.documentacion td .documento > div {
        display: block;
        text-align: center;
        padding: 10px;
        background-color: #f4f4f4;
    }
    table.documentacion td .documento > img {
        max-height: 80px;
    }
    table.documentacion td .documento > p {
        margin-bottom: 10px;
    }
    table.documentacion td .documento i.fa.fa-download {
        margin-top: 10px;
        display: none;
    }
    table.documentacion td .documento:hover > i.fa.fa-download {
        display: block;
    }
    span.nombre-campo {
        font-weight:bold;
        color:#00567D;
        margin: 2px 0;
        display: inline-block;
        line-height: 1.2;
    }
    table.documentacion td .documento span.nombre-campo {
        margin: 0 0 5px;
    }
    #Modal .modal-dialog {
        max-width: 600px;
    }
    .info-box .info-box-text {
        white-space: normal;
    }
    .info-box-number {
        font-size: 1.3rem;
        line-height: 1.3;
    }
    .info-box-number .sub-info-box {
        font-weight: normal;
    }
    .info-box-content ul li {
        line-height: 1.3;
        padding: 2px 0;
    }
</style>
@endsection

@section('content')
<div class="card card-outline card-info" style="margin-top:10px; box-sizing: content-box; width: 100%;">

    <div class="card-body">

            @if(!$data->to_cliente && !$data->is_email)
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 10px;right: 20px;">
                <span aria-hidden="true">&times;</span>
            </button>
            @endif
            
            @include('solicitud-cliente-distinguido._part-show')

    </div>
 
</div>

@endsection