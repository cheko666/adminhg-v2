<table border="0" cellpadding="0" cellspacing="0" width="100%" id="headerMessage" class="datos">
    <tr>
        <td align="center">
            <div class="logo-container">
                <img src="https://www.lancetahg.com.mx/img/store-logo-default-xs-1496332314.png" alt="">
            </div>
        </td>
    </tr>
</table>

@include('solicitud-cliente-distinguido._part-show-tocliente')

<table border="0" cellpadding="0" cellspacing="0" width="100%" id="mainContent" class="datos">
    <tr>
        <td align="left" valign="center" >
            <table style="width:100%;">
                <tr>
                    <td valign="top" align="left" style="text-align:left;vertical-align:top;">
                        <table style="width:100%">
                            <tr>
                                <td>
                                    <span><strong>Solicitud #{{ $data->scd->reference ?? '' }}</strong></span>
                                </td>
                                <td align="right" valign="top" style="padding: 0;">
                                    <strong><p style="color: #686969;font-family: Arial,sans-serif;font-size: 12px; margin: 0 !important;">Fecha de Registro: {{ \Illuminate\Support\Carbon::parse($data->scd->created_at)->format('d-m-Y H:m:s') }}</p></strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><h3>Datos Generales</h3></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Especialidad Médica:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->especialidad_formatted  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Nombre:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->titulo_nombre_formatted ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">CURP:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->curp  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Fecha de Nacimiento:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->fecha_nacimiento_formatted  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Teléfono:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->telefono  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Email:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->email  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><h3>Datos de Dirección</h3></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Dirección:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->direccion  ?? 'N/A' }}</span></td>
                            </tr>

                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Colonia o población:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->colonia  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Alcaldía o municipio:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->municipio  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Estado:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->estado_formatted  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:40%;  vertical-align:top;"><span class="nombre-campo">Código Postal:</span></td>
                                <td valign="top" style="width:60%; vertical-align:top;"><span>{{ $data->scd->codigo_postal  ?? 'N/A' }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><h3>Documentación</h3></td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <table width="100%" class="documentacion">
                                        <tr>
                                            <td style="width: 50%;">
                                                <div class="documento">
                                                    <span class="nombre-campo">Identificación Oficial:</span>
                                                    <div>
                                                        <img src="{!! $data->docs['ine'] !!}" alt="{{$data->scd->ife}}" title="{{$data->scd->ife}}">
                                                    </div>
                                                    <span>{{ \Str::limit($data->scd->ine,30) ?? 'N/A' }}</span>
                                                </div>

                                            </td>
                                            <td style="width: 50%;">
                                                <div class="documento">
                                                    <span class="nombre-campo">Comprobante de Domicilio:</span>
                                                    <div>
                                                        <img src="{!! $data->docs['comprobante'] !!}" alt="{{ $data->scd->comprobante }}" title="{{ $data->scd->comprobante }}">
                                                    </div>
                                                    <span>{{ \Str::limit($data->scd->comprobante,30) ?? 'N/A' }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            @if(!$data->to_cliente)
                            <tr>
                                <td colspan="2">
                                    <a href="{{ route('solicitud-cliente-distinguido.download',[$data->scd->reference]) }}" class="btn btn-sm btn-info"><i class="fa fa-download fa-2x"></i> Descargar documentación</a>
                                </td>
                            </tr>
                            @endif

                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>