<style>
    .header {
        /** Extra personal styles **/
        /*background-color: red;*/
        text-align: center;
        width: 100%;
        padding-bottom: 0;
    }
    .header img.logo {
        width: 260px;
        padding-top: 20px;
    }
    .content {
        font-family:Open Sans, Arial, sans-serif;
        width: 100%;
        /*background-color: blue;*/
        top:0;
    }
    .footer {
        font-family:Open Sans, Arial, sans-serif;
        height: 35px;
        /*background-color: green;*/
        text-align: center;
        font-size: 9px;
        color: #666666;
        padding-top: 9px;
    }
    table.datos h1 {
        font-size:18px;
        font-weight: bold;
        margin:0 auto 7px;
    }
    table.datos h2,
    table.datos h3 {
        font-size: 15px;
        margin: 0;
        text-align:left;
        color:#999999;
    }
    table.documentacion td {
        text-align: center;
        padding: 10px;
    }
    table.documentacion td .documento {
        padding: 15px 0;
        display: block;
    }
    table.documentacion td .documento > div {
        display: block;
        text-align: center;
    }
    table.documentacion td .documento > img {
        max-height: 80px;
    }
    table.documentacion td .documento > p {
        margin-bottom: 10px;
    }
    table.documentacion td .documento i.fa.fa-download {
        margin-top: 10px;
        display: none;
    }
    table.documentacion td .documento:hover > i.fa.fa-download {
        display: block;
    }
    span.nombre-campo {
        font-weight:bold;
        color:#00567D;
        margin: 2px 0;
        display: inline-block;
    }
    table.documentacion td .documento span.nombre-campo {
        margin: 0 0 10px;
    }
    #Modal .modal-dialog {
        max-width: 600px;
    }
</style>
<div>
    <div style="padding:20px;color:#686969;font-size:13px;line-height:18px;margin:0;font-family:Open Sans, Arial, sans-serif;" class="col-md-12">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 10px;right: 20px;">
            <span aria-hidden="true">&times;</span>
        </button>

                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="mainContent" class="datos">
                        <tr>
                            <td align="center" valign="center" style="background-color:#FFFFFF; font-family:Arial,sans-serif;font-size:16px;color:#03587C;text-transform:uppercase;">
                                <h1>Solicitud de Cliente Distinguido</h1>
                            </td>
                        </tr>
                        <!-- PRODUCTOS -->
                        <tr>
                            <td align="left" valign="center" style="background-color:#E1E7EA; padding:10px;">
                                <table style="width:100%; font-family:Arial,sans-serif;font-size:13px;color:#686969;">
                                    <tr>
                                        <td valign="top" align="left" style="text-align:left;vertical-align:top;">
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <span><strong>Solicitud No. {{ $data->id ?? '' }}</strong></span>
                                                    </td>
                                                    <td align="right" valign="top" style="padding: 0;">
                                                        <strong><p style="color: #686969;font-family: Arial,sans-serif;font-size: 12px; margin: 0 !important;">Fecha de Registro: {{ \Illuminate\Support\Carbon::parse($data->created_at)->format('d-m-Y') }}</p></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><h3>Datos Generales</h3></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Tipo:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->tipo ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Tratamiento:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->tratamiento ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Especialidad(es):</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->especialidades_list  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Persona Fiscal:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->persona  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">RFC:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->rfc  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Razón Social:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->razon  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Nombre:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->nombre  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Apellidos:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->apellido  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Fecha de Nacimiento:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ ($data->day.'-'.$data->month.'-'.$data->year)  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Teléfono:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->telefono  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Email:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->mail  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><h3>Datos de Dirección</h3></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Calle:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->direccion  ?? 'N/A' }}</span></td>
                                                </tr>

                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Número Exterior:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->num_ext  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Número Interior:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->num_int  ?? 'N/A' }}</span></td>
                                                </tr>

                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Colonia o población:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->colonia  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Delegación o municipio:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->delegacion  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Ciudad:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->ciudad  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Estado:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->estado  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Código Postal:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->cod_postal  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><h3>Documentación</h3></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table width="100%" class="documentacion">
                                                            <tr>
                                                                <td>
                                                                    <div class="documento">
                                                                        <span class="nombre-campo">Identificación Oficial:</span>
                                                                        <div>
                                                                            <img src="{!! $docs['ine'] !!}" alt="{{$data->ife}}" title="{{$data->ife}}">
                                                                        </div>
                                                                        <span>{{ \Str::limit($data->ife,30) ?? 'N/A' }}</span>
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="documento">
                                                                        <span class="nombre-campo">Comprobante de Domicilio:</span>
                                                                        <div>
                                                                            <img src="{!! $docs['dom'] !!}" alt="{{ $data->comprobante }}" title="{{ $data->comprobante }}">
                                                                        </div>
                                                                        <span>{{ \Str::limit($data->comprobante,30) ?? 'N/A' }}</span>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="documento">
                                                                        <span class="nombre-campo">Constancia Fiscal:</span>
                                                                        <div>
                                                                            <img src="{!! $docs['rfc'] !!}" alt="{{ $data->rfc_file }}" title="{{ $data->rfc_file }}">
                                                                        </div>
                                                                        <span>{{ \Str::limit($data->rfc_file,30) ?? 'N/A' }}</span>

                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="documento">
                                                                        <span class="nombre-campo">PDF con todo:</span>
                                                                        <div>
                                                                            <img src="{!! $docs['pdf_todo'] !!}" alt="{{ $data->pdf_doc_file }}" title="{{ $data->pdf_doc_file }}">
                                                                        </div>
                                                                        <span>{{ \Str::limit($data->pdf_doc_file,30) ?? 'N/A' }}</span>

                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">

                                                                    <a href="{{ route('solicitud-cliente-distinguido.download',[$data->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-download fa-2x"></i> Descargar documentación</a>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>

    </div>
</div>