@if($data->to_cliente && $data->is_email)
<div class="info-box bg-success">
    @if(!$data->is_email)
    <span class="info-box-icon bg-success"><i class="far fa-thumbs-up"></i></span>
    @endif
    <div class="info-box-content">
    <span class="info-box-number">{{ $data->subtitulo ?? '' }}</span>
    <span class="info-box-text">
    <span class="sub-info-box"></span>
        <ul>
            <li>La solicitud será revisada y procesada en las próximas horas. El tiempo aproximado para obtener su registro es de 2 a 3 días hábiles.</li> 
            <li>En ese periodo de tiempo, usted recibirá una notificación indicándole el estatus de la solicitud al correo electrónico que usó al registrarse.</li>
            <li>Revise muy bien su bandeja de entrada, spam o correo no deseado ya que a veces puede que llegué ahí el mensaje.</li>
            <li>Si pasado el tiempo regular de registro, no recibe notificación alguna, le pedimos de favor se comunique al 55-5578-1958 para preguntar por el status de la solicitud.</li> 
            <li>Al recibir la notificación por correo, en caso de que haya sido positivo su registro, se le proporcionará su nuevo No. de Cliente Distinguido.</li>
            <li>Una vez conociendo usted su No. de Cliente Distinguido, podrá acudir a cualquiera de nuestras tiendas para recibir el plástico físicamente.</li> 
            <li><strong>Muy importante:</strong> La entrega del plástico así como la aplicación de su descuento en cualquier compra, será posible hasta que se le asigne en sistema un numero de Cliente Distinguido, no antes.</li> 
        </ul>
        <span class="sub-info-box">¡Agradecemos su preferencia para Lanceta HG!</span>
    </span>
    </div>
</div>
@elseif ($data->is_email)
<div class="info-box bg-info">
    @if(!$data->is_email)
    <span class="info-box-icon bg-info"><i class="far fa-inbox"></i></span>
    @endif
    <div class="info-box-content">
    <span class="info-box-number">{{ $data->subtitulo ?? '' }}</span>
    <span class="info-box-text">
    <span class="sub-info-box"></span>
        <ul>
            <li>Si el correo registrado está asociado a un Cliente Distinguido actual, proceder a notificar al cliente.</li> 
            <li>Si la documentación es errónea o incompleta, contactar al cliente y solicitarle lo faltante. Posteriormente, adjuntar la documentación a la solicitud.</li>
            <li><strong>Muy importante:</strong> Notificar al cliente cuando haya alguna inconsistencia en el proceso de registro de No. de Cliente Distinguido.</li> 
        </ul>
        <span class="sub-info-box">&nbsp;</span>

    </span>
    </div>
</div>
@endif