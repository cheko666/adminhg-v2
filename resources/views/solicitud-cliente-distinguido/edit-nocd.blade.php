
<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 10px;right: 20px;">
    <span aria-hidden="true">&times;</span>
</button>
<h3 class="text-center" style="margin-top: 20px;">{{ $title }}</h3>
<div class="scd-container" style="box-sizing: content-box; width: 100%;">
    <div class="card-header text-center">
        <h4>Solicitud No. {{ $cd->id }} perteneciente a: </br>{{ $cd->nombre }}</h4>
        <div class="errores">
            <ul id="list-errores"></ul>
        </div>
    </div>
    {!! Form::open([
        'route' => 'solicitud-cliente-distinguido-nocd.update',
        'id'=> 'cdno-form',
    ],) !!}
        <input type="hidden" name="reference" id="reference" value="{{ $cd->reference }}">
        <div class="card-body">     
            <section class="header_form mb-4">
                <p>Los campos con <sup>*</sup> son requeridos</p>
            </section>

            <section class="mb-4">
                <h2 class="collapse">No. de Cliente</h2>
                <div class="row">    
                    <div class="col-12 col-sm-10 col-md-10">
                        <div class="form-group">
                            <label for="no_cd">No. de Cliente Distinguido: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="no_cd" name="no_cd" value="{{ old('codigo_postal') }}" placeholder="Sin prefijo 'CD' | 2 ... 999999" required />
                            </div>
                            @error('no_cd')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="card-footer">
            <button type="button" class="btn btn-block btn-info float-right" id="btnRegistrar">Registrar No. de Cliente</button>
        </div>
    {!! Form::close() !!}
</div>

<script>
    var token = '{{ csrf_token() }}';
    var ref = '{{ $cd->reference }}'
    var url = uri + '/nocd';
    var error_bag = $('#list-errores');
    $(document).ready( function () {
        $(document).on('click','#btnRegistrar',function(e) {
            e.preventDefault();

            var data = {
                _token: '{{ csrf_token() }}',
                reference: '{{ $cd->reference }}',
                no_cd: $('#no_cd').val(),
                id: '{{ $cd->id }}'
            }
            
            $.ajax({
                type: 'POST',
                url: url,
                cache: false,
                data: data,
                dataType: 'json',
                success: function (response) {
                    
                    // console.log(response);
                    if(response.status == 400)
                    {
                        if($(error_bag).is(':hidden'))
                            $(error_bag).show()
                        $(error_bag).html('');
                        $(error_bag).addClass('list-unstyled alert alert-danger');
                        $.each(response.messages, function(inputs,valores) {
                            $.each(valores, function(k,error) {
                                $(error_bag).append('<li>'+ error +'</li>');
                            })
                        })
                        setTimeout(function(){
                          $(error_bag).fadeOut();
                        }, 1000);
                    }
                    else if (response.status == 200)
                    {
                        if($(error_bag).is(':hidden'))
                            $(error_bag).show()
                        if($(error_bag).hasClass('alert-danger'))
                            $(error_bag).removeAttr('class');
                        $(error_bag).html('');
                        $(error_bag).addClass('list-unstyled alert alert-success');
                        $(error_bag).append('<li>'+ response.message.message +'</li>');
                        // $('#Modal').modal('hide');
                        
                        setTimeout(function(){
                          $('#Modal').modal('hide');
                          window.location.href = uri;
                        }, 1000);
                    }

                    // $('.modal-content').html(response.Xml);
                }
            });
        });

    });

</script>