@extends('layouts.iframe')

@section('title') {{ $title }} @stop

@section('content_header')
<h1 class="text-center">{{ $title }}</h1>
@stop

@section('adminlte_css')
<style>
    #solicitudtdc-form sup {
        color:red;
    }
    #solicituddc-form .content-header h1 {
        color: #03587C;
    }
</style>
@endsection

@section('content')
<div class="scd-container" style="box-sizing: content-box; width: 100%;">
    <div class="card-header text-center">Favor de tener a la mano la documentación mencionada en archivos JPG, JPEG o PDF.</div>
    {!! Form::open([
        'route' => 'solicitud-cliente-distinguido.store',
        'autocomplete' => 'on',
        'enctype' => 'multipart/form-data',
        'id'=> 'solicitudtdc-form'
    ]) !!}
        <div class="card-body">     
            <section class="header_form mb-4">
                <p>Los campos con <sup>*</sup> son requeridos</p>
            </section>

            <section id="personal_datas" class="mb-4">
                <h2>Datos Personales</h2>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="">
                                Es Usted: <sup>*</sup>
                            </label>
                            <select name="titulo" id="titulo" class="form-control">
                                <option value="sr" {{ old('titulo') == 'sr'  ? 'selected' : '' }}>Sr.</option>
                                <option value="sra" {{ old('titulo') == 'sra'  ? 'selected' : '' }}>Sra.</option>
                                <option value="estudiante" {{ old('titulo') == 'estudiante'  ? 'selected' : '' }}>Estudiante en Medicina</option>
                                <option value="dr" {{ old('titulo') == 'dr'  ? 'selected' : '' }}>Dr.</option>
                                <option value="dra" {{ old('titulo') == 'dra'  ? 'selected' : '' }}>Dra.</option>
                                <option value="institucion" {{ old('titulo') == 'institucion'  ? 'selected' : '' }}>Institución</option>
                                <option value="empresa" {{ old('titulo') == 'empresa'  ? 'selected' : '' }}>Empresa</option>
                            </select>
                        </div>
                    </div>
                    <div id="especialidad_content" class="col-12 col-sm-7 collapse">
                        <div class="form-group">
                            <label for="especialidad">
                                Especialidad(es):
                            </label>
                            <select name="id_especialidad_medica" id="id_especialidad_medica" class="form-control" size="8">
                                @foreach ($especialidades as $k => $v)
                                    <option value="{{ $k }}" {{ old('id_especialidad_medica') == $k  ? 'selected' : '' }}>{{ $v }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-7">
                        <div class="form-group">
                            <label for="nombre">Nombre<sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" class="form-control" type="text" id="nombre" name="nombre" value="{{ old('nombre') }}" placeholder="Nombre y Apellidos / Institución o Empresa" required />
                            </div>
                            @error('nombre')
					            <small class="text-danger">{{ $message }}</small>
				            @enderror
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-5">
                        <div class="form-group">
                            <label for="curp">CURP<sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" class="form-control" type="text" id="curp" name="curp" value="{{ old('curp') }}" placeholder="Clave Única de Registro de Población" required />
                            </div>
                            @error('curp')
					            <small class="text-danger">{{ $message }}</small>
				            @enderror
                        </div>
                    </div>
    
                    <div class="col-12 col-sm-12 col-md-7">
                        <label for="">
                            Fecha de nacimiento: <sup>*</sup>
                        </label>
                        <div class="row">
                            <div class="col-3 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <div style="display:inline;">
                                        <select class="form-control form-inline date" id="day" name="day" required>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-5 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <div style="display:inline;">
                                        <select class="form-control form-inline date" id="month" name="month" required>
                                            <option value="01">Enero</option>
                                            <option value="02">Febrero</option>
                                            <option value="03">Marzo</option>
                                            <option value="04">Abril</option>
                                            <option value="05">Mayo</option>
                                            <option value="06">Junio</option>
                                            <option value="07">Julio</option>
                                            <option value="08">Agosto</option>
                                            <option value="09">Septiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <div style="display:inline;">
                                        <select class="form-control form-inline date" id="year" name="year" required>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-5">
                        <div class="form-group">
                            <label for="telefono">Teléfono: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="tel" id="telefono" name="telefono" placeholder="10 dígitos" value="{{ old('telefono') }}" required />
                            </div>
                            @error('telefono')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="email">Correo electrónico: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" placeholder="sucorrero@dominio.com" required />
                            </div>
                            @error('email')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="email_confirmation">Confirmar correo electrónico: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="email" id="email_confirmation" name="email_confirmation"  placeholder="sucorrero@dominio.com" required />
                            </div>
                            @error('email_confirmation')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror                            
                        </div>
                    </div>
                </div>
            </section>
            <section class="mb-4">
                <h2>Datos de Dirección</h2>
                <div class="row">    
                    <div class="col-12 col-sm-12 col-md-9">
                        <div class="form-group">
                            <label for="direccion">Dirección: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="direccion" name="direccion" value="{{ old('direccion') }}" placeholder="Calle, No. Exterior, No. Interior" required />
                            </div>
                            @error('direccion')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror       
                        </div>
                    </div>
                    <div class="col-12 col-sm-5 col-md-3">
                        <div class="form-group">
                            <label for="codigo_postal">Código Postal: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="codigo_postal" name="codigo_postal" value="{{ old('codigo_postal') }}" placeholder="5 dígitos" required />
                            </div>
                            @error('codigo_postal')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-8 col-md-4">
                        <div class="form-group">
                            <label for="estado">Estado: <sup>*</sup></label>
                            <select name="id_state" id="id_state" required class="form-control">
                                @foreach ($estados as $k => $v)
                                    <option value="{{ $k }}" {{ old('id_state') == $k  ? 'selected' : '' }}>{{ $v }}</option>
                                @endforeach
                            </select>
                            @error('id_state')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-8 col-md-4">
                        <div class="form-group">
                            <label for="municipio">Alcaldía/Municipio: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="municipio" name="municipio" value="{{ old('municipio') }}" required />
                            </div>
                            @error('municipio')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="colonia">Colonia: <sup>*</sup></label>
                            <div class="input-group">
                                <input class="form-control" type="text" id="colonia" name="colonia" value="{{ old('colonia') }}" placeholder="Colonia/Población" required />
                            </div>
                            @error('colonia')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
            </section>
            
            <section id="documentacion" class="mb-4">
                <h2>Documentación</h2>
                <div class="row bootstrap-duallistbox-container">
                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="ine">Identificacion oficial: <sup>*</sup></label>
                            <span class="info-container"><span class="info" style="display:block;">INE, Pasaporte o Cédula Profesional</span></span>
                            <div class="input-group">
                                <input class="form-control" name="ine" id="ine" type="file" required />
                            </div>
                            @error('ine')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label for="comprobante">Comprobante de domicilio: <sup>*</sup></label>
                            <span class="info-container"><span class="info" style="display:block;">Recibo telefónico, predial, agua, etc.</span></span>
                            <div class="input-group">
                                <input class="form-control" name="comprobante" id="comprobante" type="file" required />
                            </div>
                            @error('comprobante')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
            </section>

            <section class="mb-4">
                <h2 class="collapse">Autorización</h2>
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-5 col-lg-3">
                        <div class="form-group clearfix">
                            <div class="custom-control custom-switch custom-switch-on-success">
                                <input type="checkbox" class="custom-control-input" name="terminos" id="terminos" required>
                                <label class="custom-control-label" for="terminos">Términos y condiciones</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="custom-control custom-switch custom-switch-on-success">
                                <input type="checkbox" class="custom-control-input" name="privacidad" id="privacidad" required>
                                <label class="custom-control-label" for="privacidad">Aviso de privacidad</label>
                            </div>
                        </div>                        
                    </div>
                </div>
            </section>
            <section id="recaptcha" class="mb-4">

            </section>

            <section class="mb-4">
                <button type="submit" class="btn btn-block btn-info float-right" style="background-color: #19AF96;" name="RegistrarBtn" id="RegistrarBtn" disabled>
                    <span class="fa fa-spin fa-spinner mr-1 d-none"></span> <span class="texto">Registrarse</span></button>
            </section>
            
        </div>

    {!! Form::close() !!}
</div>
@endsection

@section('adminlte_js')
<script>
    $(document).ready(function() {
        $('#solicitudtdc-form section#recaptcha').append('<div class="text-center"><div id="g-recaptcha"></div></div>');
    });
</script>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

<script>
    $("#RegistrarBtn").on('click',function (e) {
        e.preventDefault();

        $("#RegistrarBtn span.fa-spinner").removeClass('d-none');
        $("#RegistrarBtn span.texto").text('Enviando ...');
        window.setTimeout($("#solicitudtdc-form").submit(), 500);
    });

    var MaxYear = parseInt( new Date().getFullYear() - 18 ),
        MinYear = parseInt( MaxYear - 80 ),
        optionsYear = '';
    for (var i = MaxYear; i > MinYear; i--) {
        optionsYear += '<option value="'+i+'">'+i+'</option>';
    }
    $('select#year').html(optionsYear);
    $('select[name="titulo"]').change(function(){
        var valueTipo = $(this).val();
        if(valueTipo === 'estudiante' || valueTipo === 'dr' || valueTipo === 'dra'){
            if ($('div#especialidad_content').hasClass('collapse')){
                $('div#especialidad_content').removeClass('collapse');
            }
        }else{
            if (!$('div#especialidad_content').hasClass('collapse')){
                $('div#especialidad_content').addClass('collapse');
            }
        }
    });

    var gCallBack = function() {
        $('#RegistrarBtn').removeAttr('disabled')
    }
    var gExpired = function() {
        $('#RegistrarBtn').attr('disabled','disabled');
    }
    var onloadCallback = function() {
        
        grecaptcha.render('g-recaptcha', {
            'sitekey': '6LfduysbAAAAAPaQ0497lirXSlrYOQdaSrtORCj8',
            'callback': gCallBack,
            'expired-callback': gExpired
        });
    };

</script>
@stop
