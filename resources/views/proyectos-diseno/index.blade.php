@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('content_header')
<h1>{{ $title }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 variation">
            <table id="dt-index" class="table table-striped">
                <thead>
                <tr>
                    {{--<th width="24">ID</th>--}}
                    <th width="70">Referencia</th>
                    <th width="200">Nombre</th>
                    <th width="90">Solicita</th>
                    {{--<th width="50">Fecha Solic.</th>--}}
                    {{--<th width="50">Fecha Ini.</th>--}}
                    {{--<th width="50">Fecha Fin.</th>--}}
                    <th>Últ. Evento</th>
                    <th width="80">Fase</th>
                    <th width="80">Estatus</th>
                    <th width="60">% Avance</th>
                    <th width="184">Acción</th>
                </tr>
                </thead>
            </table>
        </div>

    </div><!--row-->

    <div class="modal fade" id="Modal"  role="dialog">
        <div class="modal-dialog">
            <div class="modal-content form-content">

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('css')

@stop

@section('plugins.Datatables', true)

@section('js')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url()->full() }}';

        $(document).ready( function () {
            $('#dt-index').DataTable({
                processing: true,
                serverSide: false,
                ajax: "{!! route('proyectos-diseno.index') !!}",
                columns: [
//                    {data:'id', name:'id'},
                    {data:'referencia',name:'referencia'},
                    {data:'nombre',name:'nombre'},
                    {data:'solicita',name:'solicita'},
//                    {data:'fecha_solicitud',name:'fecha_solicitud'},
//                    {data:'fecha_ini',name:'fecha_ini'},
//                    {data:'fecha_fin',name:'fecha_fin'},
                    {data:'ultimo_evento',name:'ultimo_evento'},
                    {data:'fase_proceso',name:'fase_proceso'},
                    {data:'estatus',name:'estatus'},
                    {data:'porcentaje',name:'porcentaje'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
                initComplete: function(settings) {
                    //settings.nTable.id --> Get table ID

                    $('#'+settings.nTable.id+'_filter input')
                            .wrap('<div class="d-inline-flex position-relative"></div>')
                            .after('<button type="button" class="close position-absolute" aria-label="Close" style="right:5px"><span aria-hidden="true">&times;</span></button>')
                            .attr('required','required').attr('title','Search');

                    // Click Event on Clear button
                    $(document).on('click', '#'+settings.nTable.id+'_filter button', function(){
                        $('#'+settings.nTable.id).DataTable({
                            "retrieve": true,
                        }).search('').draw(); // reDraw table
                    });
                }
            });
        } );

        $('#Modal').on('shown.bs.modal', function (event) {
//            $('#content').html('<div class="loading"><img src="images/loader.gif" alt="loading" /><br/>Un momento, por favor...</div>');

            var button = $(event.relatedTarget); // Button that triggered the modal
            var this_id = button.attr('data-id');
            var model = button.attr('data-model');
            var action = button.attr('data-action');

            var url = uri + '/'+ this_id;

            console.log(url);
            $.ajax({
                url: url,
                cache: false,
                type: 'GET',
                data: {_token: token},
                success: function (data) {
                    setTimeout(function()
                    {
                        $("fa-spinner").fadeOut("slow");
                    },1000);
                    $('.modal-content').html(data.view);

                }
            });

        });

        $('#Modal').on('shown.bs.modal', function (event) {
//            $('#content').html('<div class="loading"><img src="images/loader.gif" alt="loading" /><br/>Un momento, por favor...</div>');

            var button = $(event.relatedTarget); // Button that triggered the modal
            var this_id = button.attr('data-id');
            var model = button.attr('data-model');
            var action = button.attr('data-action');

            var url = uri + '/'+ this_id;

            console.log(url);
            $.ajax({
                url: url,
                cache: false,
                type: 'GET',
                data: {_token: token},
                success: function (data) {
                    setTimeout(function()
                    {
                        $("fa-spinner").fadeOut("slow");
                    },1000);
                    $('.modal-content').html(data.view);

                }
            });

        });

        $('#Modal').on('hidden.bs.modal', function (event) {
            $('.modal-content').data('');
        });
    </script>
@endsection