<style>
    .header {
        /** Extra personal styles **/
        /*background-color: red;*/
        text-align: center;
        width: 100%;
        padding-bottom: 0;
    }
    .header img.logo {
        width: 260px;
        padding-top: 20px;
    }
    .content {
        font-family:Open Sans, Arial, sans-serif;
        width: 100%;
        /*background-color: blue;*/
        top:0;
    }
    .footer {
        font-family:Open Sans, Arial, sans-serif;
        height: 35px;
        /*background-color: green;*/
        text-align: center;
        font-size: 9px;
        color: #666666;
        padding-top: 9px;
    }
    table.datos h1 {
        font-size:18px;
        font-weight: bold;
        margin:0 auto 7px;
    }
    table.datos h2,
    table.datos h3 {
        font-size: 15px;
        margin: 0;
        text-align:left;
        color:#999999;
    }
    table.documentacion td {
        text-align: center;
        padding: 10px;
    }
    table.documentacion td .documento {
        padding: 15px 0;
        display: block;
    }
    table.documentacion td .documento > div {
        display: block;
        text-align: center;
    }
    table.documentacion td .documento > img {
        max-height: 80px;
    }
    table.documentacion td .documento > p {
        margin-bottom: 10px;
    }
    table.documentacion td .documento i.fa.fa-download {
        margin-top: 10px;
        display: none;
    }
    table.documentacion td .documento:hover > i.fa.fa-download {
        display: block;
    }
    span.nombre-campo {
        font-weight:bold;
        color:#00567D;
        margin: 2px 0;
        display: inline-block;
    }
    table.documentacion td .documento span.nombre-campo {
        margin: 0 0 10px;
    }
    #Modal .modal-dialog {
        max-width: 600px;
    }
</style>
<div>
    <div style="padding:20px;color:#686969;font-size:13px;line-height:18px;margin:0;font-family:Open Sans, Arial, sans-serif;" class="col-md-12">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;top: 10px;right: 20px;">
            <span aria-hidden="true">&times;</span>
        </button>

                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="mainContent" class="datos">
                        <tr>
                            <td align="center" valign="center" style="background-color:#FFFFFF; font-family:Arial,sans-serif;font-size:16px;color:#03587C;text-transform:uppercase;">
                                <h1>{{ $title }}</h1>
                            </td>
                        </tr>
                        <!-- PRODUCTOS -->
                        <tr>
                            <td align="left" valign="center" style="background-color:#E1E7EA; padding:10px;">
                                <table style="width:100%; font-family:Arial,sans-serif;font-size:13px;color:#686969;">
                                    <tr>
                                        <td valign="top" align="left" style="text-align:left;vertical-align:top;">
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <span><strong>Proyecto No. {{ $data->id ?? '' }}</strong></span>
                                                    </td>
                                                    <td align="right" valign="top" style="padding: 0;">
                                                        <strong><p style="color: #686969;font-family: Arial,sans-serif;font-size: 12px; margin: 0 !important;">Fecha de Registro: {{ \Illuminate\Support\Carbon::parse($data->created_at)->format('d-m-Y') }}</p></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Referencia:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->referencia ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Nombre:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->nombre ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Solicita:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->solicita ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Fecha de Solicitud:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ \Illuminate\Support\Carbon::parse($data->fecha_solicitud)->format('d-m-Y') }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Fecha de inicio:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ \Illuminate\Support\Carbon::parse($data->fecha_inicio)->format('d-m-Y') }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Fecha de Término:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ \Illuminate\Support\Carbon::parse($data->fecha_fin)->format('d-m-Y') }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Último Evento:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->ultimo_evento  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Fase del Proceso:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->fase_proceso  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">% de Avance:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->porcentaje  ?? 'N/A' }}</span></td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="width:30%;  vertical-align:top;"><span class="nombre-campo">Estatus:</span></td>
                                                    <td valign="top" style="width:70%; vertical-align:top;"><span>{{ $data->estatus  ?? 'N/A' }}</span></td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                    </table>

    </div>
</div>