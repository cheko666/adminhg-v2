@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('css')
    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>
@endsection

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('plugins.Select2', true)
@section('plugins.Select2', true)

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="far fa-envelope"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Cabeceras sin producto</span>
                <span class="info-box-number text-xl">3</span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Pasillos sin producto</span>
                <span class="info-box-number text-xl">4</span>
            </div>
        </div>
    </div>
    <div class="col-md-4"></div>
</div>

{!! Form::open(['route'=>['producto-por-pasillo-tienda.update'] ]) !!}
<div class="row">
    <div class="col-md-12">
        <input type="hidden" name="suc" id="suc" value="{{ $sucursal->siglas }}"/>

        <div class="card">
            <div class="card-header">
                <h5>Por favor, ingresa los grupos de productos que se encuentren en cada pasillo y cabecera de las góndolas.</h5>
                <span>Una vez llenados todos los campos, da clic en el botón guardar para que queden registrados.</span>
            </div>
            <div class="card-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="card" id="plano">
                            <div class="card-header">
                                <h3 class="card-title" style="color: #999; text-transform: uppercase; margin-bottom:5px; display: block;">Plano de sucursal</h3>
                            </div>
                            <div class="card-body">
                                @if (file_exists(public_path('img/planos-pasillo-sucursal/plano-pasillos-'.$sucursal->siglas.'.jpg')))
                                    <img src="{{ asset('img/planos-pasillo-sucursal/plano-pasillos-'.$sucursal->siglas.'.jpg') }}" alt="" style="width: 100%;">
                                @else
                                    <img src="{{ asset('img/no-image.jpg') }}" alt="" style="width: 100%;">
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="card card-primary card-outline" id="cabeceras">
                                    <div class="card-header">
                                        <h3 class="card-title" style="text-transform: uppercase;">Cabeceras</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-animation-speed="200"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @for($i = 1;$i<=$num_cabeceras;$i++)
                                            <div style="margin-bottom: 20px; overflow: auto;">
                                                <div class="col-md-12">
                                                    <label for="title">Cabecera {{$i}}</label>
                                                    <select id="c{{$i}}" name="c{{$i}}[]" class="form-control" multiple="multiple"></select>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="card card-primary card-outline" id="pasillos">
                                    <div class="card-header">
                                        <h3 class="card-title" style="text-transform: uppercase;">Pasillos</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-animation-speed="200"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        @for($i = 1;$i<=$num_pasillos;$i++)
                                            <div style="margin-bottom: 20px; overflow: auto;">
                                                <div class="col-md-12">
                                                    <label for="title">Pasillo {{$i}}</label>
                                                    <select id="p{{$i}}" name="p{{$i}}[]" class="form-control" multiple="multiple"></select>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Guardar',['class'=>'btn btn-primary']) !!}
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@endsection

@section('js')
<script type="text/javascript">
    $('#pasillos').CardWidget('collapse');
    $('#cabeceras').CardWidget('collapse');



    var datas = [];
    @foreach ($productos_pasillos_all as $pasillo => $grupos_productos)
        @foreach($grupos_productos as $grupo_producto)
        var data = {
            id:         "{{ $grupo_producto["id"] }}",
            text:       "{!! $grupo_producto["text"] !!}",
            selected:   "{{ $grupo_producto["selected"] }}"
        };
        datas.push(data);
        @endforeach
        $("#{{ $pasillo }}").select2({
            data: datas,
            tags: true,
            tokenSeparators: [','],
        });
        datas = [];
    @endforeach

    @foreach ($productos_cabeceras_all as $cabecera => $grupos_productos)
        @foreach($grupos_productos as $grupo_producto)
        var data = {
            id:         "{{ $grupo_producto["id"] }}",
            text:       "{!! $grupo_producto["text"] !!}",
            selected:   "{{ $grupo_producto["selected"] }}"
        };
        datas.push(data);
        @endforeach
        $("#{{ $cabecera }}").select2({
            data: datas,
            tags: true,
            tokenSeparators: [','],
        });
        datas = [];
    @endforeach


{{--

    $("#"{{ $pasillo }}).select2({
        data: datas,
        tags: true,
        tokenSeparators: [','],
    });
--}}

</script>
@endsection