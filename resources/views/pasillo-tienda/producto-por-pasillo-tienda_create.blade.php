@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('css')

@endsection

@section('plugins.Select2', true)

@section('content')

<div class="title" style="margin: 10px 0;">
    <h1>{{ $title }}</h1>
</div>
<hr>

<span style="color: #999;">Escoge tu sucursal.</span>

<div class="col-sm-12 col-md-3">
    <label for="title">Sucursal</label>
    {!! Form::select('suc', $sucursales , null ,['id' => 'select-sucursal' , 'class' => 'js-example-basic-single form-control'  , 'style' => 'border-color:#990000 !important' ]) !!}
</div>

@endsection

@section('js')
<script type="text/javascript">
    $("#select-sucursal").select2({
        placeholder: "Selecciona ...",
        allowClear: true
    }).val('HG').trigger("change");
    $('[name="suc"]').change(function(){
        var sucSelected = $("option:selected", this);
        sucValue = this.value;
        document.location.href="{!!route('producto-por-pasillo-tienda.edit')!!}" + "/" + sucValue;
    });
</script>
@endsection