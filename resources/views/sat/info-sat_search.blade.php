@extends('adminlte::page')

@section('title', 'Información SAT')

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    <div class="col-md-12">
        <div class="row">
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Llena los campos para obtener la clave de producto-servicio del SAT</h3>
                </div>
                {!! Form::open(['route'=>'info-sat-producto.show','id'=>'formInfoSatSearch']) !!}
                <div class="card-body">
                    <p>Solo introducir dígitos sin ceros a la izquierda</p>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <div class="form-group">
                                {!! Form::label('serie', 'Serie') !!}
                                {!! Form::text('serie',null,['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <div class="form-group">
                                {!! Form::label('producto', 'Producto') !!}
                                {!! Form::text('producto',null,['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info float-right" id="btnBuscar">Buscar</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url()->full() }}';

        /*

         $('#btnBuscar').on('click',function (){
         var serie_s = $('#serie_s').val();
         var producto_s = $('#producto_s').val();

         var url = uri + '/search/';

         $.ajax({
         url:url,
         data: {_token:token,serie:serie_s,producto:producto_s},
         cache:false,
         success: function (response) {
         $('#btnBuscar').addClass('disabled');
         $('modal.-content').html(response);
         }
         }).done(function (data) {
         $(".alert-success").prop("hidden", false);
         }).fail(function (data) {
         alert($.each(data));

         $.each(data.responseJSON, function (key, value) {
         var input = '#formInfoSatSearch input[name=' + key + ']';
         alert(input);
         $(input + '+span>strong').text(value);
         $(input).parent().parent().addClass('has-error');
         });
         });

         });
         */

    </script>
@endsection