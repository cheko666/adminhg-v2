@extends('adminlte::page')

@section('title', 'Información SAT')

@section('content_header')
    <h1>Información de SAT</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="card card-info card-outline">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
            </div>
            <div class="card-body">
                <p>Código: {{ $data->codigo }}</p>
                <p>Descripción: {{ $data->descripcion }}</p>
                <p>Código Producto-Servicio SAT: <strong id="claveSat">{{ $data->sat_c_prodserv }}</strong></p>
                <p>Código Unidad SAT: <strong id="claveSatU">{{ $data->sat_c_unidad }}</strong></p>
                <a href="#" class="btn btn-info btn-lg btn-block" id="btnCopyClipboard" style="" data-clipboard-target="#claveSat">Copiar clave</a>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{ route('info-sat-producto') }}" class="btn btn-sm btn-secondary" style="margin-top: 30px;">< Buscar otro código</a>
    </div>
</div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.2/umd/popper.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url()->full() }}';


        var clipboard = new ClipboardJS('#btnCopyClipboard');

        clipboard.on('success', function(e) {
            var btnCopiar = $('#btnCopyClipboard');
            btnCopiar.removeClass('btn-info');
            btnCopiar.addClass('btn-success');
            btnCopiar.text('¡Copiado!');
        });
/*

        $('#btnBuscar').on('click',function (){
            var serie_s = $('#serie_s').val();
            var producto_s = $('#producto_s').val();

            var url = uri + '/search/';

            $.ajax({
                url:url,
                data: {_token:token,serie:serie_s,producto:producto_s},
                cache:false,
                success: function (response) {
                    $('#btnBuscar').addClass('disabled');
                    $('modal.-content').html(response);
                }
            }).done(function (data) {
                $(".alert-success").prop("hidden", false);
            }).fail(function (data) {
                alert($.each(data));

                $.each(data.responseJSON, function (key, value) {
                    var input = '#formInfoSatSearch input[name=' + key + ']';
                    alert(input);
                    $(input + '+span>strong').text(value);
                    $(input).parent().parent().addClass('has-error');
                });
            });

        });
*/

    </script>
@endsection