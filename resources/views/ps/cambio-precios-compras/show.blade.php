@extends('adminlte::page')

@section('title', 'Actualización de precios en productos')

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    <section class="col-md-12 col-lg-10 col-xl-9">
        <div class="card card-success">
            <div class="card-header">{{ $subtitle }}</div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Datos ingresados</h4>
                            </div>
                            <div class="card-body">
                                {!! $estadisticas_ini !!}
                                {!! $tabla_inicial['tabla'] !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h4 class="card-title"><i class="fa fa-edit"></i> Resultados de la ejecución</h4>
                                <div class="card-tools">
                                    <span class="badge"><i class="fa fa-calendar-check"></i> Fecha:</span>
                                    <span class="badge badge-primary">{{ date('d-m-Y h:m:i') }}</span>
                                </div>
                            </div>
                            <div class="card-body">
                                {!! $estadisticas_fin !!}
                                {!! $tabla_final['tabla'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('css')

@stop

@section('js')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
    </script>
@endsection