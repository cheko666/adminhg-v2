@extends('adminlte::page')

@section('title', 'Cambio de Precios')

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    <section class="col-md-12 col-lg-10 col-xl-9">
        {!! Form::open(['route'=>'cambio-precios-compras.store']) !!}
        <div class="card card-info">
            <div class="card-header">{{ $subtitle }}</div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('datos','Datos') !!}
                            {!! Form::textarea('datos',null,['id'=>'datos','class'=>'form-control','required','placeholder'=>'Codigo,Precio']) !!}
                        </div>
                        <div class="form-group">
                            <button id="btnPrevisualizar" class="btn btn-sm btn-info">Previsualizar cambios</button>
                        </div>

                    </div>
                    <div class="col-md-7" id="previsualizacion" style="overflow-y: scroll;max-height: 400px;">

                    </div>
                </div>
            </div>
            <div class="card-footer" style="display: none; text-align: right;">
                {!! Form::submit('Ejecutar cambios',['class'=>'btn btn-warning pull-right']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection

@section('css')

@stop

@section('js')
    <script type="text/javascript">
        var token='{{ csrf_token() }}';

        $('#btnPrevisualizar').click(function(e) {
            e.preventDefault();
            var text = $('textarea#datos').val();

            $.ajax({
                url: "{{ route('cambio-precios-compras.store') }}",
                cache: false,
                type: 'POST',
                data: {_token: token,datos:text},
                success: function (data) {
                    /*
                    setTimeout(function()
                    {
                        $("fa-spin").fadeOut("slow");
                    },2000);
*/
                    $('#previsualizacion').html(data.tabla);
                    $('#previsualizacion').prepend(data.estadisticas);
                    $('.card-footer').show();
                    $(this).hide();
                },
                error: function (data) {
                    var response = data.responseJSON;
                    var errorsHtml = '<div class="alert alert-danger"><ul>';

                    $.each( response.errors, function( key, value ) {
                        errorsHtml += '<li>'+ value + '</li>';
                    });
                    errorsHtml += '</ul></div>';

                    $( '#previsualizacion' ).html( errorsHtml );
                }
            });
        });
    </script>
@endsection