@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('css')

@endsection

@section('plugins.Select2', true)
@section('plugins.Date Range Picker', true)

@section('content')

    <div class="title" style="margin: 10px 0;">
        <h1>{{ $title }}</h1>
        <div class="alert alert-danger error-message-display" style="display:none">
            <ul></ul>
        </div>
        <div class="alert alert-success print-success-msg" style="display:none">
            <ul></ul>
        </div>
    </div>
    <hr>

    <span style="color: #999;">Ingresa el evento asociado al proyecto {{ $proyecto->nombre }}.</span>

    {!! Form::open(['route'=>['evento-proyecto-diseno.store',$proyecto->id],'id'=>'eventos','name'=>'eventos']) !!}

    <div class="form-group">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <label for="title">Sucursal</label>
                {!! Form::select('proyecto', $proyectos , null ,['id' => 'select-proyecto' , 'class' => 'js-example-basic-single form-control'  , 'style' => 'border-color:#990000 !important' ]) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12 col-md-6">
            <label for="evento">Evento</label>
            {!! Form::text('evento',null,['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="form-group col-md-6">
            <label for="fecha" style="width: 100%">Fecha
            </label>
                {!! Form::text('fecha',date('d-m-y'),['id'=>'calendario']) !!}
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@section('js')
    <script type="text/javascript">
        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });

        $("#select-proyecto").select2({
            placeholder: "Selecciona ...",
            allowClear: true
        }).val('0').trigger("change")
        ;

        $( "#calendario" ).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1999,
            autoApply: true,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale:{
                format: "DD-MM-YY",
                separator: " - ",
                applyLabel: "Aplicar",
                cancelLabel: "Cancelar",
                fromLabel: "Desde",
                toLabel: "Hasta",
                firstDay: 1
            },
        });
    </script>
@endsection