@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => 'www.lancetahg.com.mx/cliente-distinguido-9'])
Ver
@endcomponent

Gracias,<br>
{{ env('SITE_APP_NAME','Lanceta HG') }}
@endcomponent
