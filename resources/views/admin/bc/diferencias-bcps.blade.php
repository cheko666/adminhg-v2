@extends('adminlte::page')

@section('title') {{ $title }} @stop

@section('styles')
    <style type="text/css">
        .datatable-Log {
            font-size: 13px;
        }
        [id^='cambio_'] span {
            margin-bottom: 5px;
        }
        .cambios {
            display: table;
            border-spacing: 4px;
            width:360px;
        }
        .cambio {
            display: table-row;
            color: #999;
            font-size: 0.8rem;
            font-weight: bold;
            /*background-color: #eee;*/
        }
        .cambio span {text-transform: uppercase;}
        .cambio span,
        .cambio .valores {
            display: table-cell;
            vertical-align: middle;
            padding: 5px;
        }
        .cambio span {
            text-align: right;
            width: 85px;
            font-size: 0.9em;
        }
        .cambio .valores {
            width: auto;
            background-color: #f4f4f4;
        }
        .cambio .valor {
            width: 48%;
        }
        .cambio .valor:nth-child(1) {
            margin-right:5px;
        } 
        .cambio .valores > div {
            font-size: 0.8em;
        }
        .valor-anterior.d-inline-block.p-1.text-muted::after {
            content: ' -->';
            opacity: 0.5;
        }
    </style>

@section('content')
    <div class="row">
        <div class="col-md-9 col-lg-8">
            <form action="{{ route('bc.productos.listarDiferenciasBcPs.index') }}" method="GET" enctype="multipart/form-data">
                @csrf
                <div class="row">

                    <div class="col-sm-12 col-md-9 col-lg-5">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="idp">ID(s) de Producto</label>
                            <input type="text" id="idp" name="idp" class="form-control" value="{{ old('idp', isset($idp) ? $idp : '') }}">
                            @if($errors->has('idp'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('idp') }}
                                </em>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-9 col-lg-5">
                        <div class="form-group {{ $errors->has('campos') ? 'has-error' : '' }}">
                            <label for="campos_comparar">Campos a comparar*
                                <span class="btn btn-info btn-xs select-all">Selecc. Todos</span>
                                <span class="btn btn-info btn-xs deselect-all">Deselecc. Todos</span>
                            </label>
                            <select name="campos_comparar[]" id="campos_comparar" class="form-control select2" multiple="multiple">
                                @foreach($campos as $id => $campo)
                                    <option value="{{ $id }}" {{ (in_array($id, old('campos', []))) ? 'selected' : '' }}>{{ $campo }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('campos'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('campos') }}
                                </em>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-10 col-lg-6">    
                        <input style="margin-right: 20px;" class="btn btn-default" type="button" value="Borrar filtros">
                        <input class="btn btn-danger" type="submit" value="Filtrar">
                    </div>
                    
                </div>
            </form>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="row">
                <form action="{{ route('bc.productos.corregirDiferenciasBcPs.index') }}" method="GET" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="idp" id="idp" value="{{ $idp ?? '' }}">
                    <input type="hidden" name="campos_comparar" id="campos_comparar" value="{{ implode(',',array_flip($campos_comparar)) ?? '' }}">
                    <div class="col-md-12">
                        <div class="form-group { $errors->has('name') ? 'has-error' : '' }}">
                            {{-- <label for="tipo_producto">Tipo de productos</label> --}}
                            <div>
                                <div style="display: inline-block; padding-left:0">
                                    <input type="radio" name="tipo_producto" value="todo" checked>
                                    <label for="todo">Produc. + Combina.</label>
                                </div>
                                <div style="display: inline-block; padding-left:1rem">
                                    <input type="radio" name="tipo_producto" value="productos">
                                    <label for="productos">Productos</label>
                                </div>                       
                                <div style="display: inline-block; padding-left:1rem">
                                    <input type="radio" name="tipo_producto" value="combinaciones">
                                    <label for="combinaciones">Combinaciones</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-block btn-success" type="submit" value="Corregir Difer.">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            {{ $title ?? '' }} ({{ $collection->count() }})
        </div>

        <div class="card-body">
            @if($collection->count() > 0)
            <div class="table-responsive">
                <table class="table table-bordered datatable datatable-Log">
                    <thead>
                    <tr>
                        <th width="15">&nbsp;</th>
                        <th width="60">Imagen</th>
                        <th width="40">Código</th>
                        <th width="280">Producto</th>
                        <th width="40">Cód. PS</th>
                        <th width="40">Cód. PA</th>
                        <th width="40">Id_P</th>
                        <th width="40">Id_PA</th>
                        <th width="40">Id_SA</th>
                        <th width="40">Id_SP</th>
                        <th width="40">Princ.</th>
                        <th width="120">Bloqueo (Compra / Venta / Total)</th>
                        <th width="20">Activo</th>
                        <th>Diferencias</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($collection as $key => $collect)
                        <tr data-entry-id="{{ $collect->codigo }}">
                            <td>&nbsp;</td>
                            <td>
                                <div style="width: 100%; margin: auto 0; text-align:center;">
                                    <a href="{{ $collect->url }}" target="_blank">
                                        <img src="{{ $collect->img }}" alt="Imagen de producto" width="60">
                                    </a>
                                </div>
                            </td>
                            <td>{{ $collect->codigo ?? '' }}</td>
                            <td>{!! formatearDescripcionHtml($collect->description,35,true) ?? '' !!}</td>
                            <td>{{ $collect->p_codigo ?? '' }}</td>
                            <td>{{ $collect->pa_codigo ?? '' }}</td>
                            <td>{{ $collect->id_product ?? '' }}</td>
                            <td>{{ $collect->id_product_attribute ?? '' }}</td>
                            <td>{{ $collect->id_sa ?? '' }}</td>
                            <td>{{ $collect->id_sp ?? '' }}</td>
                            <td>{!! $collect->ps_default_on ? '<span class="badge badge-danger">*</span>' : '' !!}</td>
                            <td>
                                <span style="display: inline-block; width:30%;"><span style="display: inline; padding-right:3px;">C</span><span class="badge badge-{!! $collect->bloqueo_compra ? 'success">S</span>' : 'danger">N</span>' !!}</span>
                                <span style="display: inline-block; width:30%;"><span style="display: inline; padding-right:3px;">V</span><span class="badge badge-{!! $collect->bloqueo_venta ? 'success">S</span>' : 'danger">N</span>' !!}</span>
                                <span style="display: inline-block; width:30%;"><span style="display: inline; padding-right:3px;">T</span><span class="badge badge-{!! $collect->bloqueo ? 'success">S</span>' : 'danger">N</span>' !!}</span>
                            </td>
                            <td><span class="badge badge-{!! $collect->active ? 'success">S</span>' : 'danger">N</span>' !!}</span></td>
                            <!-- <td>
                                <div style="display:block;">Atributo 1</span> <span>Valor 1</span>
                                <div style="display:block;">Atributo 2</span> <span>Valor 2</span>
                            </td> -->
                            <td>{!! formatearDiferenciasCamposAdminhgPs($collect->diferencias) ?? '' !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <p>No hubo diferencias entre Magic (Admin HG) y Prestashop</p>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);

            $.extend(true, $.fn.dataTable.defaults, {

            });
            $('.datatable-Log:not(.ajaxTable)').DataTable({
                buttons: dtButtons,
                lengthMenu: [10,30,60],
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable()
                        .columns.adjust();
            });
        })

    </script>
@endsection