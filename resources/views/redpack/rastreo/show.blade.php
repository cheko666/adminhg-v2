@extends('adminlte::page')

@section('title', 'Info-SAT')

@section('content_header')

@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h1 class="h5 m-0">{{ $title }}</h1>
        </div>

        <div class="card-body">
            <div class="rastreo col-xs-12 col-lg-8">
                <div class="block m-b-2">
                    <div>

                        <div class="block">
                            <div class="clearfix text-center tag-{{ Illuminate\Support\Str::slug($response->situacion->descripcion,'-') }}">
                                <p class="status">{{ $response->situacion->descripcion ?? 'N/A' }}</p>
                            </div>
                        </div>

                        <div class="block">
                            <div class="clearfix rastreo-info col-sm-12">
                                <div class="row media m-y-1 header">

                                    <div class="col-xs-12 col-sm-5 header-courier">
                                        <div class="col-xs-6 col-md-5 courier-logo">
                                            <a href="http://www.estafeta.com/default.aspx" target="_blank">
                                                <img src="{{ asset('img/redpack-logo.jpg') }}">
                                            </a>
                                        </div>
                                        <div class="col-xs-6 col-md-7">
                                            <h2 class="courier-name">{{ ucfirst($transportista->name) }}</h2>
                                            <a href="tel:+52 1-800-378-2338" class="link--phone">{{ $transportista->phone ?? 'N/A' }}</a>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-7 header-info">
                                        <div class="col-xs-12 col-md-6 info-header">
                                            Número de Guía
                                            <div class="col-xs-12 info-value">{{ $response->numeroDeGuia ?? 'N/A' }}</div>
                                        </div>
                                        <div class="col-xs-12 col-md-6 info-header">
                                            No. de Pedido
                                            <div class="col-xs-12 info-value">{{ $pedido->id ?? 'N/A' }}</div>
                                        </div>

                                    </div>

                                </div>

                                <div class="row">
                                    <hr>

                                    <div class="col-xs-12 col-md-6 info-header">
                                        Tipo de Servicio
                                        <div class="col-xs-12 info-value">{{ $response->tipoServicio->descripcion ?? 'N/A' }}</div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 info-header">
                                        Estatus del envío
                                        <div class="col-xs-12 info-value">{{ $response->situacion->descripcion ?? 'N/A' }}</div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 info-header">
                                        Recibió
                                        <div class="col-xs-12 info-value">{{ $response->personaRecibio ?? 'N/A' }}</div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 info-header">
                                        Fecha de recolección
                                        <div class="col-xs-12 info-value">{{ fechaFormateada($response->fechaDocumentacion) ?? 'N/A' }}</div>
                                    </div>
                                    {{--                <div class="col-xs-12 col-md-6 info-header">
                                                        Fecha programada
                                                        <div class="col-xs-12 info-value">{!! $tracking->expected_delivery !!}</div>
                                                    </div>--}}
                                    <div class="col-xs-12 col-md-6 info-header">
                                        Fecha-Hora de entrega
                                        <div class="col-xs-12 info-value">{{ fechaFormateada($response->fechaSituacion) ?? 'N/A' }}</div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="block history">
                            <h2>Historial</h2>
                            <div class="checkpoints">
                                @if(count($response->paquetes->detallesRastreo)>0)
                                    <ul class="checkpoints__list">
                                        <?php $i = 0; ?>
                                        @foreach($response->paquetes->detallesRastreo as $detalle)
                                            <li class="checkpoint" data-mov="{{ $i }}">
                                                <div class="checkpoint__time">
                                                    {!! fechaFormateada($detalle->fechaEvento) !!}
                                                </div>
                                                <div class="checkpoint__icon {{ Illuminate\Support\Str::slug($detalle->evento,'-') }}"></div>
                                                <div class="checkpoint__content">
                                                    <strong>{!! $detalle->evento !!}</strong>
                                                    <div class="hint">{!! $detalle->localizacion !!}</div>
                                                </div>
                                            </li>
                                            <?php $i++; ?>
                                        @endforeach
                                    </ul>
                                @else
                                    <p class="noinfo">No hay movimientos que mostrar</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="btn-container pull-right">
                                <a href="{{ route('admin.rastreo.search') }}" class="btn btn-success btn-s rastrear-btn"><i class="fa fa-plus"></i> Rastrear otro</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('css/rastreo.css') }}" rel="stylesheet" />
@stop

@section('js')

@endsection