@extends('adminlte::page')

@section('title', 'Rastreo-Redpack')

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@section('content')
    {{--
        <div style="margin-bottom: 10px;" class="row hidden">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.productos.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.productos.title_singular') }}
                </a>
            </div>
        </div>
    --}}
    <div class="col-md-12">
        <div class="row">
            {!! Form::open(['route'=>'rastreo.show','method'=>'GET','class'=>'form-container']) !!}
            @csrf
                <div class="card card-info">
                    <div class="card-header">Rastrear pedido con la paquetería</div>

                    <div class="card-body">
                        <div class="row">

                            <div class="text-center">
                                <div style="display: none;">
                                    <img src="https://media.lancetahg.com.mx/guia-estafeta-ejemplo_lg.gif" alt="Ejemplo Guía Estafeta" class="guia">
                                </div>

                            </div>
                            <div class="form-group">
                                {!! Form::label('guia','No. de Guía (9 dígitos)') !!}
                                {!! Form::text('guia',null,['class'=>'form-control','required']) !!}
                            </div>

                        </div>
                    </div>

                    <div class="card-footer">
                        {!! Form::submit('Rastrear',['class'=>'btn btn-info']) !!}
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('css')
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
@stop

@section('js')

@endsection