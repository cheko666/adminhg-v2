@extends('adminlte::page')

@section('title', 'Cotización Envío')

@section('content_header')
<h1>{{ $title }}</h1>
@stop

@section('content')
    <section class="col-md-8 col-lg-8 col-xl-6">
        {!! Form::open(['route'=>'redpack.coti-envio.show']) !!}
        <div class="card card-info">
            <div class="card-header">Ingresa los datos que se indican para obtener el costo de envío.</div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            {!! Form::label('id_pedido','No. de Pedido') !!}
                            {!! Form::text('id_pedido',null,['class'=>'form-control','required','placeholder'=>'Sin ceros a la izquierda']) !!}
                        </div>
                    </div>
                    <div class="col-md-5">
                        {!! Form::label('importe_pedido','Importe del Pedido') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" name="importe_pedido" id="importe_pedido" placeholder="Sin IVA">
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('peso','Suma Peso (kg)') !!}
                            {!! Form::text('peso',null,['class'=>'form-control','required','placeholder'=>'kg']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('peso_volumetrico','Peso Volumétrico (kg)') !!}
                            {!! Form::text('peso_volumetrico',null,['class'=>'form-control','required','placeholder'=>'kg']) !!}
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::label('cp','Código Postal de destino') !!}
                            {!! Form::text('cp',null,['class'=>'form-control','required','placeholder'=>'Sin ceros a la izquierda']) !!}
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                {!! Form::submit('Cotizar',['class'=>'btn btn-info']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection

@section('css')

@stop

@section('js')
    <script type="text/javascript">
        $('#importe_pedido').keyup(function(event) {

            // skip for arrow keys
            if(event.which >= 37 && event.which <= 40) return;

            // format number
            $(this).val(function(index, value) {
                return value
                        .replace(/,/g, '')
//                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                        ;
            });
        });
    </script>
@endsection