@extends('adminlte::page')

@section('title', 'Cotización Envío')

@section('content_header')
<h1>{{ $title }}</h1>
@stop

@section('content')

    <section class="col-md-6 col-lg-8 col-xl-6" style="max-width: 600px;">

        <div class="title">
            <h1>{{ $title }}</h1>
        </div>

        <div class="alert alert-default">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="card-title"><i class="fa fa-keyboard mr-1"></i>Datos ingresados</h4>
                </div>
                <div class="col-md-5">
                    <p>No. del Pedido: <strong>#{{ $costo_envio->pedidos ?? 'N/A' }}</strong></p>
                </div>
                <div class="col-md-5">
                    <p>Monto del Pedido: <strong>$ {{ $costo_envio->monto_productos ?? 'N/A' }}</strong></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <p>Peso Real: <strong>{{ $costo_envio->peso->real ?? 'N/A'}} kg</strong></p>
                </div>
                <div class="col-md-5">
                    <p>Peso Volumétrico: <strong>{{ $costo_envio->peso->volumetrico ?? 'N/A'}} kg</strong></p>
                </div>
            </div>

            <p>C.P. Destino: <strong>{{ $costo_envio->cp_destino ?? 'N/A'}}</strong></p>
        </div>


        <div class="card card-success shadow-sm">
            <div class="card-header">
                <h3 class="card-title mb-0"><i class="fa fa-file-alt mr-1"></i>Resumen</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <div class="row">
                    <div class="col-md-5">
                        <p><span class="d-xs-block">C.P. Origen: <strong>{{ $costo_envio->cp_origen ?? 'N/A'}}</strong></span> <small class="badge badge-info"><i class="fa fa-map-marker-alt"></i> CDMX</small></p>
                    </div>
                    <div class="col-md-6">
                        <p><span class="d-xs-block">C.P. Destino: <strong>{{ $costo_envio->cp_destino ?? 'N/A'}}</strong></span> <small class="badge badge-info"><i class="fa fa-map-marker-alt"></i> {{ $costo_envio->datos_cobertura->estado }}</small></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <p><span class="d-xs-block">Tipo de Cobertura: <small class="badge badge-info">{{ $costo_envio->costo_cobertura->tipo_cobertura ?? 'N/A'}}</small></span></p>
                    </div>
                    <div class="col-md-5">
                        <p><span class="d-xs-block">Tipo de servicio: </span> <small class="badge badge-info"> {{ $costo_envio->costo_cobertura->tiempo_entrega->ecoexpress ? 'Ecoexpress' : 'N/A'}}</small> </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                            <span class="d-md-block">Tipo de Entrega:</span>
                                <span class="d-xs-block">
                                    <small class="badge badge-info">{{ $costo_envio->costo_cobertura->tipo_entrega->domicilio ?? 'N/A'}}</small> <small class="badge badge-info">{{ $costo_envio->costo_cobertura->tipo_entrega->ocurre ?? 'N/A'}}</small>
                                </span>
                    </div>
                    <div class="col-md-5">
                        <p><span class="d-xs-block">Tiempo estimado de entrega: </span> <small class="badge badge-info"><i class="fa fa-clock"></i> Hasta {{ $costo_envio->costo_cobertura->tiempo_entrega->ecoexpress ?? 'N/A'}} bábiles</small> </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <p>Peso Redondeo: <strong>{{ $costo_envio->peso->facturable ?? 'N/A'}}kg</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Guía a usarse: <strong>{{ $costo_envio->guia_usada ?? 'N/A' }}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Kg Adicionales: <strong>{{ $costo_envio->kg_adicionales ?? 'N/A'}}kg</strong> <span class="badge badge-info" style="display: inline-block">$ {{ $costo_envio->costo_cobertura->kg_cobertura ?? 'N/A'}} por kg</span></p>
                    </div>
                </div>

                <div class="row">
                </div>

            </div>
            <div class="card-footer">

                @if(auth()->id() == 1)
                <div class="text-left">
                    <h5>Desgloce del costo de envío</h5>
                    <table class="table table-sm" align="left" style="">
                        <tr>
                            <td width="150" style="border-top: none;">Importe Guía</td>
                            <td width="110" class="text-right" style="border-top: none;">
                                ${{ $costo_envio->costo_cobertura->importe_cobertura ?? 'N/A' }}
                            </td>
                        </tr>
                        <tr>
                            <td width="150" class="" style="border-top: none;">Importe Kg Adicionales ( {{ $costo_envio->kg_adicionales }} kg x $ {{ $costo_envio->costo_cobertura->kg_cobertura }})</td>
                            <td width="110" class="text-right" style="border-top: none;">
                                ${{ $costo_envio->monto_kg_adicionales ?? 'N/A' }}
                            </td>
                        </tr>
                        <tr>
                            <td width="150" style="border-top: none;">Importe Seguro @if($costo_envio->monto_productos > $costo_envio->limite_seguro) {{ '('.$costo_envio->porcentaje_seguro.'%)' }} @endif</td>
                            <td width="110" class="text-right" style="border-top: none;">
                                ${{ $costo_envio->monto_seguro ?? 'N/A' }}
                            </td>
                        </tr>
                        <tr>
                            <td width="150" style="border-top: none;">Importe por Empaque </td>
                            <td width="110" class="text-right pb-4" style="border-top: none;">
                                ${{ $costo_envio->monto_empaque ?? 'N/A' }}
                            </td>
                        </tr>
                    </table>
                </div>
                @endif

                <div class="row">
                    <div class="col-md-12">

                        <div class="alert alert-secondary pl-0 pr-0 mt-3" style=" margin-bottom: 0 !important;">
                            <table class="table table-sm mb-0" width="100%" style="width: 100% !important; color: inherit !important; background: none;">
                                <tr>
                                    <td width="150" style="border-top: none;">
                                        <span><strong>SUBTOTAL</strong> (sin IVA)</span>
                                    </td>
                                    <td width="50" class="text-right" style="border-top: none;">
                                        $ <strong><span class="bold" id="costoEnvio" style="font-size: 1.1rem;">{{ $costo_envio->monto_envio_subtotal ?? 'N/A' }}</span></strong>
                                    </td>
                                </tr>
                                @if(auth()->id() == 1)
                                <tr style="color:#cccccc !important;">
                                    <td width="150" style="border-top: none;">IVA</td>
                                    <td width="110" class="text-right" style="border-top: none;">
                                        ${{ $costo_envio->monto_envio_iva ?? 'N/A' }}
                                    </td>
                                </tr>
                                <tr style="color:#cccccc !important;">
                                    <td width="150" style="border-top: none;">Total</td>
                                    <td width="110" class="text-right" style="border-top: none;">
                                        ${{ $costo_envio->monto_envio_total ?? 'N/A' }}
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </div>
                        <span class="float-right mt-3">
                            <button class="btn btn-info btn-md" id="btnCopyClipboard" data-clipboard-target="#costoEnvio"><i class="fa fa-copy"></i> Copiar</button>
                        </span>
                    </div>

                </div>

            </div>
        </div><!-- /.card -->

        <div class="form-group">
            <a href="{{ route('redpack.coti-envio') }}" class="btn btn-sm btn-secondary" style="margin-top: 30px;">< Cotizar otro</a>
        </div>

    </section>

@endsection

@section('css')

@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.2/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>
    <script type="text/javascript">
        var token='{{ csrf_token() }}';
        var uri = '{{ url()->full() }}';


        var clipboard = new ClipboardJS('#btnCopyClipboard');

        clipboard.on('success', function(e) {
            var btnCopiar = $('#btnCopyClipboard');
            btnCopiar.removeClass('btn-primary');
            btnCopiar.addClass('btn-success');
            btnCopiar.text('¡Copiado!');
        });
    </script>
@endsection