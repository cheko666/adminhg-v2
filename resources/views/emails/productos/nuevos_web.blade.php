<div class="heading-container">
<div class="alert alert-info">
<h1>{{ $mensaje['title'] }}</h1>
<h2 style="margin-top: 7px">{{ $mensaje['subtitle'] }} {{ $mensaje['texto_count_collection'] }}</h2>
<div style="background-color: #f6f6f6; padding: 10px; margin-bottom: 10px;">
<p style="margin: 0; color:#666666; font-weight: bold; font-size: 0.9em;">Descripción de campos:</p>
<ul style="padding-left: 20px; margin-bottom: 0;">
<li><strong>Tiene Info:</strong> Si es que el producto tiene características, detalles o especificaciones.</li>
<li><strong>Activo Web:</strong> Si es que el producto está visible en la tienda web. <em style="font-style: italic">Si el producto tiene indicación de baja o agotar existencia, no estará activo en la web.</em></li>
<li><strong>Venta Web:</strong> Si es que el producto está o no restringido para venta web. <em style="font-style: italic">Si está restringido, puede ser por estas razones: 1) porque no tiene peso y dimensiones; 2) porque es producto restringido para enviar por empresas de mensajería (estafeta, redpack, etc).</em></li>
<li><strong>URL:</strong> Enlace para ver el producto en la página web.</li>
</ul>
</div>
</div>
<div class="info-cliente">
<table width="600" style="font-family: Helvetica, Arial, sans-serif;">
<thead>
<th width="80px" style="background-color: #dddddd">Imagen</th>
<th style="background-color: #dddddd">Descripción</th>
<th width="50px" style="background-color: #dddddd">Fecha Crea.</th>
<th width="50px" style="background-color: #dddddd">Tiene Info</th>
<th width="50px" style="background-color: #dddddd">Activo Web</th>
<th width="50px" style="background-color: #dddddd">Venta Web</th>
<th width="80px" style="background-color: #dddddd">URL</th>
</thead>
<tbody>
@foreach($mensaje['collection'] as $producto)
<tr>
<td valign="middle" style="vertical-align: middle; text-align:center;">
<img style="width: 100%" src="{{ $producto->url_img }}" alt="Imagen de {{ $producto->descripcion }}">
</td>
<td valign="middle" style="vertical-align: middle; text-align:left;">
<span style="display: block">Código: <strong>{{ $producto->codigo ?? '' }}</strong></span>
<span style="display: block"><strong>{{ $producto->descripcion ?? '' }}</strong></span>
<span style="display: block">Marca: {{ $producto->marca ?? '' }}</span>
</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! date('d/m/y',strtotime($producto->date_add)) ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->tieneInfo_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->activo_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->ventaWeb_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">
@if($producto->activo)
<a href="{{ $producto->url_web ?? '' }}" style="padding: 7px 16px; background-color: #f0ad4e; border-color: #eea236; color: white ; font-size: 11px; text-decoration: none; border-radius: 6px; -webkit-border-radius: 6px; -moz-border-radius: 6px;" target="_blank" data-activo={{ $producto->activo }}>Ver</a>
@else
<a href="#" style="padding: 7px 16px; background-color: #cccccc; border-color: #333333; color: #666 ; font-size: 11px; text-decoration: none; border-radius: 6px; -webkit-border-radius: 6px; -moz-border-radius: 6px;  cursor: not-allowed; filter: alpha(opacity=65); opacity: .65;">Ver</a>
@endif
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>