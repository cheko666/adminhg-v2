<div class="heading-container">
    <div class="cabecera">
        <div class="alert alert-info">
            <h1>{!! $h1 !!}</h1>
        </div>
        <h2>{!! $mensaje['estatus'] !!}</h2>
        <p>{{ $h2 }}</p>
    </div>
    <div class="container">
        {!! $contenido !!}
    </div>
</div>