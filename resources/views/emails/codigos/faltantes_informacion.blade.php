<div class="heading-container">
<div class="alert alert-info">
<h1>{{ $mensaje['title'] }}</h1>
<h2 style="margin-top: 7px">{{ $mensaje['subtitle'] }}</h2>
<h3 style="margin-top: 3px">{{ $mensaje['texto_count_collection'] }}</h3>
<div style="background-color: #f6f6f6; padding: 10px; margin-bottom: 10px;">
<p style="margin: 0; color:#666666; font-weight: bold; font-size: 0.9em;">Descripción de campos:</p>
<ul style="padding-left: 20px; margin-bottom: 0;">
<li style="padding: 5px 0;"><strong>Dimens.:</strong> Si es que el producto tiene todas sus dimensiones y peso. <em style="font-style: italic;">Si le falta algún dato en ancho, alto, fondo o peso, aparecera como incompleto.</em></li>
<li style="padding: 5px 0;"><strong>Fecha Alta:</strong> Fecha en que se dio de alta en sistema Magic.</li>
</ul>
</div>
</div>
<div class="info-cliente">
<table width="600" style="font-family: Helvetica, Arial, sans-serif;">
<thead>
<th width="80px" style="background-color: #dddddd; width: 80px !important;">Imagen</th>
<th style="background-color: #dddddd">Descripción</th>
<th width="50px" style="background-color: #dddddd">Agotar Exis.</th>
<th width="50px" style="background-color: #dddddd">Tiene Info</th>
<th width="50px" style="background-color: #dddddd">Tiene Imagen</th>
<th width="50px" style="background-color: #dddddd">Dimens.</th>
<th width="50px" style="background-color: #dddddd">Fecha Alta</th>
</thead>
<tbody>
@foreach($mensaje['collection'] as $producto)
<tr>
<td valign="middle" width="80px" style="vertical-align: middle; text-align:center; width: 80px !important;">
<img style="width: 100%" src="{{ $producto->url_img_admin }}" alt="Imagen de {{ $producto->descripcion }}">
</td>
<td valign="middle" style="vertical-align: middle; text-align:left;">
<span style="display: block">Código: <strong>{{ $producto->codigo ?? '' }}</strong></span>
<span style="display: block"><strong>{{ $producto->descripcion ?? '' }}</strong></span>
<span style="display: block">Marca: {{ $producto->marca ?? '' }}</span>
</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->por_agotar ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->tieneInfo_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->tieneFoto_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! $producto->tieneDimensiones_html ?? '' !!}</td>
<td valign="middle" style="vertical-align: middle; text-align:center;">{!! getShortToLongFormatTime($producto->fecha_alta,true) !!}</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>