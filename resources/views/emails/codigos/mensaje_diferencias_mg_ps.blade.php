{{--<h3 class="title">{{ config('app.name') }}</h3>--}}
<div class="heading-container">
    <div class="alert alert-info">
        <h1>{{ $h1 }}</h1>
        <h2>{{ $h2 }}</h2>
    </div>
    <div class="info-cliente">
        @foreach($mensaje as $parrafo)
        <p class="mensaje">{{ $parrafo }}</p>
        @endforeach
    </div>
</div>