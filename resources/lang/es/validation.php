<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines contain the default error messages used by
  | the validator class. Some of these rules have multiple versions such
  | as the size rules. Feel free to tweak each of these messages here.
  |
  */

    "accepted"         => ":attribute debe ser aceptado.",
    "active_url"       => ":attribute no es una URL válida.",
    "after"            => ":attribute debe ser una fecha posterior a :date.",
    "alpha"            => ":attribute solo debe contener letras.",
    "alpha_dash"       => ":attribute solo debe contener letras, números y guiones.",
    "alpha_num"        => ":attribute solo debe contener letras y números.",
    "array"            => ":attribute debe ser un conjunto.",
    "before"           => ":attribute debe ser una fecha anterior a :date.",
    "between"          => [
        "numeric" => "El campo :attribute debe tener mínimo :min y máximo :max.",
        "file"    => ":attribute debe pesar entre :min - :max kilobytes.",
        "string"  => ":attribute tiene que tener entre :min - :max caracteres.",
        "array"   => ":attribute tiene que tener entre :min - :max ítems.",
    ],
    "boolean"          => "El campo :attribute debe tener un valor verdadero o falso.",
    "confirmed"        => "La confirmación del :attribute no coincide.",
    "date"             => ":attribute no es una fecha válida.",
    "date_format"      => ":attribute no corresponde al formato :format.",
    "different"        => ":attribute y :other deben ser diferentes.",
    "digits"           => "El campo :attribute debe tener :digits dígitos.",
    "digits_between"   => "El campo :attribute debe tener entre :min y :max dígitos.",
    "email"            => "El campo :attribute no contiene una dirección válida",
    "exists"           => ":attribute es inválido.",
    "image"            => ":attribute debe ser una imagen.",
    "in"               => ":attribute es inválido.",
    "integer"          => "El campo :attribute debe ser un número entero.",
    "ip"               => ":attribute debe ser una dirección IP válida.",
    "max"              => [
        "numeric" => "El campo :attribute no debe ser mayor a :max números.",
        "file"    => "El campo :attribute no debe ser mayor que :max kilobytes.",
        "string"  => "El campo :attribute no debe ser mayor que :max caracteres.",
        "array"   => ":attribute no debe tener más de :max elementos.",
    ],
    "mimes"            => "El :attribute debe ser con uno de los siguientes formatos: :values.",
    "min"              => [
        "numeric" => "El tamaño de :attribute debe ser de al menos :min.",
        "file"    => "El tamaño de :attribute debe ser de al menos :min kilobytes.",
        "string"  => "El campo :attribute debe contener al menos :min caracteres.",
        "array"   => ":attribute debe tener al menos :min elementos.",
    ],
    "not_in"           => ":attribute es inválido.",
    "numeric"          => "El campo :attribute debe ser un número.",
    "regex"            => "El formato de :attribute es inválido.",
    "required"         => "El campo :attribute es obligatorio.",
    "required_if"      => "El campo :attribute es obligatorio cuando :other es :value.",
    "required_with"    => "El campo :attribute es obligatorio cuando :values está presente.",
    "required_with_all" => "El campo :attribute es obligatorio cuando :values está presente.",
    "required_without" => "El campo :attribute es obligatorio cuando :values no está presente.",
    "required_without_all" => "El campo :attribute es obligatorio cuando ninguno de :values estén presentes.",
    "same"             => ":attribute y :other deben coincidir.",
    "size"             => [
        "numeric" => "El tamaño de :attribute debe ser :size.",
        "file"    => "El tamaño de :attribute debe ser :size kilobytes.",
        "string"  => ":attribute debe contener :size caracteres.",
        "array"   => ":attribute debe contener :size elementos.",
    ],
    "timezone"         => "El :attribute debe ser una zona válida.",
    "unique"           => "El campo :attribute ya ha sido registrado.",
    "url"              => "El formato :attribute es inválido.",

  /*
  |--------------------------------------------------------------------------
  | Custom Validation Language Lines
  |--------------------------------------------------------------------------
  |
  | Here you may specify custom validation messages for attributes using the
  | convention "attribute.rule" to name the lines. This makes it quick to
  | specify a specific custom language line for a given attribute rule.
  |
  */

    'custom' => [
        'telefono' => [
            'numeric' => 'El campo :attribute debe tener sólo números (sin espacios, puntos, comas ni guiones)',
        ],
    ],

  /*
  |--------------------------------------------------------------------------
  | Custom Validation Attributes
  |--------------------------------------------------------------------------
  |
  | The following language lines are used to swap attribute place-holders
  | with something more reader friendly such as E-Mail Address instead
  | of "email". This simply helps us make messages a little cleaner.
  |
  */

    'attributes' => [
        'full_name'             =>  'Nombre Completo',
        'password_confirmation' =>  'Confirmación de Password',
        'n_interior'            =>  'Número interior',
        'n_exterior'            =>  'Número Exterior',
        'cp'                    =>  'Código Postal',
        'file'                  =>  'Archivo',
        'telefono'              =>  'Teléfono',
        'numero'                =>  'Número',
        'codigo'                =>  'Código',
        'guia'                =>  'No. de Guía',
        'no_cd' =>  'No. de Cliente Distinguido',
        'ine' => 'INE',
        'comprobante' => 'Comprobante de Domicilio',
        'nombre' => 'Nombre',
        'terminos' => 'Términos y Condiciones',
        'privacidad' => 'Aviso de Privacidad',
        'municipio' => 'Municipio',
        'email' => 'Email',
        'curp' => 'CURP',
        'codigo_postal' => 'Código Postal',
        'colonia' => 'Colonia',
        'direccion' => 'Dirección',

    ]

];
