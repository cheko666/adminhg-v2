<?php
return [
  'productsManagement'  =>  [
      'title'          => 'Administración de Usuarios',
      'title_singular' => 'Administración de Usuario',
  ],
  'productos'     => [
      'title'          => 'Permisos',
      'title_singular' => 'Permiso',
      'fields'         => [
          'id'                => 'ID',
          'id_helper'         => '',
          'title'             => 'Título',
          'title_helper'      => '',
          'created_at'        => 'Creado el',
          'created_at_helper' => '',
          'updated_at'        => 'Modificado el',
          'updated_at_helper' => '',
          'deleted_at'        => 'Borrado el',
          'deleted_at_helper' => '',
      ],

  ],
];