<?php

return [
    'catalog'  =>  [
        'title'          => 'Catálogo',
    ],
    'productos'  =>  [
        'title'          => 'Productos',
        'title_singular' => 'Producto',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Nombre',
        ]
    ],
    'productos_magic'  =>  [
        'title'          => 'Productos Magic',
        'title_singular' => 'Producto Magic',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Nombre',
        ]
    ],
    'marcas'  =>  [
        'title'          => 'Marcas',
        'title_singular' => 'Marca',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Nombre',
        ]
    ],
    'categorias'  =>  [
        'title'          => 'Categorías',
        'title_singular' => 'Categoría',
    ],
    'userManagement' => [
        'title'          => 'Administración',
        'title_singular' => 'Administración',
    ],
    'permission'     => [
        'title'          => 'Permisos',
        'title_singular' => 'Permiso',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Título',
            'title_helper'      => '',
            'created_at'        => 'Creado el',
            'created_at_helper' => '',
            'updated_at'        => 'Modificado el',
            'updated_at_helper' => '',
            'deleted_at'        => 'Borrado el',
            'deleted_at_helper' => '',
        ],
    ],
    'role'           => [
        'title'          => 'Roles',
        'title_singular' => 'Rol',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'title'              => 'Título',
            'title_helper'       => '',
            'permissions'        => 'Permisos',
            'permissions_helper' => '',
            'created_at'         => 'Creado el',
            'created_at_helper'  => '',
            'updated_at'         => 'Modificado el',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Borrado el',
            'deleted_at_helper'  => '',
        ],
    ],
    'user'           => [
        'title'          => 'Usuarios',
        'title_singular' => 'Usuario',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Nombre',
            'name_helper'              => '',
            'email'                    => 'Email',
            'email_helper'             => '',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => '',
            'password'                 => 'Password',
            'password_helper'          => '',
            'roles'                    => 'Roles',
            'roles_helper'             => '',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => '',
            'created_at'               => 'Creado el',
            'created_at_helper'        => '',
            'updated_at'               => 'Modificado el',
            'updated_at_helper'        => '',
            'deleted_at'               => 'Borrado el',
            'deleted_at_helper'        => '',
        ],
    ],
];
