<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EspecialidadMedicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql_path = base_path('database/seeders/sql/dump-especialidades_medicas.sql');
        
        DB::unprepared(
            file_get_contents($sql_path)
        );
    }
}
