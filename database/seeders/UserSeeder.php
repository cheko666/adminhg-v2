<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Sergio Ruiz',
            'email' => 'cheko666@icloud.com',
            'password' => bcrypt('cheko666'),
        ])->assignRole('superadmin');


        $user2 = User::create([
            'name' => 'Agente de Ventas',
            'email' => 'demo@lancetahg.com',
            'password' => bcrypt('demo'),
        ])->assignRole('agente_ventas');

        $user3 = User::create([
            'name' => 'Raul Reyes',
            'email' => 'sergioruiz@lancetahg.com',
            'password' => bcrypt('lanz:hg'),
        ])->assignRole('jefe_sucursal');

        $user4 = User::create([
            'name' => 'Laura Parra',
            'email' => 'clientedistinguido@lancetahg.com',
            'password' => bcrypt('lanz:2022'),
        ])->assignRole('admin_clientes_distinguidos');


    }
}
