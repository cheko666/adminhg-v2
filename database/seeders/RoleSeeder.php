<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'superadmin']);
        $role->givePermissionTo(['manage users','read redpack','manage clientes_distinguidos']);

        $role = Role::create(['name' => 'agente_ventas']);
        $role->givePermissionTo([
            'read redpack'
        ]);

        $role = Role::create(['name' => 'jefe_sucursal']);
        $role->givePermissionTo([
            'manage pasillo_tienda'
        ]);

        $role = Role::create(['name' => 'admin_clientes_distinguidos']);
        $role->givePermissionTo([
            'manage clientes_distinguidos'
        ]);
    }
}
