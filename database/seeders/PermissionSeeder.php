<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Permission::create(['name' => 'manage users']);

        Permission::create(['name' => 'read redpack']);

        Permission::create([
            'name' => 'manage pasillo_tienda',
        ]);

        Permission::create([
            'name' => 'manage clientes_distinguidos'
        ]);

    }
}
