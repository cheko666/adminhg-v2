-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (arm64)
--
-- Host: 127.0.0.1    Database: adminhg2
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `especialidades_medicas`
--

DROP TABLE IF EXISTS `especialidades_medicas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `especialidades_medicas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` char(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidades_medicas`
--

LOCK TABLES `especialidades_medicas` WRITE;
/*!40000 ALTER TABLE `especialidades_medicas` DISABLE KEYS */;
INSERT INTO `especialidades_medicas` VALUES (1,'Alergologia'),(2,'Anestesiología'),(3,'Angiología'),(4,'Cardiología'),(5,'Cirugía Cardiovascular'),(6,'Cirugía del Aparato Digestivo'),(7,'Cirugía General'),(8,'Cirugía Oral y Maxilofacial'),(9,'Cirugía Ortopédica y Traumatología'),(10,'Cirugía Pediátrica'),(11,'Cirugía Plástica Estética'),(12,'Cirugía Torácica'),(13,'Endocrinología'),(14,'Gastroenterología'),(15,'Genética'),(16,'Geriatría'),(17,'Ginecología'),(18,'Hematología'),(19,'Hepatología'),(20,'Infectología'),(21,'Nefrología'),(22,'Neonatología'),(23,'Neumología'),(24,'Neurocirugía'),(25,'Neurología'),(26,'Nutriología'),(27,'Obstetricia'),(28,'Odontología'),(29,'Oftalmología'),(30,'Oncología Médica'),(31,'Ortopedia'),(32,'Otorrinolaringología'),(33,'Pediatría'),(34,'Proctología'),(35,'Psiquiatría'),(36,'Radiología'),(37,'Reumatología'),(38,'Toxicología'),(39,'Traumatología'),(40,'Urología');
/*!40000 ALTER TABLE `especialidades_medicas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-06 19:08:47
