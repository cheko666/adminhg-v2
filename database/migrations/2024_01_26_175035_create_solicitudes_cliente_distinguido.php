<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesClienteDistinguido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes_cliente_distinguido', function (Blueprint $table) {
            $table->id();
            
            $table->string('reference',8);
            $table->string('titulo',24);
            $table->unsignedSmallInteger('id_especialidad_medica')->nullable();
            $table->string('curp',18)->unique('curp');
            $table->string('nombre',255);
            $table->date('fecha_nacimiento');
            $table->string('telefono',10);
            $table->string('email',128)->unique('correo');
            $table->string('no_cd',12)->nullable();
            $table->timestamp('fecha_registro')->nullable();
            $table->timestamp('fecha_notificacion')->nullable();
            $table->string('direccion',255);
            $table->string('codigo_postal',5);
            $table->unsignedSmallInteger('id_state')->nullable();
            $table->string('iso_country',4)->default('MX');
            $table->string('municipio',64);
            $table->string('colonia',64);
            $table->boolean('privacidad');
            $table->boolean('terminos');
            $table->string('ine',255);
            $table->string('comprobante',255);
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_cliente_distinguido');
    }
}
