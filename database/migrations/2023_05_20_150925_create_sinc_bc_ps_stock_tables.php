<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSincBcPsStockTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_stock', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',6);
            $table->char('almacen',2)->nullable(false);
            $table->mediumInteger('cantidad')->default('0');
            $table->timestamps();
            $table->unique(['codigo','almacen',],'cod_al');
        });

        Schema::create('bc_stock_mov', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',6);
            $table->char('tipo');
            $table->char('movimientos',16);
            $table->dateTime('created_at', $precision = 0);
        });

        DB::unprepared('
        CREATE TRIGGER aftins_stock_mov AFTER INSERT ON `bc_stock` FOR EACH ROW
        BEGIN
            INSERT INTO bc_stock_mov (`codigo`, `tipo`, `movimientos`, `created_at`)
            VALUES (
                NEW.codigo
                , \'insert\'
                , CONCAT_WS(\'|\',\'0\',NEW.`cantidad`,NEW.`cantidad`)
                , now()
            );
        END
        ');

        DB::unprepared('
        CREATE TRIGGER aftupd_stock_mov AFTER UPDATE ON `bc_stock` FOR EACH ROW
        BEGIN
            INSERT INTO bc_stock_mov (`codigo`, `tipo`, `movimientos`, `created_at`)
            VALUES (
                NEW.codigo
                , \'update\'
                , CONCAT_WS(\'|\',OLD.`cantidad`,(NEW.`cantidad`)-(OLD.`cantidad`),NEW.`cantidad`)
                , now()
            );
        END
        ');

        Schema::create('bc_log_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_objeto')->default(0);
            $table->char('endpoint',36);
            $table->char('method',6);
            $table->text('response');
            $table->text('request');
            $table->unsignedMediumInteger('status_code');
            $table->timestamp('created_at')->useCurrent();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER aftins_stock_mov');
        DB::unprepared('DROP TRIGGER aftupd_stock_mov');
        Schema::dropIfExists('bc_stock');
        Schema::dropIfExists('bc_stock_mov');
        Schema::dropIfExists('bc_log_requests');
    }
}
