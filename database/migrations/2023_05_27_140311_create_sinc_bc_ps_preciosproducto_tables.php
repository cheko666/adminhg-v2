<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSincBcPsPreciosproductoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bc_precio_producto', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',6);
            $table->float('unit_price',10,2)->default('0');
            $table->timestamps();
            $table->unique(['codigo','unit_price',],'cod_precio');
        });

        Schema::create('bc_precio_producto_mov', function (Blueprint $table) {
            $table->id();
            $table->char('codigo',6);
            $table->char('movimientos',21);
            $table->dateTime('created_at', $precision = 0);
        });

        DB::unprepared('
        CREATE TRIGGER aftins_precio_producto_mov AFTER INSERT ON `bc_precio_producto` FOR EACH ROW
        BEGIN
            INSERT INTO bc_precio_producto_mov (`codigo`, `movimientos`, `created_at`)
            VALUES (
                NEW.codigo
                , CONCAT_WS(\'|\',\'0\',NEW.`unit_price`)
                , now()
            );
        END
        ');

        DB::unprepared('
        CREATE TRIGGER aftupd_precio_producto_mov AFTER UPDATE ON `bc_precio_producto` FOR EACH ROW
        BEGIN
            INSERT INTO bc_precio_producto_mov (`codigo`, `movimientos`, `created_at`)
            VALUES (
                NEW.codigo
                , CONCAT_WS(\'|\',OLD.`unit_price`,NEW.`unit_price`)
                , now()
            );
        END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER aftins_precio_producto_mov');
        DB::unprepared('DROP TRIGGER aftupd_precio_producto_mov');
        Schema::dropIfExists('bc_precio_producto');
        Schema::dropIfExists('bc_precio_producto_mov');
    }
}
