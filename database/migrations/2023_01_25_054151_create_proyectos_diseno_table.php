<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectosDisenoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos_diseno', function (Blueprint $table) {
            $table->id();
            $table->string('referencia',12);
            $table->string('nombre',96);
            $table->string('solicita',128);
            $table->dateTime('fecha_solicitud')->nullable();
            $table->dateTime('fecha_ini')->nullable();
            $table->dateTime('fecha_fin')->nullable();
            $table->string('ultimo_evento',255);
            $table->string('fase_proceso',false,true)->nullable();
            $table->mediumInteger('porcentaje',false,true)->nullable();
            $table->mediumInteger('estatus',false,true)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos_diseno');
    }
}
