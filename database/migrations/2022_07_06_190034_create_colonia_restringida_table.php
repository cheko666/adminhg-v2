<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColoniaRestringidaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colonia_restringida', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_cp_sepomex');
            $table->unsignedInteger('id_cp_sat');
            $table->char('codigopostal',5);
            $table->text('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colonia_restringida');
    }
}
