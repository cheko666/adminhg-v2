<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->id();
            $table->char('siglas',2);
            $table->char('nombre',64);
            $table->char('imagenes',255);
            $table->char('direccion',128);
            $table->char('referencias',128);
            $table->char('metodo_tarjeta',128);
            $table->char('telefonos',128);
            $table->char('lada',3);
            $table->unsignedInteger('estado_id');
            $table->char('horarios',128);
            $table->boolean('activo');
            $table->decimal('latitud',13,8);
            $table->decimal('longitud',13,8);
            $table->smallInteger('cant_pasillos')->nullable();
            $table->smallInteger('cant_cabeceras')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sucursales');
    }
}
