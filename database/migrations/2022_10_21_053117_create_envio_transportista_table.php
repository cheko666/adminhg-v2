<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnvioTransportistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envio_transportista', function (Blueprint $table) {
            $table->id();
            $table->integer('id_transportista');
            $table->char('cp',5);
            $table->decimal('peso',6,3);
            $table->decimal('peso_volumetrico',6,3);
            $table->enum('tipo_envio',['domicilio','ocurre']);
            $table->decimal('costo_envio',8,3);
            $table->decimal('costo_seguro',8,3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envio_transportista');
    }
}
