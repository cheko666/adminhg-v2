<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model {

    protected $table = 'sucursales';

    public $timestamps = false;

    public function producto()
    {
        return $this->belongsTo('App\Producto','producto_id');
    }

    public function usuarios()
    {
        return $this->hasMany('App\User');
    }

    public function solicitudesServicio()
    {
        return $this->hasMany('App\SoporteServicio');
    }

}
