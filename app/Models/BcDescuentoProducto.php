<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcDescuentoProducto extends Model
{
    use HasFactory;

    protected $connection = 'production_presta';
    protected $table = 'lsc_descuento_producto';


}
