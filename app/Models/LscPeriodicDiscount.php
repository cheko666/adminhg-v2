<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LscPeriodicDiscount extends Model
{
    use HasFactory;
    const UPDATED_AT = 'modified_at';
    protected $connection = 'production_presta';
    protected $table = 'lsc_periodic_discount';
    protected $primaryKey = 'no';
    protected $guarded = [];
}
