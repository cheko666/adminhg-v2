<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnvioTransportista extends Model
{
    use HasFactory;

    protected $table = 'envio_transportista';
}
