<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventoProyecto extends Model
{
    use HasFactory;

    protected $fillable = ['id_proyecto_diseno','descripcion'];
}
