<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcPrecioProducto extends Model
{
    use HasFactory;

    protected $table = 'bc_precio_producto';
    protected $fillable = ['codigo','unit_price'];
}
