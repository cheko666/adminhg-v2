<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BcMarca extends Model
{
    use HasFactory;

    protected $connection = 'production_presta';
    protected $table = 'lsc_marca';


    public function producto()
    {
        return $this->belongsTo('App\Models\BcProducto','special_group_code','code');
    }


    public function getNombreAttribute()
    {
        return $this->Description;
    }

}
