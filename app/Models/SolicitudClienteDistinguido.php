<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SolicitudClienteDistinguido extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'solicitudes_cliente_distinguido';
    protected $guarded = ['estado'];
    public function scopeRegistradosSinNotificar($query)
    {
        // return $query->whereNotNull('no_cd')->whereNull('fecha_notificacion')->where('fecha_registro','<',Carbon::now()->subDay());
        return $query->whereNotNull('no_cd')->whereNull('fecha_notificacion')->where('fecha_registro','<',Carbon::today()->startOfDay());
    }
    public function getFechaRegistroAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('H:i:s');
    }
    public function getTituloFormattedAttribute()
    {
        $titulo = Str::ucfirst($this->titulo);
        if($this->titulo == 'dr' || $this->titulo == 'dra') {
            return $titulo . '.';
        }
        return $titulo;
    }
    public function getTituloNombreFormattedAttribute()
    {
        if($this->titulo == 'dr' || $this->titulo == 'dra') {
            return $this->titulo_formatted . ' ' . $this->nombre;
        }
        return $this->nombre;
    }
    public function getEspecialidadFormattedAttribute()
    {
        $especialidad = EspecialidadMedica::find($this->id_especialidad_medica);
        $especialidad = $especialidad ? $especialidad->nombre : null;
        
        return Str::ucfirst($especialidad);
    }
    public function getEstadoFormattedAttribute()
    {
        $estado = DB::connection('production_presta')->table('state')->select('name')->where('id_country',145)->where('id_state',$this->id_state)->first();        
        
        return Str::upper($estado->name);
    }
    public function getFechaNacimientoFormattedAttribute()
    {
        return Carbon::parse($this->fecha_nacimiento)->format('d-M-Y');
    }
    public function getEstatusTextAttribute()
    {

        if(!$this->no_cd)
        {
            $estatus = 'Pendiente';

        } elseif ($this->no_cd && !(bool)$this->fecha_notificacion)
        {
            $estatus = 'Registrado';

        } elseif ($this->no_cd && (bool)$this->fecha_notificacion)
        {
            $estatus = 'Terminado';
        }
        return $estatus;
    }
    public function setCurpAttribute($value)
    {
        $this->attributes['curp'] = Str::upper($value);
    }
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = Str::upper($value);
    }
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = Str::upper($value);
    }
    public function setDireccionAttribute($value)
    {
        $this->attributes['direccion'] = Str::upper($value);
    }
    public function setMunicipioAttribute($value)
    {
        $this->attributes['municipio'] = Str::upper($value);
    }
    public function setColoniaAttribute($value)
    {
        $this->attributes['colonia'] = Str::upper($value);
    }
}
