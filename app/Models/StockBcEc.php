<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockBcEc extends Model
{
    use HasFactory;

    protected $table = 'stock_bc_ec';
    protected $fillable = ['id','codigo','sa'];
    public $timestamps = false;
}
