<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PsProductAttribute extends Model
{
    use HasFactory;

    protected $connection = 'production_presta';
    protected $table = 'product_attribute';
    protected $primary_key = 'id_product_attribute';

    public function psproduct()
    {
        return $this->belongsTo('App\Models\PsProduct','id_product','id_product');
    }
}
