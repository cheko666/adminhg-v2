<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Codigo extends Model
{
  protected $table = 'codigos';

  protected $connection = 'lanceta_bd_ec';

  protected $database;

//  protected $with = ['producto'];

  public function __construct()
  {
    $this->database = $this->getPsTableFromEnv();

  }

  public function logs()
  {
    return $this->hasMany('App\Models\LogCodigo')->orderBy('created_at','desc');
  }

  public function producto()
  {
    return $this->belongsTo('App\Models\Producto');
  }


  public function logsAvanzado($pendiente = true)
  {
    return $this->hasMany('App\Models\LogCodigo')
        ->logsPendientes()
        ->orderBy('created_at','desc');
  }

  public function scopeLogsPendientes($query, $pendientes = true)
  {
    if(!$pendientes) return $query;
  }

  public function getIdAttribute($value)
  {
    return str_pad($value,6,0,STR_PAD_LEFT);
  }

  public function getCodigoAttribute()
  {
    return str_pad($this->id,6,0,STR_PAD_LEFT);
  }

  public function getSerieAttribute()
  {
    return floor($this->id / 1000);
  }
  public function marca()
  {
    return $this->belongsTo('App\Models\Marca');
  }

  public function scopeSoloProductos($query)
  {
    return $query->where('id','>','001002');
  }

  public function scopeConProducto($query)
  {
    return $query->whereNotNull('producto_id')->where('producto_id','>',0);
  }

  public function scopeAplicarFiltros($query,$filtros)
  {
    if(!$filtros) return $query;

    $query->where(function($query) use ($filtros)
    {

      if($filtros['todo'])
      {
        unset($filtros['todo']);

      }
      foreach($filtros as $key => $value) {
        if($value == 1) {
          $text = $key.' <> ps_'.$key;
          $query->orWhereRaw($text);
        }
      }
    });

    return $query;

  }

  public function scopeJoinWithPsTables($query)
  {
    return $query->leftJoin(DB::raw("
      (
        select
        ifnull(pa.reference,p2.reference) as ps_reference,
        p2.id_product as id_product,
        LPAD(p2.id_manufacturer,3,0) ps_marca_id,
        round(p2.price,2) as price,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
        round(ifnull(pa.price,0),2) pa_price,
        round(ifnull(pas.price,0),2) pas_price,
        round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
        round( p2.width, 3) as ps_ancho,
        round( p2.height, 3) as ps_altura,
        round( p2.depth, 3) as ps_fondo,
        round(ifnull(sp.descuento,0)) as ps_descuento,
        #ifnull(sp.id_specific_price,0) id_specific_price,
        #round(ifnull(sp.reduction,0),2) reduction,
        round( ifnull(pa.price,0) + p2.price,2 ) as precio_base_ps,
        round( ifnull(pas.price,0) + p2.price,2 ) as ps_precio,
        round( (ifnull(pa.price,0) + p2.price) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
        if(p2.id_tax_rules_group = 1,16,0) as ps_iva
        from " . $this->database . ".ps0516_product p2
        left join (
          select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference
          from " . $this->database . ".ps0516_product_attribute
        ) pa on pa.id_product = p2.id_product
        left join " . $this->database . ".ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
        left join (
          SELECT
          *,
          ps0516_specific_price.reduction*100 as descuento
          FROM " . $this->database . ".ps0516_specific_price
          where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
        ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
        where p2.active = 1
      ) as ps
      "), 'codigos.id', '=', 'ps.ps_reference')
        ;
  }
  public function codigosJoinWithPsTables($params)
  {
    $query = DB::table(DB::raw('codigos as c'))
        ->leftJoin(DB::raw("
      (
        select
        ifnull(pa.reference,p2.reference) as ps_reference,
        p2.id_product as id_product,
        LPAD(p2.id_manufacturer,3,0) ps_marca_id,
        round(p2.price,2) as price,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
        round(ifnull(pa.price,0),2) pa_price,
        round(ifnull(pas.price,0),2) pas_price,
        round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
        round( p2.width, 3) as ps_ancho,
        round( p2.height, 3) as ps_altura,
        round( p2.depth, 3) as ps_fondo,
        round(ifnull(sp.descuento,0)) as ps_descuento,
        #ifnull(sp.id_specific_price,0) id_specific_price,
        #round(ifnull(sp.reduction,0),2) reduction,
        round( ifnull(pa.price,0) + p2.price,2 ) as precio_base_ps,
        round( ifnull(pas.price,0) + p2.price,2 ) as ps_precio,
        round( (ifnull(pa.price,0) + p2.price) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
        if(p2.id_tax_rules_group = 1,16,0) as ps_iva
        from staging_presta.ps0516_product p2
        left join (
          select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference
          from staging_presta.ps0516_product_attribute
        ) pa on pa.id_product = p2.id_product
        left join staging_presta.ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
        left join (
          SELECT
          *,
          ps0516_specific_price.reduction*100 as descuento
          FROM staging_presta.ps0516_specific_price
          where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
        ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
        where p2.active = 1
      ) as ps
      "), 'c.id', '=', 'ps.ps_reference')
        ->where('c.id','>',10002)
    ;
    return  $this->scopeAplicarFiltros($query,$params);
  }

  public function getPsTableFromEnv()
  {
    if(env('APP_ENV') == 'production') {
      return 'production_presta';
    }else{
      return 'staging_presta';
    }
  }
  public function listDiferenciasAdminhgPs() {

    $data = $this->_query = DB::select(DB::raw($this->queryVerificarDiferenciasAdminhgPs()));

    $log = array();

    if (count($data) > 0) {

      // Obtenemos campos con diferencias
      foreach ($data as &$registro) {
        $campos_diferencias = array();
        $diferencias = array();

        $registro_log['id_product'] = $registro->id_product;
        $registro_log['id_product_attribute'] = $registro->id_product_attribute;

        // Checar diferencias
        foreach ($this->campos_compara as $key => $value) {
          if ($key == 'todo') continue;
          $campo_ps = 'ps_' . $key;
          if ($registro->$key != $registro->$campo_ps) {
            $campos_diferencias[] = $key;
            $diferencias[$key][] = $registro->$key;
            $diferencias[$key][] = $registro->$campo_ps;
          }
        }
        $registro_log['diferencias'] = json_encode($diferencias);


        $registro->campos_dif = implode(',', $campos_diferencias);
        $registro->diferencias = $diferencias;

        $log[$registro->codigo] = $registro_log;

        collect($registro);
      }

    }
    return $collection = collect($data);
  }

  public function corregirDiferenciasAdminhgPs() {

    return DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoSimple())) && DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoCombinaciones()))
        ? true : false;
  }

  public function queryCorregirDiferenciasAdminhgPsProductoSimple()
  {
    return "
      update production_presta.ps0516_product A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        round(p.price,2) as p_price,
        round(ifnull(pa.price,0),2) as pa_price,

        c.iva,

        c.peso peso_mg,
        round(ifnull(pa.weight,0) + p.weight,3) as weight_ps,
        round(p.weight,3) as p_weight,
        round(ifnull(pa.weight,0),3) as pa_weight,

        c.ancho ancho_mg,
        round(p.width,3) width,

        c.altura altura_mg,
        round(p.height,3) height,

        c.fondo fondo_mg,
        round(p.depth,3) depth,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        c.marca_id,
        p.id_manufacturer,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null
        #left join lanceta_bd_ec.codigos c2 on c2.id = concat(LPAD(c.Serie,3,0),LPAD(c.Producto,3,0))

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
          or c.ancho <> round(p.width,3)
          or c.altura <> round(p.height,3)
          or c.fondo <> round(p.depth,3)
          or c.marca_id <> p.id_manufacturer
          or IF(c.iva = 'S',16,0) <> if(p.id_tax_rules_group = 1,16,0)
        )
        #and pa.id_product_attribute is null
      ) as B on A.id_product = B.id_product

      set
      A.id_tax_rules_group = IF(B.iva = 'S',1,2),
      A.price = B.precio_mg,
      A.weight = B.peso_mg,
      A.width = B.ancho_mg,
      A.height = B.altura_mg,
      A.depth = B.fondo_mg,
      A.supplier_reference = B.modelo,
      A.id_manufacturer = B.marca_id
      ;
    ";
  }

  public function queryCorregirDiferenciasAdminhgPsProductoCombinaciones()
  {
    return "
      update production_presta.ps0516_product_attribute A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        p.price as p_price,
        ifnull(pa.price,0) as pa_price,
        c.precio - p.price as pa_price_ok,

        @pweight := p.weight as p_weight,
        ifnull(pa.weight,0) as pa_weight,
        c.peso peso_mg,
        pa.ps_weight as weight_ps,
        c.peso - p.weight as pa_weight_ok,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN (
			    SELECT *,round( (@pweight+weight),3 ) ps_weight
          FROM production_presta.ps0516_product_attribute
        ) pa on p.id_product = pa.id_product
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
        )
        and pa.id_product_attribute is not null
      ) as B on A.id_product_attribute = B.id_product_attribute
      #inner join production_presta.ps0516_product_attribute_shop C on C.id_product_attribute = A.id_product_attribute

      set
      A.price = B.pa_price_ok,
      A.weight = B.pa_weight_ok,
      A.supplier_reference = B.modelo
      ;
    ";
  }

  public function deletePsSpecificPriceById($id) {

    return DB::delete('DELETE FROM production_presta.ps0516_specific_price WHERE id_specific_price = ?',[$id]);

  }

  public function createPsSpecificFromAdminhg($descuento,$id_product,$id_product_attribute) {
    return DB::insert('
      INSERT INTO production_presta.ps0516_specific_price (id_specific_price,id_specific_price_rule,id_cart,id_product,id_shop,id_shop_group,id_currency,id_country,id_group,id_customer,id_product_attribute,price,from_quantity,reduction,reduction_tax,reduction_type,`from`,`to`)
      VALUES (null,0,0,?,0,0,0,0,0,0,?,-1,1,?,1,"percentage","0000-00-00 00:00:00","0000-00-00 00:00:00")
      ',[$id_product,$id_product_attribute,$descuento/100]);
  }

  public function updatePsSpecificPriceById($id,$descuento) {

    return DB::update('UPDATE production_presta.ps0516_specific_price SET reduction = ? WHERE id_specific_price =?',[$descuento/100,$id]);
  }

  public function corregirPrecioAdminhgPs() {

    return DB::statement(DB::raw($this->queryCorregirPrecioAdminhgPsProductoSimple())) && DB::statement(DB::raw($this->queryCorregirPrecioAdminhgPsProductoCombinaciones()))
        ? true : false;
  }

  public function queryCorregirPrecioAdminhgPsProductoSimple()
  {
    return "
      UPDATE " . $this->database . ".ps0516_product p2
      left join (
        select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference
        from " . $this->database . ".ps0516_product_attribute
      ) pa on pa.id_product = p2.id_product
    ";
  }
}
