<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BcLog extends Model
{
    use HasFactory;

    const MOV_CREATE = 1;
    const MOV_UPDATE = 2;
    const MOV_DELETE = 3;
    const MOV_READ = 4;

    protected $connection = 'production_presta';
    protected $table = 'lsc_log';

    protected $fillable = ['id_objecto','status','objeto','cambios','movimiento','created_at','deleted_at'];

    public function codigo()
    {
        return $this->belongsTo('App\Models\BcProducto','id','id')->where('id','>','001003');
    }

    public function bcproducto()
    {
        return $this->belongsTo('App\Models\BcProducto','id','id')->where('id','>','1003');
    }

    public function psproduct()
    {
        return $this->belongsTo('App\PSProduct','id_p','id_product')
          ->join('product_lang',function ($join) {
              $join->on('product_lang.id_product','=','product.id_product')
                ->where('product_lang.id_lang','=','2');
          })
          ;
    }

    public function getIdObjetoAttribute()
    {
      return $this->id_objecto;
    }

    public function scopeTodo($query)
    {
        return $query
          ->tipoCambio()
          ->pendientes()
          ->orderBy('id')->orderBy('created_at','desc');
    }

    public function scopeLogTipo($query,$entidad = null,$movimiento = null,$cambio = null,$tiempo = '1-d')
    {
        $fecha = Carbon::now();

        list($cantidad,$periodo)=explode('-',$tiempo);

        if($periodo && $cantidad != '')
        {

            switch($periodo)
            {
                case 'm':
                    $fecha_pivote = $cantidad == '0' || $cantidad == '1' ? $fecha->startOfMonth() : $fecha->subMonth($cantidad)->startOfDay();
                    break;
                case 'd':
                    $fecha_pivote = $cantidad == '0' || $cantidad == '1' ? $fecha->subDays(2)->startOfDay() : $fecha->subDays($cantidad)->startOfDay();
                    break;
                case 'h':
                    $fecha_pivote = $fecha->subHours($cantidad);
                    break;
                case 'i':
                    $fecha_pivote = $fecha->subMinutes($cantidad);
                    break;
                default:
                    $fecha_pivote = $fecha->yesterday()->startOfDay();
            }

        } else {

            $fecha_pivote = $fecha->yesterday()->startOfDay();
        }

        return $query
//            ->pendientes()
          ->where('objeto','LIKE',$entidad.'%')
          ->where('movimiento',(int)$movimiento)
          ->where('created_at','>',$fecha_pivote->toDateTimeString())
          ->orderBy('id')->orderBy('created_at','desc');
    }

    public function scopePendientes($query)
    {
        return $query->where(function($query) {
            $query->orWhere('status','');
            $query->orWhere('status','=',null);
        });
    }

    public function scopeTipoCambio($query,$movimiento,$cambio=null)
    {
        if($movimiento=='UPDATE')
        {
            return $query->where(function ($query)
            {
                $query->where('cambios','like','%description%')
                  ->orWhere('cambios','like','%discount%')
                  ->orWhere('cambios','like','%price%')
                  ->orWhere('cambios','like','%supplier_reference%')
                  ->orWhere('cambios','like','%unity%')
                  ->orWhere('cambios','like','%id_manufacturer%')
                  ->orWhere('cambios','!=','');
            });
        }

        return $query;
    }

    public function getCambiosArrayAttribute()
    {
        return (array)json_decode($this->cambios);
    }

    public function getDiferenciasCambiosAttribute()
    {
        $cambios_array = $this->cambios_array;
        $html = '';


        foreach($cambios_array as $k => $v)
        {
            list($v_antes,$v_despues) = explode('|',$v);
            $html .='<span style="display: block;">'.$k.': <span class="badge alert-info">'.$v_antes.'</span> > <span class="badge alert-danger">'.$v_despues.'</span></span>';
        }

        return $html;

        return array_filter(array_map(function($valores,$campo) {

            $diferencias = array();
            list($v_antes,$v_despues) = explode('|',$valores);

            if($v_antes != $v_despues)
            {
                return '<span style="display: block;">'.$campo.': <span class="badge alert-info">'.$v_antes.'</span> > <span class="badge alert-danger">'.$v_despues.'</span></span>';
            }

        },$cambios_array,array_flip($cambios_array)),'strlen');

    }

  public function getFormatCambiosHtmlAttribute()
  {
      $cambios = $this->cambiosArray;
      $cambios_array = array();
      $i = 0;

      foreach($cambios as $k => $v)
      {
        list($v_ini,$v_fin) = explode('|', $v);


        $cambio = array();
        $cambios_array[$i]['id'] = $this->id;
        $cambios_array[$i]['id_objeto'] = $this->id_objecto;
        $cambios_array[$i]['campo'] = $k;
        $cambios_array[$i]['ini'] = trim($v_ini);
        $cambios_array[$i]['fin'] = trim($v_fin);
        $cambios_array[$i]['fecha'] = $this->created_at->toDateTimeString();

        $i++;
      }

      dd($cambios_array);

      return $cambios_array;

 return '';

    foreach($cambios as $k => $v)
    {

      dd($v);

    }
    return $this->cambios;
  }

  public static function insertarEnLog($movimiento = self::MOV_UPDATE,$datos_old = null,$datos_new = null,$tabla)
  {

    $id_objeto = $movimiento == 1 ? $datos_new['id'] : $datos_old['id'];
    $cambios = '';
    $registrar = true;

    switch($movimiento)
    {
      case 1:
        $cambios = json_encode($datos_new);
        break;
      case 2:
        $registrar = self::getDiferenciasCamposActualizacion($datos_old,$datos_new,$tabla);
        $cambios = $registrar ? json_encode(self::getDiferenciasCamposActualizacion($datos_old,$datos_new,$tabla)) : '';
        break;
      case 3:
        $cambios = json_encode($datos_old);
        break;
    }

    return;
  }
  /**
   * @param $datos_old
   * @param $datos_new
   * @param $tabla
   * @return array
   */
  public function getDiferenciasCamposActualizacion($datos_old,$datos_new,$tabla)
  {

    $diferencias = array();
    $quitar_campos = array();

    switch($tabla)
    {
      case 'producto':
        $quitar_campos = array('id', 'id_supplier','reference','state','active','date_add','date_upd','id_p','id_pa','id_sp','id_sa','serie','producto');
        break;
      case 'descuento_producto':
        $quitar_campos = array('id','id_shop_group','id_specific_price_rule','id_cart','id_shop','id_shop_group','id_currency','id_country','id_group','id_customer','from','to');
        break;
    }


    foreach($quitar_campos as $key) {
      unset($datos_old[$key]);
    }

    // Comparacion de datos viejos vs nuevos
    foreach($datos_old as $k => $v)
    {
      if($tabla == 'descuento_producto')
      {
        $diferencias['id'] = $datos_new['id_product'].'|'.$datos_new['id_product'];
      }
      if($v != $datos_new[$k])
      {
        $diferencias[$k] = $v.'|'.$datos_new[$k];
      }

    }

    return count($diferencias) === 0 ? (bool)false : $diferencias;
  }
}
