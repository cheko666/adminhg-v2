<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class PsViewPsIds extends Model
{
    use HasFactory;

    protected $connection = 'production_presta';
    protected $table = 'view_ps_ids';
/*
    public function __construct(array $attributes = array()) {
        $collection = Config::get('database');
        $collection['connections']['production_presta']['prefix'] = '';
        Config::set('database',$collection);
        parent::__construct($attributes);
    }
*/
    public function psproduct()
    {
        return $this->hasOne('App\Models\PsProduct','id_product','id_product');
    }

}
