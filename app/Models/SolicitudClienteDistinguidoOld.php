<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolicitudClienteDistinguidoOld extends Model
{
    use HasFactory;

    protected  $connection = 'production_presta';

    protected $table = 'distinguidos';

    public $timestamps = false;

    public function getFechaRegistroAttribute($date) {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('H:i:s');
    }

}
