<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodigoPostal extends Model
{
    use HasFactory;

    protected $connection = 'lanceta_bd_ec';

    protected $table = 'codigos_postales_callcenter';

    public function envio_transportista()
    {
        return $this->hasMany('App\Models\EnvioTransportista')->orderBy('created_at','desc');
    }

}
