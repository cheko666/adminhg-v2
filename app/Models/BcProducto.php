<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BcProducto extends Model
{
    use HasFactory;

    const CREATED_AT = 'date_add';
    const UPDATED_AT = 'date_upd';

    protected $connection = 'production_presta';
    protected $table = 'lsc_producto';


    public function marca()
    {
        return $this->hasOne('App\Models\BcMarca','code','special_group_code');
    }

    public function producto()
    {
      return $this->belongsTo('App\Models\Producto',$this->getConnection()->getDatabaseName().'.lsc_producto',
        'codigo',
        'reference');
    }
    public function getTieneDimensionesAttribute()
    {
        return (bool)($this->width > 0 && $this->height > 0 && $this->depth > 0);
    }
    public function getTienePesosAttribute()
    {
        return (bool)($this->weight > 0 && $this->peso_v > 0);
    }

    public function scopeNuevosConError($query, $tiempo_nuevo = '1-d')
    {
        return $query
        ->nuevos($tiempo_nuevo)
        ->where(function ($query) {
            $query->where('width',0)
            ->orWhere('height',0)
            ->orWhere('depth',0)
            ->orWhere('price',0)
            ->orWhere('special_group_code','')
            ->orWhere('retail_product_code','')
            ;
        });
    }
    public function scopeNuevos($query,$tiempo_nuevo = '1-d')
    {
        $fecha = Carbon::now();

        list($cantidad,$periodo)=explode('-',$tiempo_nuevo);

        if($periodo && $cantidad != '')
        {

            switch($periodo)
            {
                case 'm':
                    $fecha_pivote = $cantidad == '0' || $cantidad == '1' ? $fecha->startOfMonth() : $fecha->subMonth($cantidad)->startOfDay();
                    break;
                case 'd':
                    $fecha_pivote = $cantidad == '0' || $cantidad == '1' ? $fecha->subDays(2)->startOfDay() : $fecha->subDays($cantidad)->startOfDay();
                    break;
                case 'h':
                    $fecha_pivote = $fecha->subHours($cantidad);
                    break;
                case 'i':
                    $fecha_pivote = $fecha->subMinutes($cantidad);
                    break;
                default:
                    $fecha_pivote = $fecha->yesterday()->startOfDay();
            }

        } else {

            $fecha_pivote = $fecha->yesterday()->startOfDay();
        }

        return $query
          ->where('id_p',0)
          ->where(self::UPDATED_AT,'>',$fecha_pivote->toDateTimeString())
          ->orderBy('id')->orderBy(self::UPDATED_AT,'desc');
    }    
}
