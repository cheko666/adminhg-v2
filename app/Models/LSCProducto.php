<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LSCProducto extends Model
{
  protected $connection = 'production_presta';

  protected $table = 'lsc_producto';
  public $id_p;

  protected $fillable = [
      'id',
      'serie',
      'producto',
      'ps_reference',
      'id_tax',
      'lsc_clave_marca',
      'lsc_clave_categoria',
      'descripcion',
      'ps_supplier_reference',
      'unidad',
      'registro_sanitario',
      'sat_c_prodserv',
      'sat_c_unidad',
      'peso',
      'anchura',
      'altura',
      'profundidad',
      'costo',
      'precio',
      'descuento',
      'created_at',
      'updated_at'
  ];

  protected static $campos_comparar = [
    'precio'=>'Precio',
    'iva'=>'IVA',
    'peso'=>'Peso',
    'ancho'=>'Ancho',
    'alto'=>'Alto',
    'fondo'=>'Fondo',
    'modelo'=>'Modelo',
    'idmarca'=>'IDMarca',
    // 'descuento'=>'Descuento'
  ];

  public static function getCamposComparar()
  {
    return self::$campos_comparar;
  }

  public static function setCamposCompararFromIds($ids)
  {
    if(!array_key_exists('precio',array_flip($ids)))
    {
      array_unshift($ids,'precio');
    }
    $campos = array();
    foreach($ids as $key)
    {
      $campos[$key] = self::$campos_comparar[$key];
    }
    return $campos;
  }
  public function scopelistarDiferenciasBcPs($query,$idp,$campos_compara,$tipo_productos)
  {      
      $data = DB::select(DB::raw($this->queryConsultarDiferenciasBcPs($idp,$campos_compara,$tipo_productos)));

      $log = array();

      if (count($data) > 0)
      {
        // Obtenemos campos con diferencias
        foreach ($data as &$registro)
        {
            $campos_diferencias = array();
            $diferencias = array();

            $registro_log['id_product'] = $registro->id_product;
            $registro_log['id_product_attribute'] = $registro->id_product_attribute;
            $registro_log['id_stock_available'] = $registro->id_sa;
            $registro_log['id_specific_price'] = $registro->id_sp;
            $registro_log['descripcion'] = $registro->description;

            // Checar diferencias
            foreach ($campos_compara as $key => $value)
            {
              if($registro->ps_default_on == 1 && ($key == 'iva' || $key == 'idmarca') )
              {
                $campo_ps = 'bc_' . $key;
              } else {  
                $campo_ps = 'ps_' . $key;
              }
              $campo_ps = 'ps_' . $key;
              $campo_bc = 'bc_' . $key;
              
              if ($registro->$campo_bc != $registro->$campo_ps) {
                  $campos_diferencias[] = $key;
                  $diferencias[$key][] = $registro->$campo_bc;
                  $diferencias[$key][] = $registro->$campo_ps;
              }  
            }
            
            $registro_log['diferencias'] = json_encode($diferencias);

            $registro->campos_dif = implode(',', $campos_diferencias);
            $registro->diferencias = $diferencias;
            
            $log[$registro->codigo] = $registro_log;

            collect($registro);
        }

      }
      return $collection = collect($data);
  }

  public function queryConsultarDiferenciasBcPs($idp = null,$campos_comparar,$tipo_productos)
  {
      $query = "";
      $where_idproduct = '';
      $where_idproduct = !$idp ? " " : " AND bcp.id_p IN (". $idp.")";
      
      $query_datos_ps = (string)$this->querySelectDatosProductosPs($idp,$tipo_productos);
      $query_datos_bc = (string)$this->querySelectDatosProductosBc($idp);

      $query .= "
      SELECT
      bcp.codigo
      , psp.p_codigo
      , psp.pa_codigo

      , psp.url 
      , psp.img
      
      ## IDs PS
      , bcp.id_p
      , psp.id_product

      , bcp.id_pa
      , psp.id_product_attribute

      , bcp.id_sa
      , bcp.id_sp

      ## DESCRIPCION/NOMBRE
      , bcp.description
      , psp.name
  
      ## PRECIO
      , bcp.precio AS bc_precio
      , psp.precio_ps AS ps_precio
      
      , psp.p_price AS ps_p_price
      , psp.pa_price AS ps_pa_price
      , ROUND(bcp.precio - psp.p_price,3) as ajuste_precio_pa

      ## DESCUENTO
      , CAST( ifnull(psp.id_specific_price,0) AS UNSIGNED ) id_specific_price
      
      , bcp.descuento AS bc_descuento
      , psp.descuento_ps AS ps_descuento

      ## IVA
      , bcp.iva AS bc_iva
      , psp.iva_ps AS ps_iva

      ## MARCA
      , bcp.idmarca AS bc_idmarca
      , psp.idmarca_ps AS ps_idmarca

      , bcp.marca AS bc_marca
      , psp.marca_ps AS ps_marca

      ## MODELO
      , bcp.modelo AS bc_modelo
      , psp.modelo_ps AS ps_modelo

      ## ANCHO
      , bcp.ancho AS bc_ancho
      , psp.ancho_ps AS ps_ancho

      ## ALTURA
      , bcp.alto AS bc_alto
      , psp.alto_ps AS ps_alto

      ## LONGITUD
      , bcp.fondo AS bc_fondo
      , psp.fondo_ps AS ps_fondo

      # PESO
      , bcp.peso AS bc_peso
      , psp.peso_ps AS ps_peso
      
      , psp.p_peso
      , psp.pa_peso
      , ROUND(bcp.peso - psp.p_peso,3) as ajuste_peso_pa

      # PRODUCTO PADRE
      , bcp.principal AS bc_default_on
      , psp.default_on AS ps_default_on
      , bcp.bloqueo_compra
      , bcp.bloqueo_venta
      , bcp.bloqueo
      , psp.active
  
      # Datos PS_Productos
      FROM (".$query_datos_ps.") psp

      # Datos LSC_Productos
      JOIN (".$query_datos_bc.") bcp
        ON bcp.codigo = psp.codigo AND bcp.id_p = psp.id_product
        
      WHERE 1
      AND ( ".$this->queryCompararCamposBcPs($campos_comparar)." )
      ".$where_idproduct."

      ORDER BY id_p
    ";
    // dd($query);
    return $query;
  }
  public function querySelectDatosProductosBc($idp)
  {
    $where_idproduct = '';
    $where_idproduct = !$idp ? " " : " AND p.id_p IN (". $idp.")";

    $query = "
      SELECT
      ( p.reference COLLATE utf8_unicode_ci ) AS codigo,
      p.id_p,
      p.id_pa,
      p.id_sa,
      p.id_sp,
      ROUND(p.price,2) AS precio,
      p.id_tax_rules_group AS iva,
      p.description, 
      IFNULL(m.description,'') AS marca, 
      p.id_manufacturer AS idmarca,
      IFNULL(CAST(od.descuento AS UNSIGNED),0) AS descuento,
      ROUND(p.weight,3) AS peso,
      ROUND(p.width,2) AS ancho,
      ROUND(p.height,2) AS alto,
      ROUND(p.depth,2) AS fondo,
      (p.supplier_reference COLLATE utf8_unicode_ci ) AS modelo,
      p.bloqueo_compra,
      p.bloqueo_venta,
      p.bloqueo,
      p.principal
      FROM production_presta.ps0516_lsc_producto AS p
      LEFT JOIN production_presta.ps0516_lsc_marca AS m ON ( m.Code = p.special_group_code COLLATE utf8_unicode_ci )
      LEFT JOIN production_presta.ps0516_bc_oferta_descuento od ON ( od.codigo = p.reference COLLATE utf8_unicode_ci )
      WHERE 1 ".$where_idproduct."
    ";
    return $query;
  }

  public function querySelectDatosProductosPs($idp = null, $tipo_productos = 'todo')
  {
    $where_idproduct = '';
    $where_idproduct = !$idp ? " " : " AND p.id_product IN (". $idp.")";

    $q_tipo_productos = '';
    $q_prod_principal = '';
    $q_prod_principal2 = '';

    $q_tipo_productos = ( $tipo_productos == 'todo' || $tipo_productos == 'combinaciones' ) ? "" : "AND ifnull(pa.id_product_attribute,0) = 0";
    
    $q_prod_principal = ( $tipo_productos == 'todo' || $tipo_productos == 'combinaciones' ) ? "" : "WHERE ps0516_product_attribute.default_on = 1";
    
    $q_prod_principal2 = ( $tipo_productos == 'todo' || $tipo_productos == 'combinaciones' ) ? "IFNULL(pa.default_on,0)" : "1";

    $query = "
      SELECT
      ( ifnull(pa.reference,p.reference) COLLATE utf8_unicode_ci ) as codigo,
      p.reference AS p_codigo,
      IFNULL(pa.reference,'') AS pa_codigo,
      p.id_product,
      ifnull(pa.id_product_attribute,0) id_product_attribute,
      pl.name,
      # MARCA
      IF(IFNULL(pa.default_on,0) = 1,p.id_manufacturer,bc.id_manufacturer) AS idmarca_ps,
      m.name AS marca_ps,
      # IVA
      p.id_tax_rules_group AS iva_ps,
      #IF(IFNULL(pa.default_on,0) = 1,p.id_tax_rules_group,bc.id_tax_rules_group) AS iva_ps,
      # ANCHO
      ROUND(IF(bcp.principal = 1,p.width,bcp.width),2) AS ancho_ps,
      # ALTO
      ROUND(IF(bcp.principal = 1,p.height,bcp.height),2) AS alto_ps,
      # FONDO
      ROUND(IF(bcp.principal = 1,p.depth,bcp.depth),2) AS fondo_ps,
      # MODELO
      p.supplier_reference as p_modelo,
      ifnull(pa.supplier_reference,'') as pa_modelo,
      ( ifnull(pa.supplier_reference,p.supplier_reference) COLLATE utf8_unicode_ci ) as modelo_ps,
      # PRECIO
      ROUND(p.price,2) as p_price,
      ROUND(ifnull(pa.price,0),2) as pa_price,
      ROUND(ifnull(pa.price,0) + p.price,2) as precio_ps,
      # PESO
      ROUND( ifnull(pa.weight+p.weight,p.weight),3 ) peso_ps,
      @pweight := ROUND(p.weight,3) as p_peso,
      ROUND(ifnull(pa.weight,0),3) as pa_peso,
      # DESCUENTO
      ifnull(sp.id_specific_price,0) AS id_specific_price,
      CAST(round(ifnull(sp.descuento,0)) AS UNSIGNED) as descuento_ps,
      
      ".$q_prod_principal2." AS default_on,
      CONCAT('".env('PRESTASHOP_URL','https://www.lancetahg.com.mx')."/productos/',p.id_product,'/',link_rewrite) AS url,
      CONCAT('".env('PRESTASHOP_URL','https://www.lancetahg.com.mx')."/img/tmp/product_mini_',p.id_product,'_1.jpg') AS img,
      p.active
    
      FROM production_presta.ps0516_product AS p
      JOIN production_presta.ps0516_product_lang AS pl ON pl.id_product = p.id_product AND pl.id_lang = 2
    LEFT JOIN (
      SELECT ps0516_product_attribute.id_product, ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.reference, ps0516_product_attribute.weight, ps0516_product_attribute.price, ps0516_product_attribute.supplier_reference, ps0516_product_attribute.default_on
      FROM production_presta.ps0516_product_attribute
      ".$q_prod_principal."
    ) AS pa ON pa.id_product = p.id_product
      ".$q_tipo_productos."
      JOIN production_presta.ps0516_lsc_producto AS bc ON bc.reference = ( ifnull(pa.reference,p.reference) COLLATE utf8_unicode_ci )
      LEFT JOIN production_presta.ps0516_manufacturer m ON m.id_manufacturer = p.id_manufacturer
      LEFT JOIN production_presta.ps0516_image i ON i.id_product = p.id_product AND i.cover = 1
      LEFT JOIN (
        SELECT
        ps0516_specific_price.id_specific_price, ps0516_specific_price.id_product, ps0516_specific_price.id_product_attribute, ps0516_specific_price.reduction,
        ps0516_specific_price.reduction*100 as descuento
        FROM production_presta.ps0516_specific_price
        WHERE reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0 AND id_cart = 0
      ) as sp on sp.id_product = p.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
      JOIN production_presta.ps0516_lsc_producto bcp on ( bcp.reference = ifnull(pa.reference,p.reference) COLLATE utf8_unicode_ci ) AND bcp.id_p is not null
      
      WHERE 1 ".$where_idproduct."
    ";
    
    return $query;
  }
  public function corregirDiferenciasBcPs($idp,$campos_comparar,$tipo_productos)
  {
    switch($tipo_productos)
    {
      case 'todo':
        return (bool) DB::statement(DB::raw( $this->queryCorregirDiferenciasBcPsProducto($idp,$campos_comparar) ))
        && DB::statement(DB::raw( $this->queryCorregirDiferenciasBcPsCombinaciones($idp,$campos_comparar) ));
      case 'productos':
        return (bool) DB::statement(DB::raw( $this->queryCorregirDiferenciasBcPsProducto($idp,$campos_comparar) ));
      case 'combinaciones':
        return (bool) DB::statement(DB::raw( $this->queryCorregirDiferenciasBcPsCombinaciones($idp,$campos_comparar) ));
    }
  }
  public function queryCorregirDiferenciasBcPsProducto($idp,$campos_comparar)
  {
    $query = "";
    $where_idproduct = '';
    $where_idproduct = !$idp ? " " : " AND bcp.id_p IN (". $idp.")";

    $query_datos_ps = (string)$this->querySelectDatosProductosPs($idp,'productos');
    $query_datos_bc = (string)$this->querySelectDatosProductosBc($idp);

    $query .= "
      update production_presta.ps0516_product A
      join (
        SELECT
        psp.codigo
        , psp.id_product_attribute
        , psp.id_product
        , bcp.id_p        
  
        ## PRECIO
        , bcp.precio AS bc_precio
        , psp.precio_ps AS ps_precio
        
        , psp.p_price AS ps_p_price
        , psp.pa_price AS ps_pa_price
        , ROUND(bcp.precio - psp.p_price,3) as ajuste_precio_pa

        , bcp.iva AS bc_iva

        , bcp.ancho AS bc_ancho
        , psp.ancho_ps

        , bcp.alto AS bc_alto
        , psp.alto_ps

        , bcp.fondo AS bc_fondo
        , psp.fondo_ps

        # PESO
        , bcp.peso AS bc_peso
        , psp.peso_ps AS ps_peso
        
        , psp.p_peso
        , psp.pa_peso
        , ROUND(bcp.peso - psp.p_peso,3) as ajuste_peso_pa

        ## MODELO
        , bcp.modelo AS bc_modelo
        , psp.modelo_ps AS ps_modelo

        , psp.p_modelo
        , psp.pa_modelo

        , bcp.idmarca AS bc_idmarca
        , psp.idmarca_ps

        , psp.default_on
        , psp.active

        #FROM production_presta.ps0516_product p
        #LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0

        # Datos PS_Productos
        FROM (".$query_datos_ps.") psp

        # Datos LSC_Productos
        JOIN (".$query_datos_bc.") bcp
        ON bcp.codigo = psp.codigo AND bcp.id_p = psp.id_product

        WHERE 1
        AND bcp.id_p is not null
        ".$where_idproduct."
        AND ( ".$this->queryCompararCamposBcPs($campos_comparar)." )

      ) AS B ON A.id_product = B.id_product

      SET ".$this->querySetValoresCamposBcPs($campos_comparar,'productos')." ;
    ";
    // dd($query);
    return $query;
  }
  public function queryCorregirDiferenciasBcPsCombinaciones($idp,$campos_comparar)
  {
    $query = "";
    $where_idproduct = '';
    $where_idproduct = !$idp ? " " : " AND bcp.id_p IN (". $idp.")";

    $query_datos_ps = (string)$this->querySelectDatosProductosPs($idp,'combinaciones');
    $query_datos_bc = (string)$this->querySelectDatosProductosBc($idp);

    $query .= "
      update production_presta.ps0516_product_attribute A
      join (
        SELECT
        psp.codigo
        , psp.id_product_attribute
        , psp.id_product
        , bcp.id_p

        ## PRECIO
        , bcp.precio AS bc_precio
        , psp.precio_ps AS ps_precio
        
        , psp.p_price AS ps_p_price
        , psp.pa_price AS ps_pa_price
        , ROUND(bcp.precio - psp.p_price,3) as ajuste_precio_pa   

        # PESO
        , bcp.peso AS bc_peso
        , psp.peso_ps AS ps_peso
        
        , psp.p_peso
        , psp.pa_peso
        , ROUND(bcp.peso - psp.p_peso,3) as ajuste_peso_pa

        ## MODELO
        , bcp.modelo AS bc_modelo
        , psp.modelo_ps AS ps_modelo

        , psp.p_modelo
        , psp.pa_modelo

        , psp.default_on
        , psp.active

        #FROM production_presta.ps0516_product p
        #LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0
        #left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null

        # Datos PS_Productos
        FROM (".$query_datos_ps.") psp

        # Datos LSC_Productos
        JOIN (".$query_datos_bc.") bcp
        ON bcp.codigo = psp.codigo AND bcp.id_p = psp.id_product

        WHERE 1
        AND bcp.id_p is not null
        ".$where_idproduct."
        AND ( ".$this->queryCompararCamposBcPs($campos_comparar)." )

      ) AS B ON A.id_product_attribute = B.id_product_attribute

      SET ".$this->querySetValoresCamposBcPs($campos_comparar,'combinaciones')." ;
    ";
    // dd($query);
    return $query;
  }
  public function ejecutarCorreccion($idp,$campos_comparar,$tipo_productos)
  {
    $q_tabla = "product";
    $q_join = "A.id_product = B.id_p";
    if($tipo_productos == 'combinaciones')
    {
      $query = "
        ## QUERY COMBINACIONES
        UPDATE production_presta.ps0516_product_attribute A
        JOIN (
          ".$this->queryConsultarDiferenciasBcPs($idp,$campos_comparar,$tipo_productos)."
        ) AS B on A.id_product_attribute = B.id_product_attribute
        SET ".$this->querySetValoresCamposBcPs($campos_comparar,$tipo_productos)." ;
      ";
    } else {
      $query = "
        ## QUERY PRODUCTOS PADRE
        UPDATE production_presta.ps0516_product A
        JOIN (
          ".$this->queryConsultarDiferenciasBcPs($idp,$campos_comparar,$tipo_productos)."
        ) AS B on A.id_product = B.id_p
        SET ".$this->querySetValoresCamposBcPs($campos_comparar,$tipo_productos)." ;
      ";
    }
    // dd($query);
    return $query;
  }  
  public function queryCompararCamposBcPs($campos_comparar)
  {
    $query = '';

    foreach ($campos_comparar as $key => $value)
    {
        $campo_ps = $key.'_ps';
        $campo_bc = $key;
        $query .= $key == 'precio' ? '' : ' OR ';
        $query .= 'bcp.'.$campo_bc.' <> psp.'.$campo_ps.'';
    }  
    return $query;
  }

  public function querySetValoresCamposBcPs($campos_comparar,$tipo_productos = 'todos')
  {
    $query = '';
    $separator = ',';
    if($tipo_productos == 'combinaciones')
    {
      unset($campos_comparar['iva']);
      unset($campos_comparar['ancho']);
      unset($campos_comparar['alto']);
      unset($campos_comparar['fondo']);
      unset($campos_comparar['idmarca']);
    }
    $ultimo = strtolower(end($campos_comparar));
    
    foreach($campos_comparar as $k => $v)
    {
      if($k == $ultimo)
        $separator = '';
      switch($k)
      {
        case 'precio':
          if($tipo_productos == 'productos') {
            $query .= 'A.price = B.bc_precio'.$separator;
          } else {
            $query .= 'A.price = B.ajuste_precio_pa'.$separator;
          }
          break;
        case 'iva':
          if($tipo_productos == 'productos') {
            $query .= 'A.id_tax_rules_group = B.bc_iva'.$separator;
          } else {
            $query .= '';
          }
          break;
        case 'modelo':
          $query .= 'A.supplier_reference = B.bc_modelo'.$separator;
          break;
        case 'peso':
          if($tipo_productos == 'productos') {
            $query .= 'A.weight = B.bc_peso'.$separator;
          } else {
            $query .= 'A.weight = B.ajuste_peso_pa'.$separator;
          }
          break;
        case 'alto':
          if($tipo_productos == 'productos') {
            $query .= 'A.width = B.bc_ancho'.$separator;
          } else {
            $query .= '';
          }
          break;
        case 'ancho':
          if($tipo_productos == 'productos') {
            $query .= 'A.height = B.bc_alto'.$separator;
          } else {
            $query .= '';
          }
          break;
        case 'fondo':
          if($tipo_productos == 'productos') {
            $query .= 'A.depth = B.bc_fondo'.$separator;
          } else {
            $query .= '';
          }
          break;
        case 'idmarca':
          if($tipo_productos == 'productos') {
            $query .= 'A.id_manufacturer = B.bc_idmarca'.$separator;
          } else {
            $query .= '';
          }
          break;
      }
    }
    return $query;
  }
  public function deletePsSpecificPriceById($id)
  {
    return DB::delete('DELETE FROM production_presta.ps0516_specific_price WHERE id_specific_price = ?',[$id]);
  }

  public function createPsSpecificFromAdminhg($descuento,$id_product,$id_product_attribute)
  {
    return DB::insert('
    INSERT INTO production_presta.ps0516_specific_price (id_specific_price,id_specific_price_rule,id_cart,id_product,id_shop,id_shop_group,id_currency,id_country,id_group,id_customer,id_product_attribute,price,from_quantity,reduction,reduction_tax,reduction_type,`from`,`to`)
    VALUES (null,0,0,?,0,0,0,0,0,0,?,-1,1,?,1,"percentage","0000-00-00 00:00:00","0000-00-00 00:00:00")
    ',[$id_product,$id_product_attribute,$descuento/100]);
  }

  public function updatePsSpecificPriceById($id,$descuento)
  {
      return DB::update('UPDATE production_presta.ps0516_specific_price SET reduction = ? WHERE id_specific_price =?',[$descuento/100,$id]);
  }

  public function psproduct()
  {
      return $this->belongsTo('App\Models\PSProduct','id_p','id_product')
        ->join('product_lang',function ($join) {
            $join->on('product_lang.id_product','=','product.id_product')
              ->where('product_lang.id_lang','=','2');
        })
        ;
  }
  public function producto()
  {
    return $this->belongsTo('App\Models\Producto',$this->getConnection()->getDatabaseName().'.lsc_producto',
      'codigo',
      'reference');
  }

  public function getUrlWebAttribute()
  {
    return 'https://www.lancetahg.com.mx/productos/'.$this->id_p.'/'.$this->psproduct()->name;
  }

}
