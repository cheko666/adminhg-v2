<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PsProduct extends Model
{
  use HasFactory;

  const CREATED_AT = 'date_add';
  const UPDATED_AT = 'date_upd';

  protected $connection = 'production_presta';
  protected $table = 'product';
  protected $primaryKey = 'id_product';

  protected $fillable = ['date_upd'];

  public function psproduct_attributes()
  {
      return $this->hasMany('App\Models\PsProductAttribute','id_product','id_product');
  }

  public function product_shop()
  {
    return $this->hasOne(self::class,'id_product')->join('product_shop','product_shop.id_product','=','product.id_product');
  }

  public static function corregirDiferenciasPreciosBcPs() {

    return DB::statement(DB::raw(self::queryCorregirDiferenciasPrecioBcPsProductoSimple())) && DB::statement(DB::raw(self::queryCorregirDiferenciasPrecioBcPsProductoCombinaciones()))
        ? true : false;
  }

  public static function queryCorregirDiferenciasPrecioBcPsProductoSimple()
  {
    return "
      UPDATE production_presta.ps0516_product A

      JOIN (
        SELECT ifnull(pa.reference,p.reference) as reference,
        p.id_product,

        c.price precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps

        FROM production_presta.ps0516_product p
        LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0
        left join production_presta.ps0516_lsc_producto c on c.id = CAST(ifnull(pa.reference,p.reference) AS UNSIGNED) and c.id_p is not null

        where c.id_p is not null
        and c.price <> round(ifnull(pa.price,0) + p.price,2)
      ) as B ON A.id_product = B.id_product

      set A.price = B.precio_mg
      ;
    ";
  }

  public static function queryCorregirDiferenciasPrecioBcPsProductoCombinaciones()
  {
    return "
      update production_presta.ps0516_product_attribute A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,

        c.price precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        p.price as p_price,
        ifnull(pa.price,0) as pa_price,
        c.price - p.price as pa_price_ok,

        pa.default_on

        FROM production_presta.ps0516_product p
        LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product
        left join production_presta.ps0516_lsc_producto c on c.id = CAST(ifnull(pa.reference,p.reference) AS UNSIGNED) and c.id_p is not null

        where c.id_p is not null
        and c.price <> round(ifnull(pa.price,0) + p.price,2)
        and pa.id_product_attribute is not null
      ) as B on A.id_product_attribute = B.id_product_attribute

      set A.price = B.pa_price_ok
      ;
    ";
  }


}
