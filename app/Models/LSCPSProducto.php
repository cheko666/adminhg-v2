<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LSCPSProducto extends Model
{
    use HasFactory;

    public function listDiferenciasLscPs() {

        $data = DB::select(DB::raw(self::queryVerificarDiferenciasLscPs()));

        $log = array();

        if (count($data) > 0) {

            // Obtenemos campos con diferencias
            foreach ($data as &$registro) {
                $campos_diferencias = array();
                $diferencias = array();

                $registro_log['id_product'] = $registro->id_product;
                $registro_log['id_product_attribute'] = $registro->id_product_attribute;

                // Checar diferencias
                foreach ($this->campos_compara as $key => $value) {
                    if ($key == 'todo') continue;
                    $campo_ps = 'ps_' . $key;
                    if ($registro->$key != $registro->$campo_ps) {
                        $campos_diferencias[] = $key;
                        $diferencias[$key][] = $registro->$key;
                        $diferencias[$key][] = $registro->$campo_ps;
                    }
                }
                $registro_log['diferencias'] = json_encode($diferencias);


                $registro->campos_dif = implode(',', $campos_diferencias);
                $registro->diferencias = $diferencias;

                $log[$registro->codigo] = $registro_log;

                collect($registro);
            }

        }
        return $collection = collect($data);
    }

    public function queryVerificarDiferenciasLscPs()
    {
        return "
      select
      LPAD(c.id,6,'0') as codigo,
      @id_p := c.producto_id as id_product,
      #@id_p,
      ps.id_product_attribute,

      c.precio,
      @prec_base_ps := ps.ps_precio ps_precio,
      @prec_base_pas := ps.ps_pas_precio ps_pas_precio,
      c.precio_neto,
      ps.ps_precio_neto,

      c.descuento,
      ps.ps_descuento,
      #ps.reduction,
      CAST( ifnull(ps.id_specific_price,0) AS UNSIGNED ) id_specific_price,

      c.iva,
      ps.ps_iva,

      c.baja,
      c.por_agotar,

      c.principal principal,

      c.peso,
      ps.ps_peso,

      c.ancho ancho,
      ps.ps_ancho,

      c.altura altura,
      ps.ps_altura,

      c.fondo fondo,
      ps.ps_fondo,

      c.marca_id,
      ps.ps_marca_id,
      c.nombre_marca,
      ps.ps_nombre_marca,

      c.modelo,
      ps.ps_modelo,

      ps.active,

      case
        when c.precio <> ps.ps_precio then 'precio'
        when c.descuento <> ps.ps_descuento then 'descuento'
        when c.peso <> ps.ps_peso then 'peso'
        when c.ancho <> ps.ps_ancho then 'ancho'
        when c.altura <> ps.ps_altura then 'altura'
		when c.fondo <> ps.ps_fondo then 'fondo'
        when c.iva <> ps.ps_iva then 'iva'
        when c.marca_id <> ps.ps_marca_id then 'id marca'
        when c.modelo <> ps.ps_modelo then 'modelo'
      end
      as campo_dif

      ############
      # AdminHG
      ############
      from (
        select
        P.id,
        P.producto_id,
        P.precio as precio,
        round((P.precio * (1-(P.descuento / 100)) * if(P.iva = 'S',1.16,1)),2) as precio_neto,
        P.descuento,
        P.peso,
          round(P.ancho,2) ancho,
          round(P.altura,2) altura,
          round(P.fondo,2) fondo,
        if(P.iva = 'S', 16,0) as iva,
        P.baja,
        P.por_agotar,
        P.marca_id,
          P.modelo,
        P.principal,
        ifnull(m.nombre,'Sin Marca') as nombre_marca

          from lanceta_bd_ec.codigos P
        left join lanceta_bd_ec.marcas m on m.id = P.marca_id

        where P.producto_id is not null
        and P.producto_id > 0
        #and P.baja = 'N'
        #and P.producto_id = 604

      ) as c

      ############
      # PRESTASHOP
      ############
      join (
        select ifnull(pa.reference,p2.reference) as reference,
        p2.id_product as id_product,
        p2.id_manufacturer ps_marca_id,
        m.name as ps_nombre_marca,
        round(p2.price,2) as price,
        pa.id_product_attribute,
        ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
		    round(ifnull(pa.price,0),2) pa_price,
        round(ifnull(pas.price,0),2) pas_price,
        round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
        pa.default_on ,
        if( cc.principal = 1,round(p2.width,2), cc.ancho ) as ps_ancho,
        if( cc.principal = 1,round(p2.height,2), cc.altura ) as ps_altura,
        if( cc.principal = 1,round(p2.depth,2), cc.fondo ) as ps_fondo,
        CAST(round(ifnull(sp.descuento,0)) AS UNSIGNED) as ps_descuento,
        ifnull(sp.id_specific_price,0) id_specific_price,
        round(ifnull(sp.reduction,0),2) reduction,
        round( ifnull(pa.price,0) + p2.price,2 ) as ps_precio,
        round( ifnull(pas.price,0) + p2.price,2 ) as ps_pas_precio,
        round( (ifnull(pa.price,0) + p2.price) * (1-(ifnull(sp.reduction,0))) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
        if(p2.id_tax_rules_group = 1,16,0) as ps_iva,
        p2.active

        from production_presta.ps0516_product p2
        left join production_presta.ps0516_manufacturer m ON p2.id_manufacturer = m.id_manufacturer
        left join (
          select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference, ps0516_product_attribute.default_on
          from production_presta.ps0516_product_attribute
        ) pa on pa.id_product = p2.id_product
        left join production_presta.ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
        left join (
        SELECT
          *,
          ps0516_specific_price.reduction*100 as descuento
          FROM production_presta.ps0516_specific_price
          where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
        ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
        left join lanceta_bd_ec.codigos cc ON cc.id = ifnull(pa.reference,p2.reference)

        #where p2.active = 1
        #and pa.id_product_attribute is null
        #and p2.id_product = 604

      ) as ps on c.id = ps.reference

      where (
        c.precio <> ps.ps_precio
        #or c.precio <> ps.ps_pas_precio
        or c.descuento <> ps.ps_descuento
        or c.iva <> ps.ps_iva
        or c.peso <> ps.ps_peso
        or c.ancho <> ps.ps_ancho
        or c.altura <> ps.ps_altura
        or c.fondo <> ps.ps_fondo
        or c.marca_id <> ps.ps_marca_id
        or c.modelo <> ps.ps_modelo
      )
      #and id_product = 4075
      #and left(c.id,3) in (60,250,288,2992)

      order by id_product, codigo

      #limit 10
    ";
    }

}
