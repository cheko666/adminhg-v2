<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DiferenciaPreciosBcPsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email_vars;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        $this->subject = 'Reporte Diferencias en Precio BC vs PS';
        $this->email_vars = $email_vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.sincprecioproductobcps')
        ->with([
          'asunto'=> $this->subject,
          'titulo'=> $this->email_vars->titulo,
          'mensaje'=> $this->email_vars,
        ]);
    }
}
