<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class CodigosDiferenciasMgPsEncontrados extends Mailable
{
    use Queueable, SerializesModels;

  public $subject;

  public $h1;

  public $h2;

  public $data;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
      $this->subject = 'Corrección de diferencias BC vs PS';
      $this->h1 =  'Script Diferencias entre Business Central-Admin HG y Prestashop';
      $this->h2 = 'Diferencias encontradas.';
      $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from($this->from)
            ->view('emails.codigos.mensaje_diferencias_mg_ps')
            ->with([
                'subject' => $this->subject,
                'h1' => $this->h1,
                'h2' => $this->h2,
                'mensaje' => $this->data['mensaje'],
            ])
            ->attach(Storage::disk('samba_media')->path($this->data['ruta_archivo']));
    }
}
