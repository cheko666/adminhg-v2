<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificacionAppMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email_vars;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        $this->email_vars = $email_vars;
        $this->subject = 'Notificaciones Nuevos Clientes Distinguidos';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('MAIL_FROM_ADDRESS'),env('APP_NAME'));
        return $this->view('mails.notificacion-app')->with([
            'data' => $this->email_vars
        ]);
    }
}
