<?php

namespace App\Mail;

use App\Http\Controllers\SolicitudClienteDistinguidoController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StoreSolicitudClienteDistinguidoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $subtitulo;
    public $email_vars;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        $from = array();
        $from[] = ['name'=>'Lanceta HG','address'=>'ventas@lancetahg.com'];
        $this->email_vars = $email_vars;
        $this->subject = 'Solicitud Tarjeta Cliente Distinguido';
        $this->from = $from;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $vista_mail = 'solicitud-cliente-distinguido.show';

        if(isset($this->email_vars->hay_error) && $this->email_vars->hay_error)
        {
            $this->subject = 'Error en '.$this->subject;
        }
        // $vista = $scdController->maquetarVistaDatosSolicitud($this->email_vars->reference,null,$to_cliente);
        // $this->email_vars->cuerpo_mensaje = $vista->render();

        if(isset($this->email_vars->store_nocd) && $this->email_vars->store_nocd)
        {
            $vista_mail = 'mails.nocd-update';
        }

        return $this->view($vista_mail)
        ->with([
            'data' => $this->email_vars,
        ]);
    }
}
