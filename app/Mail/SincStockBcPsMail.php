<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SincStockBcPsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $titulo;
    public $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        if($email_vars->has_error) {
            $msj_error = 'Error en ';
          }
    
        $this->subject = $msj_error.'Reporte Sincronización BC-PS - Stock';
        $this->titulo = 'Movimientos en inventario';
        $this->mensaje = $email_vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.sincstockbcps')
        ->with([
            'asunto'=> $this->subject,
            'titulo'=> $this->titulo,
            'mensaje'=> $this->mensaje,
          ]);
    }
}
