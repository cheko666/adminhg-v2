<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificacionNoCdMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $email_vars;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        $this->email_vars = $email_vars;
        $this->subject = 'Registro de Cliente Distinguido Aprobado';        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.nocd-update')->with([
            'data' => $this->email_vars,
        ]);
    }
}
