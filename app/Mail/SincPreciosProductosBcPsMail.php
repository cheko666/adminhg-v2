<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SincPreciosProductosBcPsMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $titulo;
    public $cuerpo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_vars)
    {
        $this->subject = 'Reporte Sincronización BC-PS - Precios Productos';
        $this->cuerpo = $email_vars;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.sincprecioproductobcps')
          ->with([
            'asunto'=> $this->subject,
            'titulo'=> $this->cuerpo->estadisticas_ini,
            'mensaje'=> $this->cuerpo,
          ]);
    }
}
