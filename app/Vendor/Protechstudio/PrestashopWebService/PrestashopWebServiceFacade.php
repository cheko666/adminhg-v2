<?php

namespace App\Vendor\Protechstudio\PrestashopWebService;

use Illuminate\Support\Facades\Facade as IlluminateFacade;


class PrestashopWebServiceFacade extends IlluminateFacade {

  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor() {
    return PrestashopWebServiceOverride::class;
  }

}