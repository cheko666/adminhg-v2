<?php

namespace App\Vendor\Protechstudio\PrestashopWebService;

use Protechstudio\PrestashopWebService\Exceptions\PrestashopWebServiceException;
use Protechstudio\PrestashopWebService\PrestashopWebService as PrestashopWebServiceOriginal;

/**
 * @package PrestaShopWebService
 */
class PrestashopWebServiceOverride extends PrestashopWebServiceOriginal
{
  public $status_code;
  public $method;
  /**
   * Prepares and validate a CURL request to PrestaShop WebService. Can throw exception.
   * @param string $url Resource name
   * @param mixed $curl_params CURL parameters (sent to curl_set_opt)
   * @return array status_code, response
   * @throws PrestashopWebServiceException
   */
  protected function executeRequest($url, $curl_params = array())
  {
    $defaultParams = array(
        CURLOPT_HEADER => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLINFO_HEADER_OUT => true,
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
        CURLOPT_USERPWD => $this->key.':',
        CURLOPT_HTTPHEADER => array( 'Expect:' ),
        CURLOPT_SSL_VERIFYPEER => config('app.env') === 'local' ? 0 : 1,
        CURLOPT_SSL_VERIFYHOST => config('app.env') === 'local' ? 0 : 2 // value 1 is not accepted https://curl.haxx.se/libcurl/c/CURLOPT_SSL_VERIFYHOST.html
    );

    $curl_options = array();
    foreach ($defaultParams as $defkey => $defval) {
      if (isset($curl_params[$defkey])) {
        $curl_options[$defkey] = $curl_params[$defkey];
      } else {
        $curl_options[$defkey] = $defaultParams[$defkey];
      }
    }
    foreach ($curl_params as $defkey => $defval) {
      if (!isset($curl_options[$defkey])) {
        $curl_options[$defkey] = $curl_params[$defkey];
      }
    }

    list($response, $info, $error) = $this->executeCurl($url, $curl_options);

    $status_code = $info['http_code'];
    if ($status_code === 0 || $error) {
      throw new PrestashopWebServiceException('CURL Error: '.$error);
    }

    $index = $info['header_size'];
    if ($index === false && $curl_params[CURLOPT_CUSTOMREQUEST] !== 'HEAD') {
      throw new PrestashopWebServiceException('Bad HTTP response');
    }

    $header = substr($response, 0, $index);
    $body = substr($response, $index);

    $headerArray = array();
    foreach (explode("\n", $header) as $headerItem) {
      $tmp = explode(':', $headerItem, 2);
      if (count($tmp) === 2) {
        $tmp = array_map('trim', $tmp);
        $headerArray[$tmp[0]] = $tmp[1];
      }
    }

    if (array_key_exists('PSWS-Version', $headerArray)) {
      $this->isPrestashopVersionSupported($headerArray['PSWS-Version']);
      $this->version = $headerArray['PSWS-Version'];
    }
/*
    $this->printDebug('HTTP REQUEST HEADER', $info['request_header']);
    $this->printDebug('HTTP RESPONSE HEADER', $header);

    if ($curl_params[CURLOPT_CUSTOMREQUEST] == 'PUT' || $curl_params[CURLOPT_CUSTOMREQUEST] == 'POST') {
      $this->printDebug('XML SENT', urldecode($curl_params[CURLOPT_POSTFIELDS]));
    }
    if ($curl_params[CURLOPT_CUSTOMREQUEST] != 'DELETE' && $curl_params[CURLOPT_CUSTOMREQUEST] != 'HEAD') {
      $this->printDebug('RETURN HTTP BODY', $body);
    }*/
    $this->status_code = $status_code;

    return array(
        'status_code' => $status_code,
        'response' => $body,
        'header' => $header,
        'headers' => $headerArray
    );
  }


  /**
   * Add (POST) a resource
   * <p>Unique parameter must take : <br><br>
   * 'resource' => Resource name<br>
   * 'postXml' => Full XML string to add resource<br><br>
   * Examples are given in the tutorial</p>
   * @param array $options
   * @return SimpleXMLElement status_code, response
   * @throws PrestashopWebServiceException
   */
  public function add($options)
  {
    $xml = '';
    $this->method = 'POST';

    if (isset($options['resource'], $options['postXml']) || isset($options['url'], $options['postXml'])) {
      $url = (isset($options['resource']) ? $this->url.'/api/'.$options['resource'] : $options['url']);
      $xml = $options['postXml'];
      if (isset($options['id_shop'])) {
        $url .= '&id_shop='.$options['id_shop'];
      }
      if (isset($options['id_group_shop'])) {
        $url .= '&id_group_shop='.$options['id_group_shop'];
      }
    } else {
      throw new PrestashopWebServiceException('Bad parameters given');
    }
    $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'POST', CURLOPT_POSTFIELDS => $xml));

    $this->checkRequest($request);
    return $this->parseXML($request['response']);
  }

  /**
   * Retrieve (GET) a resource
   * <p>Unique parameter must take : <br><br>
   * 'url' => Full URL for a GET request of WebService (ex: http://mystore.com/api/customers/1/)<br>
   * OR<br>
   * 'resource' => Resource name,<br>
   * 'id' => ID of a resource you want to get<br><br>
   * </p>
   * <code>
   * <?php
   * require_once('./PrestaShopWebService.php');
   * try
   * {
   * $ws = new PrestaShopWebService('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
   * $xml = $ws->get(array('resource' => 'orders', 'id' => 1));
   *    // Here in $xml, a SimpleXMLElement object you can parse
   * foreach ($xml->children()->children() as $attName => $attValue)
   *    echo $attName.' = '.$attValue.'<br />';
   * }
   * catch (PrestashopWebServiceException $ex)
   * {
   *    echo 'Error : '.$ex->getMessage();
   * }
   * ?>
   * </code>
   * @param array $options Array representing resource to get.
   * @return SimpleXMLElement status_code, response
   * @throws PrestashopWebServiceException
   */
  public function get($options)
  {
    $this->method = 'GET';
    if (isset($options['url'])) {
      $url = $options['url'];
    } elseif (isset($options['resource'])) {
      $url = $this->url.'/api/'.$options['resource'];
      $url_params = array();
      if (isset($options['id'])) {
        $url .= '/'.$options['id'];
      }

      $params = array('filter', 'display', 'sort', 'limit', 'id_shop', 'id_group_shop','date', 'price');
      foreach ($params as $p) {
        foreach ($options as $k => $o) {
          if (strpos($k, $p) !== false) {
            $url_params[$k] = $options[$k];
          }
        }
      }
      if (count($url_params) > 0) {
        $url .= '?'.http_build_query($url_params);
      }
    } else {
      throw new PrestashopWebServiceException('Bad parameters given');
    }

    $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'GET'));

    $this->checkRequest($request);// check the response validity
    return $this->parseXML($request['response']);
  }

  /**
   * Head method (HEAD) a resource
   *
   * @param array $options Array representing resource for head request.
   * @return SimpleXMLElement status_code, response
   * @throws PrestashopWebServiceException
   */
  public function head($options)
  {
    $this->method = 'HEAD';
    if (isset($options['url'])) {
      $url = $options['url'];
    } elseif (isset($options['resource'])) {
      $url = $this->url.'/api/'.$options['resource'];
      $url_params = array();
      if (isset($options['id'])) {
        $url .= '/'.$options['id'];
      }

      $params = array('filter', 'display', 'sort', 'limit');
      foreach ($params as $p) {
        foreach ($options as $k => $o) {
          if (strpos($k, $p) !== false) {
            $url_params[$k] = $options[$k];
          }
        }
      }
      if (count($url_params) > 0) {
        $url .= '?'.http_build_query($url_params);
      }
    } else {
      throw new PrestashopWebServiceException('Bad parameters given');
    }
    $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'HEAD', CURLOPT_NOBODY => true));
    $this->checkRequest($request);// check the response validity
    return $request['header'];
  }

  /**
   * Edit (PUT) a resource
   * <p>Unique parameter must take : <br><br>
   * 'resource' => Resource name ,<br>
   * 'id' => ID of a resource you want to edit,<br>
   * 'putXml' => Modified XML string of a resource<br><br>
   * Examples are given in the tutorial</p>
   * @param array $options Array representing resource to edit.
   * @return SimpleXMLElement
   * @throws PrestashopWebServiceException
   */
  public function edit($options)
  {
    $this->method = 'PUT';
    $xml = '';
    if (isset($options['url'])) {
      $url = $options['url'];
    } elseif ((isset($options['resource'], $options['id']) || isset($options['url'])) && $options['putXml']) {
      if (isset($options['url'])) {
        $url = $options['url'];
      } else {
        $url = $this->url.'/api/'.$options['resource'].'/'.$options['id'];
      }
      $xml = $options['putXml'];
      if (isset($options['id_shop'])) {
        $url .= '&id_shop='.$options['id_shop'];
      }
      if (isset($options['id_group_shop'])) {
        $url .= '&id_group_shop='.$options['id_group_shop'];
      }
    } else {
      throw new PrestashopWebServiceException('Bad parameters given');
    }

    $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'PUT', CURLOPT_POSTFIELDS => $xml));
    $this->checkRequest($request);// check the response validity
    return $this->parseXML($request['response']);
  }

  /**
   * Delete (DELETE) a resource.
   * Unique parameter must take : <br><br>
   * 'resource' => Resource name<br>
   * 'id' => ID or array which contains IDs of a resource(s) you want to delete<br><br>
   * <code>
   * <?php
   * require_once('./PrestaShopWebService.php');
   * try
   * {
   * $ws = new PrestaShopWebService('http://mystore.com/', 'ZQ88PRJX5VWQHCWE4EE7SQ7HPNX00RAJ', false);
   * $xml = $ws->delete(array('resource' => 'orders', 'id' => 1));
   *    // Following code will not be executed if an exception is thrown.
   *    echo 'Successfully deleted.';
   * }
   * catch (PrestashopWebServiceException $ex)
   * {
   *    echo 'Error : '.$ex->getMessage();
   * }
   * ?>
   * </code>
   * @param array $options Array representing resource to delete.
   * @return bool
   */
  public function delete($options)
  {
    $this->method = 'DELETE';
    if (isset($options['url'])) {
      $url = $options['url'];
    } elseif (isset($options['resource']) && isset($options['id'])) {
      if (is_array($options['id'])) {
        $url = $this->url.'/api/'.$options['resource'].'/?id=['.implode(',', $options['id']).']';
      } else {
        $url = $this->url.'/api/'.$options['resource'].'/'.$options['id'];
      }
    }
    if (isset($options['id_shop'])) {
      $url .= '&id_shop='.$options['id_shop'];
    }
    if (isset($options['id_group_shop'])) {
      $url .= '&id_group_shop='.$options['id_group_shop'];
    }
    $request = $this->executeRequest($url, array(CURLOPT_CUSTOMREQUEST => 'DELETE'));
    $this->checkRequest($request);// check the response validity
    return true;
  }
}
