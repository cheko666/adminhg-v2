<?php

namespace App\Vendor\Protechstudio\PrestashopWebService;

use Illuminate\Support\ServiceProvider;


class PrestashopWebServiceProvider extends ServiceProvider
{
  protected $defer = true;

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    $this->publish();
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {

    $this->app->singleton(PrestashopWebServiceOverride::class, function () {
      return new PrestashopWebServiceOverride(
          config('prestashop-webservice.url'),
          config('prestashop-webservice.token'),
          config('prestashop-webservice.debug')
      );
    });
  }

  public function provides()
  {
    return ['PrestashopWebServiceOverride'];
  }

  private function publish()
  {
    $this->publishes([
        __DIR__ . '/config.php' => config_path('prestashop-webservice.php'),
    ], 'config');
  }

}
