<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 21/01/20
 * Time: 14:39
 */

namespace App\Repositories;


interface RepositoryInterface
{
  const TYPE_INT     = 1;
  const TYPE_BOOL    = 2;
  const TYPE_STRING  = 3;
  const TYPE_FLOAT   = 4;
  const TYPE_DATE    = 5;
  const TYPE_HTML    = 6;
  const TYPE_NOTHING = 7;
  const TYPE_SQL     = 8;  
  public function index();

  public function create(array  $data);

  public function update(array $data, $id);

  public function delete($id);

  public function find($id);

}