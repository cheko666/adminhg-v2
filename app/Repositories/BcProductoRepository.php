<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 16/05/20
 * Time: 10:40
 */

namespace App\Repositories;

use App\Models\Codigo;
use App\Models\BcProducto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class BcProductoRepository implements RepositoryInterface
{
  protected $model;
  protected $campos_obligatorios = [];
  protected $_query = '';
  protected $campos_compara = ['todo'=>false,'precio'=>false,'descuento'=>false,'iva'=>false,'marca_id'=>false,'modelo'=>false,'peso'=>false,'ancho'=>false,'altura'=>false,'fondo'=>false];
  public $codigo;
  public function __construct(BcProducto $bcProducto){
    $this->model = $bcProducto;
    $this->codigo = $bcProducto->reference;
    $this->campos_obligatorios = config('adminhg-bc.objetos.producto.campos.obligatorios');
  }

  public static $definition = [
    'table' => 'lsc_producto',
    'primary' => 'id',
    'multilang' => false,
    'multi_system' => true,
    'fields' => [
        'reference' => ['type' => self::TYPE_STRING, 'validate' => 'isReference', 'size' => 64],
    ]
  ];

  public function index()
  {
    // TODO: Implement all() method.
    return $this->model->all();
  }

  public function create(array  $data)
  {
    // TODO: Implement create() method.
    return $this->model->create($data);
  }

  public function update(array $data, $id)
  {
    // TODO: Implement update() method.
    return $this->model->where('id', $id)->update($data);
  }

  public function delete($id)
  {
    // TODO: Implement delete() method.
    return $this->model->destroy($id);
  }

  public function find($id)
  {
    // TODO: Implement find() method.
    if (null == $collect = $this->model->find($id)) {
      throw new ModelNotFoundException($this->model->getTable() . " not found");
    }
    return $collect;
  }
  public function allErroresCapturaDatosProducto($fecha = '2023-02-11',$codigos = false)
  {
    $this->_query = $this->queryErroresCapturaDatosProducto($fecha,$codigos);
    $data = DB::select(DB::raw($this->_query));
    
    if (count($data) > 0)
    {
      // Obtenemos campos con errores
      foreach ($data as &$registro)
      {
          $campos_errores = array();
          $errores = array();

          $registro_log['id_p'] = $registro->id_p;
          $registro_log['id_pa'] = $registro->id_pa;
          $registro_log['descripcion'] = $registro->descripcion;

          // Checar errores
          foreach ($this->campos_obligatorios as $key)
          {
            $campo = $key;
            
            if ($registro->$campo == '' || $registro->$campo == 0) {
                $campos_errores[] = $key;
                $errores[$key][] = $registro->$campo;
            }  
          }
          
          $registro_log['errores'] = json_encode($errores);

          $registro->campos_error = implode(',', $campos_errores);
          $registro->errores = $errores;
          
          $log[$registro->codigo] = $registro_log;

          collect($registro);
      }

    }

    dd($data);
    return $collection = collect($data);
  }
  public function queryErroresCapturaDatosProducto($fecha_desde,$codigos = null)
  {
    $where_codigos = '';
    $where_codigos = !$codigos ? " " : "AND codigo IN (". $codigos.")";

    $where_fecha = '';
    $where_fecha = !$fecha_desde ? " " : "AND A.date_add > '".$fecha_desde."' ";

    $query = "
    SELECT 
    A.`serie`
    , A.`reference` AS codigo
    , A.`description` AS descripcion    
    , A.`id_supplier` AS no_proveedor
    , A.`retail_product_code` AS categoria
    , A.`special_group_code` AS marca
    , A.`vat_posting_group` AS iva
    , A.`supplier_reference` AS modelo
    , A.`registro_sanitario`
    , A.`sat_c_prodserv` AS cod_sat
    , A.`sat_c_unidad` AS cod_unidad_sat
    , IF(A.`width`=0,'',ROUND(A.`width`,2)) AS ancho
    , IF(A.`height`=0,'',ROUND(A.`height`,2)) AS alto
    , IF(A.`depth`=0,'',ROUND(A.`depth`,2)) AS fondo
    , IF(A.`weight`=0,'',ROUND(A.`weight`,3)) AS peso
    , IF(A.`peso_v`=0,'',ROUND(A.`peso_v`,3)) AS peso_vol
    , IF(A.`price`=0,'',ROUND(A.`price`,2)) AS precio
    , A.`unity` AS unidad_medida_base
    , IF(A.`bloqueo_ecommerce` = 1, 'S','N') AS bloqueo_ec
    , IF(A.`bloqueo_compra` = 1, 'S','N') AS bloqueo_compra
    , IF(A.`bloqueo_venta` = 1, 'S','N') AS bloqueo_venta
    , IF(A.`bloqueo` = 1, 'S','N') AS bloqueo
    , IF(A.`restringido` > 0, 'S','N') AS restringido
    , DATE(A.`date_add`) AS fecha_crea
    , DATE(A.`date_upd`) AS fecha_modif
    , IF(ps.`active` = 1, 'S','N') AS activo_web
    , IFNULL(DATE(ps.`date_add`),'') AS fecha_crea_ps
    , A.`id_p`
    , IFNULL(A.`id_pa`,0) AS id_pa

    FROM production_presta.ps0516_lsc_producto A
    JOIN production_presta.ps0516_bc_notificacion_producto_nuevo B ON CAST(B.codigo AS UNSIGNED) = A.id AND B.fecha_notificacion IS NULL
    LEFT JOIN production_presta.ps0516_product AS ps ON ps.id_product = A.id_p
    
    WHERE 1  
    AND A.`reference` NOT IN ('001001','999998')
    ".$where_codigos.$where_fecha."
    AND A.description NOT LIKE '%PRODUCTO PRUEBA%'
    AND A.description <> ''
    AND A.bloqueo_ecommerce = 0
    AND A.bloqueo_compra = 0 
    AND A.bloqueo_venta = 0
    AND A.restringido = 0
    AND ( A.`description` = '' OR A.`price` = 0 OR A.`special_group_code` = '' OR A.`retail_product_code` = '' OR A.`width` = 0 OR A.`height` = 0 OR A.`depth` = 0 OR A.`weight` = 0 OR A.`peso_v` = 0 )
    ";
  
    return $query;

  }

}