<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 16/05/20
 * Time: 10:40
 */

namespace App\Repositories;

use App\Models\Codigo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CodigoRepository implements CodigoRepositoryInteface
{
  protected $model;

  protected $_query = '';

  protected $campos_compara = ['precio'=>false,'descuento'=>false,'iva'=>false,'peso'=>false,'ancho'=>false,'altura'=>false,'fondo'=>false,/* 'marca_id'=>false, */'modelo'=>false];
  // protected $campos_compara = ['precio'=>false,'descuento'=>false,'iva'=>false];

  public function __construct(Codigo $codigo){
    $this->model = $codigo;
  }

  public function index()
  {
    // TODO: Implement all() method.
    return $this->model->all();
  }

  public function create(array  $data)
  {
    // TODO: Implement create() method.
    return $this->model->create($data);
  }

  public function update(array $data, $id)
  {
    // TODO: Implement update() method.
    return $this->model->where('id', $id)->update($data);
  }

  public function delete($id)
  {
    // TODO: Implement delete() method.
    return $this->model->destroy($id);
  }

  public function find($id)
  {
    // TODO: Implement find() method.
    if (null == $collect = $this->model->find($id)) {
      throw new ModelNotFoundException($this->model->getTable() . " not found");
    }
    return $collect;
  }

  public function listDiferenciasAdminhgPs() {

    $data = $this->_query = DB::select(DB::raw($this->queryVerificarDiferenciasAdminhgPs()));

    $log = array();

    if (count($data) > 0) {

      // Obtenemos campos con diferencias
      foreach ($data as &$registro) {
        $campos_diferencias = array();
        $diferencias = array();

        $registro_log['id_product'] = $registro->id_product;
        $registro_log['id_product_attribute'] = $registro->id_product_attribute;

        // Checar diferencias
        foreach ($this->campos_compara as $key => $value) {
          if ($key == 'todo') continue;
          $campo_ps = 'ps_' . $key;
          if ($registro->$key != $registro->$campo_ps) {
            $campos_diferencias[] = $key;
            $diferencias[$key][] = $registro->$key;
            $diferencias[$key][] = $registro->$campo_ps;
          }
        }
        $registro_log['diferencias'] = json_encode($diferencias);


        $registro->campos_dif = implode(',', $campos_diferencias);
        $registro->diferencias = $diferencias;

        $log[$registro->codigo] = $registro_log;

        collect($registro);
      }

    }
    return $collection = collect($data);
  }

  public function listProductosNuevosFromDate($cant_periodo = 1 ,$periodo = 'd',$notificados = false,$codigos = false) {

    $date_from_now = getDateFromPeriodOfTime($cant_periodo,$periodo);

    $data = $this->_query = DB::select(DB::raw($this->queryProductosNuevosWeb()),[$date_from_now,$notificados ? 'NOT NULL': 'NULL']) ;

    if (count($data) > 0) {
      self::mapearData($data,__FUNCTION__);
    }

    return $collection = $data;
  }

  public function listProductosNuevos($codigos = false,$notificados = false) {

    $data = $this->_query = DB::select(DB::raw($this->queryProductosNuevosWeb($codigos))) ;

    if (count($data) > 0) {
      self::mapearData($data,__FUNCTION__);
    }

    return $collection = $data;
  }

  public function listEstatusInformacionProductosFromDate($codigos,$cant_periodo = 1 ,$periodo = 'd',$notificados = false) {

    $date_from_now = getDateFromPeriodOfTime($cant_periodo,$periodo);

    $data = $this->_query = DB::select(DB::raw($this->queryProductosNuevosWeb($codigos)),[$date_from_now,$notificados ? 'NOT NULL': 'NULL']) ;

    if (count($data) > 0) {
      self::mapearData($data,__FUNCTION__);
    }

    return $collection = $data;
  }

  public function listarProductosFaltantesInformacion() {

//    $fecha_desde_n_semanas = getDateFromPeriodOfTime($cant_periodo,$periodo);

    $data = $this->_query = DB::select(DB::raw($this->queryProductosFaltantesInformacion())) ;

    if (count($data) > 0) {
      self::mapearData($data,__FUNCTION__);
    }

    return $collection = $data;
  }

  public function listEstatusInformacionProductos($cant=null,$periodo = null) {

    if($periodo && $cant) {
      $data = $this->_query = DB::select(DB::raw($this->queryEstatusInformacionProductos($cant,$periodo))) ;
    }
    $data = $this->_query = DB::select(DB::raw($this->queryEstatusInformacionProductos($cant,$periodo))) ;

    if (count($data) > 0) {
      self::mapearData($data,__FUNCTION__);
    }

    return $collection = $data;
  }

  public function corregirDiferenciasAdminhgPs() {

    return DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoSimple())) && DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoCombinaciones()))
        ? true : false;
  }

  public function mapearData($data,$function) {
    $registro = '';
    $campos_booleanos = array('activo','estado','cod_activo','agotar_exis','baja','bloqueo_ec','ventaWeb','tieneInfo') ;

    // Obtenemos campos con diferencias
    foreach ($data as &$registro) {
      foreach($registro as $key => $value) {

        $key_html = $key.'_html';
        $uri = 'https://www.lancetahg.com.mx/';

        switch($key) {
          case $value == 1 && is_integer($value):
            if(in_array($key,$campos_booleanos)) {
              $registro->$key = 'S';
            } else {
              $registro->$key = 1;
            }
            $registro->$key_html = '<img src="'.$uri.'img/os/2.gif">';
            break;
          case $value == 0 && is_integer($value):
            if(in_array($key,$campos_booleanos)) {
              $registro->$key = 'N';
            } else {
              $registro->$key = 0;
            }
            $registro->$key_html = '<img src="'.$uri.'img/os/6.gif">';
            break;
          case $key == 'codigo':
            $registro->codigo = setCodigoWithZeroFill($registro->codigo);
            break;
          case $key == 'id_image':
            $registro->url_img_admin = $registro->id_image ? 'http://media.lancetahg.com.mx/p/' . $registro->id_p . '-cart_default.jpg' : 'http://media.lancetahg.com.mx/p/0.jpg';
            if($function == 'listarProductosFaltantesInformacion') {
              $registro->url_img = $uri . 'img/p/mx-default-cart_default.jpg';

            } else {
              $digits = $value ? str_split($value) : null;
              $registro->url_img = $digits ? $uri . 'img/p/' . implode('/',$digits) . '/' . $registro->id_image . '-cart_default.jpg' : $uri . 'img/p/mx-default-cart_default.jpg';
            }
            break;
          case $key == 'url_web':
            $registro->url_web = 'https://www.lancetahg.com.mx/productos/'.$registro->id_p.'/'.$registro->link_rewrite.$this->getAnchor($registro->id_p, $registro->id_pa);
        }
      }
    }

    return $registro;

  }

  /**
   *  Esta es la consulta que se realiza actualmente para productos nuevos de la web
   * @return string
   */
  public function queryProductosNuevosWeb($codigos) {
    $where_codigos = '';
    $where_codigos = !$codigos ? " " : " AND A.codigo IN (". $codigos.")";
    
    $query = "
    SELECT A.codigo
    , B.serie
    , @id_p :=  B.id_p AS id_p
    , @id_pa :=  B.id_pa AS id_pa
    , A.fecha_notificacion
    , B.description AS descripcion
    , B.special_group_code AS marca
    , CAST(IFNULL(psp.active,0) AS unsigned) AS activo
    , B.state AS estado
    , B.active AS cod_activo
    , B.bloqueo_compra AS agotar_exis
    , B.bloqueo_venta AS baja
    , B.bloqueo_ecommerce AS bloqueo_ec
    , !B.restringido AS ventaWeb
    , IF( psp.description = '' OR LENGTH(psp.description) < 30, FALSE,TRUE ) tieneInfo
    , psp.link_rewrite
    , CONCAT('https://www.lancetahg.com.mx/productos/',A.id_p,'/',psp.link_rewrite) AS url_web
    , CONCAT('https://www.lancetahg.com.mx/img/tmp/product_mini_',A.id_p,'_1.jpg') AS url_img
    , psp.id_image
    , psp.date_add
    
    FROM production_presta.ps0516_bc_notificacion_producto_nuevo A
    JOIN production_presta.ps0516_lsc_producto B ON ( A.codigo = B.reference COLLATE utf8_unicode_ci )
    LEFT JOIN (
      SELECT p.id_product, IFNULL(pa.id_product_attribute,0) AS id_product_attribute, p.active, (IFNULL(pa.reference,p.reference) COLLATE utf8_unicode_ci) AS reference
        , CONCAT_WS(' ', pl.name,IFNULL(pa.atributos,'')) AS name_attr
        , pl.name
        , pl.description
        , pl.link_rewrite
        , IFNULL(
          (SELECT ps0516_product_attribute_image.id_image FROM production_presta.ps0516_product_attribute_image JOIN production_presta.ps0516_image ON ps0516_image.id_image = ps0516_product_attribute_image.id_image WHERE ps0516_product_attribute_image.id_product_attribute = @id_pa ORDER BY ps0516_image.position LIMIT 1)
          ,
          (SELECT ps0516_image.id_image FROM production_presta.ps0516_image WHERE id_product = @id_p AND ps0516_image.cover = 1)
        ) AS id_image    
        , IFNULL(pa.atributos,'') AS atributos
        , p.date_add
        FROM production_presta.ps0516_product p
        JOIN production_presta.ps0516_product_lang pl ON pl.id_product = p.id_product AND pl.id_lang = 2
        LEFT JOIN (
        SELECT pa1.id_product_attribute, pa1.reference, pa1.id_product, atr.atributos
        FROM production_presta.ps0516_product_attribute pa1
        LEFT JOIN (# PARA NOMBRE PRODUCTO + ATRIBUTO(S)
          SELECT 
                pac.id_product_attribute
                #, GROUP_CONCAT(agl.name) AS atributos
                , GROUP_CONCAT(DISTINCT al.name ORDER BY a.position DESC SEPARATOR ' / ') AS atributos
          FROM production_presta.`ps0516_product_attribute_combination` pac
          JOIN production_presta.`ps0516_attribute` a ON pac.id_attribute = a.id_attribute
          JOIN production_presta.`ps0516_attribute_lang` al ON a.id_attribute = al.id_attribute AND al.id_lang = 2
          LEFT JOIN production_presta.`ps0516_attribute_group` ag ON a.id_attribute_group = ag.id_attribute_group
          JOIN production_presta.`ps0516_attribute_group_lang` agl ON agl.id_attribute_group = ag.id_attribute_group AND agl.id_lang = 2
                GROUP BY id_product_attribute
        ) atr ON atr.id_product_attribute = pa1.id_product_attribute
        
            ORDER BY id_product_attribute
        ) pa ON pa.id_product = p.id_product
    ) psp ON psp.reference = A.codigo

    WHERE 1".$where_codigos."
    AND B.description NOT LIKE '%PRODUCTO PRUEBA%'
    AND B.description <> ''
    AND B.`special_group_code` <> ''
    AND B.`retail_product_code` <> ''
    #AND A.id_p > 0
    #AND B.state = 1
    AND A.fecha_notificacion IS NULL
    ORDER BY A.codigo
    ";
    return $query;
  }
  public function queryEstatusInformacionProductos() {
    return "
      SELECT c.id 'codigo'
      , c.descripcion
      , m.nombre marca
      , IFNULL(i.id_image,NULL) id_image
      , IF(i.id_image IS NULL,FALSE,TRUE) tieneFoto
      , IF( p.descripcion = '' OR LENGTH(p.descripcion) < 30, FALSE,TRUE ) tieneInfo
      , IF(pp.active = 1,TRUE,FALSE) activo
      , c.por_agotar
      , IF(pp.available_for_order = 1,TRUE,FALSE) ventaWeb
      #, CONCAT('https://www.lancetahg.com.mx/',IF(i.id IS NULL,'img/p/mx',p.id),'-cart_default.jpg') url_img
      , CONCAT('https://www.lancetahg.com.mx/productos/',p.id,'/',p.url) url

      FROM lanceta_bd_ec.codigos c
      LEFT JOIN lanceta_bd_ec.marcas m ON m.id = c.marca_id
      LEFT JOIN lanceta_bd_ec.productos p ON p.id = c.producto_id
      JOIN production_presta.ps0516_product pp ON pp.id_product = p.id
      #LEFT JOIN lanceta_bd_ec.imagenes i ON p.id = i.producto_id
      LEFT JOIN production_presta.ps0516_image_shop i ON (i.id_product = c.producto_id AND i.cover = 1 AND i.id_shop = 1)

      WHERE c.deleted_at IS NULL
      AND c.precio > 0
      AND c.baja = 'N'
      #AND c.por_agotar = 'N'
      AND (c.descripcion != '' OR c.marca_id != 0 OR c.precio != 0)
      AND c.id IN (
        SELECT DISTINCT codigo_id FROM lanceta_bd_ec.codigos_log
        WHERE movimiento = 'A'
      )
      AND c.created_at > ?
      AND (
		    (p.descripcion = '' OR LENGTH(p.descripcion) < 30)
        OR i.id_image IS NULL
      )

      ORDER BY c.id
    ";
  }

  public function queryVerificarDiferenciasAdminhgPs()
  {
    return "
      select
      LPAD(c.id,6,'0') as codigo,
      @id_p := c.producto_id as id_product,
      #@id_p,
      ps.id_product_attribute,

      c.precio,
      @prec_base_ps := ps.ps_precio ps_precio,
      @prec_base_pas := ps.ps_pas_precio ps_pas_precio,
      c.precio_neto,
      ps.ps_precio_neto,

      c.descuento,
      ps.ps_descuento,
      #ps.reduction,
      CAST( ifnull(ps.id_specific_price,0) AS UNSIGNED ) id_specific_price,

      c.iva,
      ps.ps_iva,

      c.baja,
      c.por_agotar,

      c.principal principal,

      c.peso,
      ps.ps_peso,

      c.ancho ancho,
      ps.ps_ancho,

      c.altura altura,
      ps.ps_altura,

      c.fondo fondo,
      ps.ps_fondo,

      c.marca_id,
      ps.ps_marca_id,
      c.nombre_marca,
      ps.ps_nombre_marca,

      c.modelo,
      ps.ps_modelo,

      ps.active,

      case
        when c.precio <> ps.ps_precio then 'precio'
        when c.descuento <> ps.ps_descuento then 'descuento'
        when c.peso <> ps.ps_peso then 'peso'
        when c.ancho <> ps.ps_ancho then 'ancho'
        when c.altura <> ps.ps_altura then 'altura'
		    when c.fondo <> ps.ps_fondo then 'fondo'
        when c.iva <> ps.ps_iva then 'iva'
        when c.marca_id <> ps.ps_marca_id then 'id marca'
        when c.modelo <> ps.ps_modelo then 'modelo'
      end
      as campo_dif

      ############
      # AdminHG
      ############
      from (
        select
        P.id,
        P.producto_id,
        P.precio as precio,
        round((P.precio * (1-(P.descuento / 100)) * if(P.iva = 'S',1.16,1)),2) as precio_neto,
        P.descuento,
        P.peso,
          round(P.ancho,2) ancho,
          round(P.altura,2) altura,
          round(P.fondo,2) fondo,
        if(P.iva = 'S', 16,0) as iva,
        P.baja,
        P.por_agotar,
        P.marca_id,
          P.modelo,
        P.principal,
        ifnull(m.nombre,'Sin Marca') as nombre_marca

          from lanceta_bd_ec.codigos P
        left join lanceta_bd_ec.marcas m on m.id = P.marca_id

        where P.producto_id is not null
        and P.producto_id > 0
        #and P.baja = 'N'
        #and P.producto_id = 604

      ) as c

      ############
      # PRESTASHOP
      ############
      join (
        select ifnull(pa.reference,p2.reference) as reference,
        p2.id_product as id_product,
        p2.id_manufacturer ps_marca_id,
        m.name as ps_nombre_marca,
        round(p2.price,2) as price,
        pa.id_product_attribute,
        ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
		    round(ifnull(pa.price,0),2) pa_price,
        round(ifnull(pas.price,0),2) pas_price,
        round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
        pa.default_on ,
        if( cc.principal = 1,round(p2.width,2), cc.ancho ) as ps_ancho,
        if( cc.principal = 1,round(p2.height,2), cc.altura ) as ps_altura,
        if( cc.principal = 1,round(p2.depth,2), cc.fondo ) as ps_fondo,
        CAST(round(ifnull(sp.descuento,0)) AS UNSIGNED) as ps_descuento,
        ifnull(sp.id_specific_price,0) id_specific_price,
        round(ifnull(sp.reduction,0),2) reduction,
        round( ifnull(pa.price,0) + p2.price,2 ) as ps_precio,
        round( ifnull(pas.price,0) + p2.price,2 ) as ps_pas_precio,
        round( (ifnull(pa.price,0) + p2.price) * (1-(ifnull(sp.reduction,0))) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
        if(p2.id_tax_rules_group = 1,16,0) as ps_iva,
        p2.active

        from production_presta.ps0516_product p2
        left join production_presta.ps0516_manufacturer m ON p2.id_manufacturer = m.id_manufacturer
        left join (
          select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference, ps0516_product_attribute.default_on
          from production_presta.ps0516_product_attribute
        ) pa on pa.id_product = p2.id_product
        left join production_presta.ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
        left join (
        SELECT
          *,
          ps0516_specific_price.reduction*100 as descuento
          FROM production_presta.ps0516_specific_price
          where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
        ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
        left join lanceta_bd_ec.codigos cc ON cc.id = ifnull(pa.reference,p2.reference)

        #where p2.active = 1
        #and pa.id_product_attribute is null
        #and p2.id_product = 604

      ) as ps on c.id = ps.reference

      where (
        c.precio <> ps.ps_precio
        #or c.precio <> ps.ps_pas_precio
        or c.descuento <> ps.ps_descuento
        or c.iva <> ps.ps_iva
        or c.peso <> ps.ps_peso
        or c.ancho <> ps.ps_ancho
        or c.altura <> ps.ps_altura
        or c.fondo <> ps.ps_fondo
        #or c.marca_id <> ps.ps_marca_id
        or c.modelo <> ps.ps_modelo
      )
      #and id_product = 4075
      #and left(c.id,3) in (60,250,288,2992)

      order by id_product, codigo

      #limit 10
    ";
  }

  public function queryCorregirDiferenciasAdminhgPsProductoSimple()
  {
    return "
      update production_presta.ps0516_product A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        round(p.price,2) as p_price,
        round(ifnull(pa.price,0),2) as pa_price,

        c.iva,

        c.peso peso_mg,
        round(ifnull(pa.weight,0) + p.weight,3) as weight_ps,
        round(p.weight,3) as p_weight,
        round(ifnull(pa.weight,0),3) as pa_weight,

        c.ancho ancho_mg,
        round(p.width,3) width,

        c.altura altura_mg,
        round(p.height,3) height,

        c.fondo fondo_mg,
        round(p.depth,3) depth,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        c.marca_id,
        p.id_manufacturer,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null
        #left join lanceta_bd_ec.codigos c2 on c2.id = concat(LPAD(c.Serie,3,0),LPAD(c.Producto,3,0))

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          #or c.marca_id <> p.id_manufacturer
          or IF(c.iva = 'S',16,0) <> if(p.id_tax_rules_group = 1,16,0)
          #or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
          #or c.ancho <> round(p.width,3)
          #or c.altura <> round(p.height,3)
          #or c.fondo <> round(p.depth,3)
          #or c.marca_id <> p.id_manufacturer
          or IF(c.iva = 'S',16,0) <> if(p.id_tax_rules_group = 1,16,0)
        )
        #and pa.id_product_attribute is null
      ) as B on A.id_product = B.id_product

      set
      A.id_tax_rules_group = IF(B.iva = 'S',1,2),
      A.price = B.precio_mg,
      A.supplier_reference = B.modelo
      
      #A.weight = B.peso_mg,
      #A.width = B.ancho_mg,
      #A.height = B.altura_mg,
      #A.depth = B.fondo_mg,
      #A.id_manufacturer = B.marca_id
      ;
    ";
  }

  public function queryProductosFaltantesInformacion() {

    return "
      SELECT npn.codigo 'codigo'
      , c.descripcion
      , m.nombre marca
      , c.modelo
      , c.por_agotar
      , CONCAT('https://www.lancetahg.com.mx/productos/',p.id,'/',p.url) url
      , IF(pp.active = 1,TRUE,FALSE) activo
      , IF(pp.available_for_order = 1,TRUE,FALSE) ventaWeb
      , IFNULL(i.id,NULL) id_image
	    , IF( p.descripcion IS NULL OR p.descripcion = '' OR LENGTH(p.descripcion) < 30, FALSE,TRUE ) tieneInfo
      , IF(i.id IS NULL,FALSE,TRUE) tieneFoto
      , IF(c.peso = 0 OR c.ancho = 0 OR c.altura = 0 OR c.fondo = 0,FALSE,TRUE) tieneDimensiones
      , p.created_at
      , npn.*
      , cl.created_at fecha_alta

      FROM lanceta_bd_ec.notificacion_producto_nuevo npn
      JOIN (
        SELECT cla.* FROM lanceta_bd_ec.codigos_log cla WHERE cla.id IN (
          SELECT clb.id FROM lanceta_bd_ec.codigos_log clb WHERE clb.created_at = (
            SELECT MAX(clc.created_at) FROM lanceta_bd_ec.codigos_log clc WHERE clc.codigo_id = clb.codigo_id AND clc.movimiento = 'A'
            )
        )
      ) cl ON cl.codigo_id = npn.codigo
      JOIN lanceta_bd_ec.codigos c ON npn.codigo = LPAD(c.id,6,'0')
      LEFT JOIN lanceta_bd_ec.marcas m ON m.id = c.marca_id
      LEFT JOIN lanceta_bd_ec.productos p ON p.id = c.producto_id
      LEFT JOIN production_presta.ps0516_product pp ON pp.id_product = p.id
      LEFT JOIN lanceta_bd_ec.imagenes i ON (i.producto_id = c.producto_id AND i.deleted_at IS NULL)

      WHERE c.baja = 'N'
      AND (
        IF( p.descripcion IS NULL OR p.descripcion = '' OR LENGTH(p.descripcion) < 30, FALSE,TRUE ) = FALSE OR
        IF(i.id IS NULL,FALSE,TRUE) = FALSE OR
        IF(c.peso = 0 OR c.ancho = 0 OR c.altura = 0 OR c.fondo = 0,FALSE,TRUE) = FALSE
        )

      ORDER BY codigo DESC
      ;
    ";
  }

  public function queryCorregirDiferenciasAdminhgPsProductoCombinaciones()
  {
    return "
      update production_presta.ps0516_product_attribute A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        p.price as p_price,
        ifnull(pa.price,0) as pa_price,
        c.precio - p.price as pa_price_ok,

        @pweight := p.weight as p_weight,
        ifnull(pa.weight,0) as pa_weight,
        c.peso peso_mg,
        pa.ps_weight as weight_ps,
        c.peso - p.weight as pa_weight_ok,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN (
			    SELECT *,round( (@pweight+weight),3 ) ps_weight
          FROM production_presta.ps0516_product_attribute
        ) pa on p.id_product = pa.id_product
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          #or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
        )
        and pa.id_product_attribute is not null
      ) as B on A.id_product_attribute = B.id_product_attribute
      #inner join production_presta.ps0516_product_attribute_shop C on C.id_product_attribute = A.id_product_attribute

      set
      A.price = B.pa_price_ok,
      #A.weight = B.pa_weight_ok,
      A.supplier_reference = B.modelo
      ;
    ";
  }

  public function deletePsSpecificPriceById($id) {

    return DB::delete('DELETE FROM production_presta.ps0516_specific_price WHERE id_specific_price = ?',[$id]);

  }

  public function createPsSpecificFromAdminhg($descuento,$id_product,$id_product_attribute) {
    return DB::insert('
      INSERT INTO production_presta.ps0516_specific_price (id_specific_price,id_specific_price_rule,id_cart,id_product,id_shop,id_shop_group,id_currency,id_country,id_group,id_customer,id_product_attribute,price,from_quantity,reduction,reduction_tax,reduction_type,`from`,`to`)
      VALUES (null,0,0,?,0,0,0,0,0,0,?,-1,1,?,1,"percentage","0000-00-00 00:00:00","0000-00-00 00:00:00")
      ',[$id_product,$id_product_attribute,$descuento/100]);
  }

  public function updatePsSpecificPriceById($id,$descuento) {

    return DB::update('UPDATE production_presta.ps0516_specific_price SET reduction = ? WHERE id_specific_price =?',[$descuento/100,$id]);
  }
  /**
   * Get the combination url anchor of the product
   *
   * @param int $id_product_attribute
   * @return string
   */
  public function getAnchor($id_product,$id_product_attribute)
  {
    $attributes = $this->getAttributesParams($id_product, $id_product_attribute);

    $anchor = '#';
    $sep = '-';
    foreach ($attributes as &$a) {

      foreach ($a as &$b) {
        $b = Str::slug($b,'_');
      }
      $anchor .= '/'.(isset($a->id_attribute) && $a->id_attribute ? (int)$a->id_attribute.$sep : '').$a->group.$sep.$a->name;
    }

    return $anchor;
  }

  public function getAtributosNombreValor($id_product,$id_product_attribute)
  {
    $attributes = $this->getAttributesParams($id_product, $id_product_attribute);
    $sep = ' ';

    foreach ($attributes as &$a) {

      return $a->group.$sep.$a->name;
    }

  }

  public function getAttributesParams($id_product, $id_product_attribute)
  {
    $q = '
    SELECT a.`id_attribute`, a.`id_attribute_group`, al.`name`, agl.`name` as `group`
    , pa.id_product_attribute
    FROM `production_presta`.`ps0516_attribute` a
    LEFT JOIN `production_presta`.`ps0516_attribute_lang` al ON (al.`id_attribute` = a.`id_attribute` AND al.`id_lang` = 2)
    LEFT JOIN `production_presta`.`ps0516_product_attribute_combination` pac ON (pac.`id_attribute` = a.`id_attribute`)
    LEFT JOIN `production_presta`.`ps0516_product_attribute` pa ON (pa.`id_product_attribute` = pac.`id_product_attribute`)
    LEFT JOIN `production_presta`.`ps0516_attribute_group_lang` agl ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = 2)
    WHERE pa.`id_product` = '.(int)$id_product.'
      AND pac.`id_product_attribute` = '.(int)$id_product_attribute.'
      AND agl.`id_lang` = 2
    ';
    
    $r = DB::select(DB::raw($q));
    return $r;

  }
}