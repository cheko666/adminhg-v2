<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 21/01/20
 * Time: 16:19
 */

namespace App\Repositories;


use App\Models\Producto;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductoRepository implements ProductoRepositoryInterface
{
  protected $model;

  public function __construct(Producto $producto){
    $this->model = $producto;
  }

  public function index()
  {
    // TODO: Implement all() method.
    return $this->model->all();
  }

  public function create(array  $data)
  {
    // TODO: Implement create() method.
    return $this->model->create($data);
  }

  public function update(array $data, $id)
  {
    // TODO: Implement update() method.
    return $this->model->where('id', $id)->update($data);
  }

  public function delete($id)
  {
    // TODO: Implement delete() method.
    return $this->model->destroy($id);
  }

  public function find($id)
  {
    // TODO: Implement find() method.
    if (null == $collect = $this->model->find($id)) {
      throw new ModelNotFoundException($this->model->getTable() . " not found");
    }
    return $collect;
  }

  public function encontrarMasActuales()
  {
    return $this->model->where('id','<', 100)->orderBy('descripcion')->get();
  }
}