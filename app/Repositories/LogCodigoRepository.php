<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 16/05/20
 * Time: 10:40
 */

namespace App\Repositories;

use App\Models\Codigo;
use App\Models\LogCodigo;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class LogCodigoRepository implements LogCodigoRepositoryInteface
{
  protected $model;

  protected $_query = '';

  protected $campos_compara = ['todo'=>false,'precio'=>false,'descuento'=>false,'iva'=>false,'marca_id'=>false,'modelo'=>false,'peso'=>false,'ancho'=>false,'altura'=>false,'fondo'=>false];

  public function __construct(Codigo $codigo, LogCodigo $logCodigo){
    $this->model = $logCodigo;
    $this->codigo = $codigo;
  }

  public function index()
  {
    // TODO: Implement all() method.
    return $this->model->all();
  }

  public function create(array  $data)
  {
    // TODO: Implement create() method.
    return $this->model->create($data);
  }

  public function update(array $data, $id)
  {
    // TODO: Implement update() method.
    return $this->model->where('id', $id)->update($data);
  }

  public function delete($id)
  {
    // TODO: Implement delete() method.
    return $this->model->destroy($id);
  }

  public function find($id)
  {
    // TODO: Implement find() method.
    if (null == $collect = $this->model->find($id)) {
      throw new ModelNotFoundException($this->model->getTable() . " not found");
    }
    return $collect;
  }

  public function listDiferenciasAdminhgPs() {

    $data = $this->_query = DB::select(DB::raw($this->codigo->queryVerificarDiferenciasAdminhgPs()));

    $log = array();

    if (count($data) > 0) {

      // Obtenemos campos con diferencias
      foreach ($data as &$registro) {
        $campos_diferencias = array();
        $diferencias = array();

        $registro_log['id_product'] = $registro->id_product;
        $registro_log['id_product_attribute'] = $registro->id_product_attribute;

        // Checar diferencias
        foreach ($this->campos_compara as $key => $value) {
          if ($key == 'todo') continue;
          $campo_ps = 'ps_' . $key;
          if ($registro->$key != $registro->$campo_ps) {
            $campos_diferencias[] = $key;
            $diferencias[$key][] = $registro->$key;
            $diferencias[$key][] = $registro->$campo_ps;
          }
        }
        $registro_log['diferencias'] = json_encode($diferencias);


        $registro->campos_dif = implode(',', $campos_diferencias);
        $registro->diferencias = $diferencias;

        $log[$registro->codigo] = $registro_log;

        collect($registro);
      }

    }
    return $collection = collect($data);
  }

  public function corregirDiferenciasAdminhgPs() {

    return DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoSimple())) && DB::statement(DB::raw($this->queryCorregirDiferenciasAdminhgPsProductoCombinaciones()))
        ? true : false;
  }

  public function queryVerificarDiferenciasAdminhgPs()
  {
    return "
      select
      LPAD(c.id,6,'0') as codigo,
      @id_p := c.producto_id as id_product,
      #@id_p,
      ps.id_product_attribute,

      c.precio,
      @prec_base_ps := ps.ps_precio ps_precio,
      @prec_base_pas := ps.ps_pas_precio ps_pas_precio,
      c.precio_neto,
      ps.ps_precio_neto,

      c.descuento,
      ps.ps_descuento,
      #ps.reduction,
      CAST( ifnull(ps.id_specific_price,0) AS UNSIGNED ) id_specific_price,

      c.iva,
      ps.ps_iva,

      c.baja,
      c.por_agotar,

      c.principal principal,

      c.peso,
      ps.ps_peso,

      c.ancho ancho,
      ps.ps_ancho,

      c.altura altura,
      ps.ps_altura,

      c.fondo fondo,
      ps.ps_fondo,

      c.marca_id,
      ps.ps_marca_id,
      c.nombre_marca,
      ps.ps_nombre_marca,

      c.modelo,
      ps.ps_modelo,

      ps.active,

      case
        when c.precio <> ps.ps_precio then 'precio'
        when c.descuento <> ps.ps_descuento then 'descuento'
        when c.peso <> ps.ps_peso then 'peso'
        when c.ancho <> ps.ps_ancho then 'ancho'
        when c.altura <> ps.ps_altura then 'altura'
		when c.fondo <> ps.ps_fondo then 'fondo'
        when c.iva <> ps.ps_iva then 'iva'
        when c.marca_id <> ps.ps_marca_id then 'id marca'
        when c.modelo <> ps.ps_modelo then 'modelo'
      end
      as campo_dif

      ############
      # AdminHG
      ############
      from (
        select
        P.id,
        P.producto_id,
        P.precio as precio,
        round((P.precio * (1-(P.descuento / 100)) * if(P.iva = 'S',1.16,1)),2) as precio_neto,
        P.descuento,
        P.peso,
          round(P.ancho,3) ancho,
          round(P.altura,3) altura,
          round(P.fondo,3) fondo,
        if(P.iva = 'S', 16,0) as iva,
        P.baja,
        P.por_agotar,
        P.marca_id,
          P.modelo,
        P.principal,
        ifnull(m.nombre,'Sin Marca') as nombre_marca

          from lanceta_bd_ec.codigos P
        left join lanceta_bd_ec.marcas m on m.id = P.marca_id

        where P.producto_id is not null
        and P.producto_id > 0
        #and P.baja = 'N'
        #and P.producto_id = 604

      ) as c

      ############
      # PRESTASHOP
      ############
      join (
        select ifnull(pa.reference,p2.reference) as reference,
        p2.id_product as id_product,
        p2.id_manufacturer ps_marca_id,
        m.name as ps_nombre_marca,
        round(p2.price,2) as price,
        pa.id_product_attribute,
        ifnull(pa.supplier_reference,p2.supplier_reference) as ps_modelo,
		    round(ifnull(pa.price,0),2) pa_price,
        round(ifnull(pas.price,0),2) pas_price,
        round( ifnull(pa.weight,0.000) + p2.weight, 3) as ps_peso,
        pa.default_on ,
        if( cc.principal = 1,round(p2.width,1), cc.ancho ) as ps_ancho,
        if( cc.principal = 1,round(p2.height,1), cc.altura ) as ps_altura,
        if( cc.principal = 1,round(p2.depth,1), cc.fondo ) as ps_fondo,
        CAST(round(ifnull(sp.descuento,0)) AS UNSIGNED) as ps_descuento,
        ifnull(sp.id_specific_price,0) id_specific_price,
        round(ifnull(sp.reduction,0),2) reduction,
        round( ifnull(pa.price,0) + p2.price,2 ) as ps_precio,
        round( ifnull(pas.price,0) + p2.price,2 ) as ps_pas_precio,
        round( (ifnull(pa.price,0) + p2.price) * (1-(ifnull(sp.reduction,0))) * ( if(p2.id_tax_rules_group = 1,1.16,1) ) ,2 ) as ps_precio_neto,
        if(p2.id_tax_rules_group = 1,16,0) as ps_iva,
        p2.active

        from production_presta.ps0516_product p2
        left join production_presta.ps0516_manufacturer m ON p2.id_manufacturer = m.id_manufacturer
        left join (
          select ps0516_product_attribute.id_product_attribute, ps0516_product_attribute.id_product, ps0516_product_attribute.reference, ps0516_product_attribute.price, ps0516_product_attribute.weight, ps0516_product_attribute.supplier_reference, ps0516_product_attribute.default_on
          from production_presta.ps0516_product_attribute
        ) pa on pa.id_product = p2.id_product
        left join production_presta.ps0516_product_attribute_shop pas on pas.id_product_attribute = pa.id_product_attribute
        left join (
        SELECT
          *,
          ps0516_specific_price.reduction*100 as descuento
          FROM production_presta.ps0516_specific_price
          where reduction_type = 'percentage' and reduction_tax = 1 and id_customer = 0
        ) as sp on sp.id_product = p2.id_product and sp.id_product_attribute = (ifnull(pa.id_product_attribute,0))
        left join lanceta_bd_ec.codigos cc ON cc.id = ifnull(pa.reference,p2.reference)

        #where p2.active = 1
        #and pa.id_product_attribute is null
        #and p2.id_product = 604

      ) as ps on c.id = ps.reference

      where (
        c.precio <> ps.ps_precio
        #or c.precio <> ps.ps_pas_precio
        or c.descuento <> ps.ps_descuento
        or c.iva <> ps.ps_iva
        or c.peso <> ps.ps_peso
        or c.ancho <> ps.ps_ancho
        or c.altura <> ps.ps_altura
        or c.fondo <> ps.ps_fondo
        or c.marca_id <> ps.ps_marca_id
        or c.modelo <> ps.ps_modelo
      )
      #and id_product = 4075
      #and left(c.id,3) in (60,250,288,2992)

      order by id_product, codigo

      #limit 10
    ";
  }

  public function queryCorregirDiferenciasAdminhgPsProductoSimple()
  {
    return "
      update production_presta.ps0516_product A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        round(p.price,2) as p_price,
        round(ifnull(pa.price,0),2) as pa_price,

        c.iva,

        c.peso peso_mg,
        round(ifnull(pa.weight,0) + p.weight,3) as weight_ps,
        round(p.weight,3) as p_weight,
        round(ifnull(pa.weight,0),3) as pa_weight,

        c.ancho ancho_mg,
        round(p.width,3) width,

        c.altura altura_mg,
        round(p.height,3) height,

        c.fondo fondo_mg,
        round(p.depth,3) depth,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        c.marca_id,
        p.id_manufacturer,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN production_presta.ps0516_product_attribute pa on p.id_product = pa.id_product AND pa.id_product_attribute = 0
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null
        #left join lanceta_bd_ec.codigos c2 on c2.id = concat(LPAD(c.Serie,3,0),LPAD(c.Producto,3,0))

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
          or c.ancho <> round(p.width,3)
          or c.altura <> round(p.height,3)
          or c.fondo <> round(p.depth,3)
          or c.marca_id <> p.id_manufacturer
          or IF(c.iva = 'S',16,0) <> if(p.id_tax_rules_group = 1,16,0)
        )
        #and pa.id_product_attribute is null
      ) as B on A.id_product = B.id_product

      set
      A.id_tax_rules_group = IF(B.iva = 'S',1,2),
      A.price = B.precio_mg,
      A.weight = B.peso_mg,
      A.width = B.ancho_mg,
      A.height = B.altura_mg,
      A.depth = B.fondo_mg,
      A.supplier_reference = B.modelo,
      A.id_manufacturer = B.marca_id
      ;
    ";
  }

  public function queryCorregirDiferenciasAdminhgPsProductoCombinaciones()
  {
    return "
      update production_presta.ps0516_product_attribute A
      join (
        SELECT ifnull(pa.reference,p.reference) as reference,
        ifnull(pa.id_product_attribute,0) id_product_attribute,
        p.id_product,
        c.producto_id,

        c.precio precio_mg,
        round(ifnull(pa.price,0) + p.price,2) as price_ps,
        p.price as p_price,
        ifnull(pa.price,0) as pa_price,
        c.precio - p.price as pa_price_ok,

        @pweight := p.weight as p_weight,
        ifnull(pa.weight,0) as pa_weight,
        c.peso peso_mg,
        pa.ps_weight as weight_ps,
        c.peso - p.weight as pa_weight_ok,

        c.modelo,
        ifnull(pa.supplier_reference,p.supplier_reference) as supplier_reference,

        pa.default_on,
        p.active

        FROM production_presta.ps0516_product p
        LEFT JOIN (
			    SELECT *,round( (@pweight+weight),3 ) ps_weight
          FROM production_presta.ps0516_product_attribute
        ) pa on p.id_product = pa.id_product
        left join lanceta_bd_ec.codigos c on c.id = ifnull(pa.reference,p.reference) and c.producto_id is not null

        where c.producto_id is not null
        and (
          c.precio <> round(ifnull(pa.price,0) + p.price,2)
          or c.modelo <> ifnull(pa.supplier_reference,p.supplier_reference)
          or c.peso <> round(ifnull(pa.weight,0) + p.weight,3)
        )
        and pa.id_product_attribute is not null
      ) as B on A.id_product_attribute = B.id_product_attribute
      #inner join production_presta.ps0516_product_attribute_shop C on C.id_product_attribute = A.id_product_attribute

      set
      A.price = B.pa_price_ok,
      A.weight = B.pa_weight_ok,
      A.supplier_reference = B.modelo
      ;
    ";
  }

  public function deletePsSpecificPriceById($id) {

    return DB::delete('DELETE FROM production_presta.ps0516_specific_price WHERE id_specific_price = ?',[$id]);

  }

  public function createPsSpecificFromAdminhg($descuento,$id_product,$id_product_attribute) {
    return DB::insert('
      INSERT INTO production_presta.ps0516_specific_price (id_specific_price,id_specific_price_rule,id_cart,id_product,id_shop,id_shop_group,id_currency,id_country,id_group,id_customer,id_product_attribute,price,from_quantity,reduction,reduction_tax,reduction_type,`from`,`to`)
      VALUES (null,0,0,?,0,0,0,0,0,0,?,-1,1,?,1,"percentage","0000-00-00 00:00:00","0000-00-00 00:00:00")
      ',[$id_product,$id_product_attribute,$descuento/100]);
  }

  public function updatePsSpecificPriceById($id,$descuento) {

    return DB::update('UPDATE production_presta.ps0516_specific_price SET reduction = ? WHERE id_specific_price =?',[$descuento/100,$id]);
  }

}