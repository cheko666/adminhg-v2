<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoClienteDistinguidoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_cd' => 'required|integer|numeric|min:2|max:6',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'The :attribute field is required.',
            'no_cd.numeric'    => 'The :attribute must be a number value.',
        ];
    }
}
