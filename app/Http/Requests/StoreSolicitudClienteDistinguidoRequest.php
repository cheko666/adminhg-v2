<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSolicitudClienteDistinguidoRequest extends FormRequest
{
    protected $redirectRoute = 'solicitud-cliente-distinguido.create';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:6|max:128',
            'curp' => 'required|string|size:18',
            'email' => 'required|email|unique:solicitudes_cliente_distinguido|confirmed',
            'direccion' => 'required|string|max:255',
            'telefono' => 'required|numeric|digits:10',
            'codigo_postal' => 'required|digits:5|numeric',
            'colonia' => 'required|string|max:64',
            'municipio' => 'required|string|max:64',
            'ine' => 'required|max:5000|mimes:jpeg,png,pdf',
            'comprobante' => 'required|max:5000|mimes:jpeg,png,pdf',
            'terminos' => 'required',
            'privacidad' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'nombre.min'    => 'The :attribute must be at least 6 characters.',
            'nombre.max'    => 'The :attribute must be a maximum of 128 characters.',
            'ine.max' => 'The :attribute must be 5 MB max.',
            'ine.mimes' => 'The :attribute must be a file type like: jpeg,png,pdf',
            'comprobante.max' => 'The :attribute must be 5 MB max.',
            'comprobante.mimes' => 'The :attribute must be a file type like: jpeg,png,pdf',
        ];
    }    
}
