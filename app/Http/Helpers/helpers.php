<?php
use Carbon\Carbon;
use Carbon\Factory;
use Illuminate\Support\Str;

/**
 * @param $fecha
 * @return string
 */
function fechaFormateada($fecha,$isoformat=null)
{
  if($fecha == null)
  {
    return false;
  }

  $factory = new Factory(
      ['local'=>'es_MX']
  );

  $fecha_factory = $factory->make(Carbon::createFromTimeString($fecha));

  return $isoformat ? Str::title($fecha_factory->isoFormat($isoformat)) : Str::title($fecha_factory->isoFormat('dddd, D MMMM YYYY, h:mm a'));

}

function formatearCambiosLog($cambios)
{

//  $array_cambios_separados = separarCambiosLog($cambios);

  $html = '<div class="cambios">';

  foreach($cambios as $key => $value)
  {
//    $row_html = '<div class="cambio">' . $key;
    $row_html = '<div id="cambio_'.$key.'" class="cambio">';
    $row_html .= '<span>'.$key.'</span>';
    $row_html .= '<div class="valores">';
    $row_html .= '<div class="valor-anterior d-inline-block p-1 text-muted col-md-4">' . formatearValor($key,$value[0]) . '</div>';
    $row_html .= '<div class="valor-final d-inline-block p-1 bg-info text-white rounded col-md-4">' . formatearValor($key,$value[1]) . '</div>';

    if((strpos($value[2], 'no existe en PS') !== false))
    {
      $row_html .= '<div class="valor-ps d-inline-block p-1 bg-warning text-white rounded col-md-4">' . $value[2] . '</div>';
    } else{
      if($key == 'precio' || $key == 'descuento')
      {
        if($value[2] == $value[1])
        {
          $row_html .= '<div class="valor-ps d-inline-block p-1 bg-success text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
        }else{
          $row_html .= '<div class="valor-ps d-inline-block p-1 bg-danger text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
        }
      }
      elseif($key == 'descripcion')
      {
        $row_html .= '<div class="valor-ps d-inline-block p-1 bg-dark text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
      }else
      {
        $row_html .= '<div class="valor-ps d-inline-block p-1 bg-muted text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
      }
    }



    $row_html .= '</div>';
    $row_html .= '</div>';
    $html .= $row_html;
  }

  $html .= '</div>';

  return $html;
}

function formatearDiferenciasCamposMgConPs($cambios)
{

//  $array_cambios_separados = separarCambiosLog($cambios);

  $html = '<div class="cambios">';

  foreach($cambios as $key => $value)
  {
    $row_html = '<div id="cambio_'.$key.'" class="cambio">';
    $row_html .= '<span>'.$key.'</span>';
    $row_html .= '<div class="valores">';
    $row_html .= '<div class="valor-anterior d-inline-block p-1 text-muted col-md-4">' . formatearValor($key,$value[0]) . '</div>';
    $row_html .= '<div class="valor-final d-inline-block p-1 bg-info text-white rounded col-md-4">' . formatearValor($key,$value[1]) . '</div>';
    /*
        if((strpos($value[2], 'no existe en PS') !== false))
        {
          $row_html .= '<div class="valor-ps d-inline-block p-1 bg-warning text-white rounded col-md-4">' . $value[2] . '</div>';
        } else{
          if($key == 'precio' || $key == 'descuento')
          {
            if($value[2] == $value[1])
            {
              $row_html .= '<div class="valor-ps d-inline-block p-1 bg-success text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
            }else{
              $row_html .= '<div class="valor-ps d-inline-block p-1 bg-danger text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
            }
          }
          elseif($key == 'descripcion')
          {
            $row_html .= '<div class="valor-ps d-inline-block p-1 bg-dark text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
          }else
          {
            $row_html .= '<div class="valor-ps d-inline-block p-1 bg-muted text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
          }
        }

        */

    $row_html .= '</div>';
    $row_html .= '</div>';
    $html .= $row_html;
  }

  $html .= '</div>';

  return $html;
}

function formatearDiferenciasCamposAdminhgPs($diferencias)
{

//  $array_cambios_separados = separarCambiosLog($cambios);

  $html = '<div class="cambios">';

  foreach($diferencias as $key => $value)
  {
    $row_html = '<div id="cambio_'.$key.'" class="cambio">';
    $row_html .= '<span>'.$key.'</span>';
    $row_html .= '<div class="valores">';
    $row_html .= '<div class="valor-anterior d-inline-block p-1 text-muted col-md-4">' . formatearValor($key,$value[0]) . '</div>';
    $row_html .= '<div class="valor-final d-inline-block p-1 bg-info text-white rounded col-md-4">' . formatearValor($key,$value[1]) . '</div>';
    /*
        if((strpos($value[2], 'no existe en PS') !== false))
        {
          $row_html .= '<div class="valor-ps d-inline-block p-1 bg-warning text-white rounded col-md-4">' . $value[2] . '</div>';
        } else{
          if($key == 'precio' || $key == 'descuento')
          {
            if($value[2] == $value[1])
            {
              $row_html .= '<div class="valor-ps d-inline-block p-1 bg-success text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
            }else{
              $row_html .= '<div class="valor-ps d-inline-block p-1 bg-danger text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
            }
          }
          elseif($key == 'descripcion')
          {
            $row_html .= '<div class="valor-ps d-inline-block p-1 bg-dark text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
          }else
          {
            $row_html .= '<div class="valor-ps d-inline-block p-1 bg-muted text-white rounded col-md-4">' . formatearValor($key,$value[2]) . '</div>';
          }
        }

        */

    $row_html .= '</div>';
    $row_html .= '</div>';
    $html .= $row_html;
  }

  $html .= '</div>';

  return $html;
}

function formatearValor($param,$valor)
{
  switch($param)
  {
    case 'precio':
      $precio = floatval($valor);
      return '$ '.number_format(floatval($precio), 2, '.', ',');
    case 'descuento':
      return '-'.$valor.'%';
    default:
      return $valor;
  }

}


function getImgTrackingStatus($status)
{
  switch($status)
  {
      case 'Delivered':
          $img = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIgoJIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIKCSBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMjggMTI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzRDQkI4Nzt9Cgkuc3Qxe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU+CjxnIGlkPSJTdGF0dXMiIHNrZXRjaDp0eXBlPSJNU1BhZ2UiPgoJPGcgaWQ9IkRlbGl2ZXJlZCIgc2tldGNoOnR5cGU9Ik1TQXJ0Ym9hcmRHcm91cCI+CgkJPGcgaWQ9IkRlbGl2ZWQiIHNrZXRjaDp0eXBlPSJNU0xheWVyR3JvdXAiPgoJCQk8cGF0aCBpZD0iT3ZhbC03LUNvcHktMiIgc2tldGNoOnR5cGU9Ik1TU2hhcGVHcm91cCIgY2xhc3M9InN0MCIgZD0iTTY0LDEyOGMzNS4zLDAsNjQtMjguNyw2NC02NFM5OS4zLDAsNjQsMFMwLDI4LjcsMCw2NAoJCQkJUzI4LjcsMTI4LDY0LDEyOHoiLz4KCQkJPHBhdGggaWQ9IlNoYXBlIiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIiBjbGFzcz0ic3QxIiBkPSJNODIuNSw1My4ybC0zLjQtMy40Yy0wLjUtMC41LTEtMC43LTEuNy0wLjdjLTAuNywwLTEuMiwwLjItMS43LDAuNwoJCQkJTDU5LjUsNjYuM2wtNy4zLTcuNGMtMC41LTAuNS0xLTAuNy0xLjctMC43Yy0wLjcsMC0xLjIsMC4yLTEuNywwLjdsLTMuNCwzLjRjLTAuNSwwLjUtMC43LDEtMC43LDEuN2MwLDAuNywwLjIsMS4yLDAuNywxLjcKCQkJCWw5LDkuMWwzLjQsMy40YzAuNSwwLjUsMSwwLjcsMS43LDAuN2MwLjcsMCwxLjItMC4yLDEuNy0wLjdsMy40LTMuNGwxNy45LTE4LjJjMC41LTAuNSwwLjctMSwwLjctMS43CgkJCQlDODMuMiw1NC4yLDgzLDUzLjcsODIuNSw1My4yTDgyLjUsNTMuMnoiLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPC9zdmc+Cg==';
          break;
      case 'OutForDelivery':
          $img = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIgoJIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIKCSBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMjggMTI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6I0Y1QTU1MTt9Cgkuc3Qxe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU+CjxnIGlkPSJTdGF0dXMiIHNrZXRjaDp0eXBlPSJNU1BhZ2UiPgoJPGcgaWQ9Ik91dC1mb3ItZGVsaXZlcnkiIHNrZXRjaDp0eXBlPSJNU0FydGJvYXJkR3JvdXAiPgoJCTxnIGlkPSJvdXQtZm9yLWRlbGl2ZXJ5IiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIj4KCQkJPGVsbGlwc2UgaWQ9Ik92YWwtNy1Db3B5IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIiBjbGFzcz0ic3QwIiBjeD0iNjQiIGN5PSI2NCIgcng9IjY0IiByeT0iNjQiPgoJCQk8L2VsbGlwc2U+CgkJCTxwYXRoIGlkPSJTaGFwZSIgc2tldGNoOnR5cGU9Ik1TU2hhcGVHcm91cCIgY2xhc3M9InN0MSIgZD0iTTQxLDUwYy0yLjUtMC44LTIuNS0yLjQsMC0zbDE5LTEwYzIuMy0xLjMsNS43LTEuMyw4LDBsMTksMTAKCQkJCWMyLjUsMC44LDIuNSwyLjQsMCwzTDY4LDYwYy0yLjMsMS4zLTUuNywxLjMtOCwwTDQxLDUwTDQxLDUweiBNNjUsNzBjMC0yLjUsMS43LTUuMyw0LTZsMjAtMTFjMi4zLTAuNyw0LDAuNCw0LDN2MTkKCQkJCWMwLDIuNy0xLjcsNS41LTQsN0w2OSw5MmMtMi4zLDAuNy00LTAuNC00LTNWNzBMNjUsNzB6IE0zNyw4MWMtMi4zLTAuNy00LTMuNC00LTZWNTZjMC0yLjUsMS43LTMuNiw0LTNsMjAsMTBjMi4zLDEuNiw0LDQuMiw0LDcKCQkJCXYxOWMwLDIuNS0xLjcsMy42LTQsM0wzNyw4MUwzNyw4MXoiLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPC9zdmc+Cg==';
          break;
      case 'Pending':
          $img = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIgoJIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIKCSBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMjggMTI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6I0NDQ0NDQzt9Cgkuc3Qxe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU+CjxnIGlkPSJTdGF0dXMiIHNrZXRjaDp0eXBlPSJNU1BhZ2UiPgoJPGcgaWQ9IlBlbmRpbmciIHNrZXRjaDp0eXBlPSJNU0FydGJvYXJkR3JvdXAiPgoJCTxnIGlkPSJHcm91cC0xMSIgc2tldGNoOnR5cGU9Ik1TTGF5ZXJHcm91cCI+CgkJCTxjaXJjbGUgaWQ9Ik92YWwtNy1Db3B5LTQiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiIGNsYXNzPSJzdDAiIGN4PSI2NCIgY3k9IjY0IiByPSI2NCI+CgkJCTwvY2lyY2xlPgoJCQk8ZyBpZD0iR3JvdXAtOCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzQuMDAwMDAwLCAzNC4wMDAwMDApIiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj4KCQkJCTxwYXRoIGlkPSJPdmFsLTctQ29weS0yIiBjbGFzcz0ic3QxIiBkPSJNMzAsNjBjMTYuNiwwLDMwLTEzLjQsMzAtMzBTNDYuNiwwLDMwLDBTMCwxMy40LDAsMzBTMTMuNCw2MCwzMCw2MHoiLz4KCQkJPC9nPgoJCTwvZz4KCTwvZz4KPC9nPgo8cG9seWdvbiBjbGFzcz0ic3QwIiBwb2ludHM9IjY2LDYyIDY2LDQ1IDYyLDQ1IDYyLDY0IDYyLDY2LjMgNzkuMSw2Ni4zIDc5LjEsNjIgIi8+Cjwvc3ZnPgo=';
          break;
      case 'InTransit':
          $img = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zOnNrZXRjaD0iaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoL25zIgoJIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIKCSBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxMjggMTI4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzY1QUVFMDt9Cgkuc3Qxe2ZpbGw6I0ZGRkZGRjt9Cjwvc3R5bGU+CjxnIGlkPSJTdGF0dXMiIHNrZXRjaDp0eXBlPSJNU1BhZ2UiPgoJPGcgaWQ9IkluLVRyYW5zaXQiIHNrZXRjaDp0eXBlPSJNU0FydGJvYXJkR3JvdXAiPgoJCTxnIGlkPSJHcm91cCIgc2tldGNoOnR5cGU9Ik1TTGF5ZXJHcm91cCI+CgkJCTxnIGlkPSJUcmFuc2l0IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIj4KCQkJCTxwYXRoIGlkPSJPdmFsLTciIGNsYXNzPSJzdDAiIGQ9Ik02NCwxMjhjMzUuMywwLDY0LTI4LjcsNjQtNjRTOTkuMywwLDY0LDBTMCwyOC43LDAsNjRTMjguNywxMjgsNjQsMTI4eiIvPgoJCQk8L2c+CgkJCTxwYXRoIGlkPSJSZWN0YW5nbGUtMTYiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiIGNsYXNzPSJzdDEiIGQ9Ik03NSw4MUg1NWMwLTAuMiwwLTAuMywwLTAuNUM1NSw3NC43LDUwLjMsNzAsNDQuNSw3MAoJCQkJYy01LjYsMC0xMC4yLDQuNC0xMC41LDEwaC00VjYzLjFMNDQuNiw0NEg1M1YzNGg0N3Y0N2gtNGMwLTAuMiwwLTAuMywwLTAuNUM5Niw3NC43LDkxLjMsNzAsODUuNSw3MFM3NSw3NC43LDc1LDgwLjUKCQkJCUM3NSw4MC43LDc1LDgwLjgsNzUsODF6IE0zOCw2Mmg4VjUxTDM4LDYyeiBNODUuNSw4OWM0LjcsMCw4LjUtMy44LDguNS04LjVTOTAuMiw3Miw4NS41LDcyUzc3LDc1LjgsNzcsODAuNVM4MC44LDg5LDg1LjUsODl6CgkJCQkgTTQ0LjUsODljNC43LDAsOC41LTMuOCw4LjUtOC41UzQ5LjIsNzIsNDQuNSw3MlMzNiw3NS44LDM2LDgwLjVTMzkuOCw4OSw0NC41LDg5eiIvPgoJCTwvZz4KCTwvZz4KPC9nPgo8L3N2Zz4K';
          break;
      case 'AttemptFail':
          $img = 'data:image/svg+xml;charset=utf-8;base64,PHN2ZyB4bWxuczpza2V0Y2g9Imh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaC9ucyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTI4IDEyOCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTI4IDEyOCI+PHN0eWxlIHR5cGU9InRleHQvY3NzIj4uc3Qwe2ZpbGw6I0I3ODlDNzt9IC5zdDF7ZmlsbDojRkZGRkZGO308L3N0eWxlPjxnIGlkPSJGYWlsIiBza2V0Y2g6dHlwZT0iTVNMYXllckdyb3VwIj48cGF0aCBpZD0iT3ZhbC03IiBza2V0Y2g6dHlwZT0iTVNTaGFwZUdyb3VwIiBjbGFzcz0ic3QwIiBkPSJNNjQgMTI4YzM1LjMgMCA2NC0yOC43IDY0LTY0cy0yOC43LTY0LTY0LTY0LTY0IDI4LjctNjQgNjQgMjguNyA2NCA2NCA2NHoiLz48cGF0aCBpZD0iUGF0aC0yNTYiIHNrZXRjaDp0eXBlPSJNU1NoYXBlR3JvdXAiIGNsYXNzPSJzdDEiIGQ9Ik01OC4zIDY3aC0xMy4zbDMwLTM3LTcuMyAzMGgxMy4zbC0zMCAzNyA3LjMtMzB6Ii8+PC9nPjwvc3ZnPg==';
          break;
      default:
          $img = '';
  }
    return $img;

}

function getDateTimeFromAtomDate($datetime)
{
    $date = Carbon::createFromTimestamp(strtotime($datetime))->format('d/m/Y h:i a');

    return $date;
}

function getShortToLongFormatTime($string,$only_date = false)
{
    if(empty($string) || $string ==='' || $string === 'N/A')
    {
        return 'N/A';
    }

    $unformat_date = getDateTimeFromAtomDate($string);

    $date_time = explode(' ',$unformat_date);

    // Empieza con número sino devuelve vacío
    if(is_numeric($date_time[0][0]))
    {
        $date = '';
//        dd('Es numerico' . $date_time[0]);
        if(count($date_time)===1) {
            //Sólo tiene fecha dd/mm/yyyy
            $date_pieces = explode('/',$date_time[0]);

//        $date = '<strong>';
            $date .= $date_pieces[0] . ' ' . getMonthString($date_pieces[1]) . ', ' . $date_pieces[2];
//        $date .= '</strong>';

            $date_time_formated = $date;

        } else {

            $time = $date_time[1];
            $time_period = $date_time[2];

            $date_pieces = explode('/',$date_time[0]);

            $date = '<strong>';
            $date .= $date_pieces[0] . ' ' . getMonthString($date_pieces[1]) . ', ' . $date_pieces[2];
            $date .= '</strong>';

            $time_with_period = !$only_date ?'<div class="hint">' . $time . ' ' . $time_period . '</div>' : '';

            $date_time_formated = $date . ' ' . $time_with_period;
        }

        return $date_time_formated;


    } else {

//        dd('No es numerico ' . $date_time[0][0]);
        return preg_match('/Tipo de env/i', $string) === 1 ? '&nbsp;' : $string;
    }

}

function getMonthString($num_month)
{
    $month_text = '';


    switch($num_month)
    {
        case '01' :
            $month_text = 'Ene';
            break;
        case '02' :
            $month_text = 'Feb';
            break;
        case '03' :
            $month_text = 'Mar';
            break;
        case '04' :
            $month_text = 'Abr';
            break;
        case '05' :
            $month_text = 'May';
            break;
        case '06' :
            $month_text = 'Jun';
            break;
        case '07' :
            $month_text = 'Jul';
            break;
        case '08' :
            $month_text = 'Ago';
            break;
        case '09' :
            $month_text = 'Sep';
            break;
        case '10' :
            $month_text = 'Oct';
            break;
        case '11' :
            $month_text = 'Nov';
            break;
        case '12' :
            $month_text = 'Dic';
            break;
    }

    return $month_text;
}

function getDateListArray($type)
{
    $text = array();

    if($type === 'dd')
    {
        for($i = 1; $i<=31; $i++)
        {
            $text[str_pad((string)$i,2,'0',STR_PAD_LEFT)] = str_pad((string)$i,2,'0',STR_PAD_LEFT);
        }
    }
    if($type === 'mm')
    {
        for($i = 1; $i<=12; $i++)
        {
            $text[str_pad((string)$i,2,'0',STR_PAD_LEFT)] = getMonthString(str_pad((string)$i,2,'0',STR_PAD_LEFT));
        }
    }
    if($type === 'yyyy')
    {
        for($i = Carbon::now()->year(1965)->year; $i<= Carbon::now()->subYears(17)->year; $i++)
        {
            $text[$i] = $i;
        }
        arsort($text);
    }

    return $text;
}

    function getImageNameSize($size=null,$namefile=null)
    {
        $nombre = explode('.',$namefile);

        $ext = array_pop($nombre);

        switch ($size) {
            case 'thumb':
                return $nombre[0] . '-ch.' . $ext;
                break;

            case 'default':
                return $nombre[0] . '-gd.' . $ext;
                break;

            case 'full':
                return $nombre[0] . '.' . $ext;
                break;

        }

    }

    function getImageUploadSize($img_width)
    {
        $size_xxs 	= range(1, 400);
        $size_xs 	= range(401, 900);
        $size_s 	= range(901, 1500);
        $size_m 	= range(1501, 2000);
        $size_l 	= range(2001, 3000);
        $size_xl 	= range(3001, 4000);
        $size_xxl 	= range(4001, 6000);

        switch (true) {
            case in_array($img_width, $size_xxs):
                $img_tamano = 'xxs';
                break;

            case in_array($img_width, $size_xs):
                $img_tamano = 'xs';
                break;

            case in_array($img_width, $size_s):
                $img_tamano = 's';
                break;

            case in_array($img_width, $size_m):
                $img_tamano = 'm';
                break;

            case in_array($img_width, $size_l):
                $img_tamano = 'l';
                break;

            case in_array($img_width, $size_xl):
                $img_tamano = 'xl';
                break;

            case in_array($img_width, $size_xxl):
                $img_tamano = 'xxl';
                break;
        }

        return $img_tamano;
    }

    function hasPhotoDefaultSize($id_p)
    {
        $file = config('paths.img.products') . $id_p . '-default.jpg';

        $file_headers = @get_headers($file);

        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        }
        else {
            $exists = true;
        }

        return $exists;
    }


  function formatPeriodo($cantidad,$periodo) {

    switch($periodo) {
      case 'd':
        return ($cantidad > 1) ? $cantidad . ' días' : $cantidad . ' día';
        break;
      case 'm':
        return ($cantidad > 1) ? $cantidad . ' meses' : $cantidad . ' mes';
        break;
      case 'Y':
        return ($cantidad > 1) ? $cantidad . ' años' : $cantidad . ' año';
        break;
      case 'h':
        return ($cantidad > 1) ? $cantidad . ' horas' : $cantidad . ' hora';
        break;
      case 'i':
        return ($cantidad > 1) ? $cantidad . ' minutos' : $cantidad . ' minuto';
        break;
    }
  }

/**
 * @param $date Carbon Object Date
 * @param string $cantidad
 * @param string $periodo
 * @return mixed
 */
function getDateFromPeriodOfTime($cantidad='1',$periodo='d') {
  $date = Carbon::now();
    if($periodo && $cantidad != '')
    {

      switch($periodo)
      {
        case 'y':
          $date_pivot = $cantidad === '0' ? $date->startOfYear() : $date->subYear($cantidad)->startOfDay();
          break;
        case 'm':
          $date_pivot = $cantidad === '0' ? $date->startOfMonth() : $date->subMonth($cantidad)->startOfDay();
          break;
        case 'd':
          $date_pivot = $cantidad === '0' ? $date->startOfDay() : $date->subDays($cantidad)->startOfDay();
          break;
        case 's':
          $date_pivot = $cantidad === '0' ? $date->startOfWeek(Carbon::MONDAY) : $date->subWeeks($cantidad)->startOfWeek(Carbon::MONDAY);
          break;
        case 'h':
          $date_pivot = $date->subHours($cantidad);
          break;
        case 'i':
          $date_pivot = $date->subMinutes($cantidad);
          break;
        default:
          $date_pivot = $date->yesterday()->startOfDay();
      }

    } else {

      $date_pivot = $date->yesterday()->startOfDay();
    }

    return $date_pivot;
  }

  function setCodigoWithZeroFill($int_codigo) {
    return str_pad($int_codigo,6,'0',STR_PAD_LEFT);
  }

  function getPsDatabaseFromEnviroment()
  {
    if(env('APP_ENV') == 'local' || env('APP_ENV') == 'production') {
      return 'production_presta';
    }else{
      return 'staging_presta';
    }
  }

  function lineasToArray($lineas_de_texto)
  {
    return preg_split("/\r\n|\n|\r/", $lineas_de_texto);
//    return explode("\n", $lineas_de_texto);
  }

function formatearDescripcionObjectToHtml($producto, $limit = 20)
{
  $descripcion = new stdClass();
  $descripcion->full = $producto->description;
  $descripcion->limited = Str::limit($producto->description,$limit);
  $descripcion->tooltip = '<span data-toggle="tooltip" data-placement="top" title="BC: '.$producto->description.' – PS: '.$producto->name.'">'.$descripcion->limited.'</span>';
  
  return $descripcion->tooltip;
}

function formatearDescripcionHtml($descripcion_producto, $limit = 20)
{
  $descripcion = new stdClass();
  $descripcion->full = $descripcion_producto;
  $descripcion->limited = Str::limit($descripcion_producto,$limit);
  $descripcion->tooltip = '<span data-toggle="tooltip" data-placement="top" title="BC: '.$descripcion_producto.'">'.$descripcion->limited.'</span>';
  
  return $descripcion->tooltip;
}

function urlExists($url = NULL)
{
    if ($url == NULL) return false;
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_TIMEOUT, 2);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $data = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return ($httpcode >= 200 && $httpcode < 300) ? true : false;
    
}