<?php

namespace App\Http\Controllers;

use App\Models\BcDescuentoProducto;
use App\Models\BcLog;
use App\Models\BcProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class BcLogController extends Controller
{
  const MOV_CREATE = 1;
  const MOV_UPDATE = 2;
  const MOV_DELETE = 3;
  const MOV_READ = 4;

  protected $request;
  protected $campos_objeto;
  protected $movimiento;
  protected $txt_movimiento;
  protected $objeto;
  protected $tiempo = '1-d';

  public function __construct(Request $request)
  {
    $this->request = $request;
    $this->movimiento = $request->has('m') ? $request->get('m') : self::MOV_UPDATE;
    $this->txt_movimiento = self::getTextoFromTipoMovimiento($this->movimiento);
    $this->objeto = $request->has('o') ? $request->get('o') : 'producto';
  }

  public function index(Request $request)
  {
    $cambio = $request->has('c') ? $request->get('c') : '';

    $title = 'Logs de sincronización BC-PS (' . ucfirst($this->txt_movimiento) . ' en tabla '. $this->objeto .')';
    $html = '';
    $codigos_obj = array();
    $objeto = $this->objeto;

      $bc_logs = BcLog::logTipo($this->objeto,$this->movimiento,$cambio,$this->tiempo)->get();
// dd($bc_logs);
    /*
        foreach($bc_logs as $bc_log)
        {


          $codigo_obj = $this->getObjetoDataFromIdObjeto($objeto,$bc_log->id_objecto);

          $obj = new \stdClass();
          $obj->id = (int) $codigo_obj->id;
          $obj->codigo = (string) $codigo_obj->codigo;
          $obj->id_p = (int) $codigo_obj->id_p;
          $obj->descripcion = (string) $codigo_obj->description;
          $obj->marca = (string) $codigo_obj->special_group_code;
          $obj->id_marca = $codigo_obj->id_manufacturer;
          $obj->descuento = $codigo_obj->descuento;

          $tag_campo_html = '<span style="padding:1px;display: block;">%nombre_campo%</span>';

          $codigo_obj->valor_log = null;
          $codigo_obj->v_iguales = null;
    }

    for($i=0;$i<count($bc_logs);$i++)
    {
      $cambios_array = json_decode($bc_logs->cambios);
    }
    */

/*
    [
                    {data:'id', name:'id'},
                    {data:'id_objecto',name:'id_objecto'},
                    {data:'cambios',name:'cambios'},
                    {data:'status',name:'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
*/


    if($request->ajax())
    {
      return Datatables::of($bc_logs)
        ->addColumn('id_objeto', function($bc_log) use($objeto) {
          return $bc_log->id_objecto;
        })
        ->editColumn('cambios',function($bc_log){
          return $bc_log->DiferenciasCambios;
        })
        ->editColumn('status', function($bc_log) {
          return ($bc_log->status == 0) ? '<span style="color:red">No</span>' : '<p style="color:green">Si</p>';
        })
        ->addColumn('action', function($bc_log){
          $btn = '<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Modal" data-model="bc/log" data-id="'.$bc_log->id.'" data-action="show" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Ver firma"><i class="fa fa-eye"></i></button>';
//                    $btn .= '<a role="button" class="btn btn-sm btn-warning" href="'.route('firma-email.edit',['id'=>$firma->id]).'" style="margin: 0 5px;" data-toggle="tooltip" data-placement="left" title="Editar firma"><i class="fa fa-edit"></i></a>';
//                    $btn .= '<a href="'.route('firma-email.download',['id'=>$firma->id]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar firma en HTML"><i class="fa fa-download"></i></a>';
          return $btn;
        })

        ->escapeColumns([])

        ->make(true);
    }


    return view('bc.logs.index',compact('title','bc_logs','objeto'));
  }

  public function getCamposByObjeto($objeto)
  {
    $tabla = '';

    switch($objeto)
    {
      case 'producto':
        $tabla = 'lsc_producto';
        break;
      case 'descuento':
        $tabla = 'lsc_descuento_producto';
        break;
      case 'marca':
        $tabla = 'lsc_marca';
        break;
      case 'categoria':
        $tabla = 'lsc_categoria';
        break;
      case 'direccion':
        $tabla = 'lsc_direccion_cliente';
        break;
      case 'cliente':
        $tabla = 'lsc_cliente';
        break;
      case 'stock':
        $tabla = 'lsc_stock_producto';
        break;
    }

    return Schema::connection('production_presta')->getColumnListing($tabla);
  }

  public function getCamposSelectByObjeto($objeto)
  {

    $campos = array_flip($this->getCamposByObjeto($objeto));
    $quitar_campos = array();
    $campos_select = array();

    switch($objeto)
    {
      case 'producto':
        $quitar_campos = array('id_supplier','reference','state','active','date_add','date_upd','id_p','id_pa','id_sp','id_sa','serie','producto');
        break;
      case 'descuento':
        $quitar_campos = array('id_shop_group','id_specific_price_rule','id_cart','id_shop','id_shop_group','id_currency','id_country','id_group','id_customer','from','to','reduction_tax','price','from_quantity');
        break;
      case 'marca':
        $quitar_campos = 'lsc_marca';
        break;
      case 'categoria':
        $quitar_campos = 'lsc_categoria';
        break;
      case 'direccion':
        $quitar_campos = 'lsc_direccion_cliente';
        break;
      case 'cliente':
        $quitar_campos = 'lsc_cliente';
        break;
      case 'stock':
        $quitar_campos = 'lsc_stock_producto';
        break;
    }

    foreach($quitar_campos as $key)
    {
      unset($campos[$key]);

    }

    foreach($campos as $k => $v)
    {
      $campos_select[]=$k;
    }

    $this->campos_objeto = $campos_select;

    return;
  }

  public function getObjetoDataFromIdObjeto($objeto,$id_objecto)
  {
    $modelo_log = false;

    self::getCamposSelectByObjeto($objeto);

    switch($objeto)
    {
      case 'producto':
        $modelo_log = BcProducto::select($this->campos_objeto)->find((int)$id_objecto);
        break;
      case 'descuento':
        $modelo_log = BcDescuentoProducto::select($this->campos_objeto)->find((int)$id_objecto);
        break;
      case 'marca':
        $quitar_campos = 'lsc_marca';
        break;
      case 'categoria':
        $quitar_campos = 'lsc_categoria';
        break;
      case 'direccion':
        $quitar_campos = 'lsc_direccion_cliente';
        break;
      case 'cliente':
        $quitar_campos = 'lsc_cliente';
        break;
      case 'stock':
        $quitar_campos = 'lsc_stock_producto';
        break;
    }

    return $modelo_log;
  }

  public function getDiferenciasEntreCampos($bc_log)
  {
    foreach($bc_log->toArray() as $k => $v)
    {
      if($k=='id')
      {
        continue;
      }
      $valor_campo = '';
      $class_alert = 'danger';
      $btn_corregir = '<a class="btn btn-xs btn-'.$class_alert.'" href="'.route('bc-log.edit',['id'=>$codigo_obj->id]).'"><i class="fa fa-edit"></i> Corregir</a>';
      $tag_campo = Str::limit($k,4,'.');
      $valor_campo = $codigo_obj->{$k};

      $v_iguales = $v['fin'] == $valor_campo ? true : false;
      $tr_class = $v_iguales ? 'default' : 'alert alert-'.$class_alert.'';
      $class_valor = $v_iguales ? '' : 'style="font-size: 1.05em;" class="badge alert-danger"';
      $obj->valor_codigo = $valor_campo;
      $obj->valor_log = '<span '.$class_valor.'>'.$v['fin'].'</span></td>';

      $acciones = $v_iguales ? '<i class="fa fa-check"></i>' : $btn_corregir;

      $obj->hay_diferencia = ($v_iguales ? 'No' : 'Si');
      $obj->campo_diferencia = '<strong>'.($v_iguales ? '-' : $k).'</strong>';
      $obj->btn_acciones = $acciones;

      $html_fila = '<tr id="r_'.$codigo_obj->id.'" class="">';
      $html_fila .= '<td class="text-center" width="80">'.$codigo_obj->codigo.'</td>';
      $html_fila .= '<td class="text-center" width="60">'.$codigo_obj->producto_id.'</td>';
      $html_fila .= '<td class="text-left" width="">'.Str::limit($codigo_obj->descripcion,50).'</td>';
      $html_fila .= '<td class="text-center" width="160">'.Str::limit($codigo_obj->marca->nombre,20).'</td>';
      $html_fila .= '<td class="text-center" width="80">'.($v_iguales ? 'No' : 'Si').'</td>';
      $html_fila .= '<td class="text-center" width="80"><strong>'.($v_iguales ? '-' : $k).'</strong></td>';
      $html_fila .= '<td class="text-center" width="80">'.$valor_campo.'</td>';
      $html_fila .= '<td class="text-center" width="80">'.'<span '.$class_valor.'>'.$v['fin'].'</span></td>';
      $html_fila .= '<td class="text-center" width="100">'.$acciones.'</td>';
      $html .= $html_fila."\r\n";

      dd($obj);

      $codigos_obj[] = json_decode(json_encode($obj));


    }
  }

  public function getTextoFromTipoMovimiento($num_mov)
  {
    switch($num_mov)
    {
      case 1:
        $this->txt_movimiento = 'Inserción';
      break;
      case 2:
        $this->txt_movimiento = 'Actualización';
      break;
      case 3:
        $this->txt_movimiento = 'Borrado';
      break;
      case 4:
        $this->txt_movimiento = 'Lectura';
      break;
    }

    return $this->txt_movimiento;
  }

}
