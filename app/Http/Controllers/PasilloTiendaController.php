<?php

namespace App\Http\Controllers;

use App\Models\Sucursal;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PasilloTiendaController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexProductoPorPasillo()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createProductoPorPasillo()
    {
        $title = 'Productos por pasillo en tienda';
        $sucursales = Sucursal::where('activo',1)->whereNotNull('cant_pasillos')->whereNotNull('cant_cabeceras')->pluck('nombre','siglas');

        foreach($sucursales as $suc => &$v)
        {
            $sucursales[$suc] = $v. ' ('.strtoupper($suc).')';
        }

        return view('pasillo-tienda.producto-por-pasillo-tienda_create', compact(
            'title',
            'sucursales'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeProductoPorPasillo()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProductoPorPasillo($suc = null)
    {
        $title = 'Productos por pasillo en tienda';
        $suc = isset($suc) && $suc != '' ? strtoupper($suc) : null;

        $validator = Validator::make(array('suc'=>$suc), [
            'suc'  => 'required|max:2',
        ]);

        if ($validator->fails() || !$suc) {
            return redirect()->route('producto-por-pasillo-tienda.create')->withErrors(['message' => 'No existen las siglas de la sucursal o no ingresaste nada.']);
        }

        $sucursal = Sucursal::where('siglas', $suc)->first();
        $title .= ' ' . $sucursal->nombre . ' (' . strtoupper($sucursal->siglas) . ')';
        $num_pasillos = $sucursal->cant_pasillos;
        $num_cabeceras = $sucursal->cant_cabeceras;

        $productos_lugar = DB::table('sucursal_productos')->select('productos', 'lugar')->where('sucursal', strtoupper($sucursal->siglas))->orderBy('id')->get();
        $productos_cabeceras = [];
        $productos_pasillos = [];
        $grupos_producto = DB::table('grupo_producto')->pluck('nombre', 'id');

        foreach ($productos_lugar as $productos) {
            switch ($productos->lugar) {
                case strpos($productos->lugar, 'p') !== false :
                    $productos_pasillos[$productos->lugar] = json_decode($productos->productos, true);
                    break;
                case strpos($productos->lugar, 'c') !== false :
                    $productos_cabeceras[$productos->lugar] = json_decode($productos->productos, true);
                    break;
            }
        }

        for($i = 1; $i <= $num_cabeceras; $i++)
        {
            if(!isset($productos_cabeceras['c'.$i]))
            {
                $productos_cabeceras['c'.$i] = [];
            }
        }

        for($i = 1; $i <= $num_pasillos; $i++)
        {
            if(!isset($productos_pasillos['p'.$i]))
            {
                $productos_pasillos['p'.$i] = [];
            }
        }

        foreach ($grupos_producto as $k => $v) {

            foreach ($productos_cabeceras as $k2 => $v2) {
                $selected = false;
                if (array_key_exists($k, $v2)) {
                    $selected = true;
                }
                $productos_cabeceras_all[$k2][] = [
                    'id' => $k,
                    'text' => $v,
                    'selected' => $selected

                ];
            }
        }

        foreach ($grupos_producto as $k => $v) {
            foreach ($productos_pasillos as $k2 => $v2) {
                $selected = false;
                if (array_key_exists($k, $v2)) {
                    $selected = true;
                }
                $productos_pasillos_all[$k2][] = [
                    'id' => $k,
                    'text' => $v,
                    'selected' => $selected

                ];
            }
        }

        return view('pasillo-tienda.producto-por-pasillo-tienda_edit', compact(
            'title',
            'sucursal',
            'num_pasillos',
            'num_cabeceras',
            'productos_pasillos_all',
            'productos_cabeceras_all'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProductoPorPasillo(Request $request)
    {

        $nombre_sucursal = $request->has('suc') && $request->get('suc') != '' ? strtoupper($request->get('suc')) : null;
        $sucursal = Sucursal::where('siglas', $nombre_sucursal)->first();

        $grupos_producto = DB::table('grupo_producto')->pluck('nombre', 'id')->toArray();

        $num_pasillos = $sucursal->cant_pasillos;
        $num_cabeceras = $sucursal->cant_cabeceras;

        /**  Cabeceras **/
        for ($i = 1; $i <= $num_cabeceras; $i++) {
            $productos_cabeceras_all['c' . $i] = [];
            $cabeceras_input = $request->has('c' . $i) && !empty($request->get('c' . $i)) ? $request->get('c' . $i) : [];

            if(!empty($cabeceras_input))
            {
                foreach ($cabeceras_input as $id)
                {
                    if( !is_numeric('1') )
                    {

                    }
                    if (array_key_exists($id, $grupos_producto)) {
                        $productos_cabeceras_all['c' . $i][$id] = $grupos_producto[$id];
                    }
                }

            }

            $productos_lugar = DB::table('sucursal_productos')->where('sucursal', strtoupper($sucursal->siglas))->where('lugar', 'c' . $i)->first();
            $data = [
                'sucursal' => strtoupper($sucursal->siglas),
                'lugar' => 'c' . $i,
                'productos' => json_encode($productos_cabeceras_all['c' . $i],JSON_UNESCAPED_UNICODE)
            ];

            if(!$productos_lugar)
            {
                DB::table('sucursal_productos')->insert($data);
            }
            else
            {
                DB::table('sucursal_productos')->where('id',$productos_lugar->id)->limit(1)->update($data);
            }
        }

        /**  Pasillos **/
        for ($i = 1; $i <= $num_pasillos; $i++) {
            $productos_pasillos_all['p' . $i] = [];
            $pasillos_input = $request->has('p' . $i) && !empty($request->get('p' . $i)) ? $request->get('p' . $i) : [];

            if(!empty($pasillos_input)) {
                foreach ($request->get('p' . $i) as $id) {
                    if (array_key_exists($id, $grupos_producto)) {
                        $productos_pasillos_all['p' . $i][$id] = $grupos_producto[$id];
                    }
                }
            }
            $productos_lugar = DB::table('sucursal_productos')->where('sucursal', strtoupper($sucursal->siglas))->where('lugar', 'p' . $i)->first();
            $data = [
                'sucursal' => strtoupper($sucursal->siglas),
                'lugar' => 'p' . $i,
                'productos' => json_encode($productos_pasillos_all['p' . $i],JSON_UNESCAPED_UNICODE)
            ];

            if(!$productos_lugar)
            {
                DB::table('sucursal_productos')->insert($data);
            }
            else
            {
                DB::table('sucursal_productos')->where('id',$productos_lugar->id)->limit(1)->update($data);
            }
        }

        return redirect()->route('producto-por-pasillo-tienda.edit', ['suc' => $nombre_sucursal]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyProductoPorPasillo($id)
    {
        //
    }
}
