<?php

namespace App\Http\Controllers;

use App\Models\BcLog;
use App\Models\BcLogRequest;
use App\Models\BcPrecioProducto;
use App\Models\BcProducto;
use App\Models\Codigo;
use App\Models\LSCPSProducto;
use App\Models\PsProduct;
use App\Models\PsViewPsIds;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PsProductoController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function createCambioPrecioProductoCompras()
    {
        $title = 'Actualización de precios en productos';
        $subtitle = 'Introduce los códigos con sus precios nuevos.';

        return view('ps.cambio-precios-compras.create',compact('title','subtitle'));

    }

    public function storeCambioPrecioProductoCompras()
    {
        $title = 'Actualización de precios en productos';
        $subtitle = 'Introduce los códigos con sus precios nuevos.';
        $noview = (bool)$this->request->get('noview');

        $datos = $this->request->get('datos');

        if($datos == 0)
        {
            if($this->request->ajax())
            {
                return response()->json([
                  'success' => 'false',
                  'errors'  => ['Debes incliur los códigos con el cambio de precio según la imagen de ejemplo.'],
                ], 400);
            }
        }
        if(is_array($datos))
        {
            $datos_array = $datos;

        } else {
            $datos_array = array();
            $lineas = lineasToArray($datos);

            foreach($lineas as $linea)
            {
                $codigo = explode("\t",$linea);
                $datos_array[$codigo[0]] = trim($codigo[1]);
            }
        }

        $num_cambios = count($datos_array);
        // Tabla previsualizacion
        $tabla_inicial = self::maquetarTablaResultados($datos_array,false);

        $estadisticas_ini = '<p><strong>'.$tabla_inicial['cant_diferencias'].' diferencias de precio</strong> en la Tienda Wed de acuerdo a los '.$num_cambios.' códigos que se ingresaron.</p>';

        if($this->request->ajax())
        {
            return response()->json(['success'=>1,'tabla'=>$tabla_inicial['tabla'],'estadisticas'=>$estadisticas_ini],200);
        }

        // Ejecucion cambio de precios en BD
        $tabla_final = self::maquetarTablaResultados($datos_array,true);
        $subtitle = 'Se ejecutaron los cambios con éxito. Favor de revisar los movimientos.';
        $estadisticas_fin = '<p>Se modificaron <strong>'. $tabla_final['cant_modificaciones'] . ' registros</strong> de acuerdo a los datos ingresados anteriormente.</p>';

        if($noview){
            return ['success'=>1,'tabla_inicial'=>$tabla_inicial['tabla'],'tabla_final'=>$tabla_final['tabla'],'estadisticas_ini'=>$estadisticas_ini,'estadisticas_fin'=>$estadisticas_fin];
        }

        return view('ps.cambio-precios-compras.show',compact('title','subtitle','tabla_inicial','tabla_final','estadisticas_ini','estadisticas_fin'));

    }

    public function maquetarTablaResultados($datos_array,$final = false)
    {
        $tabla = '<table id="cambios" class="table table-sm table-striped table-hover" style="width:500px;">';
        $thead = '<thead class="">';
        $thead .= '<th width="60" >Código</th><th width="60" >ID_P</th><th>Precio BC &nbsp;&nbsp;vs&nbsp;&nbsp; Precio PS</th>';
        if(!$final) {
            $thead .= '<th width="80">Hay Dif.</th>';
        }
        if($final) {
            $thead .= '<th width="80" >&nbsp;</th>';
        }
        $thead .= '</thead>';
        $tbody = '<tbody>';
        $cant_diferencias = 0;
        $cant_modificaciones = 0;

        // Modificacion precio tablas lsc_producto y lsc_log
        foreach($datos_array as $codigo => $precio_nuevo)
        {
            $precio_nuevo = number_format($precio_nuevo,2,'.','');
//            $producto = Codigo::find($codigo);
            $producto = BcProducto::find($codigo);

            $modificado = false;

            if($final) {
                $precio_viejo = $producto->price;
                $producto->price = $precio_nuevo;
                if ($producto->isDirty()) {
                    // dd($producto);
                    $modificado = $producto->save();
                    $cant_modificaciones = $modificado ? $cant_modificaciones += 1 : $cant_modificaciones;
                    $bc_precio_producto = BcProductoController::savePrecioProducto($codigo,$precio_nuevo);

                    $bc_log_request = BcLogRequest::create([
                        'id' => null,
                        'id_objeto' => $producto->id,
                        'endpoint' => 'products_price',
                        'method' => 'PUT',
                        'response' => '',
                        'request' => $this->xmlRequestProductsPrice($producto->id,$precio_nuevo),
                        'status_code' => 200,

                    ]);

                    PsProduct::corregirDiferenciasPreciosBcPs();
                }
                $cant_modificaciones += $cant_modificaciones;

            }

            $es_diferente = $producto->price != $precio_nuevo;
            $cant_diferencias = $es_diferente ? $cant_diferencias+=1 : $cant_diferencias;
            $estatus_txt =  $es_diferente ? 'red;"> Si' : 'green;"> No';
            $flecha = $es_diferente ? 'fa-not-equal' : 'fa-equals';
            $td_class =  $es_diferente ? 'table-danger' : '';
            $texto_modificado =  $modificado ? 'fa-check-circle' : 'fa-times-circle';

            $tbody .= '<tr id="'.$producto->codigo.'">';
            $tbody .= '<td>'.$codigo.'</td>';
            $tbody .= '<td>'.$producto->id_p.'</td>';
            $tbody .= '<td><table style="width:200px;" class="table table-sm table-borderless mb-0"><tr>';
            $tbody .= '<td width="45%" style="text-align: center;"><span class="badge">'.number_format($precio_nuevo,2,'.','').'</span></td>';
            $tbody .= '<td width="10%" style="text-align: center;"><i class="fa '.$flecha.'"></i></td>';
            $tbody .= '<td width="45%" style="text-align: center;"><span class="badge '.$td_class.'">'.number_format($producto->price,2,'.','').'</span></td>';
            $tbody .= '</tr></table></td>';
            if(!$final) {
                $tbody .= '<td><span style="font-weight:bold; color:' . $estatus_txt . '</span></td>';
            }
            if($final) {
                $tbody .= '<th width="80" ><i class="fa '.$texto_modificado.'"></i></th>';
            }

            $tbody .= '</tr>';
        }

        $tabla .= $thead.$tbody;
        $tabla .= '</tbody></table>';

        $datos['tabla'] = $tabla;
        $datos['cant_diferencias'] = $cant_diferencias;
        $datos['cant_modificaciones'] = $cant_modificaciones;

        return $datos;
    }

    public function xmlRequestProductsPrice($codigo,$precio)
    {
        if(!$codigo || $codigo=='' || !$precio || $precio == '')
        {
            return false;
        }
        return '<?xml version="1.0" encoding="UTF-8"?> <prestashop xmlns:xlink="http://www.w3.org/1999/xlink"> <products_price> 	<id></id> 	<id_product>'.$codigo.'</id_product> 	<id_shop>1</id_shop> 	<id_shop_group>0</id_shop_group> 	<unit_price>'.$precio.'</unit_price> </products_price> </prestashop>';
    }
}
