<?php

namespace App\Http\Controllers;

use App\Models\BcDescuentoProducto;
use App\Models\BcLog;
use App\Models\LscPeriodicDiscount;
use App\Vendor\Protechstudio\PrestashopWebService\PrestashopWebServiceOverride;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class BcDescuentoController extends Controller
{
    public $ws;
  
    public function __construct(PrestashopWebServiceOverride $ws)
    {
      $this->ws = $ws;
    }
  /**
   * Vista de productos en Lista Oferta Descuento en BC
   *
   * @return string de tipo json
   */
  public function indexDescuentosFromBc()
  {
//    return response()->json(['stock'=>json_encode($this->getStockBcFromDB())], 200);
    dd($this->getStockBcFromBcApi());
  }

  /**
   * Consulta de datos en API-BC getDescuentos --> BcApiController::getPeriodicDiscountsTableData
   * @return \Illuminate\Http\JsonResponse|string tipo json plano
   */
  public function getDescuentosFromBcApi($codigo=null)
  {
    $response = Http::get('http://lan-ec.ddns.me:8081/bc/api/getDescuentos/'.$codigo);

    return $response->body();
  }
  /**
   * Consulta de datos en API-BC getDescuentos --> BcApiController::getPeriodicDiscountsTableData
   * @return \Illuminate\Http\JsonResponse|string tipo json plano
   */
  public function getListaOfertaDescuentoFromBcApi()
  {
    $response = Http::get('http://lan-ec.ddns.me:8081/bc/api/getPeriodicDiscountTableData');

    return $response->body();
  }
   /**
   * Consulta y sincronización de Lista de Ofertas entre BC y PS por medio de consulta de API-BC
   * enviando a PS solo las diferencias encontradas.
   * Función usada en agenda de comandos (schedule).
   *
   * @return \Illuminate\Http\JsonResponse|void
   */
  public function sincListaOfertaDescuentoBcPS()
  {
    $data = json_decode($this->getListaOfertaDescuentoFromBcApi(),true);
    $cant_sinc = count($data);
    $movimientos = array();

    if($cant_sinc == 0)
    {
        return response()->json(['success'=>true,'data'=>$movimientos,'mensaje'=>'No existen listas de oferta-descuento']);
    }

    $cant_upd = 0;
    $cant_ins = 0;
    
    foreach($data as $bc_descuento)
    {
      $lsc_pd = LscPeriodicDiscount::find($bc_descuento['no']);
      unset($bc_descuento['type']);
      unset($bc_descuento['price_group']);
      
      foreach($bc_descuento as $k => $v) 
      {
        if($k == 'status' || $k == 'priority')
        $bc_descuento[$k] = (int)$v;
    }
    
    $bc_log = new BcLog([
      'id_objecto' => $bc_descuento['no'],
      'objeto' => 'periodic_discount',
    ]);
    $salvado = false;
      
      if($lsc_pd) // Existe en tabla?
      {
        $cambios_log = array();

        $lsc_pd->fill($bc_descuento);

        // Se actualiza registro
        if ($lsc_pd->isDirty())
        {
          foreach($lsc_pd->getDirty() as $k => $v)
          {
            $cambios_log[$k] = $lsc_pd->getOriginal($k).'|'.$v;
          }
          $salvado = $lsc_pd->saveOrFail();
          
          $bc_log->movimiento = BcLog::MOV_UPDATE;
          $bc_log->cambios = json_encode($cambios_log);
          $bc_log->status = $salvado ? 1 : 0;
          $bc_log->save();
        }
        continue;
      }
      else
      {  
        $lsc_pd = new LscPeriodicDiscount();
        $lsc_pd->fill($bc_descuento);
        $salvado = $lsc_pd->saveOrFail();

        $bc_log->movimiento = BcLog::MOV_CREATE;
        $bc_log->cambios = json_encode($lsc_pd->getAttributes());
        $bc_log->status = $salvado ? 1 : 0;
        $bc_log->save();
      }
    }
    return;
  }
  /**
   * Consulta y sincronización de stock entre BC y PS por medio de consulta de API-BC
   * enviando a PS solo las diferencias encontradas.
   * Función usada en agenda de comandos (schedule).
   *
   * @return \Illuminate\Http\JsonResponse|void
   */
  public function sincDescuentosBcPS($codigo=null)
  {
    $data = json_decode($this->getDescuentosFromBcApi($codigo));
    $cant_sinc = count($data);
    $movimientos = array();

    if($cant_sinc == 0)
    {
        return response()->json(['success'=>true,'data'=>$movimientos,'mensaje'=>'No existe el producto '.$codigo.' ingresado con descuento']);
    }

    $cant_upd = 0;
    $cant_ins = 0;

    foreach($data as $descuento)
    {

    }

  }

}
