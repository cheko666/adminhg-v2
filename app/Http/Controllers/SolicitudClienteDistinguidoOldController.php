<?php

namespace App\Http\Controllers;

use App\DataTables\SolicitudesClientesDistinguidosDataTable;
use App\Models\SolicitudClienteDistinguidoOld;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Editor\Editor;
use ZipArchive;
use App\Http\Requests\StoreSolicitudClienteDistinguidoRequest;
use App\Models\SolicitudClienteDistinguidoNew;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class SolicitudClienteDistinguidoOldController extends Controller
{
    protected $storage_path;

    public function __construct() {
        $this->storage_path = Storage::disk('samba-solicitudes-cd')->getDriver()->getAdapter()->getPathPrefix();
    }

    public function storeClienteNo(Request $request)
    {
        dd($request->all());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $title = 'Lista de Solicitudes Viejas';
        $data = SolicitudClienteDistinguidoOld::select([
            'id',
            'rfc',
            'nombre',
            'apellido',
            'telefono',
            'mail',
            'estado',
            'cod_postal',
            'ife',
            'comprobante',
            'rfc_file',
            'created_at'
        ])
//            ->whereYear('created_at', date('Y'))
            ->orderBy('id','desc')
            ->get();

        if($request->ajax())
        {
            return Datatables::of($data)
                ->addColumn('nombre', function($data)
                {
                    return Str::limit($data->nombre.' '.$data->apellido,30);
                })
                ->addColumn('fecha_registro', function($data)
                {
                    return Carbon::parse($data->created_at)->format('d-m-Y');
                })
                ->addColumn('action', function($data)
                {

                    $token = explode('_',$data->comprobante);
                    $btn = '<div class="row">';
                    $btn .= '<div style="margin: 0 5px 0;"><button role="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal" data-id="'.$data->id.'" data-model="" data-toggle="tooltip" data-placement="left" title="Ver información de la solicitud"><i class="fa fa-eye"></i> Ver</button></div>';
                    $btn .= '<div><a href="'.route('solicitud-cliente-distinguido.download-old',['id'=>$data->id]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar documentación del cliente"><i class="fa fa-download"></i></a></div>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('solicitud-cliente-distinguido.index-old',compact('title','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Solicitud de Tarjeta Cliente Distinguido';

        return view('solicitud-cliente-distinguido.create',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSolicitudClienteDistinguidoRequest $request)
    {
        $path = $this->storage_path;
        $inputs = $request->except('_token');
        $reference = Str::upper(Str::random(12));

        $ine_file = $request->file('ine');
        $comprobante_file = $request->file('comprobante');
        $ine_original_name = $ine_file->getClientOriginalName();

        $comprobante_original_name = $inputs['comprobante']->getClientOriginalName();
        
        $inputs['fecha_nacimiento'] = $request->year.'-'.$request->month.'-'.$request->day;
        $inputs['especialidad'] = $request->has('especialidad') ? implode(',',$inputs['especialidad']) : null;
        $inputs['ine'] = $ine_original_name;
        $inputs['comprobante'] = $comprobante_original_name;
        $inputs['reference'] = $reference;
        $inputs['terminos'] = $request->get('terminos') == 'on' ? 1 : 0;
        $inputs['privacidad'] = $request->get('privacidad') == 'on' ? 1 : 0;
        $inputs['reference'] = $reference;
        $id_ps_state = DB::connection('production_presta')->table('state')->select('id_state')->where('id_country',145)->where('name','like',''.$inputs['estado'].'%')->first();
        $inputs['id_state'] = $id_ps_state->id_state ?? 62;

        unset($inputs['day']);
        unset($inputs['month']);
        unset($inputs['year']);
        unset($inputs['email_confirmation']);

        $solicitud_cd = new SolicitudClienteDistinguidoNew($inputs);
        $solicitud_cd->save();
        
        mkdir($path.$reference,0755);
        $ine_file->move($path.$reference, $ine_original_name);
        $comprobante_file->move($path.$reference, $comprobante_original_name);
        // dd($dir_path);
        return redirect()->route('solicitud-cliente-distinguido2.show',$solicitud_cd->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $data = SolicitudClienteDistinguidoOld::find($id);

        $token_files = explode('_',$data->comprobante)[0];

        $storage_path = Storage::disk('samba_clientes')->getDriver()->getAdapter()->getPathPrefix();

        $docs = array();

        $files = Storage::disk('samba_clientes')->files($token_files);

        if(!$files)
        {
            return response()->json([
                'view' => '<div class="alert alert-danger">Ya no existen los archivos</div>'
            ]); 
        }
        foreach($files as $file)
        {
            $nom_archivo = explode('/',$file)[1];

            switch($nom_archivo)
            {
                case $nom_archivo == $data->ife:
                    $tipo_doc = 'ine';
                    break;
                case $nom_archivo == $data->comprobante:
                    $tipo_doc = 'dom';
                    break;
                case $nom_archivo == $data->rfc_file:
                    $tipo_doc = 'rfc';
                    break;
                default : $tipo_doc = 'pdf_todo'; $pdf_doc_file = $nom_archivo;
            }

            if(Storage::disk('samba_clientes')->mimeType($token_files.'/'.$nom_archivo) == 'application/pdf' || $tipo_doc == 'pdf_todo') {
                $docs[$tipo_doc] = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAVeUlEQVR4Xu1dC3hU1bXe+8zwCE+RZILiC1T0+qheUHFmAjnzCkISRBSrtlKp9tOrvd766K29vvCT1ifeTyv1fpWWWtveq3itkESEzJAJJhkspFYt9baoWAtIHrwKSB4zZ99/n3kwTDLvM+cRZn9fviQz++y99tr/WXvttddamxKdlIrqigkjeszziEBqCGPTCKGngDQLfkw6IVFZMhj5pc/nX6Jso9m3RrN/RNknXC5XOSGhhwll38GkD1e2dX23BuY/7PX6l2lJpaYAcLorl1BCXwADxmjJBA37Zhj/N73ept9qRYMmAFi0aJFp34HOpwmj92o1cB3124slz+PzNb+rBU2aAMDlEp8llNynxYD12SfbJ4UEW1NT01/Upk91ADg94i2UkVVqD1Tv/WEiPgkGidXv93erSauqABBFsdRkZp9C2Run5iAN1FdLKEg8AEGPWjSrCgCXp/J5rPt3qzU4I/YDpXB1RUXTDUuXEkkN+lUDgN1uHzuyZFgnBjVSjYEZug/Gfgyl8EE1xqAaAJyeykWU0dezGNSnjJBVjFI/DdGdfSV9/8jiWd1UHdFnriaMvJotQZAEt2J7+Itsn8u2vmoAcLkrX8HavzgDAkMg6qH9+w8tb29v78+gvq6rAPjXAPhv5kBkP/gwD4Yibw7PZvyIigAQ20CVNQ1l3DByA5CfjaTIeLBaVMwDAJzcg1KI2rE93FYo2tUDgEvcgb3/WakHQp/xeZv+vVCD1aLdPAFAsHx8HgrJ28M9haBfPQC4xUMYQFKTLwjZT4h5itfrPViIgWrVZt4ACBO+ZVTJWLGuru4rpcehJgAOg/jRSQdAySu+Rv8tSg9Q6/YUAgAfRv3JE8oWrF69OqTkmHQDAErYnV5v80tKDk4PbSkIAOjQ7DlfY7OiJnTdAIBRtnBjY/Pv9DBpStKgKAA4YYze5fM1/VQpGvUDAIHN37ihuU6pgemlHcUBAOcJidKFTY1Na5UYYxEASnAxRRsFAADv7bBAyazGRv8f8yW/CIB8OZjm+QIBgK8Fu00Cu3LDhk1/z2cIRQDkw70Mni0cALiJgPxJIOaKfLbORQBkMIn5VCkkACJ0vYMj5FoYioK50FkEQC5cy+IZFQDARcFKeBjDqTb7UgRA9jzL6glVABDeHt6H7eFzWRGHykUAZMuxLOurBgDCJEqERThIy+rksQiALCc02+rqAUCm7Cgm1Ikj5M2Z0kk7a2yqeKJ2EHKOxKiQjLAJlO2Cq9CRTAnXeb0jlvq26ZxGlQHAu+wOmXB6uN7/SSY84gDAbqJYlOUAAwAC8smnBgDg3X7cOyJob2lowQlr6lIEQDoO5fS95gDgNoLmiRPKqnB62JdqCEUA5DTB6R7SHgCcQnhXrYJS+O0iANLNl+Lf6wMAfFiQBA9t9Pp/lGyIRQmg+OTLbD+mA1RV1lKJKnJylyOpDEft38BR+38P9nwRADlyNfVjxwDgclU6CKUbC9JN5o32YHvowfawJfGRIgAyZ2IWNeMkgNN5NhWkjLZkWXSQS9W9hJlsPp/vr/EPpwUAY+w/oU5sz6XHofsMvYRSdnvy8R0DAA+F37+/qwtr8QQd8GM7QGAFCPZGaUkLAJgXHWX1LX4dEK8bEjpqrQsQ7JHCfe0YADjRCId/FSr5N3UygJbhw0rc69at6+X06BoAbCkR9rbbRERJWhFSPg4q7aayhrYGrRmZAwCuAwBWa013XP+v+bz+G7m2qlsAdFRXTMXa+RYm/eLjGMfIVoFIi0sbNn+sFUOzBYDVai0ZNXoEX0Yna0XzgH4pfdzX2PSILgGwuxZ5BFhfOzTXM5Iw7BATQueWr30PRwzql2wBEFkGboUUWKk+tUl7RBgeceoSAF01tkehNC2NI30zhFUIfvH2mPLCyO1YDn6mBUNzAYCcF2lfVztAcIkWNA/aJyM36hIAOKD6AgSfzokGTJeX17Xdz//urLHCoEJrw4OhT1rqW3+oBTNzAQCns6pq9pSQJPwef5ZqQfeAPvUIgM55l08iwrAvo8QKTLogut4DGO/g8znh+ScPWerakpo4C8ngXAHAaXJWOZ1Ukvg4hhWSxoza1iMAumttTokRX2QAX5Vd1jaWRtKlAAAf4fOLwgCgt1nqWn+e0UAVrpQPADgpDo/DDmDDc4fyTKjaFT0CoKvadgPEftRu/QkcK86Ncqiz1nYAu4Lx/H8YqK4qbwis14J7+QKA0+x2u6cSEnwdus4MLcYg96lHAGCdvwtvxosRpmwGAOSkEnxnYGZ9XVFmUYGeX7a2VRVvpsQJUgIAkTYpHEaug1HpCfx/tupA0CMAOmptd8LosyIRAB01Niu2LTzLCC+hspKDo+jqbSmdHQrFUAUBIJOIHcJw7BCcSJS9AG9lDT5Sx16gRwBgnV8CBkSTI30ACXApZ1JHjXUxHByQZ0gun+Fz9d+YSOdKAyARqEigPZHS/jMkwTxWkKScE2hLlJVBuiTPQ6xLAFRbF0HBi+YI2omJlreDXTXWpxmh35eZRek7UADnFuoNT9duoQGQrv9MvxevEs8yBcmOpPX1CICu2iunMya0R4hmpGT4OMtq/2HoBk2YeTHy+TIA4+FMGaF0PaMAwJnuKFqPANhT9bXRwvAxPJ9QOGZBYNPL1gb+COvgPvx3Ev8IHi7XlNcF3lJ6YjNtzygAcDgc5wkm9n+GkgBhcW/7HNujM8PSnn6HhOgmJkgxjd9kMp8xcU1+YdGZTvZg9YwCAOgSFxAaSp5iTo8SgDO8o9r2W0oJP64EAMivJcYa4xTADoj/SflMYL7PGgUAbvfsixkRPjScBOiosd+BpFHRhFE7sRY0QSLcHBnIGwDAonwnMZ/njQIAj0e8FFbV9w0HgO5a6/kII4s772cIGaNyijnGyJ3lDW2aZhMzEAAuAwC2GA4AnGDYA7gDxTkDiQ9Ns9S/p6mPolEA4KhyzBQkljxQVK86QEQRfApi/7i0sfj/i/L6Nlk51LIYBQDYBtrgVdVqSAnQXWO9QiL0vQTiV2H9TxnqpAYwjAIAxCTMgha9yZAAwNtOsR38M4g/PzoArff/UTqMAgC3WxTBRxjQkhQ9LwHyMlBrfwTHvo9FyTeZTBdOXPMuB4WmxUAAcAMAjcYFQI3NiwG44gawEktATsmQlESMcQDgmMMI495Hgxc9S4A919gsQj/ZDcrj7w6WEGg5s+zt1q1KTmi2bRkFAPA8micwljyOQs8AgGfQffAMenaQydkCN7Ero25i2U6eEvWNAgBnushkvQKARwR1bZXtAHCbkt2//owzgQuikweHEc1cwjkNhgFAuvuK9AqArpqKWkakWEy9JLGvCZQux/mgJwKCvUTqv8jy9paCXKOSTkoYBQCISUwdkqZfANgboby45YmgpAnu387OmplwDjVxr+AR4Qli/rKS09xU4Rs00k2+kSQAAPB18O9/DKUERs4B+FZP9gdAeotryxta5eSHMA8/hF+PxwbEyAOWhranMpk0JesYRQK43Y6b8CL9xlAA6Ky2roT16lZ58mH6tZRMnhp9yxnCq7qO7uIxA5UR6dCPW7dnTXq7JdFiqOR8D2jLQABYDABE/SgH8kRvS0DHXNvZ1Ew+xszLUTOw/H0fnj/H7QT2zp15Wshk4hclTIyM6DNzT+/0k73tqt02ZhQAON2VS+BHkfz2Ub0BoLPatgqC/5bIxHZKfYenTtrw4YDsoXtqbVcLjPAEDdFUt2+XlUyer5Y+YBgAeMTbsGN62RBLQOd86zlEkn0AzMne/viBwEn0J5j/78Z99gKshP9WUNkfadwoAIASeDtekf8yBgBq7FirWPRu4Y4g7Z16al170osS2aILh3cdHb8hpg/ISwa5C5HEit2olYxxxgGA406E1EeDbPSrA3TOs12KqBhu3pXNvjD63FtW14rkVKnLwTnWk3uH0QBqTYvUDEHkXV3oNDIGAsDdAMDzupcACT7/u3pL2Lmnrw4cTQcA/v2eWvuVlLFmKAPhCBpKDkIUrMHvEuwhx0OhRJ4regB1eiAhDlCJ/A2//0aZsKN01P6PcgkvMwoA3O7KezD85JdI6EEJRCzgtXhr34hONiZnMcT4q4mTz7eAe3p2n4+w6hnQbKdjwi/C4C6E4M/ZQxjbzD70/SEg0soE2mAZcaA5E0AYBQBOt3g/+PSMbiXADlEcOXpML4w+dEpY9LP3SusCPAiU8TW+u2ecVZKQx4ZSJz7656hjaCaSIcc6/wAoVgr9pmfL1r8bS1KR2JZhAOARfwCAP6lbACRY9vDy05uREu5UuIRzM3AFfkZlNJGU9AMyOyERugCeK+Ke4Xch/ARv+FEsBSPxTym+5z6FZ+GHR+AmuzGlB2vnY2V1gac4GA0LALf4IOhfpksAdM2rmIZonw9AHC4KkQtndCZX2OxC1T/APfx9KgjbcJX2tkm7e/5K29v7eSOJCabQ4DbSb/IkvtH7F4gn9QZ7ZwhMmInJvgqP2vAT73vA5dDrQaFvSeJuxCgSwOVxPIIxxDyqBgBBKx1g9zzbmWZKfxOf9SsJSiEQMNGEbQQ0NrFQcGsmJ4DIJbAcE39vrE1KPmWSUFXe0PJZsrchkpruu3jubtSJpXWFCF1Tennbwnj/A8MAwFX5GNbVR3QhAZgomrvG9C0AMbfhhx/rJrs/aBfCwdZKEvWODEr+8esDPCg0q8IdSjur7S8l5PPdjSCJOZMa2v6UqrFIkqr/jUiEcFVKluBE8pfR54wCADiFLgMvHtQUALDdj5NMJj7pd0cDPhMJ4oc+UPTeRCrANyzTNweU8PbhIOiutj+N8wQ5xRwv+GwfROJN6XILbZ87d8R400Ge4p0vC7z8BVbGmHeyUQCAXcATkGgPaAKAcJj3WIhTxpM6JMuU3YMMFvNKG1r9gylbWb32SSpDEvwHlpr4dHJInMGWljYElqXqMyFPAQmZpSmnvLX5c96NUQAACfAMQB97AQawqBA6gOzPX2v/Nl43aJ+p9+io+wNE+jytxESnaqOr1no7/Ap44in5nCFS1mNS74hO6mDPY5fCjVFhJZXR2ZaG1neNBACn2/Ec9Kd7VJMAEWcOnr51VkKnPDU5d/LAXj5cMPkbLZe1eZQQ95kACCCoROYReMfEg5Idwd5zad9IsiLR8ignq6byRQ/yzgT6w8VR/cEoEsDlqXwewOVK7eBFSQnQWWu/FevrC+gpfu8exGevYHO1Akke3gQrz4pQslcg7JLS+gC2dOqVrvn2U5nEXkOP3MYQX7oBjJdhHm6EA2oISujlAMb3UOG0SKUOHDdPjh43GwYALvFF8Bxp9woIAG6i7fxq94pBbtBohtn2XyYeGbm9e0zfO3EBHtjCs2vL6wMpLlwoHCjCXkU7cYxMuWvZ2Ex6SjxlNAoAoAO8BL7fUTAAyFu7sb2vQcwsPNYJOwKL3j2l9W0ruZLVUW19Dhp+bB3CmvRMWX3guKjfTCZB6Tp7r559ejAUfAJv+/VRD6QBfcDCCOvik8hI9mi8wmgUAMAf4GVIAL4DK8wSACWJuxvxvH5y4f77JoFcC3u+nJgIVrl7gcDlsd4ZaSwbNXmuWp47mYCGLwtY3+/AzqAa9flWj6/53C1tM9KrLB/MeGQUAMAp9BeQtrH5GcCPfHQAKFU3QbM+5nHKaOtwpHI/KXJfLU75boYVjTskRs27n/UNC15x2u9+H7uwKJMJ0mMdowDA5a4E/2nUyWYgK3MFAJsxY1jnKSM+wcyGb/Sg5CPz0d5ZUcdMbANvgzTgrkhR23on0r3ZLWsDerg+LW9MGQcA4q8x2G8ovgR0zLd7qMS4OxYvkKDC9PL6lg94SFf3VtvDEPuPxr35h6BdO8oaWqLJH/OeAK0bMBAAeFDI1xUHAN7we/CGRz1NAjCT2rgNIMToCkgFnN3Hyn6AoRrGHu62NWSKYQDgEVdDl7lOcQBA+eNbKB6lEwncJJ9iHeC5e49Z2ij5EoaXuVwyDJmZjwzEMABwidz2co3yADg+ofPA9hHPR0L9N2VydGtEcBgFAPAJXINt7HzFAbDPPWN8cOQIfqwatZRF++iA5v9I6ajJP9fTVk9pkBkIAA0AwDzFAcAb3FM7e4pAQtzOPBnrwEE4HjQeDI5bc27kSlKlma6n9owCAJdbPHbJ1mAMzHUbqKfJ0IIWAwGAJ4gKh9kXAaAcVAwEAO7U4igCQLm5l1syDABclc1YmmcXAXCiAsBT2YqDuqhb20AuZKIDMCZ5LKMOJU83qjBzjdAcglL51irFdfDsiKU+MEbrseA4GAdaZGZeEkDrQRizf30AALsAnir+siIAVEeRbgDwBww95oY3gA2ZLAGq825IdKgbAPDrYi4uSgDVQaUXAFTiwqhjCTaLEkA1IOgDAAgM+Rins7GAliIATjAAYBewHbuAQa7diTCiqAMUChH6kADYBfBgWDn3wqClCIAhD4AvMEL57uXkAKi2J0bxFIQrj/eb1uAOoGQxgmQalR6/0Swlv92iIFQVplEkug4hADV61X1hOsmgVTiFIvCGnpoSABm0o0gVENMBYixJwUjZ4o2NzQNyAynS+QnaCJYAnuYmeQ4lvgSoxZt0AIDr0vd8jf7kKc3UInQI9QMAHMBwxhtCAgCJL3q9/n8dQvzXdCiiKI4xmQm/hT15UVcCiKkVEkTj+Lz+2K0gmnJvCHSOsLCrIFXX6QkAqe3SoBRSYBakQMsQ4L/mQ0Cm8NcQo3l9SgFA2fWq6QDwUF0PB8WqNJxpmVXhr1y6lPDkUMWSIwciN4Y2h9+p5AVfOlQDAERS6lj1CJ0g6EeQAnLMQbFkzwFxjniOKUR4JpP0GVSZ6Tw1AZD6AqP4sVL2wleH+x4IBDLLF5w9m4bkE9TpqbwObvmIyaQnZzDCI6GgnDhTnQKttNRkZtwWkCw9XAIh7AtUfYVRaYuJ0S+DyEChDqXG6sUkkVJkvboU3OExgMnP/hOHhQs3fD7/QtUAwPvHvrQev3gcfrFozAEoiN/yept+pSoAnFVOJ5UkfulTsWjLgR3Dh5X80zoE8agKAFkKpPNU1ZYxJ0bvMABB/Mv3CaoOAKfTeTYVJH7rl+ZesyfGbB8/SqSMeXOjt5mHjMs6leoA4J06PSJy8pCXTsQJ0HjM748qGVtRV1cXu4tJEwCElwLxPmBwsNvBNebR0Ower3urWRi+cMOGDZ3xI9QMABEQ3AUQ8CtNSoYm23UxqhC/OezAvsP3t0fuVNANAGQQuFzTkCX8p0CiSxfsGjJEMAk8rWfM/EOfz8fT9A5aNJUA8RQ5HI7zBDP5FnIPzUbyxjOhI4weMnOhwkAg4g/gZzd493fkPNyIiV+LiYfhLXXRDQDSEVr8vjAc+H/fkdGl+9VE+gAAAABJRU5ErkJggg==';

            } else {
                $img = Image::make($storage_path.'/'.$file )->widen(100)->encode('jpg');
                $docs[$tipo_doc] = 'data:image/jpg;base64,' . base64_encode($img);
            }
        }

        $data->pdf_doc_file = $pdf_doc_file;

        if($request->ajax())
        {
            return response()->json([
                'view' => view('solicitud-cliente-distinguido.show-old',compact('data','docs'))->render()
            ]);
        }

        return view('solicitud-cliente-distinguido.show-old',compact('data','docs'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showGuest($id, Request $request)
    {
        $title = 'Solicitud de Cliente Distinguido';
        $reference = $id; 
        $path = $this->storage_path;
        $cd = SolicitudClienteDistinguidoNew::select('id')->where('reference',$reference)->first();
        
        $data = SolicitudClienteDistinguidoNew::find((int)$cd->id);
        $fecha_nacimiento = Carbon::parse($data->fecha_nacimiento)->format('d-M-Y');
        $data->fecha_nacimiento = $fecha_nacimiento;
        $estado = DB::connection('production_presta')->table('state')->select('name')->where('id_country',145)->where('id_state',$data->id_state)->first();
        $data->estado = $estado->name;

        $files = Storage::disk('samba-solicitudes-cd')->files($reference);
        
        $docs = array();
        
        foreach($files as $file)
        {
            $nom_archivo = explode('/',$file)[1];

            switch($nom_archivo)
            {
                case $nom_archivo == $data->ine:
                    $tipo_doc = 'ine';
                    break;
                case $nom_archivo == $data->comprobante:
                    $tipo_doc = 'comprobante';
                    break;
            }

            if(Storage::disk('samba-solicitudes-cd')->mimeType($reference.'/'.$nom_archivo) == 'application/pdf') {
                $docs[$tipo_doc] = 'data:image/jpg;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAVeUlEQVR4Xu1dC3hU1bXe+8zwCE+RZILiC1T0+qheUHFmAjnzCkISRBSrtlKp9tOrvd766K29vvCT1ifeTyv1fpWWWtveq3itkESEzJAJJhkspFYt9baoWAtIHrwKSB4zZ99/n3kwTDLvM+cRZn9fviQz++y99tr/WXvttddamxKdlIrqigkjeszziEBqCGPTCKGngDQLfkw6IVFZMhj5pc/nX6Jso9m3RrN/RNknXC5XOSGhhwll38GkD1e2dX23BuY/7PX6l2lJpaYAcLorl1BCXwADxmjJBA37Zhj/N73ept9qRYMmAFi0aJFp34HOpwmj92o1cB3124slz+PzNb+rBU2aAMDlEp8llNynxYD12SfbJ4UEW1NT01/Upk91ADg94i2UkVVqD1Tv/WEiPgkGidXv93erSauqABBFsdRkZp9C2Run5iAN1FdLKEg8AEGPWjSrCgCXp/J5rPt3qzU4I/YDpXB1RUXTDUuXEkkN+lUDgN1uHzuyZFgnBjVSjYEZug/Gfgyl8EE1xqAaAJyeykWU0dezGNSnjJBVjFI/DdGdfSV9/8jiWd1UHdFnriaMvJotQZAEt2J7+Itsn8u2vmoAcLkrX8HavzgDAkMg6qH9+w8tb29v78+gvq6rAPjXAPhv5kBkP/gwD4Yibw7PZvyIigAQ20CVNQ1l3DByA5CfjaTIeLBaVMwDAJzcg1KI2rE93FYo2tUDgEvcgb3/WakHQp/xeZv+vVCD1aLdPAFAsHx8HgrJ28M9haBfPQC4xUMYQFKTLwjZT4h5itfrPViIgWrVZt4ACBO+ZVTJWLGuru4rpcehJgAOg/jRSQdAySu+Rv8tSg9Q6/YUAgAfRv3JE8oWrF69OqTkmHQDAErYnV5v80tKDk4PbSkIAOjQ7DlfY7OiJnTdAIBRtnBjY/Pv9DBpStKgKAA4YYze5fM1/VQpGvUDAIHN37ihuU6pgemlHcUBAOcJidKFTY1Na5UYYxEASnAxRRsFAADv7bBAyazGRv8f8yW/CIB8OZjm+QIBgK8Fu00Cu3LDhk1/z2cIRQDkw70Mni0cALiJgPxJIOaKfLbORQBkMIn5VCkkACJ0vYMj5FoYioK50FkEQC5cy+IZFQDARcFKeBjDqTb7UgRA9jzL6glVABDeHt6H7eFzWRGHykUAZMuxLOurBgDCJEqERThIy+rksQiALCc02+rqAUCm7Cgm1Ikj5M2Z0kk7a2yqeKJ2EHKOxKiQjLAJlO2Cq9CRTAnXeb0jlvq26ZxGlQHAu+wOmXB6uN7/SSY84gDAbqJYlOUAAwAC8smnBgDg3X7cOyJob2lowQlr6lIEQDoO5fS95gDgNoLmiRPKqnB62JdqCEUA5DTB6R7SHgCcQnhXrYJS+O0iANLNl+Lf6wMAfFiQBA9t9Pp/lGyIRQmg+OTLbD+mA1RV1lKJKnJylyOpDEft38BR+38P9nwRADlyNfVjxwDgclU6CKUbC9JN5o32YHvowfawJfGRIgAyZ2IWNeMkgNN5NhWkjLZkWXSQS9W9hJlsPp/vr/EPpwUAY+w/oU5sz6XHofsMvYRSdnvy8R0DAA+F37+/qwtr8QQd8GM7QGAFCPZGaUkLAJgXHWX1LX4dEK8bEjpqrQsQ7JHCfe0YADjRCId/FSr5N3UygJbhw0rc69at6+X06BoAbCkR9rbbRERJWhFSPg4q7aayhrYGrRmZAwCuAwBWa013XP+v+bz+G7m2qlsAdFRXTMXa+RYm/eLjGMfIVoFIi0sbNn+sFUOzBYDVai0ZNXoEX0Yna0XzgH4pfdzX2PSILgGwuxZ5BFhfOzTXM5Iw7BATQueWr30PRwzql2wBEFkGboUUWKk+tUl7RBgeceoSAF01tkehNC2NI30zhFUIfvH2mPLCyO1YDn6mBUNzAYCcF2lfVztAcIkWNA/aJyM36hIAOKD6AgSfzokGTJeX17Xdz//urLHCoEJrw4OhT1rqW3+oBTNzAQCns6pq9pSQJPwef5ZqQfeAPvUIgM55l08iwrAvo8QKTLogut4DGO/g8znh+ScPWerakpo4C8ngXAHAaXJWOZ1Ukvg4hhWSxoza1iMAumttTokRX2QAX5Vd1jaWRtKlAAAf4fOLwgCgt1nqWn+e0UAVrpQPADgpDo/DDmDDc4fyTKjaFT0CoKvadgPEftRu/QkcK86Ncqiz1nYAu4Lx/H8YqK4qbwis14J7+QKA0+x2u6cSEnwdus4MLcYg96lHAGCdvwtvxosRpmwGAOSkEnxnYGZ9XVFmUYGeX7a2VRVvpsQJUgIAkTYpHEaug1HpCfx/tupA0CMAOmptd8LosyIRAB01Niu2LTzLCC+hspKDo+jqbSmdHQrFUAUBIJOIHcJw7BCcSJS9AG9lDT5Sx16gRwBgnV8CBkSTI30ACXApZ1JHjXUxHByQZ0gun+Fz9d+YSOdKAyARqEigPZHS/jMkwTxWkKScE2hLlJVBuiTPQ6xLAFRbF0HBi+YI2omJlreDXTXWpxmh35eZRek7UADnFuoNT9duoQGQrv9MvxevEs8yBcmOpPX1CICu2iunMya0R4hmpGT4OMtq/2HoBk2YeTHy+TIA4+FMGaF0PaMAwJnuKFqPANhT9bXRwvAxPJ9QOGZBYNPL1gb+COvgPvx3Ev8IHi7XlNcF3lJ6YjNtzygAcDgc5wkm9n+GkgBhcW/7HNujM8PSnn6HhOgmJkgxjd9kMp8xcU1+YdGZTvZg9YwCAOgSFxAaSp5iTo8SgDO8o9r2W0oJP64EAMivJcYa4xTADoj/SflMYL7PGgUAbvfsixkRPjScBOiosd+BpFHRhFE7sRY0QSLcHBnIGwDAonwnMZ/njQIAj0e8FFbV9w0HgO5a6/kII4s772cIGaNyijnGyJ3lDW2aZhMzEAAuAwC2GA4AnGDYA7gDxTkDiQ9Ns9S/p6mPolEA4KhyzBQkljxQVK86QEQRfApi/7i0sfj/i/L6Nlk51LIYBQDYBtrgVdVqSAnQXWO9QiL0vQTiV2H9TxnqpAYwjAIAxCTMgha9yZAAwNtOsR38M4g/PzoArff/UTqMAgC3WxTBRxjQkhQ9LwHyMlBrfwTHvo9FyTeZTBdOXPMuB4WmxUAAcAMAjcYFQI3NiwG44gawEktATsmQlESMcQDgmMMI495Hgxc9S4A919gsQj/ZDcrj7w6WEGg5s+zt1q1KTmi2bRkFAPA8micwljyOQs8AgGfQffAMenaQydkCN7Ero25i2U6eEvWNAgBnushkvQKARwR1bZXtAHCbkt2//owzgQuikweHEc1cwjkNhgFAuvuK9AqArpqKWkakWEy9JLGvCZQux/mgJwKCvUTqv8jy9paCXKOSTkoYBQCISUwdkqZfANgboby45YmgpAnu387OmplwDjVxr+AR4Qli/rKS09xU4Rs00k2+kSQAAPB18O9/DKUERs4B+FZP9gdAeotryxta5eSHMA8/hF+PxwbEyAOWhranMpk0JesYRQK43Y6b8CL9xlAA6Ky2roT16lZ58mH6tZRMnhp9yxnCq7qO7uIxA5UR6dCPW7dnTXq7JdFiqOR8D2jLQABYDABE/SgH8kRvS0DHXNvZ1Ew+xszLUTOw/H0fnj/H7QT2zp15Wshk4hclTIyM6DNzT+/0k73tqt02ZhQAON2VS+BHkfz2Ub0BoLPatgqC/5bIxHZKfYenTtrw4YDsoXtqbVcLjPAEDdFUt2+XlUyer5Y+YBgAeMTbsGN62RBLQOd86zlEkn0AzMne/viBwEn0J5j/78Z99gKshP9WUNkfadwoAIASeDtekf8yBgBq7FirWPRu4Y4g7Z16al170osS2aILh3cdHb8hpg/ISwa5C5HEit2olYxxxgGA406E1EeDbPSrA3TOs12KqBhu3pXNvjD63FtW14rkVKnLwTnWk3uH0QBqTYvUDEHkXV3oNDIGAsDdAMDzupcACT7/u3pL2Lmnrw4cTQcA/v2eWvuVlLFmKAPhCBpKDkIUrMHvEuwhx0OhRJ4regB1eiAhDlCJ/A2//0aZsKN01P6PcgkvMwoA3O7KezD85JdI6EEJRCzgtXhr34hONiZnMcT4q4mTz7eAe3p2n4+w6hnQbKdjwi/C4C6E4M/ZQxjbzD70/SEg0soE2mAZcaA5E0AYBQBOt3g/+PSMbiXADlEcOXpML4w+dEpY9LP3SusCPAiU8TW+u2ecVZKQx4ZSJz7656hjaCaSIcc6/wAoVgr9pmfL1r8bS1KR2JZhAOARfwCAP6lbACRY9vDy05uREu5UuIRzM3AFfkZlNJGU9AMyOyERugCeK+Ke4Xch/ARv+FEsBSPxTym+5z6FZ+GHR+AmuzGlB2vnY2V1gac4GA0LALf4IOhfpksAdM2rmIZonw9AHC4KkQtndCZX2OxC1T/APfx9KgjbcJX2tkm7e/5K29v7eSOJCabQ4DbSb/IkvtH7F4gn9QZ7ZwhMmInJvgqP2vAT73vA5dDrQaFvSeJuxCgSwOVxPIIxxDyqBgBBKx1g9zzbmWZKfxOf9SsJSiEQMNGEbQQ0NrFQcGsmJ4DIJbAcE39vrE1KPmWSUFXe0PJZsrchkpruu3jubtSJpXWFCF1Tennbwnj/A8MAwFX5GNbVR3QhAZgomrvG9C0AMbfhhx/rJrs/aBfCwdZKEvWODEr+8esDPCg0q8IdSjur7S8l5PPdjSCJOZMa2v6UqrFIkqr/jUiEcFVKluBE8pfR54wCADiFLgMvHtQUALDdj5NMJj7pd0cDPhMJ4oc+UPTeRCrANyzTNweU8PbhIOiutj+N8wQ5xRwv+GwfROJN6XILbZ87d8R400Ge4p0vC7z8BVbGmHeyUQCAXcATkGgPaAKAcJj3WIhTxpM6JMuU3YMMFvNKG1r9gylbWb32SSpDEvwHlpr4dHJInMGWljYElqXqMyFPAQmZpSmnvLX5c96NUQAACfAMQB97AQawqBA6gOzPX2v/Nl43aJ+p9+io+wNE+jytxESnaqOr1no7/Ap44in5nCFS1mNS74hO6mDPY5fCjVFhJZXR2ZaG1neNBACn2/Ec9Kd7VJMAEWcOnr51VkKnPDU5d/LAXj5cMPkbLZe1eZQQ95kACCCoROYReMfEg5Idwd5zad9IsiLR8ignq6byRQ/yzgT6w8VR/cEoEsDlqXwewOVK7eBFSQnQWWu/FevrC+gpfu8exGevYHO1Akke3gQrz4pQslcg7JLS+gC2dOqVrvn2U5nEXkOP3MYQX7oBjJdhHm6EA2oISujlAMb3UOG0SKUOHDdPjh43GwYALvFF8Bxp9woIAG6i7fxq94pBbtBohtn2XyYeGbm9e0zfO3EBHtjCs2vL6wMpLlwoHCjCXkU7cYxMuWvZ2Ex6SjxlNAoAoAO8BL7fUTAAyFu7sb2vQcwsPNYJOwKL3j2l9W0ruZLVUW19Dhp+bB3CmvRMWX3guKjfTCZB6Tp7r559ejAUfAJv+/VRD6QBfcDCCOvik8hI9mi8wmgUAMAf4GVIAL4DK8wSACWJuxvxvH5y4f77JoFcC3u+nJgIVrl7gcDlsd4ZaSwbNXmuWp47mYCGLwtY3+/AzqAa9flWj6/53C1tM9KrLB/MeGQUAMAp9BeQtrH5GcCPfHQAKFU3QbM+5nHKaOtwpHI/KXJfLU75boYVjTskRs27n/UNC15x2u9+H7uwKJMJ0mMdowDA5a4E/2nUyWYgK3MFAJsxY1jnKSM+wcyGb/Sg5CPz0d5ZUcdMbANvgzTgrkhR23on0r3ZLWsDerg+LW9MGQcA4q8x2G8ovgR0zLd7qMS4OxYvkKDC9PL6lg94SFf3VtvDEPuPxr35h6BdO8oaWqLJH/OeAK0bMBAAeFDI1xUHAN7we/CGRz1NAjCT2rgNIMToCkgFnN3Hyn6AoRrGHu62NWSKYQDgEVdDl7lOcQBA+eNbKB6lEwncJJ9iHeC5e49Z2ij5EoaXuVwyDJmZjwzEMABwidz2co3yADg+ofPA9hHPR0L9N2VydGtEcBgFAPAJXINt7HzFAbDPPWN8cOQIfqwatZRF++iA5v9I6ajJP9fTVk9pkBkIAA0AwDzFAcAb3FM7e4pAQtzOPBnrwEE4HjQeDI5bc27kSlKlma6n9owCAJdbPHbJ1mAMzHUbqKfJ0IIWAwGAJ4gKh9kXAaAcVAwEAO7U4igCQLm5l1syDABclc1YmmcXAXCiAsBT2YqDuqhb20AuZKIDMCZ5LKMOJU83qjBzjdAcglL51irFdfDsiKU+MEbrseA4GAdaZGZeEkDrQRizf30AALsAnir+siIAVEeRbgDwBww95oY3gA2ZLAGq825IdKgbAPDrYi4uSgDVQaUXAFTiwqhjCTaLEkA1IOgDAAgM+Rins7GAliIATjAAYBewHbuAQa7diTCiqAMUChH6kADYBfBgWDn3wqClCIAhD4AvMEL57uXkAKi2J0bxFIQrj/eb1uAOoGQxgmQalR6/0Swlv92iIFQVplEkug4hADV61X1hOsmgVTiFIvCGnpoSABm0o0gVENMBYixJwUjZ4o2NzQNyAynS+QnaCJYAnuYmeQ4lvgSoxZt0AIDr0vd8jf7kKc3UInQI9QMAHMBwxhtCAgCJL3q9/n8dQvzXdCiiKI4xmQm/hT15UVcCiKkVEkTj+Lz+2K0gmnJvCHSOsLCrIFXX6QkAqe3SoBRSYBakQMsQ4L/mQ0Cm8NcQo3l9SgFA2fWq6QDwUF0PB8WqNJxpmVXhr1y6lPDkUMWSIwciN4Y2h9+p5AVfOlQDAERS6lj1CJ0g6EeQAnLMQbFkzwFxjniOKUR4JpP0GVSZ6Tw1AZD6AqP4sVL2wleH+x4IBDLLF5w9m4bkE9TpqbwObvmIyaQnZzDCI6GgnDhTnQKttNRkZtwWkCw9XAIh7AtUfYVRaYuJ0S+DyEChDqXG6sUkkVJkvboU3OExgMnP/hOHhQs3fD7/QtUAwPvHvrQev3gcfrFozAEoiN/yept+pSoAnFVOJ5UkfulTsWjLgR3Dh5X80zoE8agKAFkKpPNU1ZYxJ0bvMABB/Mv3CaoOAKfTeTYVJH7rl+ZesyfGbB8/SqSMeXOjt5mHjMs6leoA4J06PSJy8pCXTsQJ0HjM748qGVtRV1cXu4tJEwCElwLxPmBwsNvBNebR0Ower3urWRi+cMOGDZ3xI9QMABEQ3AUQ8CtNSoYm23UxqhC/OezAvsP3t0fuVNANAGQQuFzTkCX8p0CiSxfsGjJEMAk8rWfM/EOfz8fT9A5aNJUA8RQ5HI7zBDP5FnIPzUbyxjOhI4weMnOhwkAg4g/gZzd493fkPNyIiV+LiYfhLXXRDQDSEVr8vjAc+H/fkdGl+9VE+gAAAABJRU5ErkJggg==';

            } else {
                $img = Image::make($this->storage_path.'/'.$file )->widen(100)->encode('jpg');
                $docs[$tipo_doc] = 'data:image/jpg;base64,' . base64_encode($img);
            }
        }
        $data->nom_archivo = $nom_archivo;

        if($request->ajax())
        {
            return response()->json([
                'view' => view('solicitud-cliente-distinguido.show-guest',compact('data','docs','title'))->render()
            ]);
        }

        return view('solicitud-cliente-distinguido.show-guest',compact('data','docs','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($id)
    {

        $data = SolicitudClienteDistinguidoOld::find($id);

        $token_files = explode('_',$data->comprobante)[0];

        $storage_path = Storage::disk('samba_clientes')->getDriver()->getAdapter()->getPathPrefix();

        $files = Storage::disk('samba_clientes')->allFiles($token_files);

        $zip_file_name = $id.'-'.$token_files.'-'.strtoupper(Str::camel($data->nombre)).'_'.strtoupper(Str::camel($data->apellido)).'-'.strtoupper(Str::camel($data->rfc));

        $zip_file_path = $storage_path.'_downloads/'.$zip_file_name.'.zip';

        $zip = new ZipArchive();

        if ($zip->open($zip_file_path, ZipArchive::CREATE  | ZipArchive::OVERWRITE) !== true) {
            return response()->html("No se pudo crear el archivo zip");
        }

        foreach ($files as $file)
        {
//            $zip->addFile($storage_path.'/'.$file,$zip_file_name);
            if (!is_file($file))
                $zip->addFromString(explode('/',$file)[1], file_get_contents($storage_path.'/'.$file));
        }

        $zip->close();

        return response()->download($zip_file_path,$zip_file_name)->deleteFileAfterSend(true);
    }
}
