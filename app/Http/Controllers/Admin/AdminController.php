<?php
/**
 * Created by PhpStorm.
 * User: cheko
 * Date: 21/01/20
 * Time: 17:06
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class AdminController extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

}