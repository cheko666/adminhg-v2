<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BcProducto;
use App\Models\Codigo;
//use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\MessageBag;


class ProductoController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ajaxSearchInfoSatFromSku($serie,$producto)
    {
        $data = ['serie'=>$serie,'producto'=>$producto];
        $rules = array('serie' => 'required|numeric', 'producto' => 'required|numeric');

        // Setup the validator
        $request = new Request($data);
        $request->validate($rules);

        // Validate the input and return correct response
/*        if ($validator->fails())
        {
            return response()->json(array('success'=>false,'errors' => $validator->getMessageBag()->toArray()),400);
        }*/

        $data = BcProducto::select('id','sat_c_prodserv', 'sat_c_unidad')
            ->where('Serie',$data['serie'])
            ->where('Producto',$data['producto'])
            ->first();

        return response()->json(array('data'=>$data,'message'=>'Código encontrado exitosamente.','success'=>true),200);
    }

    public function searchInfoSatFromSku()
    {
        $title = 'Buscar información del SAT de producto';
//		list($serie,$producto) = explode('-',$string);

        return view('sat.info-sat_search',compact('title'));
    }

    public function showInfoSatFromSku(Request $request)
    {

        $title = 'Clave de Producto-Servicio del SAT';
        $input = $request->except('_token','_');
        $rules = array('serie' => 'required|numeric|integer', 'producto' => 'required|numeric|integer');

        // Setup the validator
        $request->validate($rules);

        $data = BcProducto::select('id','reference AS codigo','description AS descripcion','sat_c_prodserv', 'sat_c_unidad')
            ->where('serie',$input['serie'])
            ->where('producto',$input['producto'])
            ->first();
            // dd($data);
        if(!$data){
            $errors = new MessageBag();
            $errors->add('found','No se encontró producto con el código: '.$input['serie'].'-'.$input['producto']);

            return redirect()->back()->withErrors($errors->all());
        }

        return view('sat.info-sat_show',compact('title','data'));

    }

}
