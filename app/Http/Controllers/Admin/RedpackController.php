<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use stdClass;

class RedpackController extends AdminController
{
    public $name = 'redpack';
    public $phone = '01 800 378-2338';
    private $zones = array();
    private $limite_peso = 60;
    private $limite_seguro = 3001;
    private $tasa_seguro = 0.015;
    protected $costo_seguro = 39;
    protected $cp_origen = 6720;
    protected $monto_por_empaque = 10;
    protected $_ref_pedidos;
    protected $_kg_adicionales;
    protected $_guia;
    protected $_peso = array();
    protected $_zona_envio;
    protected $_monto_compra;


    public function __construct()
    {
        $this->zones = [
            [
                "zona"=>"1",
                "id_zone"=>"9",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"10",
            ],
            [
                "zona"=>"1",
                "id_zone"=>"11",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"10",
            ],
            [
                "zona"=>"2",
                "id_zone"=>"12",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"11",
            ],
            [
                "zona"=>"3",
                "id_zone"=>"13",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"17",
            ],
            [
                "zona"=>"4",
                "id_zone"=>"14",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"20",
            ],
            [
                "zona"=>"5",
                "id_zone"=>"15",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"22",
            ],
            [
                "zona"=>"7",
                "id_zone"=>"17",
                "costo_guia"=>[
                    "1kg"=>"106.11",
                    "5kg"=>"132.82",
                    "10kg"=>"166.69",
                ],
                "kg_adicional"=>"24",
            ],
        ];
    }

    public function searchRastreo()
    {

        $title = 'Rastrear su pedido';

        return view('redpack.rastreo.search',compact('title'));

    }

    /**
     * @param Request $request
     * @return $this
     */
    public function showRastreo(Request $request)
    {
        $title = 'Rastreo de su pedido';

        $validator = Validator::make($request->all(),[
            'guia'=>'required|numeric|digits:9'
        ]);


        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ;
        }

        $url = "http://ws.redpack.com.mx/RedpackAPI_WS/services/RedpackWS?wsdl";

//    $guias = array('466974949','466973843','038253094','466956174');
//    $this->_guias[] = '466973843';
        $this->_guias[] = $request->get('guia');


        $array_obj_guias = array();

        foreach($this->_guias as $guia)
        {
            $obj_guia = new stdClass();
            $obj_guia->numeroDeGuia = (string)$guia;
            $array_obj_guias[] = $obj_guia;
        }

        try {
            $client = new \SoapClient($url,array(
                    'trace' => true,
                    'keep_alive' => true,
                    'connection_timeout' => 5000,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'compression'   => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE,
                )
            );

            $response = $client->rastreo(array(
                'idUsuario' => config('transportistas.redpack.id'),
                'PIN' => config('transportistas.redpack.pin'),
                'guias' => $array_obj_guias
            ))->return;

//      dd($response->resultadoConsumoWS);
//      dd(property_exists($response->resultadoConsumoWS,'descripcion'));

            if(is_object($response->resultadoConsumoWS)){
                return view('redpack.rastreo.show',compact('title','response'))->with('transportista',$this);
            }
            if(is_array($response->resultadoConsumoWS) && $response->resultadoConsumoWS[0]->estatus == 36){
                return redirect()
                    ->back()
                    ->withErrors(['messages'=>$response->resultadoConsumoWS[0]->descripcion])
                    ;
            }


        }

        catch ( Exception $e) {
            echo('<br>'. $e->getMessage());
        }
    }

    public function cotizarEnvioShow(Request $request)
    {
        $title = 'Cotizar envío por Redpack';

        $rules = [
            'id_pedido' => 'required|numeric',
            'importe_pedido' => 'required|numeric',
            'peso'=>'required|numeric',
            'peso_volumetrico'=>'required|numeric',
            'cp'=>'required|numeric|min:4'
        ];

        // Setup the validator
        $request->validate($rules);
        $cp = $request->get('cp');
        $hay_cobertura = $this->getDatosCobertura($cp);

        if(!$hay_cobertura){
            $errors = new MessageBag();
            $errors->add('not_found','No se tiene cobertura para el código postal: '.$cp.' o no existe en el catálogo de códigos postales. Verifica la información y vuelve a intentar.');

            return redirect()->back()->withErrors($errors->all());
        }

        $costo_envio = $this->getOrderShippingCost($request->all(), false);

        if(!$costo_envio){
            $errors = new MessageBag();
            $errors->add('not_found','No se pudo establecer el costo de envío. Por favor, revisa la información que ingresaste.');

            return redirect()->back()->withErrors($errors->all());
        }


        $costo_envio = json_decode(json_encode($costo_envio), false);

        return view('redpack.coti-envio.show',compact('title','costo_envio'));
    }

    public function cotizarEnvioForm()
    {
        $title = 'Cotizar envío por Redpack';

        return view('redpack.coti-envio.form',compact('title'));
    }


    public function getOrderShippingCost($params, $shipping_cost)
    {
        $productos_transportista = 1;

        $this->_monto_compra = floatval(str_replace(',','',$params['importe_pedido']));

        $this->getTiposDePeso($params['peso'],$params['peso_volumetrico']);

        $this->_ref_pedidos = $params['id_pedido'];

        $this->_guia = $this->getPesoGuiaUsara($this->_peso['facturable']);
        $peso_guia = substr($this->_guia, 0, strlen($this->_guia) - 2);
        $this->_kg_adicionales = ($this->_peso['facturable'] - $peso_guia) <= 0 ? 0 : ($this->_peso['facturable'] - $peso_guia);

        $this->_zona_envio = $this->getZoneByCp((int)$params['cp']);

        if( $productos_transportista !== 1 && !$this->getZoneData($this->_zona_envio->id_zone) || $this->_peso['facturable'] > $this->limite_peso ){
            return false;
        }

        $costos_envio = $this->getCostoDesglosado($this->_peso,$this->_monto_compra,$params['cp']);

        return $costos_envio;
    }
/*
    public function getOrderShippingCostOld($params, $shipping_cost) {

        //$cart=new Cart(Context::getContext()->cart->id);

        //$productos_transportista = $this->isCarrierEnabled($cart->getProducts());
        $productos_transportista = 1;

        //$address_user_id = $params->id_address_delivery;

        //$zone_id = $this->getZone($address_user_id);

        //$peso_volumetrico = $this->getPesoVolumetrico($cart->getProducts());
        //$peso_bruto = $cart->getTotalWeight();
        //$monto_compra = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $monto_compra = floatval(str_replace(',','',$params['importe_pedido']));
        $this->getTiposDePeso($params['peso'],$params['peso_volumetrico']);
        $this->_ref_pedidos = $params['id_pedido'];

        $this->_guia = $this->getPesoGuiaUsara($this->_peso['facturable']);
        $peso_guia = substr($this->_guia, 0, strlen($this->_guia) - 2);
        $this->_kg_adicionales = ($this->_peso['facturable'] - $peso_guia) <= 0 ? 0 : ($this->_peso['facturable'] - $peso_guia);

        $this->_zona_envio = $this->getZoneByCp((int)$params['cp']);

        if( $productos_transportista !== 1 && !$this->getZoneData($this->_zona_envio) || $this->_peso['facturable'] > $this->limite_peso ){
            return false;
        }

        $costos_envio = $this->getCostoDesglosado($this->_peso,$monto_compra,$params['cp']);

//dd($costos_envio);
        return $costos_envio;

    }
*/
    /**
     * @param $peso_real
     * @param $peso_volumetrico
     * @return array
     */
    public function getTiposDePeso($peso_real,$peso_volumetrico)
    {
        $this->_peso['real'] = $peso_real;
        $this->_peso['volumetrico'] = $peso_volumetrico;
        $this->_peso['facturable'] = ceil( ( $peso_real > $peso_volumetrico ) ? ceil($peso_real+1) : ceil($peso_volumetrico+1) );

        return $this->_peso;
    }

    /**
     * @param string $cp
     * @return bool|\Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getDatosCobertura($cp)
    {
//        $cp=24129;
        $info = DB::connection('lanceta_bd_ec')
            ->table('redpack_cobertura')
            ->where('codigopostal','LIKE',(string)$cp)
            ->groupBy('codigopostal')
            ->first();

        if(!$info)
            return false;

        return $info; // Cobertura con costo
    }


    public function isCarrierEnabled($productos)
    {
        $bandera = 0;

        for($i = 0; $i < count($productos); $i++) {

            $query = new DbQuery();
            $query->select('producto_id');
            $query->from('producto_estafeta', 'pt');
            $query->where('pt.`producto_id` = ' . $productos[$i]["id_product"]);
            $results = Db::getInstance()->executeS($query);

            if (count($results) == 0) {
                $bandera = 0;
                break;
            }else{
                $bandera = 1;
            }
        }

        return $bandera;
    }


    public function getZone($id_address)
    {
        $id_address = $id_address != '0' ? $id_address : '63';

        $result = Db::getInstance()->getRow('
        SELECT *
        FROM `address` a
        JOIN `pqshippingcostsbasedonzipcodes_conditions` AS z ON (a.postcode between z.zipcode_min and z.zipcode_max)
        WHERE a.`id_address` = '.intval($id_address));
        $_idZones = $result['id_zone'];


        // Buscar id de zona segun estado
        if($_idZones == "") {
            $result_address = Db::getInstance()->getRow('
            SELECT *
            FROM `address` a
            WHERE a.`id_address` = '.intval($id_address));

            $id_state = $result_address["id_state"];

            $query_zona_estado = Db::getInstance()->getRow("select * FROM `"._DB_PREFIX_."state` s where s.id_state = ".$id_state);
            $_idZones = $query_zona_estado["id_zone"];

        }

        return $_idZones;
    }

    public function getZoneByCp($cp)
    {
        $result = Db::connection('production_presta')
            ->table('pqshippingcostsbasedonzipcodes_conditions')
            ->whereRaw('? between zipcode_min and zipcode_max',['cp'=>intval($cp)])
            ->first();
        /*
        $result = Db::connection('lanceta_bd_ec')
            ->table('pqshippingcostsbasedonzipcodes_conditions')
            ->whereRaw('? between zipcode_min and zipcode_max',['cp'=>intval($cp)])
            ->first();
        */
        $_idZones = $result->id_zone;

        return $result;
    }

    public function getZoneData($id_zone) {

        if(!$data = $this->findZoneData($this->zones,$id_zone))
            return false;

        return $data;
    }


    public function findZoneData($array,$id_zone){
        foreach($array as $k => $v){
            if($v['id_zone'] == $id_zone){
                return $v;
            }
        }
        return;
    }

    public function getCosto($peso,$monto_productos,$cp)
    {
        return $this->getCostoDesglosado($peso,$monto_productos,$cp)['costo_total'];
    }


    /**
     * @param $peso
     * @param $monto_productos
     * @param $cp
     * @return mixed
     */
    public function getCostoDesglosado($peso,$monto_productos,$cp)
    {
        $peso_paquete = $peso['facturable'];

        if($peso_paquete > $this->limite_peso) {
            return false;
        }

//        $montos_finales = json_decode(json_encode($this->getMontoFinalEnvioCallcenter($cp)), true);
        $datos_costo_cobertura = $this->getCostosFromCodigosPostalesCallcenter($cp);
        $datos_costo_cobertura->estado = $this->_zona_envio->d_estado;

        $num_kg_guia = 'costo_guia_'.explode('kg',$this->_guia)[0];

        if(!$datos_costo_cobertura) {
            return false;
        }

        $monto_cobertura = $datos_costo_cobertura->{'costo_cobertura_'.$this->_guia_usara};
        $monto_kg_adicionales = $this->_kg_adicionales*$datos_costo_cobertura->costo_kg_adicional;
        $this->costo_seguro = $this->_monto_compra > $this->limite_seguro ? (float)$this->_monto_compra * $datos_costo_cobertura->tasa_seguro : (float)$datos_costo_cobertura->costo_seguro;

        $subtotal = $monto_cobertura + $monto_kg_adicionales + $this->costo_seguro + $this->monto_por_empaque;
        $iva = (float) 1 + (16 / 100);
        $total = round(( $subtotal * $iva), 2);

        $costos['datos_cobertura'] = (array)$datos_costo_cobertura;
        $costos['zona_envio'] = $this->_zona_envio->id_zone;
        $costos['cp_origen'] = $this->cp_origen;
        $costos['cp_destino'] = $cp;

        $costos['costo_cobertura']['importe_cobertura'] = number_format($monto_cobertura,2,'.',',');
        $costos['costo_cobertura']['tipo_cobertura'] = $datos_costo_cobertura->tipo_cobertura;
        $costos['costo_cobertura']['kg_cobertura'] = $datos_costo_cobertura->costo_kg_adicional;
        $costos['costo_cobertura']['tiempo_entrega']['express'] = $datos_costo_cobertura->express;
        $costos['costo_cobertura']['tiempo_entrega']['ecoexpress'] = CarbonInterval::hours((int)$datos_costo_cobertura->ecoexpress)->cascade()->forHumans();
        $costos['costo_cobertura']['tipo_entrega']['domicilio'] = $datos_costo_cobertura->domicilio == 'Y' ? 'A domicilio' : '';
        $costos['costo_cobertura']['tipo_entrega']['ocurre'] = $datos_costo_cobertura->ocurre == 'Y' ? 'A Ocurre' : '';


        $costos['guia_usada'] = $this->_guia;
        $costos['kg_adicionales'] = $this->_kg_adicionales;

        $costos['pedidos'] = $this->_ref_pedidos;
        $costos['monto_productos'] = number_format($monto_productos,2,'.',',');

        $costos['peso'] = $this->_peso;

        $costos['monto_guia'] = number_format($datos_costo_cobertura->{'costo_guia_'.$this->_guia_usara},2,'.',',');
        $costos['monto_kg_adicionales'] = number_format($monto_kg_adicionales,2,'.',',');

        $costos['porcentaje_seguro'] = $monto_productos > $datos_costo_cobertura->limite_seguro ? $datos_costo_cobertura->tasa_seguro : '';
        $costos['limite_seguro'] = number_format($datos_costo_cobertura->limite_seguro,2,'.',',');
        $costos['monto_empaque'] = number_format($this->monto_por_empaque,2,'.',',');
        $costos['monto_seguro'] = number_format($this->costo_seguro,2,'.',',');
        $costos['monto_envio_subtotal'] = number_format($subtotal,2,'.',',');
        $costos['monto_envio_iva'] = number_format($subtotal * 0.16,2,'.',',');
        $costos['monto_envio_total'] = number_format($total,2,'.',',');

        return $costos;

    }

    public function getMontoFinalEnvio($cp,$datos_zona_guias)
    {
        $costo_final = array();
        $datos_costo_cobertura = $this->getDatosCobertura($cp);

        if($datos_costo_cobertura->tipo_cobertura == 'NORMAL')
        {
            $importe_cobertura = 0;
            $monto_kg_adicionales = $this->_kg_adicionales * $datos_zona_guias["kg_adicional"];
            $costo_final['importe_ckg'] = $datos_zona_guias["kg_adicional"];
            $costo_final['monto_kg_adicionales'] = $monto_kg_adicionales;
            $costo_final['costo_final'] = $datos_zona_guias["costo_guia"][$this->_guia] + $monto_kg_adicionales + $this->costo_seguro;

        }

        if( str_contains('COBERTURA CON COSTO',$datos_costo_cobertura->tipo_cobertura) )
        {
            $importe_cobertura = $datos_costo_cobertura->importecc;
            $monto_kg_adicionales = $this->_kg_adicionales * $datos_costo_cobertura->cc_kg;
            $costo_final['importe_ckg'] = $datos_costo_cobertura->cc_kg;
            $costo_final['monto_kg_adicionales'] = $monto_kg_adicionales;
            $costo_final['costo_final'] = $datos_zona_guias["costo_guia"][$this->_guia] + $importe_cobertura + $monto_kg_adicionales + $this->costo_seguro;

        }

        if( strpos($datos_costo_cobertura->tipo_cobertura,'ESPECIAL') !== false )
        {
            $importe_cobertura = $datos_costo_cobertura->importece;
            $monto_kg_adicionales = $this->_kg_adicionales * $datos_costo_cobertura->ce_kg;
            $costo_final['importe_ckg'] = $datos_costo_cobertura->ce_kg;
            $costo_final['monto_kg_adicionales'] = $monto_kg_adicionales;
            $costo_final['monto_empaque'] = $this->monto_por_empaque;
            $costo_final['costo_final'] = $datos_zona_guias["costo_guia"][$this->_guia] + $importe_cobertura + $monto_kg_adicionales + $this->costo_seguro + $this->monto_por_empaque;

        }

        $costo_final['importe_cobertura'] = $importe_cobertura;
        $costo_final['tipo_cobertura'] = $datos_costo_cobertura->tipo_cobertura;
        $costo_final['datos_cobertura'] = $datos_costo_cobertura;

        return $costo_final;
    }

    public function getMontoFinalEnvioCallcenter($cp)
    {
        $costo_final = array();
//        $datos_zona_guias = $this->getZoneData($this->_zona_envio);

        $datos_costo_cobertura = $this->getCostosFromCodigosPostalesCallcenter($cp);
        $guia_usara = 'costo_guia_'.$this->_guia_usara;
        $this->costo_seguro = $this->_monto_compra > $this->limite_seguro ? (float)$this->_monto_compra * $datos_costo_cobertura->tasa_seguro : (float)$datos_costo_cobertura->costo_seguro;
        $importe_cobertura = $datos_costo_cobertura->$guia_usara;
        $monto_kg_adicionales = $this->_kg_adicionales * $datos_costo_cobertura->costo_kg_adicional;

        $costo_final['costo_guia'] = $datos_costo_cobertura->$guia_usara;
        $costo_final['costo_cobertura'] = $datos_costo_cobertura->$guia_usara;
        $costo_final['costo_kg_adicional'] = $datos_costo_cobertura->costo_kg_adicional;
        $costo_final['costo_final'] = $importe_cobertura + $monto_kg_adicionales + $this->costo_seguro;
        $costo_final['monto_kg_adicionales'] = $monto_kg_adicionales;
        $costo_final['tipo_cobertura'] = $datos_costo_cobertura->tipo_cobertura;
        $costo_final['monto_empaque'] = $this->monto_por_empaque;
        $costo_final['datos_cobertura'] = $datos_costo_cobertura;

        return $costo_final;
    }

    /**
     * @param $peso_paquete
     * @return string |1kg|5kg|10kg
     */
    public function getPesoGuiaUsara($peso_paquete) {

//        $peso_paquete = 9.8;

        switch ($peso_paquete){
            case in_array($peso_paquete, range(0 , 5)):
                $peso_guia =  "1kg";
                $this->_guia_usara = 1;
                break;
            case in_array($peso_paquete, range(6 , 10)):
                $peso_guia =  "5kg";
                $this->_guia_usara = 5;
                break;
            case in_array($peso_paquete, range(11 , $this->limite_peso)):
            default:
                $peso_guia =  "10kg";
                $this->_guia_usara = 10;
                break;
        }

        return $peso_guia;
    }

    public function getPesoVolumetrico($productos) {

        $volumen = 0;
        $suma_volumen = 0;
        for($i = 0; $i < count($productos); $i++) {
            $consulta = "SELECT id_product, id_product_attribute from "._DB_PREFIX_."product_attribute WHERE reference = ".intval($productos[$i]["reference"])." AND id_product = ".intval($productos[$i]["id_product"]);
            $res = Db::getInstance()->getRow($consulta);


            if (count($res) > 0) {//tiene una combinacion y el volumen cambia

                $consulta2 = "SELECT * from codigos WHERE id = ".intval($productos[$i]["reference"])." and producto_id= ".intval($productos[$i]["id_product"]);
                $res2 = Db::getInstance()->getRow($consulta2);

                $suma_volumen = $productos[$i]["cart_quantity"] * (intval($res2["ancho"]) * intval($res2["altura"]) * intval($res2["fondo"]) / 5000 );
                $volumen += $suma_volumen;

            } else {
                $suma_volumen = $productos[$i]["cart_quantity"] * (intval($productos[$i]["width"]) * intval($productos[$i]["height"]) * intval($productos[$i]["depth"]) / 5000 );
                $volumen += $suma_volumen;
            }

        }

        return $volumen;

    }

    public function getCostosFromCodigosPostalesCallcenter($cp)
    {
        $datos_cp = DB::connection('lanceta_bd_ec')->table('codigos_postales_callcenter')->where('codigo','like',(string)$cp)->first();

        return $datos_cp;

    }

}
