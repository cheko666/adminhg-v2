<?php

namespace App\Http\Controllers\Admin;

use App\Mail\CodigosDiferenciasMgPsEncontrados;
use App\Models\LSCProducto;
use App\Repositories\CodigoRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
// use Maatwebsite\Excel\Facades\Excel;

class LSCProductoController extends AdminController
{
    protected $codigoRepository;
    public function __construct(CodigoRepository $codigoRepository) {
        $this->codigoRepository = $codigoRepository;
    }
    public function index()
    {
        
        $results = LSCProducto::all();
        
        return json_encode(LSCProducto::all());
    }

    public function create(Request $request)
    {

        $data = simplexml_load_string($request->getContent());
        $data = json_decode(json_encode($data), true);

        $serie_producto = array_map('intval',explode(',', number_format($data['product']['id'], 0)));

        $validator = Validator::make($data['product'], [
            "id"    => "required|integer",
            "ps_reference"    => "required|digits:6|size:6",
            'id_tax' => ['required','integer',Rule::in(['1', '2'])],
            'lsc_clave_marca' => 'required|alpha_num|max:10',
            'lsc_clave_categoria' => 'required|alpha_num|max:10',
            'descripcion' => 'required|max:100',
            'ps_supplier_reference' => 'required|max:100',
            'unidad' => 'required|min:2|max:4',
            'registro_sanitario' => 'max:36',
            'sat_c_prodserv' => 'required|max:24',
            'sat_c_unidad' => 'required|max:24',
            'peso' => 'required|between:0,999.999',
            'anchura' => 'required|between:0,999.99',
            'altura' => 'required|between:0,999.99',
            'profundidad' => 'required|between:0,999.99',
            'costo' => 'required|between:0,99999.99',
            'precio' => 'required|numeric|between:0,99999.99',
            'descuento' => 'required|integer|between:0,99',
            'created_at' => 'required|date_format:Y-m-d H:i:s',
            'updated_at' => 'required|date_format:Y-m-d H:i:s',

        ]);

        if ($validator->fails())
        {
            return response()->json([ 'status'=>'error','status_code'=> 102, 'errors'=>$validator->errors() ], 400);
        }

        $data['product']['serie'] = $serie_producto[0];
        $data['product']['producto'] = $serie_producto[1];

        $lsc_producto = json_decode(json_encode(LSCProducto::create($data['product'])));

        return response()->json($lsc_producto,200,['Content-Type'=>'application/json; charset=utf-8'],JSON_UNESCAPED_UNICODE);

    }


    public function listarDiferenciasBcPs(Request $request)
    {
        $idp = $request->get('idp') == 'todo' || !$request->has('idp') ? null : $request->get('idp');
        $campos_comparar = $request->get('campos_comparar') == 'todo' || !$request->has('campos_comparar') ? LSCProducto::getCamposComparar() : LSCProducto::setCamposCompararFromIds($request->get('campos_comparar'));
        $campos = LSCProducto::getCamposComparar();

        $collection = LSCProducto::listarDiferenciasBcPs($idp,$campos_comparar,'todo');
        // dd($collection);
        $title = 'Lista de Diferencias BC vs PS';
        return view('admin.bc.diferencias-bcps', compact('collection','title','campos','campos_comparar','idp'));

    }


    public function corregirDiferenciasBcPs(Request $request)
    {
        
        $idp = $request->get('idp') == 'todo' || !$request->has('idp') ? null : $request->get('idp');
        $campos_comparar = $request->get('campos_comparar') == 'todo' || !$request->has('campos_comparar') ? LSCProducto::getCamposComparar() : LSCProducto::setCamposCompararFromIds(explode(',',$request->get('campos_comparar')));
        $tipo_productos = $request->get('tipo_producto') == 'todo' || !$request->has('tipo_producto') ? 'todo' : $request->get('tipo_producto');
        
        $save_path = 'docs/codigos/diferenciasAdminhgPs';
        $mensaje = array();
        $data_mail = array();
        $i_correccion = 0;
        $fecha = Carbon::now()->format('Ymd_His');
        
        do {

            /**
             * Verificamos diferencias
             */
            $collection = LSCProducto::listarDiferenciasBcPs($idp,$campos_comparar,$tipo_productos);
            
            $diferencias1 = $collection->count();
            $diferencias = $diferencias1;
            $diferencias2 = 0;
            
            $intento = $i_correccion + 1;
            $lsc_producto = new LSCProducto();

            // Corregimos diferencias descuentos
            // $this->corregirDescuentos($collection);

            if($diferencias > 0) {

                // Corregimos diferencias
                $lsc_producto->corregirDiferenciasBcPs($idp,$campos_comparar,$tipo_productos);

                $diferencias2 = $lsc_producto->listarDiferenciasBcPs($idp,$campos_comparar,$tipo_productos)->count();

                // Registramos las diferencias
                $nombre_archivo = $i_correccion == 0 ? 'diferencias-' . $fecha .'.csv' : 'diferencias-'. $fecha . '-' . $i_correccion . '.csv';
                $ruta_archivo = Storage::disk('samba_media')->getDriver()->getAdapter()->getPathPrefix().$save_path.'/'.$nombre_archivo;
                $this->export($collection,'samba_media',$save_path.'/'.$nombre_archivo);
                $diferencias = $i_correccion == 0 ? $diferencias1 : $diferencias2;

                $mail_data['mensaje'][] = 'Intento '. $intento .': Se encontraron ' . $diferencias . ' diferencias. ' . ($i_correccion != 0 ? 'Ver el archivo: '. $ruta_archivo : '' );
                $mail_data['ruta_archivo'] = $save_path.'/'.$nombre_archivo;

                $i_correccion ++;

                // Notificamos que quedaron diferencias para corregir a mano
                Mail::to(env('MAIL_TEST','sergioruiz@lancetahg.com'))->send( new CodigosDiferenciasMgPsEncontrados($mail_data));
                
                return redirect()->back();

            } else {
                $mail_data['mensaje'][] = 'Intento '. $intento .': No se encontraron diferencias.';
                $i_correccion ++;
                continue;

            }


        } while ($i_correccion <= 1);

        if($diferencias2 > 0)
        {
            // Notificamos que quedaron diferencias para corregir a mano
            if(env('NOTIFICAR_MAIL'))
                Mail::to(env('MAIL_TEST','sergioruiz@lancetahg.com'))->send( new CodigosDiferenciasMgPsEncontrados($mail_data));

            return redirect()->back();
        }

    }


    public function export($collection,$disk = null, $file_name,$file_type = null)
    {
        switch ($file_type) {
            case 'pdf':
                $extension = '.pdf';
            case 'csv':
                $extension = '.csv';
            case 'xls':
                $extension = '.xlsx';
            default:
                $extension = '';
        }
        return $collection->storeExcel(
          $file_name . $extension,
          $disk,
          null,
          false
        );
    }

    public function corregirDescuentos($collection)
    {
        $lsc_producto = new LSCProducto();

        foreach($collection as $collect)
        {
            if( array_key_exists('descuento',$collect->diferencias) ) {

                if( $collect->bc_descuento == 0 && $collect->ps_descuento > 0) {
                    $lsc_producto->deletePsSpecificPriceById($collect->id_specific_price);
                    $collect->correcciones['descuento'][] = 'borrado';
                } else if( $collect->bc_descuento > 0 && $collect->ps_descuento == 0 && $collect->id_specific_price == 0) {
                    $lsc_producto->createPsSpecificFromAdminhg($collect->bc_descuento,$collect->id_product,isset($collect->id_product_attribute) ? $collect->id_product_attribute : 0);
                    $collect->correcciones['descuento'][] = 'creado';
                } else {
                    $lsc_producto->updatePsSpecificPriceById($collect->id_specific_price,$collect->bc_descuento);
                    $collect->correcciones['descuento'][] = 'modificado';
                }
            }
        }

        return;
    }
}
