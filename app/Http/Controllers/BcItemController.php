<?php

namespace App\Http\Controllers;

use App\Mail\SincPreciosProductosBcPsMail;
use App\Models\BcLogRequest;
use App\Models\BcProducto;
use App\Vendor\Protechstudio\PrestashopWebService\PrestashopWebServiceOverride;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class BcItemController extends Controller
{
  public $ws;

  public $request;
  public $cant_sinc;

  public function __construct(PrestashopWebServiceOverride $ws, Request $request)
  {
    $this->request = $request;
    $this->ws = $ws;
    $this->cant_sinc = config('prestashop-webservice.cant_sinc');
  }

  /**
   * Lista de productos de API-BC con base en spGetItemsList1
   * @return \Illuminate\Http\JsonResponse|string
   */
  public function index()
  {
    return $this->getItemList1BcFromBcApi();
  }

  /**
   * Consulta y sincronización de precios entre BC y PS por medio de consulta de API-BC
   * enviando a PS solo las diferencias encontradas.
   * Función usada en agenda de comandos (schedule).
   *
   * @return bool|void
   */
  public function sincPreciosProductosBcPs()
  {
    $data = json_decode($this->getItemList1BcFromBcApi());
    $noview = (bool)$this->request->get('noview');
    
    $errores = array();
    $datos_corregir = array();

    $cant_sinc = ($this->cant_sinc && $this->cant_sinc > 0) ? $this->cant_sinc : count($data);
    $data = array_slice($data, 0, (int)$cant_sinc);

    foreach($data as $item)
    {
      $bc_producto = BcProducto::find((int)$item->{'No_'});
      $item_price = number_format($item->{'Unit Price'},2,'.','');

      if(!$bc_producto)
      {

        $errores[$item->{'No_'}]['error'] = 'El producto ' . $item->{'No_'} . ' no existe en el catálogo de PS.';
        $errores[$item->{'No_'}]['error_code'] = 404;

      } else {

        if($bc_producto->price <> $item_price)
        {
          $errores[$item->{'No_'}]['precio_bc'] = $item_price;
          $errores[$item->{'No_'}]['precio_ps'] = $bc_producto->price;
          $errores[$item->{'No_'}]['error'] = 'El producto ' . $item->{'No_'} . ' tenía diferencia de precio.';
          $errores[$item->{'No_'}]['error_code'] = 200;
        }

      }
    }
    dd($errores);
    $email_vars = new \stdClass();

    if(count($errores) > 0)
    {
      try {

        // Si hay errores, se procesan
        foreach($errores as $key => $value)
        {
          if($value['error_code'] == 200)
          {
            $datos_corregir[$key] = $value['precio_bc'];

          }
        }

        $request = new Request();
        $request->merge(['datos'=>$datos_corregir,'noview'=>1]);

        $psProductoController = new PsProductoController($request);
        $response = $psProductoController->storeCambioPrecioProductoCompras();
        $response['datos_corregir'] = $datos_corregir;

//        dd($response);

        $email_vars->tabla_inicial = $response['tabla_inicial'];
        $email_vars->tabla_final = $response['tabla_final'];
        $email_vars->estadisticas_ini = $response['estadisticas_ini'];
        $email_vars->estadisticas_fin = $response['estadisticas_fin'];
        $email_vars->datos_corregir = $response['datos_corregir'];

        Mail::to('sergioruiz@lancetahg.com')->send(new SincPreciosProductosBcPsMail($email_vars));

        return;
//        return response()->json($response,200);

      } catch (Exception $e) {

        $email_vars->estadisticas_ini = $e->getMessage();
        Mail::to('sergioruiz@lancetahg.com')->send(new SincPreciosProductosBcPsMail($email_vars));
        return;
//      return response()->json(['success'=>false,'data'=>'','error'=>$e->getMessage()]);

      }


    }
    $email_vars->estadisticas_ini = '<p>No hay cambio de precios.</p>';
    Mail::to('sergioruiz@lancetahg.com')->send(new SincPreciosProductosBcPsMail($email_vars));

    return;

  }

  /**
   * En proceso
   * Compara los precios entre tabla Item-base y los mostrados en API-BC
   * @return \Illuminate\Http\JsonResponse
   */
  public function compararPreciosBC()
  {
    $errors = array();
    $precios_bc = $this->getItemListBcFromDB();
    $precios_retail = array_column(json_decode($this->getItemList1BcFromBcApi()),'Unit Price','No_');

    foreach($precios_bc as $precio_bc)
    {
      $codigo = $precio_bc->{'No_'};
      $precio_bc = $precio_bc->{'Unit Price'};

      if((array_key_exists($codigo,$precios_retail)))
      {

        $precio_retail = (float)$precios_retail[$codigo];

        if( $precio_bc != $precio_retail )
        {
          dd($codigo.': '.$precio_bc . ' vs ' .$precio_retail);
          $errors[] = 'Precio retail: '.$precio_retail.' es distinto del precio base de BC: '.$precio_bc.' en el producto '.$codigo;
        }
      } else {

        $errors[] = 'Código '. $codigo . ' no existe en precios API';

      }

    }

    return response()->json(['num_errors'=>count($errors),'errors'=>$errors],200);

  }

  /**
   * Consulta de datos en API-BC con base en spGetItemsList1
   * @return \Illuminate\Http\JsonResponse|string tipo json plano
   */
  public function getItemList1BcFromBcApi()
  {
    $email_vars = new \stdClass();

    try{

      $response = Http::get('http://lan-ec.ddns.me:8081/bc/spGetItemsList1');
      return $response->body();

    } catch (Exception $e) {

      $email_vars->estadisticas_ini = $e->getMessage();
      Mail::to('sergioruiz@lancetahg.com')->send(new SincPreciosProductosBcPsMail($email_vars));

      return response()->json(['success'=>false,'data'=>'','error'=>$e->getMessage()]);
    }

  }

  /**
   * Solo puede ejecutarse en local para pruebas o verificaciones.
   * @param null $id
   * @return array
   */
  public function getItemListBcFromDB($id = null)
  {
    $where_id = '';
    if ($id) {
      $where_id = 'AND I.[No_] = \'' . $id . '\'';
    }
    $sql = '
    SELECT
    --TOP(10)
    I.[No_], I.[Unit Price]
    FROM [LANCETA HG SA DE CV$Item$437dbf0e-84ff-417a-965d-ed2bb9650972] AS I
    WHERE I.[Inventory Posting Group] = \'PT\'
    ' . $where_id . '

    ORDER BY I.[No_]
    ';

    $items = DB::connection('sql_bc_ho')->select($sql);

    return $items;
  }
  /**
   *,[Sales Type]
   *,[Sales Code]
   *,[Starting Date]
   *,[Currency Code]
   *,[Variant Code]
   *,[Unit of Measure Code]
   *,[Minimum Quantity]
   *,[Enviar a Prestashop]
   */
  public function getPreciosCheckPsMal()
  {
    $sql = '
    SELECT DISTINCT([Item No_]) AS codigo FROM [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982]
    WHERE [Item No_] IN (
    SELECT t1.[Item No_]--, t1.[Starting Date], t1.[Enviar a Prestashop]
    FROM [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982]  AS t1
    WHERE t1.[Starting Date] = (
      SELECT MAX(t2.[Starting Date])
        FROM [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982] t2
        WHERE t1.[Item No_] = t2.[Item No_]
        )
    --AND t1.fecha_fin = \'0000-00-00\'
    AND  t1.[Enviar a Prestashop] = 0
    )
    ';

    $items = DB::connection('sql_bc_ho')->select($sql);
    dd($items);
    foreach ($items as $item)
    {
      $codigo = $item->codigo;
      if($codigo == '000000' || $codigo == '3019')
      {
        continue;
      }
      $q1 = '
      UPDATE [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982]
      SET [Enviar a Prestashop] = 0
      WHERE [Item No_] = \''.$codigo.'\'
      ';
      DB::connection('sql_bc_ho')->unprepared($q1);

      $q2 = '
      UPDATE [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982]
      SET [Enviar a Prestashop] = 1
      WHERE [Item No_] = \''.$codigo.'\'
      AND [Starting Date] =
      (
        SELECT MAX(t2.[Starting Date])
          FROM [HO].[dbo].[LANCETA HG SA DE CV$Sales Price$6272d1e3-e1c6-4814-8c3c-95c7781f7982] t2
          WHERE t2.[Item No_] = \''.$codigo.'\'
      )
      ';
      DB::connection('sql_bc_ho')->unprepared($q2);
      
    }

    return $items;
  }
  public function showDiferenciaPreciosTpvGral(Request $request)
  {
    $email_vars = new \stdClass();
    $title = 'Reporte Diferencias en Precio BC vs PS';
    $datos = json_decode($this->getDiferenciaPreciosTpvGralFromBcApi());
    $cant_sinc = ($this->cant_sinc && $this->cant_sinc > 0) ? $this->cant_sinc : count($datos);
// dd($datos[0]);

    if($request->ajax())
    {
        return DataTables::of($datos)
            ->addColumn('serie', function($dato)
            {
                return (int)substr($dato->codigo, 0, 3);
            })
            ->editColumn('bloqueo_compra', function($dato)
            {
                return $dato->bloqueo_compra ? 'Si' : 'No';
            })
            ->editColumn('bloqueo_venta', function($dato)
            {
                return $dato->bloqueo_venta ? 'Si' : 'No';
            })
/*             ->addColumn('fecha_registro', function($dato)
            {
                return Carbon::parse($dato->created_at)->format('d-m-Y');
            }) */
            ->addColumn('action', function($dato)
            {
                $btn = '<div class="row">';
                $btn .= '<div style="margin: 0 5px 0;"><button role="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal" dato-id="'.$dato->codigo.'" data-model="show" data-toggle="tooltip" data-placement="left" title="Ver información de la solicitud"><i class="fa fa-eye"></i> Ver</button></div>';
                $btn .= '<div><a href="'.route('solicitud-cliente-distinguido.download',['id'=>$dato->codigo]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar documentación del cliente"><i class="fa fa-download"></i></a></div>';
                $btn .= '</div>';
                return $btn;
            })
            ->make(true);
    }

    return view('bc.productos.index-diferencias-precio-tpvgral',compact('title','datos'));
  }
  public function getDiferenciaPreciosTpvGralFromBcApi()
  {
    $email_vars = new \stdClass();

    try{

      $response = Http::get('http://lan-ec.ddns.me:8081/bc/api/getDiferenciaPreciosTpvGral');
      return $response->body();

    } catch (Exception $e) {

      $email_vars->estadisticas_ini = $e->getMessage();
      Mail::to('sergioruiz@lancetahg.com')->send(new DiferenciaPreciosBcPsMail($email_vars));

      return response()->json(['success'=>false,'data'=>'','error'=>$e->getMessage()]);
    }
  }
}
