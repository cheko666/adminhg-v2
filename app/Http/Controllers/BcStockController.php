<?php

namespace App\Http\Controllers;

use App\Mail\SincStockBcPsMail;
use App\Models\BcLogRequest;
use App\Models\BcStock;
use App\Vendor\Protechstudio\PrestashopWebService\PrestashopWebServiceOverride;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\Response;
use Protechstudio\PrestashopWebService\Exceptions\PrestashopWebServiceRequestException;
use GuzzleHttp\Exception\RequestException;
class BcStockController extends Controller
{
  public $ws;
  public $cant_sinc;
  public $url_api_bc;
  public $recurso_api;

  public function __construct(PrestashopWebServiceOverride $ws)
  {
    $this->ws = $ws;
    $this->cant_sinc = config('prestashop-webservice.cant_sinc');
    $this->url_api_bc = config('prestashop-webservice.url_api_bc');
  }

  /**
   * Lista de productos de API-BC con base en spGetIExistenciaReal1
   * @return \Illuminate\Http\JsonResponse|string
   */
  public function showStockEcFromBcApi($id)
  {
    $this->recurso_api = 'exsapiec2';
    $response = Http::get($this->url_api_bc.$this->recurso_api.$id);

    return response(json_decode($response->body()),200)->header('content-type','text/json');

  }

  /**
   * Consulta y sincronización de stock entre BC y PS por medio de consulta de API-BC
   * enviando a PS solo las diferencias encontradas.
   * Función usada en agenda de comandos (schedule).
   *
   * @return \Illuminate\Http\JsonResponse|void
   */
  public function sincStockProductosBcPs()
  {
    $data = json_decode($this->getStockBcFromBcApi());

    $cant_sinc = ($this->cant_sinc && $this->cant_sinc > 0) ? $this->cant_sinc : count($data);
    $data = array_slice($data, 0, (int)$cant_sinc);

    $movimientos = array('movimientos'=>[]);
    $movimientos_txt = false;
    $cant_upd = 0;
    $cant_ins = 0;

    foreach($data as $stock)
    {
      $bc_stock = BcStock::where('codigo',$stock->{'p'})->first();

      if($bc_stock) // Existe en tabla intermedia?
      {
        // Se actualiza registro
        $cant_ini = $bc_stock->cantidad;
        $bc_stock->cantidad = $stock->c;
        if ($bc_stock->isDirty())
        {
          $bc_stock->updated_at = Carbon::now()->toDateTimeString();
          $data = $bc_stock->toArray();

          $data['tipo'] = 'update';
          $data['cant_ini'] = (int)$cant_ini;
          $data['mov'] = ((int)$stock->c)-((int)$cant_ini);
          $data['cant_fin'] = (int)$stock->c;
          $movimientos['movimientos'][(string)$stock->p]['data'] = $data;
          $movimientos['movimientos'][(string)$stock->p]['tipo'] = 'upd';
          $movimientos_txt .= '<li>Código: ' . (string)$stock->p . ' actualizado.';
          $modificado = $bc_stock->save();

          $this->postCambiosStockToPs((string)$stock->p,$data);

          $cant_upd = $modificado ? $cant_upd += 1 : $cant_upd;
        }

      } else {
        if (strlen($stock->p)>6) {
          continue;
        }
        // Se crea registro
        $bc_stock = BcStock::create([
          'codigo' => $stock->p,
          'almacen' => 'EC',
          'cantidad' => $stock->c,
          'created_at'=> Carbon::now()->toDateTimeString(),
          'updated_at'=> Carbon::now()->toDateTimeString(),
        ]);
        $data = $bc_stock->toArray();

        $data['tipo'] = 'insert';
        $data['cant_ini'] = 0;
        $data['mov'] = (int)$stock->c;
        $data['cant_fin'] = (int)$stock->c;
        $movimientos['movimientos'][(string)$stock->p]['data'] = $data;
        $movimientos['movimientos'][(string)$stock->p]['tipo'] = 'ins';
        $movimientos_txt .= '<li>Código: ' . (string)$stock->p . ' creado.';

        $this->postCambiosStockToPs((string)$stock->p,$data);

        $cant_ins = $bc_stock ? $cant_ins += 1 : $cant_ins;

      }

    }

    $movimientos['resumen']['cant_ins'] = (int)$cant_ins;
    $movimientos['resumen']['cant_upd'] = (int)$cant_upd;

    try {

      $email_vars = new \stdClass();

      // Si hay movimientos, se transmite
      if($movimientos['movimientos'] && count($movimientos['movimientos'])>0)
      {

        return;
//        return response()->json(['success'=>true,'data'=>$movimientos['movimientos'],'error'=>'']);

      }// Si no hay movimientos, no se transmite
      else {
        return;
//        return response()->json(['success'=>true,'data'=>'','error'=>'']);
      }
    } catch (Exception $e) {

      $email_vars->cuerpo_mensaje = $e->getMessage();
      //Mail::to('sergioruiz@lancetahg.com')->send(new SincStockBcPsMail($email_vars));

//      return response()->json(['success'=>false,'data'=>'','error'=>$e->getMessage()]);
    }

    return;

  }
  /**
   * Enviar a endpoint stock_availables los cambios de stock
   *
   * @param $codigo 001004
   * @param $data_movimiento
   * @return \Illuminate\Http\JsonResponse
   * @throws \Protechstudio\PrestashopWebService\Exceptions\PrestashopWebServiceException
   */
  public function postCambiosStockToPs($codigo,$data_movimiento)
  {

    try{

      $blank_xml = $this->ws->get(['url' => config('prestashop-webservice.url').'/api/stock_availables?schema=blank']);
      $resource = $blank_xml;
      $resource->lsc_stock_availables->id = (int)$codigo;
      $resource->lsc_stock_availables->id_product = $codigo;
      $resource->lsc_stock_availables->id_product_attribute = 0;
      $resource->lsc_stock_availables->id_shop = 1;
      $resource->lsc_stock_availables->id_shop_group = 0;
      $resource->lsc_stock_availables->quantity = $cant_bc = $data_movimiento['cant_fin'];
      $resource->lsc_stock_availables->depends_on_stock = 0;
      $resource->lsc_stock_availables->out_of_stock = 0;
      $resource->lsc_stock_availables->location = 0;
      $opt['resource'] = 'stock_availables';
      $opt['id'] = (int)$codigo;
      $opt['putXml'] = $resource->asXML();

      $response = $this->ws->edit($opt);

      $log = BcLogRequest::create([
        'id' => null,
        'id_objeto' => (int)$codigo,
        'endpoint' => $opt['resource'],
        'method' => $this->ws->method,
        'response' => json_encode($response),
        'request' => $opt['putXml'],
        'status_code' => $this->ws->status_code,

      ]);


    }catch (PrestashopWebServiceRequestException $ex) {
      return response()->json(['error'=>$ex->getMessage()], 400);
    }

  }

  /**
   * Vista de la info del stock de todo el catalogo de productos en BC
   *
   * @return string de tipo json
   */
  public function indexStockEcFromBc()
  {
//    return response()->json(['stock'=>json_encode($this->getAndSendStockBcFromDB())], 200);
    dd($this->getStockBcFromBcApi());
  }

  /**
   * Consulta de datos en API-BC con base en spGetIExistenciaReal1
   * @return \Illuminate\Http\JsonResponse|string tipo json plano
   */
  public function getStockBcFromBcApi($codigo = null)
  {
    $this->recurso_api = 'exsapiec2';
    $email_vars = new \stdClass();
    $email_vars->has_error = false;

      try {
        $response = Http::timeout(5)->get($this->url_api_bc.$this->recurso_api.'/'.$codigo)->throw();
  
        return $response->body();
  
      } catch (ConnectionException $e) {
        $email_vars->has_error = true;
        $email_vars->cuerpo_mensaje = $e->getMessage();
        // Mail::to('sergioruiz@lancetahg.com')->send(new SincStockBcPsMail($email_vars));
      }  
  }
  /**
   * Consulta de datos en API-BC con base en exsapi2
   * @return \Illuminate\Http\JsonResponse|string tipo json plano
   */
  public function getAllStockBcFromBcApi($codigo = null)
  {
    $this->recurso_api = 'exsapiec2';
    $limit = 10;
    $response = Http::get($this->url_api_bc.$this->recurso_api.'/'.$codigo);

    $data = json_decode($response->body(),true);
    $data2 = array();

    if(count($data)>0){
      foreach($data as $k => $v)
      {
        // $data2[$k] = $v['Item No_'];
        $data2[$v['Item No_']][$v['Location Code']] = $v['Quantity'];
      }
    }

    return response($data2,200)->header('content-type','application/json',);
    return $response->body();
  }
  public function getStockEcFromDbSp()
  {
    $limit = null;
    $query = 'SET NOCOUNT ON; EXEC spGetExistenciaReal3';
    $data =  DB::connection('sql_bc')->select($query);
    $limit = $limit ?? count($data);
    
    $stock = array_slice($data, 0,$limit);
    
    foreach ($stock as $stock_c) {
      $stock_c->c = $stock_c->{'No_'};
      $stock_c->q = $stock_c->{'Quantity'};
      unset($stock_c->{'No_'});
      unset($stock_c->{'Quantity'});
    }

    $result = json_encode($stock);
    return response($result,200)->header('content-type','text/json');
  }

  /**
   * Solo puede ejecutarse en local para pruebas o verificaciones.
   * @param null $id
   * @return array
   */
  public function getAndSendStockBcFromDB($id = null)
  {
    $where_id = '';
    if($id)
    {
      $where_id = 'AND I.[No_] = \''.$id.'\'';
    }
    $sql = '
    SELECT
    --TOP(50)
    I.[No_] AS p
    , CAST(ISNULL(S.sa,0) AS INTEGER) AS c
    FROM [LANCETA HG SA DE CV$Item$437dbf0e-84ff-417a-965d-ed2bb9650972] AS I
    LEFT JOIN (
        -- EXISTENCIAS
        SELECT e.codigo, e.unidad, e.quantity AS existencia, ISNULL(v.quantity,0) AS ventas, e.quantity+(ISNULL(v.quantity,0)) AS sa
        FROM (
          SELECT
          [Item No_] AS codigo,[Location Code] AS cod_almacen,SUM([Quantity]) AS quantity,[Unit of Measure Code] AS unidad
          FROM [LANCETA HG SA DE CV$Item Ledger Entry$437dbf0e-84ff-417a-965d-ed2bb9650972]
          WHERE [Location Code] IN (\'EC\')
          GROUP BY [Item No_],[Location Code],[Unit of Measure Code]
        ) AS e
        -- VENTAS
        LEFT JOIN (
          SELECT SL.[No_] AS codigo, \'EC\' AS cod_almacen, ABS(SUM(SL.[Quantity]))*-1 AS quantity, SL.[Unit of Measure Code] AS unidad
          FROM [LANCETA HG SA DE CV$Sales Header$437dbf0e-84ff-417a-965d-ed2bb9650972] SH
          JOIN [LANCETA HG SA DE CV$Sales Header$c59ea06e-c32d-477a-a3c6-6ba257d3fd8c] SH1
            ON SH.[No_] = SH1.[No_]
          JOIN [LANCETA HG SA DE CV$Sales Line$437dbf0e-84ff-417a-965d-ed2bb9650972] SL
            ON SH.[No_] = SL.[Document No_]
          WHERE SH1.[Estatus Lanceta]>=3 AND SH.[Location Code]=\'EC\'
          GROUP BY SL.[No_],SL.[Unit of Measure Code]
        ) AS v ON e.codigo = v.codigo
        --AND e.codigo IN (\'014127\',\'001055\',\'001004\')
        --ORDER BY e.codigo

    ) S ON I.[No_] = S.codigo

    WHERE I.[Inventory Posting Group] = \'PT\'
    '.$where_id.'

    ORDER BY I.[No_]
    ';

    $stock = DB::connection('sql_bc')->select($sql);
    $count_data = count($stock);

    $stock_json = json_encode($stock);
    
    $update_db = DB::connection('do1_sincbcps')->table('exsapi')->where('id_sucursal','ec')->update(['stock'=>$stock_json]);
    if($update_db) {
      return response()->json(['data'=>$stock,'data_count'=>$count_data,'status'=>'ok'], 200);
    }
  }

  /**
   * For a given id_product and id_product_attribute, gets its stock available
   *
   * @param int $id_product
   * @param int $id_product_attribute Optional
   * @param int $id_shop Optional : gets context by default
   * @return int Quantity
   */
  public static function getQuantityAvailableByProduct($id_product = null, $id_product_attribute = null, $id_shop = null)
  {
    // if null, it's a product without attributes
    if ($id_product_attribute === null) {
      $id_product_attribute = 0;
    }

    $key = 'StockAvailable::getQuantityAvailableByProduct_'.(int)$id_product.'-'.(int)$id_product_attribute.'-'.(int)$id_shop;
    if (!Cache::isStored($key)) {
      $query = new DbQuery();
      $query->select('SUM(quantity)');
      $query->from('stock_available');

      // if null, it's a product without attributes
      if ($id_product !== null) {
        $query->where('id_product = '.(int)$id_product);
      }

      $query->where('id_product_attribute = '.(int)$id_product_attribute);
      $query = StockAvailable::addSqlShopRestriction($query, $id_shop);
      $result = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
      Cache::store($key, $result);
      return $result;
    }

    return Cache::retrieve($key);
  }
  public function getExsapiFromCodigo($codigo = null)
  {
    $this->recurso_api = 'exsapi';
    $email_vars = new \stdClass();
    $email_vars->has_error = false;

      try {
        $response = Http::timeout(5)->get($this->url_api_bc.$this->recurso_api.'/'.$codigo)->throw();
  
        return $response->body();
  
      } catch (ConnectionException $e) {
        $email_vars->has_error = true;
        $email_vars->cuerpo_mensaje = $e->getMessage();
        // Mail::to('sergioruiz@lancetahg.com')->send(new SincStockBcPsMail($email_vars));
        echo $e->getMessage();
      }  
    // }
  }

}
