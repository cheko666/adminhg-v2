<?php

namespace App\Http\Controllers;

use App\Models\EnvioTransportista;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class EnvioTransportistaController extends Controller
{
    public function index(Request $request)
    {

        $title = 'Lista de Solicitudes Tarjeta de Cliente Distinguido';
        $data = EnvioTransportista::select([
            'id',
            'cp',
            'peso',
            'peso_volumetrico',
            'tipo_envio',
            'costo_envio',
            'costo_seguro'
        ])
            ->orderBy('id','desc')
            ->get();

//        dd($data);

        if($request->ajax())
        {
            return Datatables::of($data)
                ->addColumn('nombre', function($data)
                {
                    return Str::limit($data->nombre.' '.$data->apellido,30);
                })
                ->addColumn('fecha_registro', function($data)
                {
                    return Carbon::parse($data->created_at)->format('d/M/Y');
                })
                ->addColumn('action', function($data)
                {

                    $token = explode('_',$data->comprobante);

                    $btn = '<button role="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal" data-id="'.$data->id.'" data-model="show" style="margin: 0 5px;" data-toggle="tooltip" data-placement="left" title="Ver información de la solicitud"><i class="fa fa-eye"></i> Ver Información</button>';
                    $btn .= '<a href="'.route('solicitud-cliente-distinguido.download',['id'=>$data->id]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar documentación del cliente"><i class="fa fa-download"></i></a>';
                    return $btn;
                })
                ->make(true);
        }

        return view('solicitud-cliente-distinguido.index',compact('title','data'));
    }
}
