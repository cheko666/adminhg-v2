<?php

namespace App\Http\Controllers;

use App\Models\BcLogRequest;
use App\Models\BcPrecioProducto;
use App\Models\BcProducto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class BcProductoController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $bc_productos = BcProducto::all();

        $title = 'Productos BC-PS';
        $html = '';
// dd($bc_productos[10]->tiene_dimensiones);
        if($this->request->ajax())
        {
          return Datatables::of($bc_productos)
            ->editColumn('id', function($bc_producto){
                return '<span data-toggle="tooltip" data-placement="top" title="ID_P:'.$bc_producto->id_p.'">'.$bc_producto->reference.'</span>';
            })
            ->editColumn('price', function($bc_producto){
                return '$ '.number_format($bc_producto->price,2);
            })
            ->editColumn('description', function($bc_producto){
                return '<span>'.$bc_producto->description.'</span>';
            })
            ->editColumn('special_group_code', function($bc_producto){
                return '<span data-toggle="tooltip" data-placement="top" title="'.$bc_producto->special_group_code.'">'.Str::limit($bc_producto->special_group_code,30).'</span>';
            })
            ->addColumn('tiene_dimensiones', function($bc_producto){
                return $bc_producto->tiene_dimensiones ? '<span style="color:green">Si</span>' : '<span style="color:red">No</span>';
            })
            ->addColumn('tiene_pesos', function($bc_producto){
                return $bc_producto->tiene_pesos ? '<span style="color:green">Si</span>' : '<span style="color:red">No</span>';
            })
            /* ->addColumn('dimensiones', function($bc_producto){
                return '<span>
                        <span style="display:block;">Ancho: '. number_format($bc_producto->width,3,'.','') .'</span>
                        <span style="display:block;">Alto: '. number_format($bc_producto->height,3,'.','') .'</span>
                        <span style="display:block;">Long.: '. number_format($bc_producto->depth,3,'.','') .'</span>
                </span>';
            })
            */
            ->addColumn('action', function($bc_producto){
              $btn = '<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#Modal" data-model="bc/log" data-id="'.$bc_producto->id.'" data-action="show" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Ver firma"><i class="fa fa-eye"></i></button>';
    //                    $btn .= '<a role="button" class="btn btn-sm btn-warning" href="'.route('firma-email.edit',['id'=>$firma->id]).'" style="margin: 0 5px;" data-toggle="tooltip" data-placement="left" title="Editar firma"><i class="fa fa-edit"></i></a>';
    //                    $btn .= '<a href="'.route('firma-email.download',['id'=>$firma->id]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar firma en HTML"><i class="fa fa-download"></i></a>';
              return $btn;
            })
    
            ->escapeColumns([])
    
            ->make(true);
        }
    
    
        return view('bc.productos.index',compact('title','bc_productos'));
    }

    /**
     * @param $codigo
     * @param $precio
     * @return mixed
     */
    public static function savePrecioProducto($codigo,$precio)
    {

        $precio = number_format($precio,2,'.','');
//        $model = BcPrecioProducto::where('codigo',$codigo)->first();
        $model = BcProducto::where('reference',$codigo)->first();
        // dd($model);

        if($model) // Existe en tabla intermedia?
        {
            // Se actualiza registro
            $old_precio = $model->price;
            $model->price = $precio;
            if ($model->isDirty())
            {
                $model->updated_at = Carbon::now()->toDateTimeString();
                $data = $model->toArray();


                BcLogRequest::create([
                    'id' => null,
                    'id_objeto' => (int)$codigo,
                    'endpoint' => 'products_price',
                    'method' => 'PUT',
                    'response' => '',
                    'request' => self::xmlRequestProductsPrice($codigo,$precio),
                    'status_code' => 200,

                ]);
            }

            return $data;

        } else {
            return null;
        }


    }

    public static function xmlRequestProductsPrice($codigo,$precio)
    {
        if(!$codigo || $codigo=='' || !$precio || $precio == '')
        {
            return false;
        }
        return '<?xml version="1.0" encoding="UTF-8"?> <prestashop xmlns:xlink="http://www.w3.org/1999/xlink"> <products_price> 	<id></id> 	<id_product>'.$codigo.'</id_product> 	<id_shop>1</id_shop> 	<id_shop_group>0</id_shop_group> 	<unit_price>'.$precio.'</unit_price> </products_price> </prestashop>';
    }
}
