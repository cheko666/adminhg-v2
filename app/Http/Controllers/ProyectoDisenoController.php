<?php

namespace App\Http\Controllers;

use App\Models\ProyectoDiseno;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ProyectoDisenoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $title = 'Lista de Proyectos del Área de Diseño';
        $data = ProyectoDiseno::select([
            'id',
            'referencia',
            'nombre',
            'ultimo_evento',
            'solicita',
            'fecha_solicitud',
            'fecha_ini',
            'fecha_fin',
            'porcentaje',
            'fase_proceso',
            'estatus'
        ])
            ->orderBy('id','desc')
            ->get();

//        dd($data);

        if($request->ajax())
        {
            return Datatables::of($data)
                ->addColumn('nombre', function($data)
                {
                    return Str::limit($data->nombre,30);
                })
                ->editColumn('fecha_solicitud', function($data)
                {
                    return Carbon::parse($data->fecha_solicitud)->format('d-m-y') ?? '-';;
                })
                ->editColumn('ultimo_evento', function($data)
                {
                    $ultimo_evento = '<div id="'.$data->id_ultimo_evento.'" class="col-md-10">'.$data->ultimo_evento.'</div>';
                    $btn_add_evento = '<div class="col-md-2"><button role="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#ModalEvento" data-model="eventos" data-action="create" style="margin: 0 5px;" data-toggle="tooltip" data-placement="left" title="Crear evento del proyecto"><i class="fa fa-edit"></i> </button></div>';
                    return '<div class="row">'.$ultimo_evento.$btn_add_evento.'</div>';
                })
                ->editColumn('fecha_ini', function($data)
                {
                    return Carbon::parse($data->fecha_ini)->format('d-m-y') ?? '-';
                })
                ->editColumn('fecha_fin', function($data)
                {
                    return Carbon::parse($data->fecha_fin)->format('d-m-y') ?? '-';
                })
                ->editColumn('porcentaje', function($data)
                {
                    return $data->porcentaje.'%';
                })
                ->addColumn('action', function($data)
                {
                    $btn = '<button role="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#Modal" data-id="'.$data->id.'" data-model="proyectos-diseno" data-action="show" style="margin: 0 5px;" data-toggle="tooltip" data-placement="left" title="Ver información de la solicitud"><i class="fa fa-eye"></i> </button>';
//                    $btn .= '<a href="'.route('solicitud-cliente-distinguido.download',['id'=>$data->id]).'" class="btn btn-sm btn-primary" style="margin: 0 5px;"data-toggle="tooltip" data-placement="left" title="Descargar documentación del cliente"><i class="fa fa-download"></i></a>';
                    return $btn;
                })
                ->rawColumns(['ultimo_evento','action'])
                ->make(true)
                ;
        }

        return view('proyectos-diseno.index',compact('title','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $data = ProyectoDiseno::find($id);
        $title = 'Detalle del Proyecto No. '.$id;

        if($request->ajax())
        {
            return response()->json([
                'view' => view('proyectos-diseno.show',compact('data','title'))->render()
            ]);
        }

        return view('proyectos-diseno.show',compact('data','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
