<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TallaUniformeController extends Controller
{
    public function create()
    {
        return 'Formulario para registrar la talla del empleado';
    }
    public function store($id_empleado)
    {
        return 'Registro de la talla del empleado';
    }
}
