<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CommerceConnector3mController extends Controller
{
  protected $request;

  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function indexApi()
  {
    $productos = DB::select(DB::raw('
    SELECT
    p.mpn AS man
    ,p.referencia
    ,p.codigo
    ,pl.name AS name
    ,bcp.special_group_code AS marca
    ,ROUND(bcp.price,2) AS precio_base
    , IFNULL(CONCAT(\'-\',ROUND(bcod.descuento,0),\'%\'),\'\') AS descuento
    , IFNULL(bcod.descuento,0) AS reduction
    , ROUND(IF(bcp.vat_posting_group = \'IVA16\',bcp.price*1.16,bcp.price),2) AS precio_regular
    , ROUND( IF(bcp.vat_posting_group = \'IVA16\',bcp.price*(1-(IFNULL(bcod.descuento/100,0)))*1.16,bcp.price*bcod.descuento),2 ) AS precio_neto
    , CAST(IFNULL(sa.quantity,0) AS CHAR) as quantity
    , pl.link_rewrite
    , bcp.id_p
    , bcp.id_pa

    FROM adminhg2.productos p
    #JOIN production_presta.ps0516_product AS
    JOIN production_presta.ps0516_lsc_producto bcp ON bcp.reference = p.codigo
    JOIN production_presta.ps0516_product_lang pl ON pl.id_product = bcp.id_p AND pl.id_lang = 2
    JOIN production_presta.ps0516_stock_available sa ON sa.id_stock_available = bcp.id_sa
    LEFT JOIN production_presta.ps0516_bc_oferta_descuento bcod ON CAST(bcod.codigo AS unsigned) = bcp.id AND bcod.activo

    WHERE p.activo = 1
    '));

    foreach($productos as $producto)
    {
      $producto->nombre_producto = $producto->name.' '.$this->getAtributosNombreValor($producto->id_p, $producto->id_pa);
      $producto->url_web = 'https://www.lancetahg.com.mx/productos/'.$producto->id_p.'/'.$producto->link_rewrite.$this->getAnchor($producto->id_p, $producto->id_pa);
      $producto->url_api = url()->current().'/'.$producto->man;

      unset($producto->name);
      unset($producto->link_rewrite);
      unset($producto->id_p);
      unset($producto->id_pa);
    }

      return response()->json($productos, 200,[],JSON_UNESCAPED_SLASHES);
  }

  public function showApi($id)
  {

    $res = DB::select(DB::raw('
    SELECT
    p.mpn AS man
    ,p.referencia
    ,p.codigo
    ,pl.name AS name
    ,bcp.special_group_code AS marca
    ,ROUND(bcp.price,2) AS precio_base
    , IFNULL(CONCAT(\'-\',ROUND(bcod.descuento,0),\'%\'),\'\') AS descuento
    , IFNULL(bcod.descuento,0) AS reduction
    , ROUND(IF(bcp.vat_posting_group = \'IVA16\',bcp.price*1.16,bcp.price),2) AS precio_regular
    , ROUND( IF(bcp.vat_posting_group = \'IVA16\',bcp.price*(1-(IFNULL(bcod.descuento/100,0)))*1.16,bcp.price*bcod.descuento),2 ) AS precio_neto
    , CAST(IFNULL(sa.quantity,0) AS CHAR) as quantity
    , pl.link_rewrite
    , bcp.id_p
    , bcp.id_pa

    FROM adminhg2.productos p
    #JOIN production_presta.ps0516_product AS
    JOIN production_presta.ps0516_lsc_producto bcp ON bcp.reference = p.codigo
    JOIN production_presta.ps0516_product_lang pl ON pl.id_product = bcp.id_p AND pl.id_lang = 2
    JOIN production_presta.ps0516_stock_available sa ON sa.id_stock_available = bcp.id_sa
    LEFT JOIN production_presta.ps0516_bc_oferta_descuento bcod ON CAST(bcod.codigo AS unsigned) = bcp.id AND bcod.activo

    WHERE p.activo = 1
    AND p.mpn LIKE '.$id.'
    '));

    if(count($res)>0)
    {
      $producto = $res[0];

      $producto->nombre_producto = $producto->name.' '.$this->getAtributosNombreValor($producto->id_p, $producto->id_pa);
      $producto->url_web = 'https://www.lancetahg.com.mx/productos/'.$producto->id_p.'/'.$producto->link_rewrite.$this->getAnchor($producto->id_p, $producto->id_pa);
      $producto->url_api = url()->current();

      unset($producto->name);
      unset($producto->link_rewrite);
      unset($producto->id_p);
      unset($producto->id_pa);

      return response()->json($producto, 200,[],JSON_UNESCAPED_SLASHES);

    } else {
      return response()->json([], 404,[],JSON_UNESCAPED_SLASHES);
    }

  }

  public function downloadArchivo()
  {

    $productos = DB::select(DB::raw('
      SELECT
      p.mpn AS man
      ,p.referencia
      ,p.codigo
      ,pl.name AS name
      ,bcp.special_group_code AS marca
      ,ROUND(bcp.price,2) AS precio_base
      , IF(bcp.descuento > 0 ,CONCAT(\'-\',ROUND(bcp.descuento*100,0),\'%\') ,\'\') AS descuento
      , bcp.descuento AS reduction
      , ROUND(IF(bcp.vat_posting_group = \'IVA16\',bcp.price*1.16,bcp.price),2) AS precio_regular
      , ROUND(IF(bcp.vat_posting_group = \'IVA16\',bcp.price*(1-bcp.descuento)*1.16,bcp.price*bcp.descuento),2) AS precio_neto
      , IFNULL(sa.quantity,0) as quantity
      , pl.link_rewrite
      , bcp.id_p
      , bcp.id_pa

      FROM adminhg2.productos p
      #JOIN production_presta.ps0516_product AS
      JOIN production_presta.ps0516_lsc_producto bcp ON bcp.reference = p.codigo
      JOIN production_presta.ps0516_product_lang pl ON pl.id_product = bcp.id_p AND pl.id_lang = 2
      JOIN production_presta.ps0516_stock_available sa ON sa.id_stock_available = bcp.id_sa

      WHERE p.activo = 1
    '));
/*
    foreach($productos as $producto)
    {
      $producto->nombre_producto = $producto->name.' '.$this->getAtributosNombreValor($producto->id_p, $producto->id_pa);
      $producto->url_web = 'https://www.lancetahg.com.mx/productos/'.$producto->id_p.'/'.$producto->link_rewrite.$this->getAnchor($producto->id_p, $producto->id_pa);
      $producto->url_api = url()->current().'/'.$producto->man;
      unset($producto->name);
      unset($producto->link_rewrite);
      unset($producto->id_p);
      unset($producto->id_pa);
    }

    $storage_path = Storage::disk('samba_clientes')->getDriver()->getAdapter()->getPathPrefix();

    $files = Storage::disk('samba_clientes')->allFiles($token_files);

    $zip_file_name = $id.'-'.$token_files.'-'.strtoupper(Str::camel($data->nombre)).'_'.strtoupper(Str::camel($data->apellido)).'-'.strtoupper(Str::camel($data->rfc));

    $zip_file_path = $storage_path.'_downloads/'.$zip_file_name.'.zip';

    $zip = new ZipArchive();

    if ($zip->open($zip_file_path, ZipArchive::CREATE  | ZipArchive::OVERWRITE) !== true) {
      return response()->html("No se pudo crear el archivo zip");
    }

    foreach ($files as $file)
    {
//            $zip->addFile($storage_path.'/'.$file,$zip_file_name);
      if (!is_file($file))
        $zip->addFromString(explode('/',$file)[1], file_get_contents($storage_path.'/'.$file));
    }

    $zip->close();

    return response()->download($zip_file_path,$zip_file_name)->deleteFileAfterSend(true);
*/
  }

  /**
   * Get the combination url anchor of the product
   *
   * @param int $id_product_attribute
   * @return string
   */
  public function getAnchor($id_product,$id_product_attribute)
  {
    $attributes = $this->getAttributesParams($id_product, $id_product_attribute);

    $anchor = '#';
    $sep = '-';
    foreach ($attributes as &$a) {

      foreach ($a as &$b) {
        $b = Str::slug($b,'_');
      }
      $anchor .= '/'.(isset($a->id_attribute) && $a->id_attribute ? (int)$a->id_attribute.$sep : '').$a->group.$sep.$a->name;
    }

    return $anchor;
  }

  public function getAtributosNombreValor($id_product,$id_product_attribute)
  {
    $attributes = $this->getAttributesParams($id_product, $id_product_attribute);
    $sep = ' ';

    foreach ($attributes as &$a) {

      return $a->group.$sep.$a->name;
    }

  }

  public function getAttributesParams($id_product, $id_product_attribute)
  {

    $r = DB::select(DB::raw('
        SELECT a.`id_attribute`, a.`id_attribute_group`, al.`name`, agl.`name` as `group`
        , pa.id_product_attribute
        FROM `production_presta`.`ps0516_attribute` a
        LEFT JOIN `production_presta`.`ps0516_attribute_lang` al ON (al.`id_attribute` = a.`id_attribute` AND al.`id_lang` = 2)
        LEFT JOIN `production_presta`.`ps0516_product_attribute_combination` pac ON (pac.`id_attribute` = a.`id_attribute`)
        LEFT JOIN `production_presta`.`ps0516_product_attribute` pa ON (pa.`id_product_attribute` = pac.`id_product_attribute`)
        LEFT JOIN `production_presta`.`ps0516_attribute_group_lang` agl ON (a.`id_attribute_group` = agl.`id_attribute_group` AND agl.`id_lang` = 2)
        WHERE pa.`id_product` = '.(int)$id_product.'
          AND pac.`id_product_attribute` = '.(int)$id_product_attribute.'
          AND agl.`id_lang` = 2
    '));

    return $r;

  }

}
