<?php

namespace App\Console\Commands;

use App\Http\Controllers\SolicitudClienteDistinguidoController;
use Illuminate\Console\Command;

class NotificarNoClienteDistinguido extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notificanocd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía notificación a los clientes que ya fueron registrados como Clientes Distinguidos';
    protected $scd_controller;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SolicitudClienteDistinguidoController $solicitudClienteDistinguidoController)
    {
        parent::__construct();
        $this->scd_controller = $solicitudClienteDistinguidoController;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->scd_controller->notificarClientesNoCd();
    }
}
