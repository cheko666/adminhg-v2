<?php

namespace App\Console\Commands;

use App\Http\Controllers\BcStockController;
use Illuminate\Console\Command;

class SincStockBcPs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sincstockbcps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizar existencias BC-PS. Consulta stock BC, verifica si hay cambios y transmite a PS (cortes-app)';

    protected $bcStockController;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BcStockController $bcStockController)
    {
        parent::__construct();
        $this->bcStockController = $bcStockController;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->bcStockController->sincStockProductosBcPs();
    }
}
