<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\AdminCodigoController;
use App\Http\Controllers\Admin\LSCProductoController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class procesarDiferenciasAdminhgPs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:procesarDiferenciasAdminhgPs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Arreglar diferencias entre AdminHG y Prestashop';

    protected $lSCProductoController;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LSCProductoController $lSCProductoController)
    {
        parent::__construct();
      $this->lSCProductoController = $lSCProductoController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request = new Request();
        $request->merge(['idp'=>'todo']);
        $request->merge(['campos_comparar'=>'todo']);
        $request->merge(['tipo_producto'=>'todo']);
        $this->lSCProductoController->corregirDiferenciasBcPs($request);
    }
}
