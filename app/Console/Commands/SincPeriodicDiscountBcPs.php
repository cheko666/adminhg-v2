<?php

namespace App\Console\Commands;

use App\Http\Controllers\BcDescuentoController;
use Illuminate\Console\Command;

class SincPeriodicDiscountBcPs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sincperiodicdiscbcps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincroniza la Lista de Oferta Descuento entre BC y PS por medio de consulta de API-BC (cortes-app)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BcDescuentoController $bcDescuentoController)
    {
        parent::__construct();
        $this->bcDescuentoController = $bcDescuentoController;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->bcDescuentoController->sincListaOfertaDescuentoBcPS();
    }
}
