<?php

namespace App\Console\Commands;

use App\Http\Controllers\BcItemController;
use Illuminate\Console\Command;

class SincPreciosProductosBcPs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sincpreciosbcps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consulta y sincronización de precios entre BC y PS por medio de consulta de API-BC (cortes-app)';

    protected $bcItemController;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BcItemController $bcItemController)
    {
        parent::__construct();
        $this->bcItemController = $bcItemController;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->bcItemController->sincPreciosProductosBcPs();
    }
}
