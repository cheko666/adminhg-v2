<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
      Commands\SincStockBcPs::class,
      Commands\SincPreciosProductosBcPs::class,
      Commands\procesarDiferenciasAdminhgPs::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:sincstockbcps')->cron(env('BC_STOCK_SINC_CRON_TIME','*/5 * * * *'));
        $schedule->command('command:notificanocd')->cron(env('NOTIFICACION_CLIENTE_DISTINGUIDO','0 12 * * *'));
        $schedule->command('command:procesarDiferenciasAdminhgPs')->dailyAt(env('HORA_PROC_DIFERENCIAS_BC_PS','00:15'));
        // $schedule->command('command:sincpreciosbcps')->cron(env('BC_PRECIOS_SINC_CRON_TIME','0 0 * * *'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
