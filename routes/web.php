<?php

use App\Http\Controllers\BcDescuentoController;
use App\Http\Controllers\BcLogController;
use App\Http\Controllers\BcItemController;
use App\Http\Controllers\BcProductoController;
use App\Http\Controllers\BcStockController;
use App\Http\Controllers\CommerceConnector3mController;
use App\Http\Controllers\EventoController;
use App\Http\Controllers\PasilloTiendaController;
use App\Http\Controllers\ProyectoDisenoController;
use App\Http\Controllers\PsProductoController;
use App\Http\Controllers\PublicidadCfdiController;
use App\Http\Controllers\TallaUniformeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/');
});

Route::get('productos/info-sat',[Admin\ProductoController::class,'searchInfoSatFromSku'])->name('info-sat-producto');
Route::post('productos/info-sat',[Admin\ProductoController::class,'showInfoSatFromSku'])->name('info-sat-producto.show');

Route::group(['middleware' => ['auth:sanctum', 'verified'], 'prefix' => 'admin'
//    , 'as' => 'admin.'
], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::get('/', function () {
        return redirect('admin/dashboard');
    });
    Route::get('dashboard', [Admin\DashboardController::class,'index'])->name('home-admin');

    Route::get('/clearcache', function() {
        Artisan::call('cache:clear');
        return "Cache is cleared";
    });

    Route::get('envios/transportistas',[Controllers\EnvioTransportistaController::class,'index'])->name('envio-transportista.index');

    //	Pasillo-Tienda
    Route::get('producto-por-pasillo-tienda/create',[PasilloTiendaController::class,'createProductoPorPasillo'])->name('producto-por-pasillo-tienda.create');
    Route::get('producto-por-pasillo-tienda/edit/{suc?}',[PasilloTiendaController::class,'editProductoPorPasillo'])->name('producto-por-pasillo-tienda.edit');
    Route::post('producto-por-pasillo-tienda',[PasilloTiendaController::class,'updateProductoPorPasillo'])->name('producto-por-pasillo-tienda.update');

    /********************************************************************************
     * Info getetIExistenciareal1*******************************************************************************/

    /********************************************************************************
     * Cliente Distinguido
     *********************************************************************************/ 
    Route::get('solicitud-cliente-distinguido',[Controllers\SolicitudClienteDistinguidoController::class,'index'])->name('solicitud-cliente-distinguido.index');
    Route::get('solicitud-cliente-distinguido/nocd/{id}/edit',[Controllers\SolicitudClienteDistinguidoController::class,'editNoClienteDistinguido'])->name('solicitud-cliente-distinguido-nocd.edit');
    Route::post('solicitud-cliente-distinguido/nocd',[Controllers\SolicitudClienteDistinguidoController::class,'updateNoClienteDistinguido'])->name('solicitud-cliente-distinguido-nocd.update');
    Route::get('solicitud-cliente-distinguido/notificar-registrados',[Controllers\SolicitudClienteDistinguidoController::class,'notificarClientesNoCd'])->name('solicitud-cliente-distinguido-registrados.notificar');
    Route::get('solicitud-cliente-distinguido/{reference}',[Controllers\SolicitudClienteDistinguidoController::class,'show'])->name('solicitud-cliente-distinguido.show');
    // Route::post('solicitudes/clientes-distinguidos/show/{id}',[Controllers\SolicitudClienteDistinguidoController::class,'show'])->name('solicitud-cliente-distinguido.show');
    Route::get('solicitudes/clientes-distinguidos/download-files/{reference}',[Controllers\SolicitudClienteDistinguidoController::class,'download'])->name('solicitud-cliente-distinguido.download');
    
    Route::get('solicitud-cliente-distinguido-old',[Controllers\SolicitudClienteDistinguidoOldController::class,'index'])->name('solicitud-cliente-distinguido.index-old');
    Route::get('solicitud-cliente-distinguido-old/{id}',[Controllers\SolicitudClienteDistinguidoOldController::class,'show'])->name('solicitud-cliente-distinguido.show-old');
    Route::get('solicitud-cliente-distinguido-old/download-files/{id}',[Controllers\SolicitudClienteDistinguidoOldController::class,'download'])->name('solicitud-cliente-distinguido.download-old');

   /********************************************************************************
    * BC-PS
    *********************************************************************************/
    Route::get('bc/productos/listarDiferenciasBcPs',[Controllers\Admin\LSCProductoController::class,'listarDiferenciasBcPs'])->name('bc.productos.listarDiferenciasBcPs.index');
    Route::get('bc/productos/corregirDiferenciasBcPs',[Controllers\Admin\LSCProductoController::class,'corregirDiferenciasBcPs'])->name('bc.productos.corregirDiferenciasBcPs.index');
    
    /********************************************************************************
     * Entregas Informacion Productos Proveedor
     *********************************************************************************/
    // Route::get('productos/proveedor/informacion/entrega',[Controllers\])

    /********************************************************************************
     * Redpack
     *********************************************************************************/
    Route::get('rastreo/',[Controllers\Admin\RedpackController::class,'searchRastreo'])->name('rastreo.search');
    Route::get('rastreo/redpack/{id?}',[Controllers\Admin\RedpackController::class,'showRastreo'])->name('rastreo.show');

    Route::get('coti-envio/redpack',[Controllers\Admin\RedpackController::class,'cotizarEnvioForm'])->name('redpack.coti-envio');
    Route::post('coti-envio/redpack',[Controllers\Admin\RedpackController::class,'cotizarEnvioShow'])->name('redpack.coti-envio.show');

    /********************************************************************************
     * Proyectos de Diseño
     *********************************************************************************/
    Route::get('proyectos-diseno/',[ProyectoDisenoController::class,'index'])->name('proyectos-diseno.index');
    Route::get('proyectos-diseno/{id}',[ProyectoDisenoController::class,'show'])->name('proyectos-diseno.show');
    Route::get('proyectos-diseno/{id}/eventos/',[ProyectoDisenoController::class,'indexEventos'])->name('eventos-proyecto-diseno.index');

    Route::get('proyectos-diseno/{id}/eventos/create',[EventoController::class,'create'])->name('evento-proyecto-diseno.create');
    Route::post('proyectos-diseno/{id}/eventos',[EventoController::class,'store'])->name('evento-proyecto-diseno.store');


    /********************************************************************************
     * Sincronizacion Magic
     *********************************************************************************/
    Route::get('ps/productos/cambio-precio-compras',[PsProductoController::class,'createCambioPrecioProductoCompras'])->name('cambio-precios-compras.create');
    Route::post('ps/productos/cambio-precio-compras',[PsProductoController::class,'storeCambioPrecioProductoCompras'])->name('cambio-precios-compras.store');
//    Route::post('proyectos-diseno/{id}/eventos',[EventoController::class,'store'])->name('evento-proyecto-diseno.store');

    /******************************************************************************
     * Integracion con BC
     *******************************************************************************/
    Route::get('bc/logs',[BcLogController::class,'index'])->name('bc-log.index');
    Route::get('bc/{id}/edit',[BcLogController::class,'index'])->name('bc-log.edit');

    Route::get('bc/productos',[BcProductoController::class,'index'])->name('bc-producto.index');

    /******************************************************************************
     * Tallas Uniformes
     *******************************************************************************/
    Route::get('uniformes/tallas',[TallaUniformeController::class,'create'])->name('tallas-uniformes.create');
    Route::post('uniformes/tallas',[TallaUniformeController::class,'store'])->name('tallas-uniformes.store');

});
// API BC-HO
Route::get('bc/getAndSendStockBcFromDB',[BcStockController::class,'getAndSendStockBcFromDB']);
Route::get('bc/getStockFromDbSp',[BcStockController::class,'getStockFromDbSp'])->name('bc.index.stock-ecbcsp');
Route::get('bc/api/getStockBcFromBcApi/{id?}',[BcStockController::class,'getStockBcFromBcApi'])->name('bc.show.stock-ecbcapi');
Route::get('bc/api/getAllStockBcFromBcApi/{id?}',[BcStockController::class,'getAllStockBcFromBcApi'])->name('bc.show.stock-bcapi');
Route::get('bc/api/showdiferenciaPreciosTpvGral',[BcItemController::class,'showDiferenciaPreciosTpvGral'])->name('bc.index.diferencia-precios-tpvgral');
Route::get('bc/api/spGetItemsList1',[BcItemController::class,'index'])->name('bc-item-list1.index');
Route::get('bc/api/sincListaOfertaDescuentoBcPS',[BcDescuentoController::class,'sincListaOfertaDescuentoBcPS']);
Route::get('bc/api/descuentos/{codigo?}',[BcDescuentoController::class,'getDescuentosFromBcApi']);
Route::get('bc/api/exsapi/{codigo?}',[BcStockController::class,'getExsapiFromCodigo']);
//
Route::get('bc/producto/sincpreciosbcps',[BcItemController::class,'sincPreciosProductosBcPs'])->name('sinc_precios_bcps');
Route::get('bc/producto/sincstockbcps',[BcStockController::class,'sincStockProductosBcPs'])->name('sinc_stock_bcps');
Route::get('bc/producto/sales-price',[BcItemController::class,'getPreciosCheckPsMal']);
//Route::get('bc/producto/compararPreciosBC',[BcItemController::class,'compararPreciosBC']);

/******************************************************************************
 * Producto CommerceConnector-3M
 *******************************************************************************/
Route::get('cc3m/productos',[CommerceConnector3mController::class,'indexApi'])->name('cc3m-productos');
Route::get('cc3m/productos/{sku}',[CommerceConnector3mController::class,'showApi'])->name('cc3m-productos.show');

Route::get('solicitud-cliente-distinguido/create',[Controllers\SolicitudClienteDistinguidoController::class,'create'])->name('solicitud-cliente-distinguido.create');
Route::get('solicitud-cliente-distinguido/{reference}',[Controllers\SolicitudClienteDistinguidoController::class,'show'])->name('solicitud-cliente-distinguido.show-guest');
Route::post('solicitud-cliente-distinguido',[Controllers\SolicitudClienteDistinguidoController::class,'store'])->name('solicitud-cliente-distinguido.store');